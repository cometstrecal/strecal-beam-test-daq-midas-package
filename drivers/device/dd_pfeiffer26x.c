/********************************************************************\

  Name:         dd_pfeiffer26x.c
  Created by:   Kou Oishi

  Contents:     ISEG HV (NHQ serise w/ RS232C communication)

  $Id$
\********************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "midas.h"
#include "dd_pfeiffer26x.h"
#include "rs232.h"

// Send string one char by one char
//#define WRITE_ONEBYONE

/*---- globals -----------------------------------------------------*/

#define PFEIFFER26X_ETX  ('\3')
#define PFEIFFER26X_ENQ  ('\5')
#define PFEIFFER26X_ACK  ('\6')
#define PFEIFFER26X_NAK  ('\21')

/*
typedef struct {
} PFEIFFER26X_SETTINGS;

#define PFEIFFER26X_SETTINGS_STR "\
"
*/
typedef struct {
  //PFEIFFER26X_SETTINGS settings;
  int num_channels;
  void* rs232_info;
} PFEIFFER26X_INFO;

/* specialized read from/write to ISEG NHQ */
INT pfeiffer26x_write(PFEIFFER26X_INFO* info, char* str_in)
{
  int i;
  
  char str[128];
  sprintf(str, "%s\r\n", str_in);
  
  /* write one char by one char (sending at once will fail)*/
#ifdef WRITE_ONEBYONE
  for(i=0; i<strlen(str); i++){
    rs232(CMD_WRITE, info->rs232_info, &(str[i]), 1);
    ss_sleep(20);
  }
#else
  rs232(CMD_WRITE, info->rs232_info, str, strlen(str));
#endif
  
  /* read ACKnowledgement */
  ss_sleep(100);
  rs232(CMD_READ, info->rs232_info, str, 3, 100); /* timeout is fixed */
  
  /* check if output = "[ACK] [CR] [LF]" indicating an error */
  if(str[0] != PFEIFFER26X_ACK){
    if(str[0] == PFEIFFER26X_NAK){
      cm_msg(MERROR, "dd_pfeiffer26x", "Negative acknowledge returned.");
      return FE_ERR_HW;
    }
    cm_msg(MERROR, "dd_pfeiffer26x", "Not acknowledge returned but unknown error.");
  }
  if(str[1] != '\r' || str[2] != '\n'){
    cm_msg(MERROR, "dd_pfeiffer26x", "unknown newline error.");
  }
  
  return FE_SUCCESS;
}


/* specialized read from/write to ISEG NHQ */
INT pfeiffer26x_rw(PFEIFFER26X_INFO* info, char* str_in, char* str_out, int size)
{
  int i;
  
  char str[128];
  char enq = PFEIFFER26X_ENQ;

  if(FE_SUCCESS != pfeiffer26x_write(info, str_in)) return FE_ERR_HW;
  
  /* send enquire */
  rs232(CMD_WRITE, info->rs232_info, enq, 1);

  /* read */
  ss_sleep(100);
  rs232(CMD_READ, info->rs232_info, str_out, size, 100); /* timeout is fixed */

  /*  
  if(str_out[strlen(str_out)-2] != '\r' ||
     str_out[strlen(str_out)-1] != '\n' ){
    cm_msg(MERROR, "dd_pfeiffer26x", "unknown newline error.");
    return FE_ERR_HW;
  }
  */
  return FE_SUCCESS;
}

/*
  read integer value nnn from expected string format (str_out_exp) 
  str_out_exp should be consistent with sscanf format
  The number of the expected readout integers should be < 5.
  The size of the integer array 'val' must be larger than
  or equal to number of readout integers.
*/
INT pfeiffer26x_rw_int(PFEIFFER26X_INFO* info, char* str_in, char* str_out_exp, int* val)
{
  char str_out[100];
  int  i, ret, tval[5];
  
  /* write / read */
  if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 100)) return FE_ERR_HW;
  
  /* sscanf */
  ret = sscanf(str_out, str_out_exp, &tval[0], &tval[1], &tval[2], &tval[3], &tval[4]);
  if(ret <= 0){
    //cm_msg(MERROR, "dd_pfeiffer26x", "No integer values obtained.");
    return FE_ERR_HW;
  }

  /* copy */
  for(i=0; i<ret; i++){
    val[i] = tval[i];
  }
  
  return FE_SUCCESS;
}


/*
  read float value (sx.xxxxEsyy) from expected string format (str_out_exp) 
  str_out_exp should be consistent with sscanf format
  The number of the expected readout floats should be < 5.
  The size of the float array 'val' must be larger than
  or equal to number of readout floats.
*/
INT pfeiffer26x_rw_float(PFEIFFER26X_INFO* info, char* str_in, char* str_out_exp, float* val)
{
  char  str_out[100];
  int   i, ret;
  float tval[5];
  
  /* write / read */
  if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 100)) return FE_ERR_HW;
  
  /* sscanf */
  ret = sscanf(str_out, str_out_exp, &tval[0], &tval[1], &tval[2], &tval[3], &tval[4]);
  if(ret <= 0){
    //cm_msg(MERROR, "dd_pfeiffer26x", "No integer values obtained.");
    return FE_ERR_HW;
  }

  /* copy */
  for(i=0; i<ret; i++){
    val[i] = tval[i];
  }
  
  return FE_SUCCESS;
}


/*
  read string xxx from expected string format (str_out_exp) 
  str_out_exp should be consistent with sscanf format
*/
INT pfeiffer26x_rw_str(PFEIFFER26X_INFO* info, char* str_in, char* str_out_exp, char* val)
{
  char str_out[100];
  int  i, ret;
  
  /* write / read */
  if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 100)) return FE_ERR_HW;
  
  /* sscanf */
  ret = sscanf(str_out, str_out_exp, val);
  if(ret <= 0){
    cm_msg(MERROR, "dd_pfeiffer26x", "No string obtained.");
    return FE_ERR_HW;
  }
  
  return FE_SUCCESS;
}

/*---- device driver routines --------------------------------------*/
INT pfeiffer26x_init(HNDLE hkey, void **pinfo, INT channels, INT(*bd) (INT cmd, ...))
{
  int  status, size, ch;
  HNDLE hDB, hkeydd;
  PFEIFFER26X_INFO *info;
  char str_in[100], str_out_exp[100];
  int ival[2];
  
  /* allocate info structure */
  info = (PFEIFFER26X_INFO *) calloc(1, sizeof(PFEIFFER26X_INFO));
  *pinfo = info;
  
  /* check number of channels */
  if(channels < 1 || channels > 2){
    cm_msg(MERROR, "pfeiffer26x", "Illegal channel number.");
    return FE_ERR_HW;
  }
  info->num_channels = channels;
  
  cm_get_experiment_database(&hDB, NULL);

  /* create PFEIFFER26X settings record */
  /*
  status = db_create_record(hDB, hkey, "PFEIFFER26x", PFEIFFER26X_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
   
  db_find_key(hDB, hkey, "PFEIFFER26x", &hkeydd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeydd, &info->settings, &size, 0);
  */
  
  /* open device on RS232 */
  status = rs232(CMD_INIT, hkey, &(info->rs232_info));
  if (status != SUCCESS ) {
    cm_msg(MERROR, "pfeiffer26x_init",
           "Cannot access via RS232, Check power and connection.");
    return FE_ERR_HW;
  }
  
  /* check status */
  if(info->num_channels == 1)
    sprintf(str_in, "SEN 1,0");
  if(info->num_channels == 2) 
    sprintf(str_in, "SEN 1,1");
  sprintf(str_out_exp, "%%d,%%d\r\n");
  
  status = pfeiffer26x_rw_int(info, str_in, str_out_exp, ival);
  if( status != FE_SUCCESS ||
      (info->num_channels == 1 && ival[0] != 1 ) ||
      (info->num_channels == 2 && ival[0] != 1 && ival[1] != 1) ){
    cm_msg(MERROR, "pfeiffer26x_init", "Cannot start sensor");
    return FE_ERR_HW;
  }
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT pfeiffer26x_exit(PFEIFFER26X_INFO * info)
{
  int status;
  status = rs232(CMD_EXIT, info->rs232_info);
  
  free(info);
  
  return FE_SUCCESS;
}


/*----------------------------------------------------------------------------*/
// Set Voltage
INT pfeiffer26x_set(PFEIFFER26X_INFO * info, INT channel, float value)
{
  char str_in[30], str_out[30];
  int ivalue = (int)(value+0.5);

  // HV off
  if(ivalue < 0) ivalue = 0;
  
  sprintf(str_in, "D%d=%d", channel+1, ivalue);
  if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 30)){
    cm_msg(MERROR, "pfeiffer26x_set", "Cannot set HV ch%d", channel);
    return FE_ERR_HW;
  }
  
  sprintf(str_in, "G%d", channel+1);
  if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 30)){
    cm_msg(MERROR, "pfeiffer26x_set", "Cannot start to change HV ch%d", channel);
    return FE_ERR_HW;
  }
  
  if(ivalue == 0) cm_msg(MINFO, "pfeiffer26x", "Channel %d HV OFF", channel);
  else            cm_msg(MINFO, "pfeiffer26x", "Channel %d HV ON", channel);
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT pfeiffer26x_get(PFEIFFER26X_INFO * info, INT channel, float *pvalue)
{
  char str_in[30], str_out_exp[30] = "%d;%e\r\n";
  float ivalue[2];
  
  sprintf(str_in, "PR%d", channel+1);
  if(FE_SUCCESS != pfeiffer26x_rw_float(info, str_in, str_out_exp, ivalue)){
    //cm_msg(MERROR, "pfeiffer26x_get", "Cannot get pressure ch%d", channel);
    return FE_ERR_HW;
  }
  // Check status
  if( 0 != (int)ivalue[0] ){
    cm_msg(MERROR, "pfeiffer26x_get", 
           "Invalid status of ch%d = %d", channel, (int)ivalue[0]);
    return FE_ERR_HW;
  }
  *pvalue = ivalue[1];
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT pfeiffer26x_get_other(PFEIFFER26X_INFO * info, INT channel, float *pvalue, int cmd)
{
  char str_in[30], str_out_exp[30];
  int ivalue;
  
  switch (cmd) {
  case CMD_GET_DEMAND:
    sprintf(str_in, "D%d", channel+1);
    sprintf(str_out_exp, "D%d\r\n%%04d\r\n", channel+1);
    if(FE_SUCCESS != pfeiffer26x_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "pfeiffer26x_get_other", "Cannot get demand HV ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;
    break;
  
  case CMD_GET_CURRENT_LIMIT:
    /* used for trip current */
    sprintf(str_in, "L%d", channel+1);
    sprintf(str_out_exp, "L%d\r\n%%04d\r\n", channel+1);
    if(FE_SUCCESS != pfeiffer26x_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "pfeiffer26x_get_other", "Cannot get current limit HV ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;
    
    break;
  
  case CMD_GET_TRIP_TIME:
    /* No implemented for ISEG NHQ */
    break;

  case CMD_GET_RAMPUP:
    sprintf(str_in, "V%d", channel+1);
    sprintf(str_out_exp, "V%d\r\n%%03d\r\n", channel+1);
    if(FE_SUCCESS != pfeiffer26x_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "pfeiffer26x_get_other", "Cannot get ramp up speed ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;

    break;

  case CMD_GET_RAMPDOWN:  // use it for current ramp speed
    /* No implemented for ISEG NHQ */
    break;
  }

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT pfeiffer26x_set_other(PFEIFFER26X_INFO * info, int channel, float limit, int cmd)
{
  char str_in[30], str_out[30];
  int ivalue = (int)(limit + 0.5);
  
  switch (cmd) {
  case CMD_SET_RAMPUP:
    sprintf(str_in, "V%d=%d", channel+1, ivalue);
    if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 30)){
      cm_msg(MERROR, "pfeiffer26x_set_other", "Cannot set ramp up speed ch%d", channel);
      return FE_ERR_HW;
    }
    break;
    
  case CMD_SET_RAMPDOWN:   // Use it for current ramp speed
    /* No implemented for ISEG NHQ */
    break;
    
  case CMD_SET_CURRENT_LIMIT:
    /* ivalue = 0 means no trip */
    sprintf(str_in, "L%d=%d", channel+1, ivalue);
    if(FE_SUCCESS != pfeiffer26x_rw(info, str_in, str_out, 30)){
      cm_msg(MERROR, "pfeiffer26x_set_other", "Cannot set current trip ch%d", channel);
      return FE_ERR_HW;
    }
    break;
    
  case CMD_SET_VOLTAGE_LIMIT:
    /* No implemented for ISEG NHQ */
    break;
  }

  return FE_SUCCESS;
}

/*---- device driver entry point -----------------------------------*/
INT dd_pfeiffer26x(INT cmd, ...)
{
  static DWORD itstime=0;
  va_list argptr;
  HNDLE hKey;
  INT channel, status;
  float value, *pvalue;
  void *info, *bd;

  va_start(argptr, cmd);
  status = FE_SUCCESS;

  switch (cmd) {
  case CMD_INIT:
    hKey = va_arg(argptr, HNDLE);
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    bd = va_arg(argptr, void *);
    status = pfeiffer26x_init(hKey, (void **)info, channel, bd);
    break;

  case CMD_EXIT:
    info = va_arg(argptr, void *);
    status = pfeiffer26x_exit(info);
    break;

  case CMD_SET:  // Voltage
    info    = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    value   = (float) va_arg(argptr, double);
    status  = pfeiffer26x_set(info, channel, value);
    break;

  case CMD_GET: // Pressure
    info    = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    pvalue  = va_arg(argptr, float *);
    status  = pfeiffer26x_get(info, channel, pvalue);
    break;

  case CMD_GET_LABEL:
    status = FE_SUCCESS;
    break;

  case CMD_SET_LABEL:
    status = FE_SUCCESS;
    break;

  default:
    //      cm_msg(MERROR, "pfeiffer26x device driver", "Received unknown command %d", cmd);
    status = FE_ERR_DRIVER;
    break;
  }

  va_end(argptr);

  return status;
}

/*------------------------------------------------------------------*/
