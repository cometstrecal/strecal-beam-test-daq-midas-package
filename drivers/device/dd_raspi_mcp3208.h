/********************************************************************\

  Name:         dd_raspi_mcp3208.h
  Created by:   Kou Oishi

  Contents:     Device driver function declarations

  $Id$

\********************************************************************/
INT dd_raspi_mcp3208(INT cmd, ...);

