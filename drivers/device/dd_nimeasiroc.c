/********************************************************************\

  Name:         dd_nimeasiroc.c
  Created by:   Pierre-Andre Amaudruz

  Based on the mscbhvr device driver

  Contents:     Raspberry pi ADC controller

  $Id$
\********************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

#include "midas.h"
#include "dd_nimeasiroc.h"
#include "sitcp.h"


/*---- globals -----------------------------------------------------*/

#define MAX_CHS 64


typedef struct {
  float temp_calib[2];
  float temp_offset[2];
} NIMEASIROC_SETTINGS;

#define NIMEASIROC_SETTINGS_STR "\
[.] \n\
Temp Calib Factor = FLOAT[2] : \n\
[0] 0.02876 \n\
[1] 0.02886 \n\
\
Temp Calib Offset = FLOAT[2] : \n\
[0] 275.0 \n\
[1] 275.7 \n\
"

typedef struct {
  NIMEASIROC_SETTINGS settings;
  int num_channels;
  void* sitcp_info;
} NIMEASIROC_INFO;

/*---- device driver routines --------------------------------------*/
INT nimeasiroc_init(HNDLE hkey, void **pinfo, INT channels, INT(*bd) (INT cmd, ...))
{
  int  i, status, size;
  unsigned char data;
  HNDLE hDB, hkeydd;
  NIMEASIROC_INFO *info;

  /* allocate info structure */
  info = (NIMEASIROC_INFO *) calloc(1, sizeof(NIMEASIROC_INFO));
  *pinfo = info;

  cm_get_experiment_database(&hDB, NULL);

  /* create NIMEASIROC settings record */
  status = db_create_record(hDB, hkey, "NIM_EASIROC", NIMEASIROC_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;

  db_find_key(hDB, hkey, "NIM_EASIROC", &hkeydd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeydd, &(info->settings), &size, 0);
  
  /* channel */
  if(channels > MAX_CHS){
    cm_msg(MERROR, "nimeasiroc", "Number of Channels: %d is too much.", channels);
    return FE_ERR_HW;
  }
  info->num_channels = channels;
  
  /* open device on SiTCP (UDP) */
  status = sitcp_udp(CMD_INIT, hkey, &(info->sitcp_info));
  if(status != SUCCESS) return FE_ERR_HW;
  status = sitcp_udp(CMD_OPEN, info->sitcp_info);
  if(status != SUCCESS) return FE_ERR_HW;
  printf("SiTCP (UDP) initialized.\n");

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_exit(NIMEASIROC_INFO * info)
{
  BYTE data;
  
  data = 0;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x10, &data, sizeof(data), FALSE);
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x11, &data, sizeof(data), FALSE);

  data = 1;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x1e, &data, sizeof(data), FALSE);

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_set_bias(NIMEASIROC_INFO * info, INT channel, double value)
{
  INT  hvdac;
  BYTE data;

  if(channel != 0) return FE_SUCCESS;
  
  if(value < 0 || value >= 70){
    cm_msg(MERROR, "nimeasiroc", "The input bias voltage %.1lf is too much. Ignored.", value);
    return FE_SUCCESS;
  }

  printf("Setting bias to %.1f V\n", value);
  
  /* Set dac value */
  hvdac = (INT)(413.183 * value + 780);
  
  SITCP_RBCP_COMMAND command[] = {
    {0x10, 0xFF & hvdac>>8, TRUE },
    {0x11, 0xFF & hvdac   , TRUE },
    {0x1e, 0x01           , TRUE }
  };
  sitcp_udp(CMD_WRITE_BLOCK, info->sitcp_info, 3, command, 50);
  ss_sleep(1000);
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_get_madc(NIMEASIROC_INFO * info, INT channel, INT loop)
{
  INT i, ret;
  BYTE ch;
  BYTE data, bvalue[2];
  
  /* set ADC rate to 50 Hz */
  data = 248;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x12, &data, sizeof(data), FALSE);
  data = 0;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x1f, &data, sizeof(data), FALSE);
  usleep(20000);

  ch = channel;
  data = 1;
  for(i=0; i<loop; i++){
    sitcp_udp(CMD_WRITE, info->sitcp_info, 0x12, &ch, sizeof(ch), FALSE);
    sitcp_udp(CMD_WRITE, info->sitcp_info, 0x1f, &data, sizeof(data), FALSE);
    usleep(20000);
  }
  
  data = 240;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x12, &data, sizeof(data), FALSE);
  data = 0;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x1f, &data, sizeof(data), FALSE);
  
  bvalue[0] = bvalue[1] = 0;
  sitcp_udp(CMD_READ,  info->sitcp_info, 0x04, &(bvalue[0]), sizeof(bvalue[0]), 3000);
  sitcp_udp(CMD_READ,  info->sitcp_info, 0x05, &(bvalue[1]), sizeof(bvalue[1]), 3000);
  
  ret =  (0xFF & bvalue[0])<<8;
  ret += (0xFF & bvalue[1]);
  
  ss_sleep(10);
  return ret;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_get_dac(NIMEASIROC_INFO * info, INT channel, float *pvalue)
{
  int chipch, status;
  BYTE data, mux, chip;
  BYTE bvalue[2];
  
  /* channel */
  if(0> channel || channel >= info->num_channels){
    cm_msg(MINFO, "nimeasiroc", "Illegal channel: %d", channel);
    return FE_SUCCESS;
  }
  chip   = channel/(MAX_CHS/2) + 1;
  chipch = channel%(MAX_CHS/2);

  /* prepare mux */
  if     (chipch%2 == 0 && chipch < 16               ) mux = 199 - chipch/2;      
  else if(chipch%2 == 1 && chipch < 16               ) mux = 207 - chipch/2;      
  else if(chipch%2 == 0 && chipch < 32 && chipch > 15) mux =  55 - (chipch -16)/2;      
  else if(chipch%2 == 1 && chipch < 32 && chipch > 15) mux =  63 - (chipch -16)/2;      

  /* readout */
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x00000013, &mux, sizeof(mux), FALSE);
  usleep(2000);
  
  *pvalue = 0.0000685 * nimeasiroc_get_madc(info, chip, 2);
  
  /* finish */
  mux = 0;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x00000013, &mux, sizeof(mux), FALSE);
  usleep(2000);
  
  //printf("GOT dac ch %d = %.2f\n", channel, *pvalue);
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_get_bias(NIMEASIROC_INFO * info, INT channel, float *pvalue)
{
  if(channel != 0) return FE_SUCCESS;
    
  BYTE ch;
  BYTE data, bvalue[2];
  bvalue[0] = bvalue[1] = 0;

  /* readout */
  *pvalue = 0.00208 * nimeasiroc_get_madc(info, 3, 1);

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_get_current(NIMEASIROC_INFO * info, INT channel, float *pvalue)
{
  if(channel != 0) return FE_SUCCESS;
  
  /* readout */
  *pvalue = 0.034 * nimeasiroc_get_madc(info, 4, 2);
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT nimeasiroc_get_temp(NIMEASIROC_INFO * info, INT channel, float *pvalue)
{
  BYTE data;
  float calib, offset;
  INT adc;
  
  if(channel != 0 && channel != 1){
    //cm_msg(MINFO, "nimeasiroc", "Illegal temp channel: %d", channel);
    return FE_SUCCESS;
  }
  
  /* calibration factor */
  calib  = info->settings.temp_calib[channel];
  offset = info->settings.temp_offset[channel];
  
  /* set ADC rate to 50 Hz */
  data = 248;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x12, &data, sizeof(data), FALSE);
  data = 0;
  sitcp_udp(CMD_WRITE, info->sitcp_info, 0x1f, &data, sizeof(data), FALSE);
  usleep(20000);
  
  /* readout */
  if(channel == 0){
    adc = nimeasiroc_get_madc(info, 5, 1);
  }
  else{
    adc = nimeasiroc_get_madc(info, 0, 1);
  }
  
  *pvalue = (calib * adc) - offset;
  
  //printf("ch = %d adc = %d\n", channel, adc);

  return FE_SUCCESS;
}

 /*---- device driver entry point -----------------------------------*/
 INT dd_nimeasiroc(INT cmd, ...)
 {
   static DWORD itstime=0;
   va_list argptr;
   HNDLE hKey;
   DWORD flag;
   INT channel, status;
   double value;
   float *pvalue;
   void *info, *bd;

    va_start(argptr, cmd);
    status = FE_SUCCESS;

    switch (cmd) {
    case CMD_INIT:
       hKey = va_arg(argptr, HNDLE);
       info = va_arg(argptr, void *);
       channel = va_arg(argptr, INT);
       flag = va_arg(argptr, DWORD);
       bd = va_arg(argptr, void *);
       status = nimeasiroc_init(hKey, (void **)info, channel, bd);
       break;

    case CMD_EXIT:
       info = va_arg(argptr, void *);
       status = nimeasiroc_exit(info);
       break;

    case CMD_SET:
      info    = va_arg(argptr, void *);
      channel = va_arg(argptr, INT);
      value   = va_arg(argptr, double);
      status  = nimeasiroc_set_bias(info, channel, value);
      break;

    case CMD_GET: // HV DAC value
       info    = va_arg(argptr, void *);
       channel = va_arg(argptr, INT);
       pvalue  = va_arg(argptr, float *);
       status  = nimeasiroc_get_dac(info, channel, pvalue);
       break;
       
    case CMD_GET_CURRENT:
       info    = va_arg(argptr, void *);
       channel = va_arg(argptr, INT);
       pvalue  = va_arg(argptr, float *);
       status  = nimeasiroc_get_current(info, channel, pvalue);
       break;

    case CMD_GET_BIAS:
       info    = va_arg(argptr, void *);
       channel = va_arg(argptr, INT);
       pvalue  = va_arg(argptr, float *);
       status  = nimeasiroc_get_bias(info, channel, pvalue);
       break;
       
   case CMD_GET_TEMPERATURE:
       info    = va_arg(argptr, void *);
       channel = va_arg(argptr, INT);
       pvalue  = va_arg(argptr, float *);
       status  = nimeasiroc_get_temp(info, channel, pvalue);
       break;

   case CMD_GET_LABEL:
      status = FE_SUCCESS;
      break;

   case CMD_SET_LABEL:
      status = FE_SUCCESS;
      break;

   default:
      break;
   }

   va_end(argptr);

   return status;
}

/*------------------------------------------------------------------*/
