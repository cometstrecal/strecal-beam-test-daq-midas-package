/********************************************************************\

  Name:         dd_isegnhq.c
  Created by:   Kou Oishi

  Contents:     ISEG HV (NHQ serise w/ RS232C communication)

  $Id$
\********************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "midas.h"
#include "dd_isegnhq.h"
#include "rs232.h"

/*---- globals -----------------------------------------------------*/

#define _ISEGNHQ_ALLOW_READONLY_

#define ISEGNHQ_QUA  (1<<7)
#define ISEGNHQ_ERR  (1<<6)
#define ISEGNHQ_INH  (1<<5)
#define ISEGNHQ_KILL (1<<4)
#define ISEGNHQ_OFF  (1<<3)
#define ISEGNHQ_POL  (1<<2)
#define ISEGNHQ_MAN  (1<<1)
#define ISEGNHQ_UI   (1<<0)
#define ISEGNHQ_AB   (1<<0)

typedef struct {
  INT  unit_no;
  BOOL polarity[2];
} ISEGNHQ_SETTINGS;

#define ISEGNHQ_SETTINGS_STR "\
Unit No. = INT : -1\n\
Polarity = BOOL[2] :\n\
[0] y\n\
[1] y\n\
"

typedef struct {
  ISEGNHQ_SETTINGS settings;
  int num_channels;
  int hv_limit;
  int cur_limit;
  void* rs232_info;
} ISEGNHQ_INFO;


/* specialized read from/write to ISEG NHQ */
INT isegnhq_rw(ISEGNHQ_INFO* info, char* str_in, char* str_out, int size)
{
  int i;
  
  char str[128];
  sprintf(str, "%s\r\n", str_in);
  
  /* write one char by one char (sending at once will fail)*/
  for(i=0; i<strlen(str); i++){
    rs232(CMD_WRITE, info->rs232_info, &(str[i]), 1);
    ss_sleep(20);
  }
  
  /* read */
  ss_sleep(100);
  rs232(CMD_READ, info->rs232_info, str_out, size, 100); /* timeout is fixed */
  
  /* check if output contains '?' indicating an error */
  for(i=0; i<strlen(str_out); i++){
    if(str_out[i] == '?'){
      cm_msg(MERROR, "dd_isegnhq", "Syntax error.");
      return FE_ERR_HW;
    }
  }

  return FE_SUCCESS;
}

/*
  read integer value nnn from expected string format (str_out_exp) 
  str_out_exp should be consistent with sscanf format
  The number of the expected readout integers should be < 5.
  The size of the integer array 'val' must be larger than
  or equal to number of readout integers.
*/
INT isegnhq_rw_int(ISEGNHQ_INFO* info, char* str_in, char* str_out_exp, int* val)
{
  char str_out[100];
  int  i, ret, tval[5];
  
  /* write / read */
  if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 100)) return FE_ERR_HW;
  
  /* sscanf */
  ret = sscanf(str_out, str_out_exp, &tval[0], &tval[1], &tval[2], &tval[3], &tval[4]);
  if(ret <= 0){
    //cm_msg(MERROR, "dd_isegnhq", "No integer values obtained.");
    return FE_ERR_HW;
  }

  /* copy */
  for(i=0; i<ret; i++){
    val[i] = tval[i];
  }
  
  return FE_SUCCESS;
}

/*
  read string xxx from expected string format (str_out_exp) 
  str_out_exp should be consistent with sscanf format
*/
INT isegnhq_rw_str(ISEGNHQ_INFO* info, char* str_in, char* str_out_exp, char* val)
{
  char str_out[100];
  int  i, ret;
  
  /* write / read */
  if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 100)) return FE_ERR_HW;
  
  /* sscanf */
  ret = sscanf(str_out, str_out_exp, val);
  if(ret <= 0){
    cm_msg(MERROR, "dd_isegnhq", "No string obtained.");
    return FE_ERR_HW;
  }
  
  return FE_SUCCESS;
}

/* get ISEG NHQ status value. return -1 if any error happened. */
INT isegnhq_get_stat(ISEGNHQ_INFO* info, int channel)
{
  int ret, stat;
  char str_in[30], str_out_exp[30];
  
  /* prepare */
  sprintf(str_in, "T%01d", channel+1);
  sprintf(str_out_exp, "T%01d\r\n%%d\r\n", channel+1);

  /* rw */
  ret = isegnhq_rw_int(info, str_in, str_out_exp, &stat);
  if(ret != FE_SUCCESS){
    cm_msg(MERROR, "isegnhq_get_stat", "Cannot get status from ch=%d.", channel);
    return -1;
  }
  
  return stat;
}

/* check and get hardware parameter */
INT isegnhq_check_module(ISEGNHQ_INFO* info)
{
  int status, value[5];
  char str_in[10] = "#";
  char str_out_exp[] = "#\r\n%06d;%01d.%02d;%04dV;%01mA\r\n";
  
  if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, value)){
    cm_msg(MERROR, "isegnhq_check_module", "Cannot get hardware information");
    return FE_ERR_HW;
  }

  /* if unit number is specified, check its consisntency */
  if(info->settings.unit_no >= 0 &&
     info->settings.unit_no != value[0]){
    cm_msg(MERROR, "isegnhq_check_module", "The unit number is inconsistent with ODB.");
    return FE_ERR_ODB;
  }

  /* limit values */
  info->hv_limit  = value[3];
  info->cur_limit = value[4]*1000;

  return FE_SUCCESS;
}

/*---- device driver routines --------------------------------------*/
INT isegnhq_init(HNDLE hkey, void **pinfo, INT channels, INT(*bd) (INT cmd, ...))
{
  int  status, size, ch;
  HNDLE hDB, hkeydd;
  ISEGNHQ_INFO *info;

  /* allocate info structure */
  info = (ISEGNHQ_INFO *) calloc(1, sizeof(ISEGNHQ_INFO));
  *pinfo = info;
  
  /* check number of channels */
  if(channels < 1 || channels > 2){
    cm_msg(MERROR, "isegnhq", "Illegal channel number.");
    return FE_ERR_HW;
  }
  info->num_channels = channels;
  
  
  cm_get_experiment_database(&hDB, NULL);

  /* create ISEGNHQ settings record */
  status = db_create_record(hDB, hkey, "ISEG_NHQ", ISEGNHQ_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  db_find_key(hDB, hkey, "ISEG_NHQ", &hkeydd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeydd, &info->settings, &size, 0);
    
  /* open device on RS232 */
  status = rs232(CMD_INIT, hkey, &(info->rs232_info));
  if (status != SUCCESS ) {
    cm_msg(MERROR, "isegnhq_init",
           "Cannot access via RS232, Check power and connection.");
    return FE_ERR_HW;
  }
  
  /* check hardware */
  status = isegnhq_check_module(info);
  if(status != FE_SUCCESS) return status;
  
  /* check status */
  for(ch=0; ch<channels; ch++){
    status = isegnhq_get_stat(info, ch);
    if(status < 0) return FE_ERR_HW;
    
    if(status & ISEGNHQ_OFF){
      cm_msg(MERROR, "isegnhq", "ch%d output switch on the panal is off.", ch);
      return FE_ERR_HW;
    }
    
    if(status & ISEGNHQ_MAN){
#ifdef _ISEGNHQ_ALLOW_READONLY_
      cm_msg(MINFO, "isegnhq", "ch%d is now read only mode."
             " Switch DAC mode if you want control output voltage.", ch);
#else
      cm_msg(MERROR, "isegnhq", "ch%d is set to manual mode.", ch);
      return FE_ERR_HW;
#endif
    }
    
    if( ( info->settings.polarity[ch] && !(status & ISEGNHQ_POL) ) ||
        (!info->settings.polarity[ch] &&  (status & ISEGNHQ_POL) ) ){
      cm_msg(MERROR, "isegnhq", "ch%d polarity setting differs from the ODB settings.", ch);
      return FE_ERR_ODB;
    }
  }
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT isegnhq_exit(ISEGNHQ_INFO * info)
{
  int status;
  status = rs232(CMD_EXIT, info->rs232_info);
  
  free(info);
  
  return FE_SUCCESS;
}


/*----------------------------------------------------------------------------*/
// Set Voltage
INT isegnhq_set(ISEGNHQ_INFO * info, INT channel, float value)
{
  INT status;
  char str_in[30], str_out[30];
  int ivalue = (int)(value+0.5);

#ifdef _ISEGNHQ_ALLOW_READONLY_
  // check status
  status = isegnhq_get_stat(info, channel);
  if(status < 0) return FE_ERR_HW;
  else if(status & ISEGNHQ_MAN) return FE_SUCCESS;
#endif

  // HV off
  if(ivalue < 0) ivalue = 0;
  
  sprintf(str_in, "D%d=%d", channel+1, ivalue);
  if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 30)){
    cm_msg(MERROR, "isegnhq_set", "Cannot set HV ch%d", channel);
    return FE_ERR_HW;
  }
  
  sprintf(str_in, "G%d", channel+1);
  if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 30)){
    cm_msg(MERROR, "isegnhq_set", "Cannot start to change HV ch%d", channel);
    return FE_ERR_HW;
  }
  
  if(ivalue == 0) cm_msg(MINFO, "isegnhq", "Channel %d HV OFF", channel);
  else            cm_msg(MINFO, "isegnhq", "Channel %d HV ON", channel);
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT isegnhq_get(ISEGNHQ_INFO * info, INT channel, float *pvalue)
{
  char str_in[30], str_out_exp[30];
  int ivalue;
  
  sprintf(str_in, "U%d", channel+1);
  sprintf(str_out_exp, "U%d\r\n%c%%04d\r\n", channel+1, 
          (info->settings.polarity[channel]? '+':'-'));
  if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, &ivalue)){
    //cm_msg(MERROR, "isegnhq_get", "Cannot get HV ch%d", channel);
    return FE_ERR_HW;
  }
  
  *pvalue = 1.0*ivalue;
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT isegnhq_get_current(ISEGNHQ_INFO * info, INT channel, float *pvalue)
{
  char str_in[30], str_out_exp[30];
  int ivalue[2];
  
  sprintf(str_in, "I%d", channel+1);
  sprintf(str_out_exp, "I%d\r\n%%04d-%%d\r\n", channel+1);
  if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, ivalue)){
    //cm_msg(MERROR, "isegnhq_get_current", "Cannot get current ch%d", channel);
    return FE_ERR_HW;
  }
  
  // Basic unit = uA
  *pvalue = ivalue[0] * (float)pow(10.0, -ivalue[1] + 6.0); 
  
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT isegnhq_get_other(ISEGNHQ_INFO * info, INT channel, float *pvalue, int cmd)
{
  char str_in[30], str_out_exp[30];
  int ivalue;
  
  switch (cmd) {
  case CMD_GET_DEMAND:
    sprintf(str_in, "D%d", channel+1);
    sprintf(str_out_exp, "D%d\r\n%%04d\r\n", channel+1);
    if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "isegnhq_get_other", "Cannot get demand HV ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;
    break;
  
  case CMD_GET_CURRENT_LIMIT:
    /* used for trip current */
    sprintf(str_in, "L%d", channel+1);
    sprintf(str_out_exp, "L%d\r\n%%04d\r\n", channel+1);
    if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "isegnhq_get_other", "Cannot get current limit HV ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;
    
    break;
  
  case CMD_GET_TRIP_TIME:
    /* No implemented for ISEG NHQ */
    break;

  case CMD_GET_RAMPUP:
    sprintf(str_in, "V%d", channel+1);
    sprintf(str_out_exp, "V%d\r\n%%03d\r\n", channel+1);
    if(FE_SUCCESS != isegnhq_rw_int(info, str_in, str_out_exp, &ivalue)){
      cm_msg(MERROR, "isegnhq_get_other", "Cannot get ramp up speed ch%d", channel);
      return FE_ERR_HW;
    }
    *pvalue = 1.0 * ivalue;

    break;

  case CMD_GET_RAMPDOWN:  // use it for current ramp speed
    /* No implemented for ISEG NHQ */
    break;
  }

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT isegnhq_set_other(ISEGNHQ_INFO * info, int channel, float limit, int cmd)
{
  char str_in[30], str_out[30];
  int ivalue = (int)(limit + 0.5);
  
  switch (cmd) {
  case CMD_SET_RAMPUP:
    sprintf(str_in, "V%d=%d", channel+1, ivalue);
    if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 30)){
      cm_msg(MERROR, "isegnhq_set_other", "Cannot set ramp up speed ch%d", channel);
      return FE_ERR_HW;
    }
    break;
    
  case CMD_SET_RAMPDOWN:   // Use it for current ramp speed
    /* No implemented for ISEG NHQ */
    break;
    
  case CMD_SET_CURRENT_LIMIT:
    /* ivalue = 0 means no trip */
    sprintf(str_in, "L%d=%d", channel+1, ivalue);
    if(FE_SUCCESS != isegnhq_rw(info, str_in, str_out, 30)){
      cm_msg(MERROR, "isegnhq_set_other", "Cannot set current trip ch%d", channel);
      return FE_ERR_HW;
    }
    break;
    
  case CMD_SET_VOLTAGE_LIMIT:
    /* No implemented for ISEG NHQ */
    break;
  }

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
void isegnhq_CsrTest(ISEGNHQ_INFO * info)
{
  /*
  INT csr;
  char str[128];

  csr = isegnhq_RegisterRead(info->fd, info->settings.base, ISEGNHQ_MODULE_STATUS);
  sprintf(str, "CSR:0x%x >> ", csr);
  strcat(str, csr & 0x0001 ? "FineAdj /" : " NoFineAdj /"); 
  strcat(str, csr & 0x0080 ? "CmdCpl /" : " /"); 
  strcat(str, csr & 0x0100 ? "noError /" : " ERROR /"); 
  strcat(str, csr & 0x0200 ? "noRamp /" : " RAMPING /"); 
  strcat(str, csr & 0x0400 ? "Saf closed /" : " /"); 
  strcat(str, csr & 0x0800 ? "Evt Active /" : " /"); 
  strcat(str, csr & 0x1000 ? "Mod Good /" : " MOD NO good /"); 
  strcat(str, csr & 0x2000 ? "Supply Good /" : " Supply NO good /"); 
  strcat(str, csr & 0x4000 ? "Temp Good /" : " Temp NO good /"); 
  printf("CSR:%s\n", str);
  if (!(csr & 0x100)) {
    isegnhq_RegisterWrite(info->fd, info->settings.base, ISEGNHQ_MODULE_CONTROL, 0x40);    // doCLEAR
    cm_msg(MERROR, "isegnhq", str);
    cm_msg(MERROR, "isegnhq", "Command doCLEAR issued, check the Voltages!");
  } else
    //     cm_msg(MINFO, "isegnhq", str);
    return;
  */
}

/*---- device driver entry point -----------------------------------*/
INT dd_isegnhq(INT cmd, ...)
{
  static DWORD itstime=0;
  va_list argptr;
  HNDLE hKey;
  INT channel, status;
  float value, *pvalue;
  void *info, *bd;

  va_start(argptr, cmd);
  status = FE_SUCCESS;

  switch (cmd) {
  case CMD_INIT:
    hKey = va_arg(argptr, HNDLE);
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    bd = va_arg(argptr, void *);
    status = isegnhq_init(hKey, (void **)info, channel, bd);
    break;

  case CMD_EXIT:
    info = va_arg(argptr, void *);
    status = isegnhq_exit(info);
    break;

  case CMD_SET:  // Voltage
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    value = (float) va_arg(argptr, double);
    status = isegnhq_set(info, channel, value);
    break;

  case CMD_GET: // Voltage
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    pvalue = va_arg(argptr, float *);
    if((ss_time() - itstime) > 10) {
      isegnhq_CsrTest(info);
      itstime = ss_time();
    }
    status = isegnhq_get(info, channel, pvalue);
    break;

  case CMD_GET_CURRENT:  // Current
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    pvalue = va_arg(argptr, float *);
    status = isegnhq_get_current(info, channel, pvalue);
    break;

  case CMD_GET_RAMPUP:
  case CMD_GET_RAMPDOWN:
  case CMD_GET_TRIP_TIME:
  case CMD_GET_DEMAND:          // Desired Voltage
  case CMD_GET_CURRENT_LIMIT:  //
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    pvalue = va_arg(argptr, float *);
    status = isegnhq_get_other(info, channel, pvalue, cmd);
    break;

  case CMD_SET_RAMPUP:
  case CMD_SET_RAMPDOWN:
  case CMD_SET_CURRENT_LIMIT:
    info = va_arg(argptr, void *);
    channel = va_arg(argptr, INT);
    value = (float) va_arg(argptr, double);
    status = isegnhq_set_other(info, channel, value, cmd);
    break;

  case CMD_GET_LABEL:
    status = FE_SUCCESS;
    break;

  case CMD_SET_LABEL:
    status = FE_SUCCESS;
    break;

  default:
    //      cm_msg(MERROR, "isegnhq device driver", "Received unknown command %d", cmd);
    status = FE_ERR_DRIVER;
    break;
  }

  va_end(argptr);

  return status;
}

/*------------------------------------------------------------------*/
