/********************************************************************\

  Name:         dd_pfeiffer26x.h
  Created by:   Kou Oishi

  Contents:     Device driver function declarations

  $Id$

\********************************************************************/
INT dd_pfeiffer26x(INT cmd, ...);

