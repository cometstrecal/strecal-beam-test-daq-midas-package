/********************************************************************\

  Name:         dd_nimeasiroc.h
  Created by:   Kou Oishi

  Contents:     Device driver function declarations

  $Id$

\********************************************************************/
INT dd_nimeasiroc(INT cmd, ...);

