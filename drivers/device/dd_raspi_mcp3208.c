/********************************************************************\

  Name:         dd_raspi_mcp3208.c
  Created by:   Kou Oishi

  Based on the mscbhvr device driver

  Contents:     Raspberry pi ADC (MCP3208) controller

  $Id$
\********************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "wiringPi.h"

#include "midas.h"
#include "dd_raspi_mcp3208.h"
#include "raspi.h"

// If define this, ce8 and ce9 is inverted for the LED driver circuit.
#define INVERT_CE8_9


/*---- globals -----------------------------------------------------*/

#define MAX_CHIPS 25
#define CHS_PER_CHIP 8

typedef struct {
  INT chip2pin_lut[MAX_CHIPS]; /* ADC chip ch -> Pi GPIO Pin LUT  */
  INT spi_speed;               /* SPI clock speed (kHz)           */
  INT num_ave_samples;         /* Number of samples for avaraging */
} RASPI_MCP3208_SETTINGS;

#define RASPI_MCP3208_SETTINGS_STR "\
[.]\n\
Used ChipToPin LUT = INT[25] : \n\
[ 0] 8\n\
[ 1] 9\n\
[ 2] 7\n\
[ 3] 0\n\
[ 4] 2\n\
[ 5] 3\n\
[ 6] 15\n\
[ 7] 16\n\
[ 8] 1\n\
[ 9] 4\n\
[10] -1\n\
[12] -1\n\
[13] -1\n\
[14] -1\n\
[15] -1\n\
[16] -1\n\
[17] -1\n\
[18] -1\n\
[19] -1\n\
[20] -1\n\
[21] -1\n\
[22] -1\n\
[23] -1\n\
[24] -1\n\
SPI Clock (kHz) = INT : 100\n\
Averaging Samples = INT : 100\n\
"

typedef struct {
  RASPI_MCP3208_SETTINGS settings;
  int num_chips;
  int num_channels;
  void* raspi_info;
} RASPI_MCP3208_INFO;

// debug
extern BOOL debug;

#define DEBUG(str, ...) if(debug) printf(str, __VA_ARGS__)

/*---- device driver routines --------------------------------------*/
INT raspi_mcp3208_init(HNDLE hkey, void **pinfo, INT channels, INT(*bd) (INT cmd, ...))
{
  int  i, status, size;
  HNDLE hDB, hkeydd;
  BOOL enable;
  RASPI_MCP3208_INFO *info;
  
  /* allocate info structure */
  info = (RASPI_MCP3208_INFO *) calloc(1, sizeof(RASPI_MCP3208_INFO));
  *pinfo = info;
  
  cm_get_experiment_database(&hDB, NULL);
  
  /* create RASPI_MCP3208 settings record */
  status = db_create_record(hDB, hkey, "Raspi_Mcp3208", RASPI_MCP3208_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  db_find_key(hDB, hkey, "Raspi_Mcp3208", &hkeydd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeydd, &info->settings, &size, 0);
  
  /* channel */
  if(channels > MAX_CHIPS*CHS_PER_CHIP){
    cm_msg(MERROR, "raspi_mcp3208", "Number of Channels: %d is too much.", channels);
    return FE_ERR_HW;
  }
  if(channels%CHS_PER_CHIP == 0)
    info->num_chips = channels/CHS_PER_CHIP;
  else
    info->num_chips = channels/CHS_PER_CHIP + 1;
  info->num_channels = channels;
  
  /* open device on Raspberry Pi */
  status = raspi(CMD_INIT, hkey, &(info->raspi_info));
  if(status != SUCCESS){
    return FE_ERR_HW;
  }
  printf("Raspberry Pi initialized.\n");
  
  /* initialize SPI */
  status = raspi(CMD_RASPI_INIT_SPI, info->raspi_info,
                 /* SPI ch */ 0, 
		 /* SPI clocking speed*/ info->settings.spi_speed * 1000 /* kHz -> Hz */,
		 /* SPI clocking mode */ 0);
  if(status != SUCCESS){
    return FE_ERR_HW;
  }
  printf("SPI initialized.\n");
  
  /* initialize GPIO pins as extra CE and enable them all (meaning disable for SPI) */
  for(i=0; i<info->num_chips ; i++){
    if(0 > info->settings.chip2pin_lut[i]){
      cm_msg(MERROR, "raspi_mcp3208", "GPIO number for chip %d is not set.", i);
      return FE_ERR_HW;
    }
    printf("Now setting chip: %d -> GPIO : %d\n", i, info->settings.chip2pin_lut[i]);
    status = raspi(CMD_RASPI_SET_PINMODE, info->raspi_info, info->settings.chip2pin_lut[i], OUTPUT);
    if(status != SUCCESS) return FE_ERR_HW;

    enable = TRUE;
#ifdef INVERT_CE8_9
    if(i==8 || i==9) enable = FALSE;
#endif
    status = raspi(CMD_WRITE, info->raspi_info, info->settings.chip2pin_lut[i], enable);
    if(status != SUCCESS) return FE_ERR_HW;
  }
  printf("CE initialized.\n");

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT raspi_mcp3208_exit(RASPI_MCP3208_INFO * info)
{
  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
INT raspi_mcp3208_get(RASPI_MCP3208_INFO * info, INT channel, float *pvalue)
{
  int chip, chipch, i, status;
  BYTE in[3], buff[3];
  
  if(0> channel || channel >= info->num_channels){
    cm_msg(MINFO, "raspi_mcp3208", "Illegal channel: %d", channel);
    return FE_SUCCESS;
  }
  chip   = channel/CHS_PER_CHIP;
  chipch = channel%CHS_PER_CHIP;

  DEBUG("Start reading from ch%d from chip %d\n", chipch, chip);
  
  /* SPI command */
  in[0] = 6 | ((chipch & 0x4) >> 2);
  in[1] = (chipch & 0x3) << 6;
  in[2] = 0;
  
  /* readout */
  *pvalue = 0.0;
  for(i=0; i<info->settings.num_ave_samples; i++){
    BOOL enable = FALSE;
#ifdef INVERT_CE8_9
    if(chip==8 || chip==9) enable = TRUE;
#endif
    /* Chip enable */
    raspi(CMD_WRITE, info->raspi_info, info->settings.chip2pin_lut[chip], enable);

    memcpy(buff, in, 3);    
    status = raspi(CMD_RASPI_SPI_WR, info->raspi_info, 0, buff, 3);
    if(status != SUCCESS){ 
      cm_msg(MERROR, "raspi_mcp3208", "Cannot use spi communication");
      return -1;
    }
    
    /* Chip disable */
    raspi(CMD_WRITE, info->raspi_info, info->settings.chip2pin_lut[chip], !enable);
    // return value
    *pvalue += ((buff[1] & 0xF)<<8) + buff[2];
  }
  
  /* averaging */
  *pvalue /= (float)info->settings.num_ave_samples;
  DEBUG("got a value : %.3f\n", *pvalue);

  return FE_SUCCESS;
}


/*---- device driver entry point -----------------------------------*/
INT dd_raspi_mcp3208(INT cmd, ...)
{
  static DWORD itstime=0;
  va_list argptr;
  HNDLE hKey;
  DWORD flag;
  INT channel, status;
  float value, *pvalue;
  void *info, *bd;
  
   va_start(argptr, cmd);
   status = FE_SUCCESS;

   switch (cmd) {
   case CMD_INIT:
      hKey = va_arg(argptr, HNDLE);
      info = va_arg(argptr, void *);
      channel = va_arg(argptr, INT);
      flag = va_arg(argptr, DWORD);
      bd = va_arg(argptr, void *);
      status = raspi_mcp3208_init(hKey, (void **)info, channel, bd);
      break;

   case CMD_EXIT:
      info = va_arg(argptr, void *);
      status = raspi_mcp3208_exit(info);
      break;

   case CMD_GET: // ADC value
     info    = va_arg(argptr, void *);
     channel = va_arg(argptr, INT);
     pvalue  = va_arg(argptr, float *);
     status  = raspi_mcp3208_get(info, channel, pvalue);
     break;
     
   case CMD_GET_LABEL:
      status = FE_SUCCESS;
      break;

   case CMD_SET_LABEL:
      status = FE_SUCCESS;
      break;

   default:
      break;
   }

   va_end(argptr);

   return status;
}

/*------------------------------------------------------------------*/
