/********************************************************************\

  Name:         cd_raspi_hvtmon.h
  Created by:   Kou Oishi

  Contents:     RASPI_TEMP_HV_MON / Raspberry Pi Temperature & HV monitor

  $Id:$

\********************************************************************/

/* class driver routines */
INT cd_raspi_hvtmon(INT cmd, PEQUIPMENT pequipment);
INT cd_raspi_hvtmon_read(char *pevent, int);
