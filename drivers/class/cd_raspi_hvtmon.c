/********************************************************************\

  Name:         cd_raspi_hvtmon.c
  Created by:   Kou Oishi

  Based on the Generic class
  Contents:     RASPI_HVTMON Raspberry Pi Temperature & HV monitor

  $Id: cd_raspi_hvtmon.c$

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "midas.h"
#include "dd_raspi_mcp3208.h"

typedef struct {
  
  /* ODB keys */
  HNDLE hKeyRoot, hKeyHV, hKeyTemp;
  
  /* globals */
  INT num_hv_channels;
  INT num_temp_channels;
  INT format;
  INT last_hv_channel;
  INT last_temp_channel;
  
  /* items in /Variables record */
  float *hv;
  float *temp;

  /* calibration parameter to calculate HV from ADC value */
  float *calib_hv;

  /* reference resistance value to calclulate temperature from ADC value */
  float *ref_resist;

  BOOL forceRO;
} RASPI_HVTMON_INFO;

extern BOOL debug;
#define DEBUGV(str, ...) if(debug)printf(str "\n", __VA_ARGS__);
#define DEBUG(str)       if(debug)printf(str "\n");

/*------------------------------------------------------------------*/

static void free_mem(RASPI_HVTMON_INFO * raspi_hvtmon_info)
{
   free(raspi_hvtmon_info->hv);
   free(raspi_hvtmon_info->calib_hv);
   free(raspi_hvtmon_info->temp);
   free(raspi_hvtmon_info->ref_resist);
   
   free(raspi_hvtmon_info);
}

/*------------------------------------------------------------------*/
// Read HV
INT raspi_hvtmon_read_HV(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   RASPI_HVTMON_INFO *raspi_hvtmon_info;
   HNDLE hDB;
   float adc;
   int inv_channel;
   
   /*---- read HV measured value ----*/
   raspi_hvtmon_info = (RASPI_HVTMON_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   /* Connection between the ADC board and HV divider is
      opposite each other, so convert channel in the inverted order */
   inv_channel = raspi_hvtmon_info->num_hv_channels - channel;
   
   // Get the ADC value
   if (raspi_hvtmon_info->forceRO || (ss_time() - last_time) > 1) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET, inv_channel, &adc);

     /* convert ADC value into V */
     raspi_hvtmon_info->hv[channel] = adc * raspi_hvtmon_info->calib_hv[channel];
     
     /* save into ODB */
     db_set_data(hDB, raspi_hvtmon_info->hKeyHV, raspi_hvtmon_info->hv,
                 sizeof(float) * raspi_hvtmon_info->num_hv_channels,
                 raspi_hvtmon_info->num_hv_channels,
                 TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}

/*------------------------------------------------------------------*/
// Read Temp
INT raspi_hvtmon_read_Temp(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   RASPI_HVTMON_INFO *raspi_hvtmon_info;
   HNDLE hDB;
   float adc, Rt;
   
   /*---- read HV measured value ----*/
   raspi_hvtmon_info = (RASPI_HVTMON_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   // Get the ADC value
   if (raspi_hvtmon_info->forceRO || (ss_time() - last_time) > 1) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET,
                            channel + raspi_hvtmon_info->num_hv_channels, /* offset */
                            &adc);
     
     /* convert ADC value into K */
     Rt = raspi_hvtmon_info->ref_resist[channel] * adc/(4096.0 - adc);
     raspi_hvtmon_info->temp[channel] = (Rt - 10.0)/3.850 * 100;
     
     /* save into ODB */
     db_set_data(hDB, raspi_hvtmon_info->hKeyTemp, raspi_hvtmon_info->temp,
                 sizeof(float) * raspi_hvtmon_info->num_temp_channels,
                 raspi_hvtmon_info->num_temp_channels,
                 TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}


/*------------------------------------------------------------------*/

INT raspi_hvtmon_init(EQUIPMENT * pequipment)
{
   int status, size, i, j, index, offset;
   char str[256];
   HNDLE hDB, hKey, hNames, hThreshold;
   RASPI_HVTMON_INFO *raspi_hvtmon_info;

   /* allocate private data */
   pequipment->cd_info = calloc(1, sizeof(RASPI_HVTMON_INFO));
   raspi_hvtmon_info = (RASPI_HVTMON_INFO *) pequipment->cd_info;
   
   /* get class driver root key */
   cm_get_experiment_database(&hDB, NULL);
   sprintf(str, "/Equipment/%s", pequipment->name);
   db_create_key(hDB, 0, str, TID_KEY);
   db_find_key(hDB, 0, str, &raspi_hvtmon_info->hKeyRoot);

   /* save event format */
   size = sizeof(str);
   db_get_value(hDB, raspi_hvtmon_info->hKeyRoot, "Common/Format", str, &size, TID_STRING, TRUE);

   if (equal_ustring(str, "Fixed"))
      raspi_hvtmon_info->format = FORMAT_FIXED;
   else if (equal_ustring(str, "MIDAS"))
      raspi_hvtmon_info->format = FORMAT_MIDAS;
   else if (equal_ustring(str, "YBOS"))
      raspi_hvtmon_info->format = FORMAT_YBOS;

   /* Create Settings */
   db_create_key(hDB, raspi_hvtmon_info->hKeyRoot, "Settings", TID_KEY);
   
   /* Get HV/Temp channel numbers */
   sprintf(str, "Settings/Num of HV Channels");
   size = sizeof(raspi_hvtmon_info->num_hv_channels);
   db_get_value(hDB, raspi_hvtmon_info->hKeyRoot, str,  
                &(raspi_hvtmon_info->num_hv_channels),
                &size, TID_INT, TRUE);

   sprintf(str, "Settings/Num of Temp Channels");
   size = sizeof(raspi_hvtmon_info->num_temp_channels);
   db_get_value(hDB, raspi_hvtmon_info->hKeyRoot, str,  
                &(raspi_hvtmon_info->num_temp_channels),
                &size, TID_INT, TRUE);

   if (raspi_hvtmon_info->num_temp_channels +
       raspi_hvtmon_info->num_hv_channels     > pequipment->driver[0].channels) {
     cm_msg(MERROR, "raspi_hvtmon_init", "Too much number of channels ");
     return FE_ERR_ODB;
   }
   
   if (raspi_hvtmon_info->num_hv_channels + raspi_hvtmon_info->num_temp_channels == 0) {
      cm_msg(MERROR, "raspi_hvtmon_init", "No channels found in device driver list");
      return FE_ERR_ODB;
   }

   /* Allocate memory for buffers */
   raspi_hvtmon_info->hv       = (float *) calloc(raspi_hvtmon_info->num_hv_channels, sizeof(float));
   raspi_hvtmon_info->calib_hv = (float *) calloc(raspi_hvtmon_info->num_hv_channels, sizeof(float));

   raspi_hvtmon_info->temp       = (float *) calloc(raspi_hvtmon_info->num_temp_channels, sizeof(float));
   raspi_hvtmon_info->ref_resist = (float *) calloc(raspi_hvtmon_info->num_temp_channels, sizeof(float));

   /* Get calibration constants */
   sprintf(str,  "Settings/HV Calib Factor");
   db_merge_data(hDB, raspi_hvtmon_info->hKeyRoot, str,
                 raspi_hvtmon_info->calib_hv, raspi_hvtmon_info->num_hv_channels * sizeof(float),
                 raspi_hvtmon_info->num_hv_channels, TID_FLOAT);
   
   sprintf(str,  "Settings/Temp Ref Resist.");
   db_merge_data(hDB, raspi_hvtmon_info->hKeyRoot, str,
                 raspi_hvtmon_info->ref_resist, raspi_hvtmon_info->num_temp_channels * sizeof(float),
                 raspi_hvtmon_info->num_temp_channels, TID_FLOAT);
   
   /*---- Initialize device drivers ----*/
   /* call init method */
   status = device_driver(&(pequipment->driver[0]), CMD_INIT, raspi_hvtmon_info->hKeyRoot);
   if (status != FE_SUCCESS) {
     cm_msg(MERROR, "cd_raspi_hvtmon", "Couldn't initialize the device driver.");
     free_mem(raspi_hvtmon_info);
     return status;
   }
   
   /*---- create HV measured variables ----*/
   db_merge_data(hDB, raspi_hvtmon_info->hKeyRoot, "Variables/HV",
                 raspi_hvtmon_info->hv, sizeof(float) * raspi_hvtmon_info->num_hv_channels,
                 raspi_hvtmon_info->num_hv_channels, TID_FLOAT);
   db_find_key(hDB, raspi_hvtmon_info->hKeyRoot, "Variables/HV", &raspi_hvtmon_info->hKeyHV);

   /*---- create Temp measured variables ----*/
   db_merge_data(hDB, raspi_hvtmon_info->hKeyRoot, "Variables/Temp",
                 raspi_hvtmon_info->temp, sizeof(float) * raspi_hvtmon_info->num_temp_channels,
                 raspi_hvtmon_info->num_temp_channels, TID_FLOAT);
   db_find_key(hDB, raspi_hvtmon_info->hKeyRoot, "Variables/Temp", &raspi_hvtmon_info->hKeyTemp);

   /* initially read all channels */
   printf("Now initial reading out ....\n");
   raspi_hvtmon_info->forceRO = TRUE;
   for(i=0; i < raspi_hvtmon_info->num_hv_channels; i++)
     raspi_hvtmon_read_HV(pequipment, i);
   for(i=0; i < raspi_hvtmon_info->num_temp_channels; i++)
     raspi_hvtmon_read_Temp(pequipment, i);
   raspi_hvtmon_info->forceRO = FALSE;
   printf("Finished.\n");

   ss_sleep(3000);
   
   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT raspi_hvtmon_exit(EQUIPMENT * pequipment)
{
   INT i;

   free_mem((RASPI_HVTMON_INFO *) pequipment->cd_info);

   /* call exit method of device drivers */
   device_driver(&(pequipment->driver[0]), CMD_EXIT);

   return FE_SUCCESS;
}

/*------------------------------------------------------------------*/

INT raspi_hvtmon_idle(EQUIPMENT * pequipment)
{
   INT act, status;
   RASPI_HVTMON_INFO *raspi_hvtmon_info;

   raspi_hvtmon_info = (RASPI_HVTMON_INFO *) pequipment->cd_info;
   raspi_hvtmon_info->forceRO = TRUE;
   if(raspi_hvtmon_info->num_hv_channels > 0){
     /* select next HV measurement channel */
     act = (raspi_hvtmon_info->last_hv_channel + 1) % raspi_hvtmon_info->num_hv_channels;
     
     /* measure HV channel */
     status = raspi_hvtmon_read_HV(pequipment, act);
     raspi_hvtmon_info->last_hv_channel = act;
   }
   ss_sleep(1);
   if(raspi_hvtmon_info->num_temp_channels > 0){
     /* select next Temp measurement channel */
     act = (raspi_hvtmon_info->last_temp_channel + 1) % raspi_hvtmon_info->num_temp_channels;
     
     /* measure HV channel */
     status = raspi_hvtmon_read_Temp(pequipment, act);
     raspi_hvtmon_info->last_temp_channel = act;
   }
   raspi_hvtmon_info->forceRO = FALSE;

   return status;
}

/*------------------------------------------------------------------*/

INT cd_raspi_hvtmon_read(char *pevent, int offset)
{
   float *pdata;
   RASPI_HVTMON_INFO *raspi_hvtmon_info;
   EQUIPMENT *pequipment;

   pequipment = *((EQUIPMENT **) pevent);
   raspi_hvtmon_info = (RASPI_HVTMON_INFO *) pequipment->cd_info;

   if (raspi_hvtmon_info->format == FORMAT_FIXED) {
      memcpy(pevent, raspi_hvtmon_info->hv, sizeof(float) * raspi_hvtmon_info->num_hv_channels);
      pevent += sizeof(float) * raspi_hvtmon_info->num_hv_channels;

      memcpy(pevent, raspi_hvtmon_info->temp, sizeof(float) * raspi_hvtmon_info->num_temp_channels);
      pevent += sizeof(float) * raspi_hvtmon_info->num_temp_channels;

      return sizeof(float) * (raspi_hvtmon_info->num_hv_channels
                              + raspi_hvtmon_info->num_temp_channels);
   } else if (raspi_hvtmon_info->format == FORMAT_MIDAS) {
      bk_init(pevent);

      /* create HV bank */
      bk_create(pevent, "HV", TID_FLOAT, &pdata);
      memcpy(pdata, raspi_hvtmon_info->hv, sizeof(float) * raspi_hvtmon_info->num_hv_channels);
      pdata += raspi_hvtmon_info->num_hv_channels;
      bk_close(pevent, pdata);

      /* create TEMP1 bank */
      bk_create(pevent, "TEMP", TID_FLOAT, &pdata);
      memcpy(pdata, raspi_hvtmon_info->temp, sizeof(float) * raspi_hvtmon_info->num_temp_channels);
      pdata += raspi_hvtmon_info->num_temp_channels;
      bk_close(pevent, pdata);
      
      return bk_size(pevent);
   } else if (raspi_hvtmon_info->format == FORMAT_YBOS) {
     printf("Not implemented\n");
     return 0;
   } else
     return 0;
}

/*------------------------------------------------------------------*/

INT cd_raspi_hvtmon(INT cmd, EQUIPMENT * pequipment)
{
   INT status;

   DEBUGV("cmd = %d", cmd);
   
   switch (cmd) {
   case CMD_INIT:
      status = raspi_hvtmon_init(pequipment);
      break;

   case CMD_START:
      break;

   case CMD_STOP:
      break;

   case CMD_EXIT:
      status = raspi_hvtmon_exit(pequipment);
      break;

   case CMD_IDLE:
      status = raspi_hvtmon_idle(pequipment);
      break;

   default:
      cm_msg(MERROR, "cd_raspi_hvtmon", "Received unknown command %d", cmd);
      status = FE_ERR_DRIVER;
      break;
   }

   return status;
}
