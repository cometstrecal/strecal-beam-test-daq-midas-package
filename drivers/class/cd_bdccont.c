/********************************************************************\

  Name:         cd_bdccont.c
  Created by:   Kou Oishi

  Based on the Generic class
  Contents:     BDC Control

  $Id: cd_bdccont.c$

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "midas.h"
#include "dd_nimeasiroc.h"

extern INT get_frontend_index(); /* declared in mfe.o */

#define IMPLEMENT_TEMP
#define READ_ATONCE

typedef struct {
  
  /* ODB keys */
  HNDLE hKeyRoot, hKeyDemand, hKeyMeasure, 
    hKeyCurrent, hKeyHV, hKeyTemp;
  
  /* globals */
  INT num_hv_channels;
  INT num_temp_channels;
  INT format;
  INT last_hv_channel;
  INT last_temp_channel;
  
  /* items in /Variables record */
  float demand_bias;
  float bias;
  float current;
  float *hv;
  float *temp;

  /* mirror */
  float demand_bias_mirror;

  DWORD last_change;
  
  BOOL forceRO;
} BDCCONT_INFO;

char bk_name_HV[5]   = "BHV";
char bk_name_CON[5]  = "BCN";
#ifdef IMPLEMENT_TEMP
char bk_name_TEMP[5] = "BTP";
#endif

/*------------------------------------------------------------------*/

static void free_mem(BDCCONT_INFO * bdccont_info)
{
   free(bdccont_info->hv);
   free(bdccont_info->temp);
   
   free(bdccont_info);
}

/*------------------------------------------------------------------*/
// Read HV
INT bdccont_read_HV(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   BDCCONT_INFO *bdccont_info;
   HNDLE hDB;
   
   /*---- read HV measured value ----*/
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   // Get the ADC value
   if (bdccont_info->forceRO || (ss_time() - last_time) > 1) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET, channel, &bdccont_info->hv[channel]);
     
     /* convert into realistic value for MPPCs */
     bdccont_info->hv[channel] = bdccont_info->bias - bdccont_info->hv[channel];
     
     /* save into ODB */
#ifdef READ_ATONCE
     if(channel+1 == bdccont_info->num_hv_channels)
#endif
       db_set_data(hDB, bdccont_info->hKeyHV, bdccont_info->hv,
                   sizeof(float) * bdccont_info->num_hv_channels,
                   bdccont_info->num_hv_channels,
                   TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}

/*------------------------------------------------------------------*/
// Read Temp
INT bdccont_read_Temp(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   BDCCONT_INFO *bdccont_info;
   HNDLE hDB;
   
   /*---- read HV measured value ----*/
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   // Get the ADC value
   if (bdccont_info->forceRO || (ss_time() - last_time) > 5) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET_TEMPERATURE,
                            channel, &bdccont_info->temp[channel]);
     
     /* save into ODB */
#ifdef READ_ATONCE
     if(channel+1 == bdccont_info->num_temp_channels)
#endif
       db_set_data(hDB, bdccont_info->hKeyTemp, bdccont_info->temp,
                   sizeof(float) * bdccont_info->num_temp_channels,
                   bdccont_info->num_temp_channels,
                   TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}

/*------------------------------------------------------------------*/
// Read Current
INT bdccont_read_Current(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   BDCCONT_INFO *bdccont_info;
   HNDLE hDB;
   
   /*---- read HV measured value ----*/
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   // Get the ADC value
   if (bdccont_info->forceRO || (ss_time() - last_time) > 5) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET_CURRENT,
                            channel, &bdccont_info->current);
     
     /* save into ODB */
     db_set_value(hDB, bdccont_info->hKeyRoot, "Variables/Current", &bdccont_info->current,
                  sizeof(bdccont_info->current), 1, TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}

/*------------------------------------------------------------------*/
// Read Bias
INT bdccont_read_Bias(EQUIPMENT * pequipment, int channel)
{
   static DWORD last_time = 0;
   int i, status;
   BDCCONT_INFO *bdccont_info;
   HNDLE hDB;
   
   /*---- read HV measured value ----*/
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);

   // Get the ADC value
   if (bdccont_info->forceRO || (ss_time() - last_time) > 5) {
     status = device_driver(&(pequipment->driver[0]), CMD_GET_BIAS,
                            channel, &bdccont_info->bias);
     
     /* save into ODB */
     db_set_value(hDB, bdccont_info->hKeyRoot, "Variables/Measured Bias", &bdccont_info->bias,
                  sizeof(bdccont_info->bias), 1, TID_FLOAT);
     
     last_time = ss_time();
   }
   return status;
}


/*------------------------------------------------------------------*/

void bdccont_demand(INT hDB, INT hKey, void *info)
{
  INT i;
  BDCCONT_INFO *bdccont_info;
  EQUIPMENT *pequipment;
  
  pequipment = (EQUIPMENT *) info;
  bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
  
  /* check for voltage limit */
  if (bdccont_info->demand_bias > 70.0)
    bdccont_info->demand_bias = 70.0;
  
  /* set individual channels only if demand value differs */
  if (bdccont_info->demand_bias != bdccont_info->demand_bias_mirror){
    device_driver(&(pequipment->driver[0]), CMD_SET, 0, bdccont_info->demand_bias);
    
    bdccont_info->last_change = ss_millitime();
  }
  
  pequipment->odb_in++;
}

/*------------------------------------------------------------------*/

INT bdccont_init(EQUIPMENT * pequipment)
{
   int status, size, i, j, index, offset;
   char str[256];
   HNDLE hDB, hKey, hNames;
   BDCCONT_INFO *bdccont_info;

   /* set bank name */
   sprintf(bk_name_CON,  "BCN%0d", get_frontend_index());
#ifdef IMPLEMENT_TEMP
   sprintf(bk_name_TEMP, "BTP%0d", get_frontend_index());
#endif
   sprintf(bk_name_HV,   "BHV%0d", get_frontend_index());

   printf("\n\n");
   
   /* allocate private data */
   pequipment->cd_info = calloc(1, sizeof(BDCCONT_INFO));
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;
   
   /* get class driver root key */
   cm_get_experiment_database(&hDB, NULL);
   sprintf(str, "/Equipment/%s", pequipment->name);
   db_create_key(hDB, 0, str, TID_KEY);
   db_find_key(hDB, 0, str, &bdccont_info->hKeyRoot);

   /* save event format */
   size = sizeof(str);
   db_get_value(hDB, bdccont_info->hKeyRoot, "Common/Format", str, &size, TID_STRING, TRUE);

   if (equal_ustring(str, "Fixed"))
      bdccont_info->format = FORMAT_FIXED;
   else if (equal_ustring(str, "MIDAS"))
      bdccont_info->format = FORMAT_MIDAS;
   else if (equal_ustring(str, "YBOS"))
      bdccont_info->format = FORMAT_YBOS;

   /* Create Settings */
   db_create_key(hDB, bdccont_info->hKeyRoot, "Settings", TID_KEY);

   /* Channel numbers are constant now (pending) */
   bdccont_info->num_hv_channels = 64;
   bdccont_info->num_temp_channels = 2;
   
   /* Allocate memory for buffers */
   bdccont_info->hv       = (float *) calloc(bdccont_info->num_hv_channels, sizeof(float));
   bdccont_info->temp     = (float *) calloc(bdccont_info->num_temp_channels, sizeof(float));
   
   /*---- create bias voltage demanded variables ----*/
   db_create_key(hDB, bdccont_info->hKeyRoot, "Variables/Demand Bias", TID_FLOAT);
   db_find_key(hDB, bdccont_info->hKeyRoot, "Variables/Demand Bias", &bdccont_info->hKeyDemand);
   size = sizeof(bdccont_info->demand_bias);
   db_get_value(hDB, bdccont_info->hKeyRoot, "Variables/Demand Bias", 
                &(bdccont_info->demand_bias), &size, TID_FLOAT, FALSE);
   
   /*--- open hotlink to HV demand values ----*/
   db_create_key(hDB, bdccont_info->hKeyRoot, "Variables/Demand Bias", TID_FLOAT);
   db_open_record(hDB, bdccont_info->hKeyDemand, &bdccont_info->demand_bias,
                  sizeof(float), MODE_READ, bdccont_demand, pequipment);

   /*---- create bias voltage measured variables ----*/
   db_create_key(hDB, bdccont_info->hKeyRoot, "Variables/Measured Bias", TID_FLOAT);
   db_find_key(hDB, bdccont_info->hKeyRoot, "Variables/Measured Bias", &bdccont_info->hKeyMeasure);

   /*---- create current variables ----*/
   db_create_key(hDB, bdccont_info->hKeyRoot, "Variables/Current", TID_FLOAT);
   db_find_key(hDB, bdccont_info->hKeyRoot, "Variables/Current", &bdccont_info->hKeyCurrent);
   
#ifdef IMPLEMENT_TEMP
   /*---- create Temp measured variables ----*/
   db_merge_data(hDB, bdccont_info->hKeyRoot, "Variables/Temp",
                 bdccont_info->temp, sizeof(float) * bdccont_info->num_temp_channels,
                 bdccont_info->num_temp_channels, TID_FLOAT);
   db_find_key(hDB, bdccont_info->hKeyRoot, "Variables/Temp", &bdccont_info->hKeyTemp);
#endif

   /*---- create HV measured variables ----*/
   db_merge_data(hDB, bdccont_info->hKeyRoot, "Variables/HV",
                 bdccont_info->hv, sizeof(float) * bdccont_info->num_hv_channels,
                 bdccont_info->num_hv_channels, TID_FLOAT);
   db_find_key(hDB, bdccont_info->hKeyRoot, "Variables/HV", &bdccont_info->hKeyHV);

   /*---- Initialize device drivers ----*/
   /* call init method */
   status = device_driver(&(pequipment->driver[0]), CMD_INIT, bdccont_info->hKeyRoot);
   if (status != FE_SUCCESS) {
     cm_msg(MERROR, "cd_bdccont", "Couldn't initialize the device driver.");
     free_mem(bdccont_info);
     return status;
   }
   ss_sleep(5000);

   /* set bias at first */
   printf("Initially set bias voltage to %.2f V\n", bdccont_info->demand_bias);
   //device_driver(&(pequipment->driver[0]), CMD_SET, 0, bdccont_info->demand_bias);
   pequipment->driver[0].dd(CMD_SET, pequipment->driver[0].dd_info, 0, bdccont_info->demand_bias);
   
   printf("Waiting 10 seconds for the bias voltage set up...\n");
   ss_sleep(10000);
   
   /* start thread in case of multi thread*/
   status = device_driver(&(pequipment->driver[0]), CMD_START, bdccont_info->hKeyRoot);
   if (status != FE_SUCCESS) {
     cm_msg(MERROR, "cd_bdccont", "Couldn't start the device driver.");
     free_mem(bdccont_info);
     return status;
   }

   /* initially read all channels */
   printf("Now initial reading out ....\n");
   bdccont_info->forceRO = TRUE;
   for(i=0; i < bdccont_info->num_hv_channels; i++)
     bdccont_read_HV(pequipment, i);
#ifdef IMPLEMENT_TEMP
   for(i=0; i < bdccont_info->num_temp_channels; i++)
     bdccont_read_Temp(pequipment, i);
#endif 
   bdccont_info->forceRO = FALSE;
   printf("Finished.\n");
   
   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT bdccont_exit(EQUIPMENT * pequipment)
{
   INT i;

   free_mem((BDCCONT_INFO *) pequipment->cd_info);

   /* call exit method of device drivers */
   device_driver(&(pequipment->driver[0]), CMD_STOP);
   device_driver(&(pequipment->driver[0]), CMD_EXIT);

   return FE_SUCCESS;
}

/*------------------------------------------------------------------*/

INT bdccont_idle(EQUIPMENT * pequipment)
{
   INT act, status;
   BDCCONT_INFO *bdccont_info;

   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;

   bdccont_info->forceRO = TRUE;

   ss_sleep(200);
   status = bdccont_read_Bias(pequipment, 0);
   
   ss_sleep(200);
   status = bdccont_read_Current(pequipment, 0);
   
   ss_sleep(200);
   if(bdccont_info->num_hv_channels > 0){
     /* select next HV measurement channel */
     
#ifdef READ_ATONCE
     bdccont_info->forceRO = TRUE;
     for(act=0; act<bdccont_info->num_hv_channels; act++){
#else
       act = (bdccont_info->last_hv_channel + 1) % bdccont_info->num_hv_channels;
#endif
       /* measure HV channel */
       status = bdccont_read_HV(pequipment, act);
       bdccont_info->last_hv_channel = act;
     }
#ifdef READ_ATONCE
     bdccont_info->forceRO = FALSE;
#endif
   }
   
#ifdef IMPLEMENT_TEMP
   if(bdccont_info->num_temp_channels > 0){
     
#ifdef READ_ATONCE
     bdccont_info->forceRO = TRUE;
     for(act=0; act<bdccont_info->num_temp_channels; act++){
#else
       /* select next Temp measurement channel */
       act = (bdccont_info->last_temp_channel + 1) % bdccont_info->num_temp_channels;
#endif
       /* measure Temp channel */
       status = bdccont_read_Temp(pequipment, act);
       bdccont_info->last_temp_channel = act;
#ifdef READ_ATONCE
     }
     bdccont_info->forceRO = FALSE;
#endif   
   }
#endif
   
   return status;
}

/*------------------------------------------------------------------*/

INT cd_bdccont_read(char *pevent, int offset)
{
   float *pdata;
   BDCCONT_INFO *bdccont_info;
   EQUIPMENT *pequipment;

   pequipment = *((EQUIPMENT **) pevent);
   bdccont_info = (BDCCONT_INFO *) pequipment->cd_info;

   if (bdccont_info->format == FORMAT_FIXED) {
     INT size = 0;
     
     /* Demand Bias */
     memcpy(pevent, &bdccont_info->demand_bias, sizeof(bdccont_info->demand_bias));
     pevent += sizeof(bdccont_info->demand_bias);
     size   += sizeof(bdccont_info->demand_bias);
     
     /* Measured Bias */
     memcpy(pevent, &bdccont_info->bias, sizeof(bdccont_info->bias));
     pevent += sizeof(bdccont_info->bias);
     size   += sizeof(bdccont_info->bias);
     
     /* Current */
     memcpy(pevent, &bdccont_info->current, sizeof(bdccont_info->current));
     pevent += sizeof(bdccont_info->current);
     size   += sizeof(bdccont_info->current);

     /* HV */
     memcpy(pevent, bdccont_info->hv, sizeof(float) * bdccont_info->num_hv_channels);
     pevent += sizeof(float) * bdccont_info->num_hv_channels;
     size   += sizeof(float) * bdccont_info->num_hv_channels;
      
#ifdef IMPLEMENT_TEMP
     /* Temp */
     memcpy(pevent, bdccont_info->temp, sizeof(float) * bdccont_info->num_temp_channels);
     pevent += sizeof(float) * bdccont_info->num_temp_channels;
     size   += sizeof(float) * bdccont_info->num_temp_channels;
#endif
     return size;

   } else if (bdccont_info->format == FORMAT_MIDAS) {
      bk_init(pevent);
      
      /* create CON bank */
      bk_create(pevent, bk_name_CON, TID_FLOAT, &pdata);
      memcpy(pdata, &bdccont_info->demand_bias, sizeof(bdccont_info->demand_bias));
      pdata += 1;
      memcpy(pdata, &bdccont_info->bias, sizeof(bdccont_info->bias));
      pdata += 1;
      memcpy(pdata, &bdccont_info->current, sizeof(bdccont_info->current));
      pdata += 1;
      bk_close(pevent, pdata);

      /* create HV bank */
      bk_create(pevent, bk_name_HV, TID_FLOAT, &pdata);
      memcpy(pdata, bdccont_info->hv, sizeof(float) * bdccont_info->num_hv_channels);
      pdata += bdccont_info->num_hv_channels;
      bk_close(pevent, pdata);

#ifdef IMPLEMENT_TEMP
      /* create TEMP1 bank */
      bk_create(pevent, bk_name_TEMP, TID_FLOAT, &pdata);
      memcpy(pdata, bdccont_info->temp, sizeof(float) * bdccont_info->num_temp_channels);
      pdata += bdccont_info->num_temp_channels;
      bk_close(pevent, pdata);
#endif
      return bk_size(pevent);
   } else if (bdccont_info->format == FORMAT_YBOS) {
     printf("Not implemented\n");
     return 0;
   } 
   
   return 0;
}

/*------------------------------------------------------------------*/

INT cd_bdccont(INT cmd, EQUIPMENT * pequipment)
{
   INT status;

   switch (cmd) {
   case CMD_INIT:
      status = bdccont_init(pequipment);
      break;

   case CMD_START:
      break;

   case CMD_STOP:
      break;

   case CMD_EXIT:
      status = bdccont_exit(pequipment);
      break;

   case CMD_IDLE:
      status = bdccont_idle(pequipment);
      break;

   default:
      cm_msg(MERROR, "cd_bdccont", "Received unknown command %d", cmd);
      status = FE_ERR_DRIVER;
      break;
   }

   return status;
}
