/********************************************************************\

  Name:         cd_bdccont.h
  Created by:   Kou Oishi

  Contents:     BDC Control

  $Id:$

\********************************************************************/

/* class driver routines */
INT cd_bdccont(INT cmd, PEQUIPMENT pequipment);
INT cd_bdccont_read(char *pevent, int);
