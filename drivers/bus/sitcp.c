/******************************************************************** \

  Name:         sitcp.c
  Created by:   Stefan Ritt

  Contents:     TCP/IP socket communication routines

  $Id$

\********************************************************************/

#include "midas.h"
#include "msystem.h"
#include "sitcp.h"

#define SITCP_RBCP_VER       0xFF
#define SITCP_RBCP_CMD_WRITE 0x80
#define SITCP_RBCP_CMD_READ  0xC0

static int debug_last = 0, debug_first = TRUE;

typedef struct {
  char ip[256];
  char mac[256];
  int  tcp_port;
  int  udp_port;
  int  debug;
} SITCP_SETTINGS;

#define SITCP_SETTINGS_STR "\
IP = STRING : [256] 192.168.10.XX\n\
MAC Address = STRING : [256] 7C:F0:98:01:09:XX\n\
TCP Port = INT : 24\n\
UDP Port = INT : 4660\n\
Debug = INT : 0\n\
"

typedef struct {
  SITCP_SETTINGS settings;
  int tcpfd; /* device handle for TCP socket device */
  int udpfd; /* device handle for UDP socket device */
  struct sockaddr_in udp_addr; /* Addresss info of the UDP */
  BYTE ip[4], mac[6]; // IP/MAC address in numerical format
} SITCP_INFO;

typedef struct {
  unsigned char type;
  unsigned char command;
  unsigned char id;
  unsigned char length;
  unsigned int  address;
} SITCP_RBCP_HEADER;


/*----------------------------------------------------------------------------*/

INT get_ip_address(char* str, BYTE* address)
{
  int i;
  char *next, *orig;
  orig = str;
  printf("Input %s\n", orig);

  for (i = 0; i < 4; i++){
    address[i] = strtol(str, &next, 10);
    str = next;
    if ( !str || '\0' == *str ) break;
    if ( '.' == *str ) ++str;
    else return -1;
  }
  if(i != 3){
    cm_msg(MERROR, "get_ip_address",
           "Cannot get IP from %s", str);
    return -1;
  }

  return SUCCESS;
}

INT get_mac_address(char* str, BYTE* address)
{
  int i;
  char *next, *orig;
  orig=str;
  printf("Input %s\n", orig);

  for (i = 0; i < 6; i++){
    address[i] = strtol(str, &next, 16);
    str=next;
    if ( !str || '\0' == *str ) break;
    if ( '-' == *str || ':' == *str ) ++str;
    else return -1;
  }
  if(i != 5){
    return -1;
    cm_msg(MERROR, "get_ip_address",
           "Cannot get MAC Address from %s", orig);
  }

  return SUCCESS;
}

void sitcp_debug(SITCP_INFO * info, char *dbg_str)
{
  FILE *f;
  int delta;

  if (debug_last == 0)
    delta = 0;
  else
    delta = ss_millitime() - debug_last;
  debug_last = ss_millitime();

  f = fopen("sitcp.log", "a");

  if (debug_first)
    fprintf(f, "\n==== new session =============\n\n");
  debug_first = FALSE;
  fprintf(f, "{%d} %s\n", delta, dbg_str);

  if (info->settings.debug > 1)
    printf("{%d} %s\n", delta, dbg_str);

  fclose(f);
}

/*------------------------------------------------------------------*/

int sitcp_tcp_connect(char *ip, int tcp_port)
{
  struct sockaddr_in bind_addr;
  struct hostent *phe;
  int status, tcpfd;

#ifdef OS_WINNT
  {
    WSADATA WSAData;

    /* Start windows sockets */
    if (WSAStartup(MAKEWORD(1, 1), &WSAData) != 0)
      return RPC_NET_ERROR;
  }
#endif

  /* create a new socket for connecting to remote server */
  tcpfd = socket(AF_INET, SOCK_STREAM, 0);
  if (tcpfd == -1) {
    perror("sitcp_tcp_connect: socket");
    return tcpfd;
  }

  /* connect to remote node */
  memset(&bind_addr, 0, sizeof(bind_addr));
  bind_addr.sin_family = AF_INET;
  bind_addr.sin_addr.s_addr = 0;
  bind_addr.sin_port = htons(tcp_port);
  if(inet_pton(AF_INET, ip, &bind_addr.sin_addr) <= 0){
    cm_msg(MERROR, "sitcp_tcp_connect", "inet_pton error");
    return -1;
  }

#ifdef OS_VXWORKS
  {
    INT host_addr;

    host_addr = hostGetByName(ip);
    memcpy((char *) &(bind_addr.sin_addr), &host_addr, 4);
  }
#else
  phe = gethostbyname(ip);
  if (phe == NULL) {
    printf("\nsitcp_tcp_connect: unknown host name %s\n", ip);
    return -1;
  }
  memcpy((char *) &(bind_addr.sin_addr), phe->h_addr, phe->h_length);
#endif

#ifdef OS_UNIX
  do {
    status = connect(tcpfd, (void *) &bind_addr, sizeof(bind_addr));

    /* don't return if an alarm signal was cought */
  } while (status == -1 && errno == EINTR);
#else
  status = connect(tcpfd, (void *) &bind_addr, sizeof(bind_addr));
#endif

  if (status != 0) {
    perror("sitcp_tcp_connect:connect");
    return -1;
  }

  return tcpfd;
}

/*----------------------------------------------------------------------------*/

int sitcp_udp_connect(char *ip, int udp_port, struct sockaddr_in* udp_addr)
{
  struct sockaddr_in bind_addr;
  struct hostent *phe;
  int status, udpfd;

  /* create a new socket for connecting to remote server */
  udpfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (udpfd == -1) {
    perror("siudp_udp_connect: socket");
    return udpfd;
  }

  /* let OS choose any udp_port number */
  memset(&bind_addr, 0, sizeof(bind_addr));
  bind_addr.sin_family = AF_INET;
  bind_addr.sin_addr.s_addr = 0;
  bind_addr.sin_port = 0;

  status = bind(udpfd, (void *) &bind_addr, sizeof(bind_addr));
  if (status < 0) {
    perror("siudp_udp_connect:bind");
    return -1;
  }

  /* connect to remote node */
  memset(&bind_addr, 0, sizeof(bind_addr));
  bind_addr.sin_family = AF_INET;
  bind_addr.sin_addr.s_addr = 0;
  bind_addr.sin_port = htons(udp_port);

  phe = gethostbyname(ip);
  if (phe == NULL) {
    printf("\nsiudp_udp_connect: unknown host name %s\n", ip);
    return -1;
  }
  memcpy((char *) &(bind_addr.sin_addr), phe->h_addr, phe->h_length);

  *(udp_addr) = bind_addr;

  return udpfd;
}

/*----------------------------------------------------------------------------*/

int sitcp_exit(SITCP_INFO * info)
{
  if(info){
    sitcp_close_tcp(info);
    sitcp_close_udp(info);
    free(info);
    info = NULL;
  }
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_close_udp(SITCP_INFO * info)
{
  if(info->udpfd >= 0){
    close(info->udpfd);
    info->udpfd = -1;
  }
}

int sitcp_close_tcp(SITCP_INFO * info)
{
  if(info){
    if(info->tcpfd >= 0){
      closesocket(info->tcpfd);
      info->tcpfd = -1;
    }
  }
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_open_tcp(SITCP_INFO * info)
{
  sitcp_close_tcp(info);

  /* open tcp_port */
  info->tcpfd = sitcp_tcp_connect(info->settings.ip, info->settings.tcp_port);
  if (info->tcpfd < 0){
    cm_msg(MERROR, "sitcp", "Cannot open a TCP socket for %s at port, %d.",
           info->settings.ip, info->settings.tcp_port);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_open_udp(SITCP_INFO * info)
{
  sitcp_close_udp(info);

  /* open udp_port */
  info->udpfd = sitcp_udp_connect(info->settings.ip,
                                  info->settings.udp_port, &info->udp_addr);
  if (info->udpfd < 0){
    cm_msg(MERROR, "sitcp", "Cannot connect to %s from UDP port, %d.",
           info->settings.ip, info->settings.udp_port);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_write(SITCP_INFO * info, char *data, int size)
{
  int i;

  if (info->settings.debug) {
    char dbg_str[256];

    sprintf(dbg_str, "write: ");
    for (i = 0; (int) i < size; i++)
      sprintf(dbg_str + strlen(dbg_str), "%X ", data[i]);

    sitcp_debug(info, dbg_str);
  }

  i = send(info->tcpfd, data, size, 0);

  return i;
}

/*----------------------------------------------------------------------------*/

int sitcp_read(SITCP_INFO * info, char *data, int size)
{
  int i, status, n;

  n = 0;
  memset(data, 0, size);

  do {
    i = recv(info->tcpfd, data+n, size-n, 0);
    if (i <  0) return -1;
    if (i == 0) break;
    n+=i;
    if (n >= size) break;
  } while (1);

  return n;
}


int sitcp_poll(SITCP_INFO * info, char *data, int size, int millisec)
{
  fd_set readfds;
  struct timeval timeout;
  int i, status, n;

  n = 0;
  memset(data, 0, size);

  do {
    if (millisec > 0) {
      FD_ZERO(&readfds);
      FD_SET(info->tcpfd, &readfds);

      timeout.tv_sec = millisec / 1000;
      timeout.tv_usec = (millisec % 1000) * 1000;

      do {
        status = select(FD_SETSIZE, (void *) &readfds, NULL, NULL, (void *) &timeout);

        /* if an alarm signal was cought, restart select with reduced timeout */
        if (status == -1 && timeout.tv_sec >= WATCHDOG_INTERVAL / 1000)
          timeout.tv_sec -= WATCHDOG_INTERVAL / 1000;
      } while (status == -1);        /* dont return if an alarm signal was cought */

      if (!FD_ISSET(info->tcpfd, &readfds))
        break;
    }

    i = recv(info->tcpfd, data + n, 1, 0);

    if (i <= 0)
      break;

    n++;

    if (n >= size)
      break;

  } while (1);                 /* while (buffer[n-1] && buffer[n-1] != 10); */

  if (info->settings.debug) {
    char dbg_str[256];
    sprintf(dbg_str, "read: ");
    if (n == 0) sprintf(dbg_str + strlen(dbg_str), "<TIMEOUT>");
    else
      for (i = 0; i < n; i++)
        sprintf(dbg_str + strlen(dbg_str), "%X ", data[i]);
    sitcp_debug(info, dbg_str);
  }

  return n;
}

/*----------------------------------------------------------------------------*/

int sitcp_read_block(SITCP_INFO * info, char *data, int size, int millisec)
{
  fd_set readfds;
  struct timeval timeout;
  int i, status, n;
  int j=0;
  static FILE* dbg_fp = 0;
  /*
  if(!dbg_fp){
    dbg_fp = fopen("sitcp.dat", "w");
  }
  */

  n = 0;
  memset(data, 0, size);

  do {
    if (millisec > 0) {
      FD_ZERO(&readfds);
      FD_SET(info->tcpfd, &readfds);

      timeout.tv_sec = millisec / 1000;
      timeout.tv_usec = (millisec % 1000) * 1000;

      do {
        status = select(FD_SETSIZE, (void *) &readfds, NULL, NULL, (void *) &timeout);

        /* if an alarm signal was cought, restart select with reduced timeout */
        if (status == -1 && timeout.tv_sec >= WATCHDOG_INTERVAL / 1000)
          timeout.tv_sec -= WATCHDOG_INTERVAL / 1000;

      } while (status == -1);        /* dont return if an alarm signal was cought */

      if (!FD_ISSET(info->tcpfd, &readfds))
        break;
    }

    i = recv(info->tcpfd, data + n, size-n, 0);

    /*
    fprintf(dbg_fp, "%10d : ", i);
    for(j=0; j<i; j++){
      fprintf(dbg_fp, "0x%02x ", data[n+j] & 0xFF);
      if((j+1)%16 == 0) fprintf(dbg_fp, "\n%10s   ", "");
    }
    fprintf(dbg_fp, "\n");
    */

    if (i <= 0)
      break;

    n += i;

    if (n >= size)
      break;

  } while (1);                 /* while (buffer[n-1] && buffer[n-1] != 10); */

  if (info->settings.debug) {
    char dbg_str[256];

    sprintf(dbg_str, "read: ");

    if (n == 0)
      sprintf(dbg_str + strlen(dbg_str), "<TIMEOUT>");
    else
      for (i = 0; i < n; i++)
        sprintf(dbg_str + strlen(dbg_str), "%X ", data[i]);

    sitcp_debug(info, dbg_str);
  }

  return n;
}

/*----------------------------------------------------------------------------*/

int sitcp_puts(SITCP_INFO * info, char *str)
{
  int i;

  if (info->settings.debug) {
    char dbg_str[256];

    sprintf(dbg_str, "puts: %s", str);
    sitcp_debug(info, dbg_str);
  }

  i = send(info->tcpfd, str, strlen(str), 0);
  if (i < 0)
    perror("sitcp_puts");

  return i;
}

/*----------------------------------------------------------------------------*/

int sitcp_gets(SITCP_INFO * info, char *str, int size, char *pattern, int millisec)
{
  fd_set readfds;
  struct timeval timeout;
  int i, status, n;

  n = 0;
  memset(str, 0, size);

  do {
    if (millisec > 0) {
      FD_ZERO(&readfds);
      FD_SET(info->tcpfd, &readfds);

      timeout.tv_sec = millisec / 1000;
      timeout.tv_usec = (millisec % 1000) * 1000;

      do {
        status = select(FD_SETSIZE, (void *) &readfds, NULL, NULL, (void *) &timeout);

        /* if an alarm signal was cought, restart select with reduced timeout */
        if (status == -1 && timeout.tv_sec >= WATCHDOG_INTERVAL / 1000)
          timeout.tv_sec -= WATCHDOG_INTERVAL / 1000;

      } while (status == -1);        /* dont return if an alarm signal was cought */

      if (!FD_ISSET(info->tcpfd, &readfds))
        break;
    }

    i = recv(info->tcpfd, str + n, 1, 0);

    if (i <= 0)
      break;

    n += i;

    if (pattern && pattern[0])
      if (strstr(str, pattern) != NULL)
        break;

    if (n >= size)
      break;

  } while (1);                 /* while (buffer[n-1] && buffer[n-1] != 10); */

  if (info->settings.debug) {
    char dbg_str[256];

    sprintf(dbg_str, "gets [%s]: ", pattern);

    if (str[0] == 0)
      sprintf(dbg_str + strlen(dbg_str), "<TIMEOUT>");
    else
      sprintf(dbg_str + strlen(dbg_str), "%s", str);

    sitcp_debug(info, dbg_str);
  }

  return n;
}

/*----------------------------------------------------------------------------
  [sitcp_init]
  Initialize the sitcp bus driver (initialize the ODB for SITCP).

  hKey(i)     : ODB root
  pinfo(io)   : Pointer to pointer to the SITCP_INFO structure
  ---------------------------------------------------------------------------- */
int sitcp_init(HNDLE hkey, void **pinfo)
{
  HNDLE hDB, hkeybd;
  INT size, status;
  SITCP_INFO *info;

  /* allocate info structure */
  info = calloc(1, sizeof(SITCP_INFO));
  info->tcpfd = info->udpfd = -1;
  *pinfo = info;

  cm_get_experiment_database(&hDB, NULL);

  /* create SITCP settings record */
  status = db_create_record(hDB, hkey, "SiTCP", SITCP_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;

  db_find_key(hDB, hkey, "SiTCP", &hkeybd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeybd, &info->settings, &size, 0);

  /* check IP/MAC address */
  if((SUCCESS != get_ip_address(info->settings.ip, info->ip)) ||
     (SUCCESS != get_mac_address(info->settings.mac, info->mac)) ){
    cm_msg(MERROR, "sitcp_init", "Invalid IP or MAC Address.");
    return FE_ERR_ODB;
  }

  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_rbcp_rw(SITCP_INFO* info, SITCP_RBCP_HEADER* header, char* data, int millisec)
{
  fd_set   set_sel;
  int      cmd_pckt_len;
  unsigned char send_buff[2048];
  int      recv_bytes = 0;
  int      send_data_len;
  unsigned char recv_buff[2048];
  int      retries = 0;
  int      header_id = header->id;
  struct timeval timeout;

  // open udp if file discriptor is unset
  if(info->udpfd < 0 && SUCCESS != sitcp_open_udp(info)) return FE_ERR_HW;

  send_data_len = header->length;

  // Copy header data ---------------------------------------------------------
  memcpy(send_buff, header, sizeof(SITCP_RBCP_HEADER));

  // CASE: Writing
  if(header->command == SITCP_RBCP_CMD_WRITE){
    memcpy(send_buff + sizeof(SITCP_RBCP_HEADER), data, send_data_len);
    cmd_pckt_len = send_data_len + sizeof(SITCP_RBCP_HEADER);
  }
  // CASE: Reading
  else cmd_pckt_len = sizeof(SITCP_RBCP_HEADER);

  // Send a packet ------------------------------------------------------------
  sendto(info->udpfd, send_buff, 1+cmd_pckt_len, 0,
         (struct sockaddr *)&(info->udp_addr), sizeof(info->udp_addr));

  // Receive packets -----------------------------------------------------------
  // Set tiemout
  timeout.tv_sec  = /*3;*/ millisec/1000;
  timeout.tv_usec = /*0;*/ (millisec*1000)%1000000;

  while(retries < 3){
    FD_ZERO(&set_sel);
    FD_SET(info->udpfd, &set_sel);

    if(select(info->udpfd+1, &set_sel, NULL, NULL, &timeout) == 0){
      // Time out
      printf("\n***** Timeout RBCP! *****\n");
      header_id = header->id = header->id + 1;
      memcpy(send_buff, header, sizeof(SITCP_RBCP_HEADER));
      sendto(info->udpfd, send_buff, cmd_pckt_len, 0,
             (struct sockaddr *)&(info->udp_addr), sizeof(info->udp_addr));

      retries++;
    }
    else {
      // Receive ACK packet
      if(FD_ISSET(info->udpfd, &set_sel)){
        recv_bytes = recvfrom(info->udpfd, recv_buff, 2048, 0, NULL, NULL);
        recv_buff[recv_bytes] = 0; // null character (end of string)

        if(recv_bytes < sizeof(SITCP_RBCP_HEADER)){
          printf("ERROR: ACK packet is too short.\n");
          return -1;
        }
        if((0x0F & recv_buff[1]) != 0x8){
          printf("ERROR: Detected bus error.\n");
          return -1;
        }
        if(header_id != recv_buff[2]){
          printf("ERROR: returned ACK id is different from the sent packet\n");
          return -1;
        }

        // In reading command, copy to data
        if(header->command == SITCP_RBCP_CMD_READ)
          //printf("recv_bytes = %d (sizeof(header) = %d) recv+buff[8] = %d\n", recv_bytes, sizeof(SITCP_RBCP_HEADER), recv_buff[8]);
          memcpy(data, recv_buff + sizeof(SITCP_RBCP_HEADER),
                 recv_bytes - sizeof(SITCP_RBCP_HEADER));

        break;
      }
      else{
        printf("ISSET doesn't return true.\n");
      }
    }
  } // End of while
  if(retries == 3){
    cm_msg(MERROR, "sitcp_rbcp_rw",
           "three times time out when sending command 0x%02x to 0x%x.",
           header->command, header->address);
  }

  // close udp
  //sitcp_close_udp(info);

  return recv_bytes;
}

/*----------------------------------------------------------------------------*/

int sitcp_rbcp_read(SITCP_INFO* info, unsigned int address, char* data, int size, int millisec)
{
  SITCP_RBCP_HEADER header;

  header.type    = SITCP_RBCP_VER;
  header.command = SITCP_RBCP_CMD_READ;
  header.id      = address;
  header.length  = size;
  header.address = htonl(address);

  return sitcp_rbcp_rw(info, &header, data, millisec);
}

/*----------------------------------------------------------------------------*/

int sitcp_rbcp_write(SITCP_INFO* info, unsigned int address, char* data, int size, BOOL validate)
{
  int i;
  SITCP_RBCP_HEADER header;
  char* valid_data;

  header.type    = SITCP_RBCP_VER;
  header.command = SITCP_RBCP_CMD_WRITE;
  header.id      = address;
  header.length  = size;
  header.address = htonl(address);

  if(0 > sitcp_rbcp_rw(info, &header, data, 2000)){
    return -1;
  }

  if(validate){
    // Validate if the data was changed correctly
    valid_data = (char*)malloc(size);
    if(valid_data == NULL){
      printf("ERROR! Cannot malloc to copy write_data\n");
      return -1;
    }
    if(0 > sitcp_rbcp_read(info, address, valid_data, size, 2000)){
      printf("ERROR: Cannot read data for validation\n");
      return -1;
    }
    else{
      for(i=0; i<size; i++){
        if(valid_data[i] != data[i]) return -1;
      }
    }
    free(valid_data);
  }

  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_send_rbcp_block(SITCP_INFO* info, INT cmd_size,
                          SITCP_RBCP_COMMAND* cmd, INT interval)
{
  int n;
  if(cmd_size < 1) return SUCCESS;

  for(n = 0; n < cmd_size; n++){
    if(SUCCESS != sitcp_rbcp_write(info, cmd[n].address,
                                   &cmd[n].data, 1, !(cmd[n].inhibit_validation))){
      cm_msg(MERROR, "sitcp_send_rbcp_block", "Error in "
             "sending a RBCP command of [Address:0x%08x Cmd:0x%02x]",
             cmd[n].address, cmd[n].data);
      return FE_ERR_HW;
    }
    ss_sleep(interval);
  }
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int sitcp_enable_nagle(SITCP_INFO* info, BOOL enable)
{
  char read_data, write_data;

  // Read the current status
  if(0 > sitcp_rbcp_read(info, 0xffffff10, &read_data, sizeof(read_data), 2000)){
    printf("ERROR!! Failed to readout the nagle algorithm status\n");
    return -1;
  }

  // Change only the nagle algorithm status bit.
  write_data = (read_data & (~0x1)) | (enable? 1 : 0);
  if( write_data == read_data ) return SUCCESS;

  // Write
  if(SUCCESS != sitcp_rbcp_write(info, 0xffffff10, &write_data, sizeof(write_data), TRUE)){
    printf("ERROR!! Failed to write the nagle algorithm status\n");
    return -1;
  }

  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT sitcp_udp(INT cmd, ...)
{
  va_list argptr;
  HNDLE hkey;
  unsigned int address;
  INT status, size, timeout;
  void *info;
  char *str, *pattern;
  BOOL enable, validate;
  INT  *fd;
  INT  level, optname, optlen, interval;
  char* optval;
  SITCP_RBCP_COMMAND* cmds;

  va_start(argptr, cmd);
  status = FE_SUCCESS;

  switch (cmd) {
  case CMD_INIT:
    hkey = va_arg(argptr, HNDLE);
    info = va_arg(argptr, void *);
    status = sitcp_init(hkey, info);
    break;

  case CMD_EXIT:
    info = va_arg(argptr, void *);
    status = sitcp_exit(info);
    break;

  case CMD_OPEN:
    info = va_arg(argptr, void *);
    status = sitcp_open_udp(info);
    break;

  case CMD_CLOSE:
    info = va_arg(argptr, void *);
    status = sitcp_close_udp(info);
    break;

  case CMD_NAME:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    strcpy(str, "sitcp_udp");
    break;

  case CMD_WRITE:
    info    = va_arg(argptr, void*);
    address = va_arg(argptr, unsigned int);
    str     = va_arg(argptr, char*);
    size    = va_arg(argptr, INT);
    validate = va_arg(argptr, BOOL);
    status  = sitcp_rbcp_write(info, address, str, size, validate);
    break;

  case CMD_WRITE_BLOCK:
    info     = va_arg(argptr, void*);
    size     = va_arg(argptr, INT);
    cmds     = va_arg(argptr, SITCP_RBCP_COMMAND*);
    interval = va_arg(argptr, INT);
    status   = sitcp_send_rbcp_block(info, size, cmds, interval);
    break;

  case CMD_READ:
    info    = va_arg(argptr, void*);
    address = va_arg(argptr, unsigned int);
    str     = va_arg(argptr, char*);
    size    = va_arg(argptr, INT);
    timeout = va_arg(argptr, INT);
    status  = sitcp_rbcp_read(info, address, str, size, timeout);
    break;

  case CMD_PUTS:
  case CMD_GETS:
    break;

  case CMD_DEBUG:
    info = va_arg(argptr, void *);
    status = va_arg(argptr, INT);
    ((SITCP_INFO *) info)->settings.debug = status;
    break;

  case CMD_SITCP_ENABLE_NAGLE:
    info    = va_arg(argptr, void*);
    enable  = va_arg(argptr, BOOL);
    status  = sitcp_enable_nagle(info, enable);
    break;

  case CMD_SITCP_GET_UDPFD:
    info  = va_arg(argptr, void*);
    fd    = va_arg(argptr, INT*);
    *fd   = ((SITCP_INFO*)info)->udpfd;
    break;

  case CMD_SITCP_SET_SOCKOPT:
    info    = va_arg(argptr, void*);
    level   = va_arg(argptr, INT);
    optname = va_arg(argptr, INT);
    optval  = va_arg(argptr, char*);
    optlen  = va_arg(argptr, INT);
    status = setsockopt (((SITCP_INFO*)info)->udpfd,
                         level, optname, optval, optlen);
    break;
  }

  va_end(argptr);

  return status;
}

INT sitcp(INT cmd, ...)
{
  INT i;
  va_list argptr;
  HNDLE hkey;
  unsigned int address;
  INT status, size, timeout;
  void *info;
  char *str, *pattern;
  INT* fd;
  BOOL enable, validate;
  INT  level, optname, optlen, interval;
  char* optval;
  BYTE* ipmac;
  SITCP_RBCP_COMMAND* cmds;

  va_start(argptr, cmd);
  status = FE_SUCCESS;

  switch (cmd) {
  case CMD_INIT:
    hkey    = va_arg(argptr, HNDLE);
    info    = va_arg(argptr, void *);
    status = sitcp_init(hkey, info);
    break;

  case CMD_EXIT:
    info = va_arg(argptr, void *);
    status = sitcp_exit(info);
    break;

  case CMD_OPEN:
    info = va_arg(argptr, void *);
    status = sitcp_open_tcp(info);
    break;

  case CMD_CLOSE:
    info = va_arg(argptr, void *);
    status = sitcp_close_tcp(info);
    break;

  case CMD_NAME:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    strcpy(str, "sitcp");
    break;

  case CMD_WRITE:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    size = va_arg(argptr, int);
    status = sitcp_write(info, str, size);
    break;

  case CMD_READ:
    info = va_arg(argptr, void *);
    str  = va_arg(argptr, char *);
    size = va_arg(argptr, INT);
    status = sitcp_read(info, str, size);
    break;

  case CMD_READ_BLOCK:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    size = va_arg(argptr, INT);
    timeout = va_arg(argptr, INT);
    status = sitcp_read_block(info, str, size, timeout);
    break;

  case CMD_POLL:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    size = va_arg(argptr, INT);
    timeout = va_arg(argptr, INT);
    status = sitcp_poll(info, str, size, timeout);
    break;

  case CMD_PUTS:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    status = sitcp_puts(info, str);
    break;

  case CMD_GETS:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    size = va_arg(argptr, INT);
    pattern = va_arg(argptr, char *);
    timeout = va_arg(argptr, INT);
    status = sitcp_gets(info, str, size, pattern, timeout);
    break;

  case CMD_DEBUG:
    info = va_arg(argptr, void *);
    status = va_arg(argptr, INT);
    ((SITCP_INFO *) info)->settings.debug = status;
    break;

  case CMD_SITCP_RBCP_READ:
    info    = va_arg(argptr, void*);
    address = va_arg(argptr, unsigned int);
    str     = va_arg(argptr, char*);
    size    = va_arg(argptr, INT);
    timeout = va_arg(argptr, INT);
    status  = sitcp_udp(CMD_READ, info, address, str, size, timeout);
    break;

  case CMD_SITCP_RBCP_WRITE:
    info    = va_arg(argptr, void*);
    address = va_arg(argptr, unsigned int);
    str     = va_arg(argptr, char*);
    size    = va_arg(argptr, INT);
    validate = va_arg(argptr, BOOL);
    status  = sitcp_udp(CMD_WRITE, info, address, str, size, validate);
    break;

  case CMD_SITCP_RBCP_WRITE_BLOCK:
    info     = va_arg(argptr, void*);
    size     = va_arg(argptr, INT);
    cmds     = va_arg(argptr, SITCP_RBCP_COMMAND*);
    interval = va_arg(argptr, INT);
    status   = sitcp_udp(CMD_WRITE_BLOCK, info, size, cmds, interval);
    break;

  case CMD_SITCP_ENABLE_NAGLE:
    info    = va_arg(argptr, void*);
    enable  = va_arg(argptr, BOOL);
    status  = sitcp_enable_nagle(info, enable);
    break;

  case CMD_SITCP_GET_TCPFD:
    info  = va_arg(argptr, void*);
    fd    = va_arg(argptr, INT*);
    *fd   = ((SITCP_INFO*)info)->tcpfd;
    break;

  case CMD_SITCP_GET_UDPFD:
    info    = va_arg(argptr, void*);
    fd    = va_arg(argptr, INT*);
    *fd   = ((SITCP_INFO*)info)->udpfd;
    break;

  case CMD_SITCP_SET_SOCKOPT:
    info    = va_arg(argptr, void*);
    level   = va_arg(argptr, INT);
    optname = va_arg(argptr, INT);
    optval  = va_arg(argptr, char*);
    optlen  = va_arg(argptr, INT);
    status = setsockopt (((SITCP_INFO*)info)->tcpfd,
                         level, optname, optval, optlen);
    break;

  case CMD_SITCP_SET_SOCKOPT_UDP:
    info    = va_arg(argptr, void*);
    level   = va_arg(argptr, INT);
    optname = va_arg(argptr, INT);
    optval  = va_arg(argptr, char*);
    optlen  = va_arg(argptr, INT);
    status  = sitcp_udp(CMD_SITCP_SET_SOCKOPT, info, level, optname, optval, optlen);
    break;

  case CMD_SITCP_GET_IP_ADDRESS:
    info  = va_arg(argptr, void*);
    ipmac = va_arg(argptr, BYTE*);
    for(i=0; i<4; i++) ipmac[i] = ((SITCP_INFO*)info)->ip[i];
    break;

  case CMD_SITCP_GET_MAC_ADDRESS:
    info  = va_arg(argptr, void*);
    ipmac = va_arg(argptr, BYTE*);
    for(i=0; i<6; i++) ipmac[i] = ((SITCP_INFO*)info)->mac[i];
    break;
  }

  va_end(argptr);

  return status;
}
