/********************************************************************\

  Name:         raspi.h
  Created by:   Kou Oishi

  Contents:     Header file for Raspberry Pi bus driver

  $Id:$

\********************************************************************/
#include <wiringPi.h>
#include <wiringPiSPI.h>

INT raspi(INT cmd, ...);
