/********************************************************************\

  Name:         sitcp.h
  Created by:   Kou Oishi (originated from tcpip.c by Stefan Ritt)

  Contents:     Header file for SiTCP bus driver

  $Id:$

\********************************************************************/

typedef struct {
  unsigned int address;
  char         data;
  BOOL         inhibit_validation;
} SITCP_RBCP_COMMAND;


INT sitcp(INT cmd, ...);

/* For use SiTCP's udp connection */
INT sitcp_udp(INT cmd, ...);
