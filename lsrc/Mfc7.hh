#ifndef __Mfc7_hh__
#define __Mfc7_hh__

#include "uhal/uhal.hpp"
#include <iostream>

class Mfc7
{
public:
  Mfc7() {std::cerr << "-- Mfc7 created" << std::endl;}
  virtual ~Mfc7() { std::cerr << "-- Mfc7 destructed" << std::endl;}
  int open(const char *);
  int close();
  int init();
  int wait();
  int check();
  unsigned int reg_read(const char *);
  void reg_write(const char *, unsigned int);
  void sram_reset();
  int check_fct();
  int sram_read8(unsigned int, unsigned int*);
  uhal::HwInterface *hw;
  unsigned int sram_current_addr;
protected:
private:
  uhal::ConnectionManager *manager;
  unsigned int prev_sram_cfa;
};

#endif
