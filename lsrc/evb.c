/********************************************************************\

  Name:         ebuser.c
  Created by:   Pierre-Andre Amaudruz

  Contents:     User section for the Event builder

  $Id$

\********************************************************************/
/** @file ebuser.c
    The Event builder user file
*/

#define REQUIRE_ALL

#include <stdio.h>
#include <string.h>
#include "assert.h"
#include "midas.h"
#include "mrpc.h"
#include "ftplib.h"
#include "mdsupport.h"
#include "mevb.h"

#define SERVER_CACHE_SIZE  100000       /* event cache before buffer */
#define ODB_UPDATE_TIME      1000       /* 1 seconds for ODB update */
#define DEFAULT_FE_TIMEOUT  60000       /* 60 seconds for watchdog timeout */
#define TIMEOUT_ABORT         300       /* seconds waiting for data before aborting run */

#define EQUIPMENT_COMMON_STR "\
Event ID = WORD : 0\n\
Trigger mask = WORD : 0\n\
Buffer = STRING : [32] SYSTEM\n\
Type = INT : 0\n\
Source = INT : 0\n\
Format = STRING : [8] FIXED\n\
Enabled = BOOL : 0\n\
Read on = INT : 0\n\
Period = INT : 0\n\
Event limit = DOUBLE : 0\n\
Num subevents = DWORD : 0\n\
Log history = INT : 0\n\
Frontend host = STRING : [32] \n\
Frontend name = STRING : [32] \n\
Frontend file name = STRING : [256] \n\
Status = STRING : [256] \n\
Status color = STRING : [32] \n\
Hidden = BOOL : 0\n\
"

#define EQUIPMENT_STATISTICS_STR "\
Events sent = DOUBLE : 0\n\
Events per sec. = DOUBLE : 0\n\
kBytes per sec. = DOUBLE : 0\n\
"


#ifdef __cplusplus
extern "C" {
#endif

  EBUILDER_SETTINGS ebset;
  EBUILDER_CHANNEL ebch[MAX_CHANNELS];

  INT run_state;                  /* STATE_RUNNING, STATE_STOPPED, STATE_PAUSED */
  INT run_number;
  DWORD last_time;
  DWORD actual_time;              /* current time in seconds since 1970 */
  DWORD actual_millitime;         /* current time in milliseconds */

  char mevb_svn_revision[] = "$Id$";
  char host_name[HOST_NAME_LENGTH];
  char expt_name[NAME_LENGTH];
  char buffer_name[NAME_LENGTH];
  INT nfragment;

  char *dest_event;
  HNDLE hDB, hKey, hStatKey, hSubkey, hEqKey, hESetKey;
  BOOL debug = FALSE, debug1 = FALSE;

  BOOL wheel = FALSE;
  char bars[] = "|\\-/";
  int i_bar;
  BOOL abort_requested = FALSE, stop_requested = TRUE;
  DWORD stop_time = 0, request_stop_time = 0;

  BOOL tstmp_build_enabled    = FALSE;
  BOOL serno_build_requested  = FALSE;
  BOOL trgmst_build_requested = FALSE;
  BOOL trgno_build_requested  = FALSE;
  BOOL cntno_build_requested  = FALSE;
  BOOL tstmp_build_requested  = FALSE;

  static int waiting_for_stop = FALSE;

  int sys_max_event_size = DEFAULT_MAX_EVENT_SIZE;
  
  static DWORD evb_serial = 0;

  /*-- Globals -------------------------------------------------------*/

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = "Main Ebuilder";

  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;

  /* A frontend status page is displayed with this frequency in ms */
  INT display_period = 3000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 1000000;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 2000 * 50000;

  /*-- Function declarations -----------------------------------------*/
  INT(*meb_fragment_add) (char *, char *, INT *);
  INT handFlush(void);
  INT source_booking(void);
  INT source_unbooking(void);
  INT close_buffers(void);
  INT source_scan(INT fmt, EQUIPMENT_INFO * eq_info);
  INT source_scan_strecalbt(INT fmt, EQUIPMENT_INFO * eq_info);
  INT eb_mfragment_add(char *pdest, char *psrce, INT * size);
  INT eb_yfragment_add(char *pdest, char *psrce, INT * size);

  INT eb_begin_of_run(INT, char *, char *);
  INT eb_end_of_run(INT, char *);
  INT eb_user(INT, BOOL mismatch, EBUILDER_CHANNEL *, EVENT_HEADER *, void *, INT *);
  INT load_fragment(void);
  INT get_a_fragment_event(const INT fmt, const INT idx, BANK_HEADER **ppsbh);
  INT scan_fragment_for_state_running(EQUIPMENT* eq, EQUIPMENT_INFO* eq_info);
  INT scan_fragment(void);

  INT ebuilder_init();
  INT ebuilder_exit();
  INT eb_begin_of_run(INT, char *, char *);
  INT eb_end_of_run(INT, char *);
  INT ebuilder_loop();
  INT ebuser(INT, BOOL mismatch, EBUILDER_CHANNEL *, EVENT_HEADER *, void *, INT *);

  /*-- Equipment list ------------------------------------------------*/
  EQUIPMENT equipment[] = {
    {"Main EB",           /* equipment name */
     {1, 0,               /* event ID, trigger mask */
      "SYSTEM",           /* event buffer */
      0,                  /* equipment type */
      0,                  /* event source */
      "MIDAS",            /* format */
      TRUE,               /* enabled */
     },
    },

    {""}
  };

#ifdef __cplusplus
}
#endif

/********************************************************************/
/********************************************************************/
INT ebuilder_init()
{
  //setbuf(stdout, NULL);
  //setbuf(stderr, NULL);
  return EB_SUCCESS;
}

/********************************************************************/
INT ebuilder_exit()
{
  return EB_SUCCESS;
}

/********************************************************************/
INT ebuilder_loop()
{
  return EB_SUCCESS;
}

/********************************************************************/
/**
   Hook to the event builder task at PreStart transition.
   @param rn run number
   @param UserField argument from /Ebuilder/Settings
   @param error error string to be passed back to the system.
   @return EB_SUCCESS
*/
INT eb_begin_of_run(INT rn, char *UserField, char *error)
{
  printf("In eb_begin_of_run for run:%d User_field:%s \n", rn, UserField);
  return EB_SUCCESS;
}

/********************************************************************/
/**
   Hook to the event builder task at completion of event collection after
   receiving the Stop transition.
   @param rn run number
   @param error error string to be passed back to the system.
   @return EB_SUCCESS
*/
INT eb_end_of_run(INT rn, char *error)
{
  printf("In eb_end_of_run\n");
  return EB_SUCCESS;
}

/********************************************************************/
/**
   Hook to the event builder task after the reception of
   all fragments of the same serial number. The destination
   event has already the final EVENT_HEADER setup with
   the data size set to 0. It is than possible to
   add private data at this point using the proper
   bank calls. Therefore any new banks created within eb_user will be appearing
   before the collected banks from the fragments.
   When using the eb_user with the ODB flag "user build=y"
   (equipments/EB/settings) the automatic event builder is skipped.
   This allow the user to extract selectively from the different fragments the
   appropriate banks and compose the final destination event. In order to
   do so, the function "bk_copy(pevent, ebch[i].pfragment, bankname)" will copy
   a particular bank from a given fragment.

   <b>Note:</b> It exists two Midas event format to address bank size less than 32KB and
   larger bank size <4GB. This distinction is done by the call bk_init(pevent) for the small
   bank size and bk_init32(pevent) for large bank size. Within an experiment, this
   declaration has to be consistant. Therefore the bk_init in the eb_user should follow
   as well the type of the frontends.

   The ebch[] array structure points to nfragment channel structure
   with the following content:
   \code
   typedef struct {
   char  name[32];         // Fragment name (Buffer name).
   DWORD serial;           // Serial fragment number.
   char *pfragment;        // Pointer to fragment (EVENT_HEADER *)
   ...
   } EBUILDER_CHANNEL;
   \endcode

   The correct code for including your own MIDAS bank is shown below where
   \b TID_xxx is one of the valid Bank type starting with \b TID_ for
   midas format \b bank_name is a 4 character descriptor.
   \b pdata has to be declared accordingly with the bank type.
   Refers to the ebuser.c source code for further description.

   <strong>
   It is not possible to mix within the same destination event different event format!
   No bk_swap performed when user build is requested.
   </strong>

   \code
   // Event is empty, fill it with BANK_HEADER
   // If you need to add your own bank at this stage

   // Need to match the decalration in the Frontends.
   bk_init(pevent);
   //  bk_init32(pevent);
   bk_create(pevent, bank_name, TID_xxxx, &pdata);
   *pdata++ = ...;
   *dest_size = bk_close(pevent, pdata);
   pheader->data_size = *dest_size + sizeof(EVENT_HEADER);
   \endcode

   @param nfrag Number of fragment.
   @param mismatch Midas Serial number mismatch flag.
   @param ebch  Structure to all the fragments.
   @param pheader Destination pointer to the header.
   @param pevent Destination pointer to the bank header.
   @param dest_size Destination event size in bytes.
   @return EB_SUCCESS
*/

INT eb_user(INT nfrag, BOOL mismatch, EBUILDER_CHANNEL * ebch
            , EVENT_HEADER * pheader, void *pevent, INT * dest_size)
{
  INT i, frag_size, serial, status;
  DWORD  *pdata, *psrcData;

  // Do some extra fragment consistency check
  if (mismatch){
    printf("Serial number do not match across fragments\n");
    for (i = 0; i < nfrag; i++) {
      serial = ((EVENT_HEADER *) ebch[i].pfragment)->serial_number;
      printf("Ser[%i]:%d ", i + 1, serial);
    }
    printf("\n");
    return EB_USER_ERROR;
  }

  //
  // Include my own bank
  /*
  bk_init(pevent);
  bk_create(pevent, "MYOW", TID_DWORD, &pdata);
  for (i = 0; i < nfrag; i++) {
    *pdata++ = ((EVENT_HEADER *) ebch[i].pfragment)->serial_number;
    *pdata++ = ((EVENT_HEADER *) ebch[i].pfragment)->time_stamp;
  }
  *dest_size = bk_close(pevent, pdata);
  pheader->data_size = *dest_size + sizeof(EVENT_HEADER);
  */
  //
  // Loop over fragments.
  if (debug) {
    for (i = 0; i < nfrag; i++) {
      if (1) {
	if (ebset.preqfrag[i]) { // printf if channel enable
	  frag_size = ((EVENT_HEADER *) ebch[i].pfragment)->data_size;
	  serial = ((EVENT_HEADER *) ebch[i].pfragment)->serial_number;
	  printf("Frg#:%d Dsz:%d Ser:%d ", i, frag_size, serial);
	  // For Data fragment Access.
	  psrcData = (DWORD *) (((EVENT_HEADER *) ebch[i].pfragment) + 1);
	}
      }
    }
    printf("\n");
  }
  return EB_SUCCESS;
}








/*************************************************************************
 * Main Routines                                                         *
 *************************************************************************/

/********************************************************************/
INT register_equipment(void)
{
  INT index, size, status;
  char str[256];
  EQUIPMENT_INFO *eq_info;
  EQUIPMENT_STATS *eq_stats;
  HNDLE hKey;

  /* get current ODB run state */
  size = sizeof(run_state);
  run_state = STATE_STOPPED;
  db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, TRUE);
  size = sizeof(run_number);
  run_number = 1;
  status = db_get_value(hDB, 0, "/Runinfo/Run number", &run_number, &size, TID_INT, TRUE);
  assert(status == SUCCESS);

  /* scan EQUIPMENT table from mevb.C */
  for (index = 0; equipment[index].name[0]; index++) {
    eq_info = &equipment[index].info;
    eq_stats = &equipment[index].stats;

    if (eq_info->event_id == 0) {
      printf("\nEvent ID 0 for %s not allowed\n", equipment[index].name);
      cm_disconnect_experiment();
      ss_sleep(5000);
      exit(0);
    }

    /* init status */
    equipment[index].status = EB_SUCCESS;

    sprintf(str, "/Equipment/%s/Common", equipment[index].name);

    /* get last event limit from ODB */
    if (eq_info->eq_type != EQ_SLOW) {
      db_find_key(hDB, 0, str, &hKey);
      size = sizeof(double);
      if (hKey)
        db_get_value(hDB, hKey, "Event limit", &eq_info->event_limit, &size, TID_DOUBLE, TRUE);
    }

    /* Create common subtree */
    status = db_check_record(hDB, 0, str, EQUIPMENT_COMMON_STR, TRUE);
    if (status != DB_SUCCESS) {
      printf("Cannot check equipment record, status = %d\n", status);
      ss_sleep(3000);
    }
    db_find_key(hDB, 0, str, &hKey);

    if (equal_ustring(eq_info->format, "FIXED"))
      equipment[index].format = FORMAT_FIXED;
    else                      /* default format is MIDAS */
      equipment[index].format = FORMAT_MIDAS;

    gethostname(eq_info->frontend_host, sizeof(eq_info->frontend_host));
    strcpy(eq_info->frontend_name, frontend_name);
    strcpy(eq_info->frontend_file_name, frontend_file_name);

    /* set record from equipment[] table in frontend.c */
    db_set_record(hDB, hKey, eq_info, sizeof(EQUIPMENT_INFO), 0);

    /* get record once at the start equipment info */
    size = sizeof(EQUIPMENT_INFO);
    db_get_record(hDB, hKey, eq_info, &size, 0);

    /*---- Create just the key , leave it empty ---------------------------------*/
    sprintf(str, "/Equipment/%s/Variables", equipment[index].name);
    db_create_key(hDB, 0, str, TID_KEY);
    db_find_key(hDB, 0, str, &hKey);
    equipment[index].hkey_variables = hKey;

    /*---- Create and initialize statistics tree -------------------*/
    sprintf(str, "/Equipment/%s/Statistics", equipment[index].name);

    status = db_check_record(hDB, 0, str, EQUIPMENT_STATISTICS_STR, TRUE);
    if (status != DB_SUCCESS) {
      printf("Cannot create/check statistics record, error %d\n", status);
      ss_sleep(3000);
    }

    status = db_find_key(hDB, 0, str, &hKey);
    if (status != DB_SUCCESS) {
      printf("Cannot find statistics record, error %d\n", status);
      ss_sleep(3000);
    }

    eq_stats->events_sent = 0;
    eq_stats->events_per_sec = 0;
    eq_stats->kbytes_per_sec = 0;

    /* open hot link to statistics tree */
    status = db_open_record(hDB, hKey, eq_stats, sizeof(EQUIPMENT_STATS)
                            , MODE_WRITE, NULL, NULL);
    if (status != DB_SUCCESS) {
      cm_msg(MERROR, "register_equipment",
             "Cannot open statistics record, error %d. Probably other FE is using it", status);
      ss_sleep(3000);
    }

    /*---- open event buffer ---------------------------------------*/
    if (eq_info->buffer[0]) {
      status = bm_open_buffer(eq_info->buffer, 2 * max_event_size, &equipment[index].buffer_handle);
      if (status != BM_SUCCESS && status != BM_CREATED) {
        cm_msg(MERROR, "register_equipment",
               "Cannot open event buffer. Try to reduce EVENT_BUFFER_SIZE in midas.h \
          and rebuild the system.");
        return 0;
      }

      if (1)
        {
          int level = 0;
          bm_get_buffer_level(equipment[index].buffer_handle, &level);
          printf("Buffer %s, level %d, info: \n", eq_info->buffer, level);
        }

      /* set the default buffer cache size */
      bm_set_cache_size(equipment[index].buffer_handle, 0, SERVER_CACHE_SIZE);
    } else {
      cm_msg(MERROR, "register_equipment", "Destination buffer must be present");
      ss_sleep(3000);
      exit(0);
    }
  }
  return SUCCESS;
}

/********************************************************************/
INT check_fragment(HNDLE hSubkey, const INT type, const char* buffer, const char* format)
{
  INT size;

  /* equipment to be event-built */
  if(! (type & EQ_EB)) return -1;

  /* format is the same */
  if(0 != strncmp(format, equipment[0].info.format, strlen(format))) return -1;

  /* -------------------------------------------------------------
     set event building type
     ------------------------------------------------------------- */

  /* (1) normal */
  if(0 == strncmp(buffer, buffer_name, strlen(buffer_name))){
    ebch[nfragment].build_type = EB_BTYPE_SERNO;
    serno_build_requested = TRUE;
  }
  /* (2) trigger master buffer */
  else if(0 == strncmp(buffer, "CBUFF", 5) ||
          0 == strncmp(buffer, "FBUFF", 5) ||
          0 == strncmp(buffer, "PBUFF", 5) ){
    /* trigger master number is supplied */
    ebch[nfragment].build_type = EB_BTYPE_TRGMST;
    trgmst_build_requested = TRUE;
  }
  /* (3) main buffer (EROS/ROESTI/PMT EROS) */
  else if(0 == strncmp(buffer, "EBUFF", 5) ||
          0 == strncmp(buffer, "RBUFF", 5) ||
          0 == strncmp(buffer, "TBUFF", 5)  ){
    /* trigger number matching */
    ebch[nfragment].build_type = EB_BTYPE_TRGNO;
    trgno_build_requested = TRUE;
  }
  /* (4) BDC buffer  */
  else if(0 == strncmp(buffer, "BBUFF", 5)){
    /* counter number matching */
    ebch[nfragment].build_type = EB_BTYPE_CNTNO;
    cntno_build_requested = TRUE;

    /* time stamp matching */
    if(tstmp_build_enabled){
      ebch[nfragment].build_type |= EB_BTYPE_TSTMP;
      tstmp_build_requested = TRUE;
    }
  }
  /* not matched */
  else{
    return -1;
  }

  /* fill internal eb structure */
  strcpy(ebch[nfragment].format, format);
  strcpy(ebch[nfragment].buffer, buffer);
  size = sizeof(WORD);
  db_get_value(hDB, hSubkey, "common/Trigger Mask", &ebch[nfragment].trigger_mask, &size, TID_WORD, 0);
  size = sizeof(WORD);
  db_get_value(hDB, hSubkey, "common/Event ID", &ebch[nfragment].event_id, &size, TID_WORD, 0);

  /* set index */
  ebch[nfragment].idx = nfragment;

  return SUCCESS;
}

/********************************************************************/
INT load_fragment(void)
{
  INT i, size, type;
  HNDLE hEqKey, hSubkey;
  EQUIPMENT_INFO *eq_info;
  KEY key;
  char buffer[NAME_LENGTH];
  char format[8];

  /* Get equipment pointer, only one eqp for now */
  eq_info = &equipment[0].info;

  /* Scan Equipment/Common listing */
  if (db_find_key(hDB, 0, "Equipment", &hEqKey) != DB_SUCCESS) {
    cm_msg(MINFO, "load_fragment", "Equipment listing not found");
    return EB_ERROR;
  }

  /* -------------------------------------------------------------
     Scan the Equipment list for fragment info collection
     ------------------------------------------------------------- */
  nfragment = 0;
  serno_build_requested = trgno_build_requested
    = cntno_build_requested = tstmp_build_requested = FALSE;

  for (i = 0;; i++) {
    db_enum_key(hDB, hEqKey, i, &hSubkey);
    if (!hSubkey)
      break;
    db_get_key(hDB, hSubkey, &key);
    if (key.type == TID_KEY) {
      /* Equipment name */
      if (debug) printf("Equipment name:%s\n", key.name);

      /* Check if equipment is EQ_EB */
      size = sizeof(INT);
      db_get_value(hDB, hSubkey, "common/type", &type, &size, TID_INT, 0);
      size = sizeof(buffer);
      db_get_value(hDB, hSubkey, "common/Buffer", buffer, &size, TID_STRING, 0);
      size = sizeof(format);
      db_get_value(hDB, hSubkey, "common/Format", format, &size, TID_STRING, 0);

      //if (debug)
      //  printf("Equipment name: %s, Type: 0x%x, Buffer: %s, Format: %s\n",
      //         key.name, type, buffer, format);

      /* Check if equipment match EB requirements */
      if(SUCCESS == check_fragment(hSubkey, type, buffer, format)){
        printf("Fragment %d: Equipment name %s,  evID %d, buffer %s\n",
               nfragment, key.name, ebch[nfragment].event_id, buffer);

        /* When SUCCESS, a fragment is added, so count up fragment */
        nfragment++;
      }

    }
  } /* end of for(nfragment) */

  if (nfragment > 1)
    printf("Found %d fragments for event building\n", nfragment);
  else if(nfragment == 1)
    printf("Found one fragment for event building\n");
  else{
    cm_msg(MINFO, "load_fragment", "No one fragment for event building.");
    cm_disconnect_experiment();
    exit(0);
  }

  /* Point to the Ebuilder settings */
  /* Set fragment_add function based on the format */
  if (equipment[0].format == FORMAT_MIDAS)
    meb_fragment_add = eb_mfragment_add;
  else {
    cm_msg(MERROR, "load_fragment", "Unknown data format :%d", format);
    return EB_ERROR;
  }

  /* allocate destination event buffer */
  dest_event = (char *) malloc(nfragment * (max_event_size + sizeof(EVENT_HEADER)));
  memset(dest_event, 0, nfragment * (max_event_size + sizeof(EVENT_HEADER)));
  if (dest_event == NULL) {
    cm_msg(MERROR, "load_fragment", "%s: Not enough memory for event buffer", frontend_name);
    return EB_ERROR;
  }
  return EB_SUCCESS;
}

/********************************************************************/
INT scan_fragment_for_state_running(EQUIPMENT* eq, EQUIPMENT_INFO* eq_info)
{
  INT fragn, status, ch;
  time_t now;
  
  if(serno_build_requested)
    /* if serial number matching event build requested */
    status = source_scan(equipment[0].format, eq_info);
  else
    /* if other (trigger / counter number or time stamp matching)
       event build requested */
    status = source_scan_strecalbt(equipment[0].format, eq_info);

  switch (status)
    {
    case BM_ASYNC_RETURN: // No event found for now, Check for timeout

#ifndef NO_TIMEOUT_IMPLEMENT
      /* advanced checking for timeouts */
      now = time(NULL);
      int empty = 1;
      int badfrag = -1;

      /* check if we recieved any data */
      for (fragn = 0; fragn < nfragment; fragn++)
        if (ebset.received[fragn])
          empty = 0;

      /* only look for timeout if there is received data from any fragment */
      if (!empty)
        for (fragn = 0; fragn < nfragment; fragn++) {
          /*
            if (debug)
            printf("frag %d, timeout %d, threshold %d, "
            "received %d, time %d\n", fragn, ebch[fragn].timeout,
            TIMEOUT, ebset.received[fragn], now - ebch[fragn].time);
          */
          if (ebch[fragn].time && ebch[fragn].timeout)
            if (now - ebch[fragn].time > TIMEOUT_ABORT) {
              /*
                 cm_msg(MERROR, "scan_fragment", "frag %d, timeout %d, %d sec",
                 fragn, ebch[fragn].timeout, now - ebch[fragn].time);
              */
              badfrag = fragn;
            }
        }

      if (badfrag >= 0) {
        //status = SS_ABORT;
        if (!waiting_for_stop && !stop_requested) {
          cm_msg(MERROR, "scan_fragment",
                 "timeout waiting for fragment %d, restarting run", badfrag);
          cm_msg(MINFO, "scan_fragment", "spawning mtransition");
          ss_system("mtransition STOP IF \"/Logger/Auto restart\""
                    " DELAY \"/Logger/Auto restart delay\" START &");
          waiting_for_stop = TRUE;
        }
        break;
      }
#endif /* #ifndef NO_TIMEOUT_IMPLEMENT */

      for (fragn = 0; fragn < nfragment; fragn++) {
        if (ebch[fragn].timeout > TIMEOUT) {     /* Timeout */
          if (stop_requested) { /* Stop */
            if (debug)
              printf("Timeout waiting for fragment %d while stopping the run\n", fragn);
            status = close_buffers();
            break;
          } else {
            /* No stop requested  but timeout, allow a yield to not
               eat all the CPU */
            status = cm_yield(10);
            if (wheel) {
              printf("...%c Timing on %1.0lf\r",
                     bars[i_bar++ % 4], eq->stats.events_sent);
              fflush(stdout);
            }

          }
        }
        //else { /* No timeout loop back */
      }                   // for loop over all fragments
      break;
      // End of case EB_RUNNING

      /* =======================================================================
         STATE ERROR
         ======================================================================= */
    case EB_ERROR:
    case EB_USER_ERROR:
      abort_requested = TRUE;
      if (status == EB_USER_ERROR)
        cm_msg(MTALK, "scan_fragment", "%s: Error signaled by user code - stopping run...",
               frontend_name);
      else
        cm_msg(MTALK, "EBuilder", "%s: Event mismatch - Stopping run...", frontend_name);

      if (debug)
        printf("Stop requested on Error %d\n", status);
      close_buffers();

#if 0
      if (!waiting_for_stop && !stop_requested) {
        cm_msg(MINFO, "scan_fragment", "spawning mtransition");
        //ss_system("mtransition STOP IF \"/Logger/Auto restart\" DELAY \"/Logger/Auto restart delay\" START &");
        ss_system("mtransition STOP &");
        waiting_for_stop = TRUE;
      }
#endif
      for (ch=0; ch<5; ch++) {
        if (cm_transition(TR_STOP, 0, NULL, 0, TR_SYNC, 0) == CM_SUCCESS)
          break;
        cm_msg(MERROR, "scan_fragment", "%s: Stop Transition request failed, trying again!", frontend_name);
      }
      return status;
      break;

      /* =======================================================================
         STATE SUCCESS OR SKIP
         ======================================================================= */
    case EB_SUCCESS:
    case EB_SKIP:
      //   Normal path if event has been assembled
      //   No yield in this case.
      break;
    default:
      cm_msg(MERROR, "scan_fragment", "unexpected return %d", status);
      status = SS_ABORT;
    }                      // switch scan_source

  return status;
}

/********************************************************************/
INT scan_fragment(void)
{
  INT fragn, status;
  EQUIPMENT *eq;
  EQUIPMENT_INFO *eq_info;
  INT ch;
  time_t now;

  /* Get equipment pointer, only one eqp for now */
  eq_info = &equipment[0].info;
  status = 0;
  eq = NULL;

  /* Main event loop */
  do {
    switch (run_state) {

      /* =======================================================================
         STATE STOPPED OR PAUSED
         ======================================================================= */
    case STATE_STOPPED:
    case STATE_PAUSED:
      /* skip the source scan and yield */
      status = cm_yield(500);
      if (wheel) {
        printf("...%c Snoring\r", bars[i_bar++ % 4]);
        fflush(stdout);
      }
      break;

      /* =======================================================================
         STATE RUNNING
         ======================================================================= */
    case STATE_RUNNING:
      status = scan_fragment_for_state_running(eq, eq_info);
      break;
    }

    /* EB job done, update statistics if its time */
    /* Check if it's time to do statistics job */
    if ((actual_millitime = ss_millitime()) - last_time > 1000) {
      /* Force event to appear at the destination if Ebuilder is remote */
      rpc_flush_event();
      /* Force event ot appear at the destination if Ebuilder is local */
      bm_flush_cache(equipment[0].buffer_handle, BM_NO_WAIT);

      status = cm_yield(10);

      eq = &equipment[0];
      eq->stats.events_sent += eq->events_sent;
      eq->stats.events_per_sec = eq->events_sent / ((actual_millitime - last_time) / 1000.0);
      eq->stats.kbytes_per_sec = eq->bytes_sent / 1024.0 / ((actual_millitime - last_time) / 1000.0);
      eq->bytes_sent = 0;
      eq->events_sent = 0;
      /* update destination statistics */
      db_send_changed_records();
      /* Keep track of last ODB update */
      last_time = ss_millitime();
    }

    ch = 0;
    if (ss_kbhit()) {
      ch = ss_getchar(0);
      if (ch == -1)
        ch = getchar();
      if ((char) ch == '!')
        break;
    }
  } while (status != RPC_SHUTDOWN && status != SS_ABORT);

  return status;
}

/********************************************************************/
INT eb_mfragment_add(char *pdest, char *psrce, INT * size)
{
  BANK_HEADER *psbh, *pdbh;
  char *psdata, *pddata;
  INT bksize;

  /* Condition for new EVENT the data_size should be ZERO */
  *size = ((EVENT_HEADER *) pdest)->data_size;

  /* destination pointer */
  pddata = pdest + *size + sizeof(EVENT_HEADER);

  if (*size) {
    /* NOT the first fragment */

    /* Swap event source if necessary */
    psbh = (BANK_HEADER *) (((EVENT_HEADER *) psrce) + 1);
    bk_swap(psbh, FALSE);

    /* source pointer */
    psbh = (BANK_HEADER *) (((EVENT_HEADER *) psrce) + 1);
    psdata = (char *) (psbh + 1);

    /* copy all banks without the bank header */
    bksize = psbh->data_size;

    /* copy */
    memcpy(pddata, psdata, bksize);

    /* update event size */
    ((EVENT_HEADER *) pdest)->data_size += bksize;

    /* update bank size */
    pdbh = (BANK_HEADER *) (((EVENT_HEADER *) pdest) + 1);
    pdbh->data_size += bksize;

    *size = ((EVENT_HEADER *) pdest)->data_size;
  } else {
    /* First event without the event header but with the
       bank header as the size is zero */
    *size = ((EVENT_HEADER *) psrce)->data_size;

    /* Swap event if necessary */
    psbh = (BANK_HEADER *) (((EVENT_HEADER *) psrce) + 1);
    bk_swap(psbh, FALSE);

    /* copy first fragment */
    memcpy(pddata, psbh, *size);

    /* update destination event size */
    ((EVENT_HEADER *) pdest)->data_size = *size;
  }
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT eb_yfragment_add(char *pdest, char *psrce, INT * size)
{
  /* pdest : EVENT_HEADER pointer
     psrce : EVENT_HEADER pointer
     Keep pbkh for later incrementation
  */
  char *psdata, *pddata;
  DWORD *pslrl, *pdlrl;
  INT i4frgsize, i1frgsize, status;

  /* Condition for new EVENT the data_size should be ZERO */
  *size = ((EVENT_HEADER *) pdest)->data_size;

  /* destination pointer skip the header as it has been already
     composed and the usere may have modified it on purpose (Midas Control) */
  pddata = pdest + *size + sizeof(EVENT_HEADER);

  /* the Midas header is present for logger */
  if (*size) {                 /* already filled with a fragment */

    /* source pointer: number of DWORD (lrl included) */
    pslrl = (DWORD *) (((EVENT_HEADER *) psrce) + 1);

    /* Swap event if necessary */
    status = md_event_swap(FORMAT_MIDAS, pslrl);

    /* copy done in bytes, do not include LRL */
    psdata = (char *) (pslrl + 1);

    /* copy size in I*4 (lrl included, remove it) */
    i4frgsize = (*pslrl);
    i1frgsize = 4 * i4frgsize;

    /* append fragment */
    memcpy(pddata, psdata, i1frgsize);

    /* update Midas header event size */
    ((EVENT_HEADER *) pdest)->data_size += i1frgsize;

    /* update LRL size (I*4) */
    pdlrl = (DWORD *) (((EVENT_HEADER *) pdest) + 1);
    *pdlrl += i4frgsize;

    /* Return event size in bytes */
    *size = ((EVENT_HEADER *) pdest)->data_size;
  } else {                     /* new destination event */
    /* The composed event has already the MIDAS header.
       which may have been modified by the user in ebuser.c
       Will be stripped by the logger (YBOS).
       Copy the first full event ( no EVID suppression )
       First event (without the event header) */

    /* source pointer */
    pslrl = (DWORD *) (((EVENT_HEADER *) psrce) + 1);

    /* Swap event if necessary */
    status = md_event_swap(FORMAT_MIDAS, pslrl);

    /* size in byte from the source midas header */
    *size = ((EVENT_HEADER *) psrce)->data_size;

    /* copy first fragment */
    memcpy(pddata, (char *) pslrl, *size);

    /* update destination Midas header event size */
    ((EVENT_HEADER *) pdest)->data_size += *size;

  }
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT tr_start(INT rn, char *error)
{
  EBUILDER(ebuilder_str);
  INT status, size, i;
  char str[128];
  KEY key;
  HNDLE hKey, hEqkey, hEqFRkey;
  EQUIPMENT_INFO *eq_info;

  if (debug)
    printf("tr_start: run %d\n", rn);

  eq_info = &equipment[0].info;

  /* Get update eq_info from ODB */
  sprintf(str, "/Equipment/%s/Common", equipment[0].name);
  status = db_find_key(hDB, 0, str, &hKey);
  size = sizeof(EQUIPMENT_INFO);
  db_get_record(hDB, hKey, eq_info, &size, 0);

  ebset.nfragment = nfragment;

  /* reset serial numbers */
  evb_serial = 0;
  for (i = 0; equipment[i].name[0]; i++) {
    equipment[i].serial_number = 1;
    equipment[i].subevent_number = 0;
    equipment[i].stats.events_sent = 0;
    equipment[i].odb_in = equipment[i].odb_out = 0;
  }

  /* Get / Set Settings */
  sprintf(str, "/Equipment/%s/Settings", equipment[0].name);
  if (db_find_key(hDB, 0, str, &hEqkey) != DB_SUCCESS) {
    status = db_create_record(hDB, 0, str, strcomb(ebuilder_str));
  }

  /* Keep Key on Ebuilder/Settings */
  sprintf(str, "/Equipment/%s/Settings", equipment[0].name);
  if (db_find_key(hDB, 0, str, &hEqkey) != DB_SUCCESS) {
    cm_msg(MINFO, "tr_start", "/Equipment/%s/Settings not found", equipment[0].name);
  }

  /* Update or Create User_field */
  size = sizeof(ebset.user_field);
  status = db_get_value(hDB, hEqkey, "User Field", ebset.user_field, &size, TID_STRING, TRUE);

  /* Update or Create User_Build */
  size = sizeof(ebset.user_build);
  status = db_get_value(hDB, hEqkey, "User Build", &ebset.user_build, &size, TID_BOOL, TRUE);

  /* Update or Create Capable Timestamp Latency */
  size = sizeof(ebset.capa_latency);
  status = db_get_value(hDB, hEqkey, "Capable Timestamp Latency", &ebset.capa_latency, &size, TID_DWORD, TRUE);
  if(debug){
    printf("Capable timestamp latency: %d usec\n", ebset.capa_latency);
  }

  /* update ODB */
  size = sizeof(INT);
  status = db_set_value(hDB, hEqkey, "Number of Fragment", &ebset.nfragment, size, 1, TID_INT);

  /* Create or update the fragment request list */
  status = db_find_key(hDB, hEqkey, "Fragment Required", &hEqFRkey);
  status = db_get_key(hDB, hEqFRkey, &key);
  assert(status == DB_SUCCESS);

  if (key.num_values != ebset.nfragment) {
    cm_msg(MINFO, "tr_start", "Number of Fragment mismatch ODB:%d - CUR:%d", key.num_values, ebset.nfragment);
    free(ebset.preqfrag);
    size = ebset.nfragment * sizeof(BOOL);
    ebset.preqfrag = malloc(size);
    for (i = 0; i < ebset.nfragment; i++)
      ebset.preqfrag[i] = TRUE;
    status =
      db_set_value(hDB, hEqkey, "Fragment Required", ebset.preqfrag, size, ebset.nfragment, TID_BOOL);
  } else {                     // Take from ODBedit
    size = key.total_size;
    free(ebset.preqfrag);
    ebset.preqfrag = malloc(size);
    status = db_get_data(hDB, hEqFRkey, ebset.preqfrag, &size, TID_BOOL);
  }

  /* Cleanup fragment flags */
  free(ebset.received);
  free(ebset.skip);
  free(ebset.skipped);
  ebset.received = malloc(size);
  ebset.skip     = malloc(size);
  ebset.skipped  = malloc(size);
  for (i = 0; i < ebset.nfragment; i++){
    ebset.received[i] = FALSE;
    ebset.skip[i]     = FALSE;
    ebset.skipped[i]  = FALSE;
  }

  /* Check if at least one fragment is requested */
  for (i = 0; i < ebset.nfragment; i++)
    if (ebset.preqfrag[i])
      break;

  if (i == ebset.nfragment) {
    cm_msg(MERROR, "tr_start", "Run start aborted because no fragment required");
    return 0;
  }

  /* Call BOR user function */
  status = eb_begin_of_run(run_number, ebset.user_field, error);
  if (status != EB_SUCCESS) {
    cm_msg(MERROR, "tr_start", "run start aborted due to eb_begin_of_run (%d)", status);
    return status;
  }

  /* Book all fragment */
  status = source_booking();
  if (status != SUCCESS)
    return status;

  if (!eq_info->enabled) {
    cm_msg(MINFO, "tr_start", "Event Builder disabled");
    return CM_SUCCESS;
  }

  /* local run state */
  run_state = STATE_RUNNING;
  run_number = rn;
  stop_requested = FALSE;
  abort_requested = FALSE;
  printf("%s-Starting New Run: %d\n", frontend_name, rn);

  if (1) {
    int fragn;
    time_t now = time(NULL);

    // reset timeouts
    for (fragn = 0; fragn < nfragment; fragn++) {
      ebch[fragn].time = now;
      ebch[fragn].timeout = 0;
    }
  }

  /* Reset global trigger mask */
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT tr_resume(INT rn, char *error)
{
  int fragn;
  time_t now = time(NULL);

  printf("\n%s-Resume Run: %d detected\n", frontend_name, rn);

  // reset timeouts
  for (fragn = 0; fragn < nfragment; fragn++) {
    ebch[fragn].time = now;
    ebch[fragn].timeout = 0;
  }

  run_state = STATE_RUNNING;
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT tr_pause(INT rn, char *error)
{
  printf("\n%s-Pause Run: %d detected\n", frontend_name, rn);

  run_state = STATE_PAUSED;
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT tr_stop(INT rn, char *error)
{
  waiting_for_stop = FALSE;

  if (debug)
    printf("tr_stop: run %d\n", rn);

  /* local stop */
  stop_requested = TRUE;

  /* local stop time */
  request_stop_time = ss_millitime();
  return CM_SUCCESS;
}

/*--------------------------------------------------------------------*/
void free_event_buffer(INT nfrag)
{
  INT i;
  for (i = 0; i < nfrag; i++) {
    if (ebch[i].pfragment) {
      free(ebch[i].pfragment);
      ebch[i].pfragment = NULL;
    }
  }
}

/*--------------------------------------------------------------------*/
INT handFlush()
{
  int i, size, status;
  char strout[256];

  /* Do Hand flush until better way to  garantee the input buffer to be empty */
  if (debug)
    printf("Hand flushing system buffer... \n");
  for (i = 0; i < nfragment; i++) {
    do {
      status = 0;
      if (ebset.preqfrag[i]) {
        size = max_event_size;
        status = bm_receive_event(ebch[i].hBuf, ebch[i].pfragment, &size, BM_NO_WAIT);
        if (debug1) {
          sprintf(strout,
                  "booking:Hand flush bm_receive_event[%d] hndle:%d stat:%d  Last Ser:%d",
                  i, ebch[i].hBuf, status, ((EVENT_HEADER *) ebch[i].pfragment)->serial_number);
          printf("%s\n", strout);
        }
      }
    } while (status == BM_SUCCESS);
  }

  /* Empty source buffer */
  status = bm_empty_buffers();
  if (status != BM_SUCCESS)
    cm_msg(MERROR, "handFlush", "bm_empty_buffers failure [%d]", status);
  run_state = STATE_STOPPED;
  return status;
}


/*--------------------------------------------------------------------*/
INT source_booking()
{
  INT j, i, status, status1, status2;

  if (debug)
    printf("Entering booking\n");

  status1 = status2 = 0;

  /* Book all the source channels */
  for (i = 0; i < nfragment; i++) {
    /* Book only the requested event mask */
    if (ebset.preqfrag[i]) {
      /* Connect channel to source buffer */
      status1 = bm_open_buffer(ebch[i].buffer, 2 * max_event_size, &(ebch[i].hBuf));

      if (debug)
        printf("bm_open_buffer frag:%d buf:%s handle:%d stat:%d\n",
               i, ebch[i].buffer, ebch[i].hBuf, status1);

      if (1)
        {
          int level = 0;
          bm_get_buffer_level(ebch[i].hBuf, &level);
          printf("Buffer %s, level %d, info: \n", ebch[i].buffer, level);
        }


      /* Register for specified channel event ID and Trigger mask */
      status2 =
        bm_request_event(ebch[i].hBuf, ebch[i].event_id,
                         TRIGGER_ALL, GET_ALL, &ebch[i].req_id, NULL);
      if (debug)
        printf("bm_request_event frag:%d id:%d msk:%d req_id:%d stat:%d\n",
               i, ebch[i].event_id, ebch[i].trigger_mask, ebch[i].req_id, status2);
      if (((status1 != BM_SUCCESS) && (status1 != BM_CREATED)) ||
          ((status2 != BM_SUCCESS) && (status2 != BM_CREATED))) {
        cm_msg(MERROR, "source_booking",
               "Open buffer/event request failure [%d %d %d]", i, status1, status2);
        return BM_CONFLICT;
      }

      /* allocate local source event buffer */
      if (ebch[i].pfragment)
        free(ebch[i].pfragment);
      ebch[i].pfragment = (char *) malloc(max_event_size + sizeof(EVENT_HEADER));
      if (debug)
        printf("malloc pevent frag:%d pevent:%p\n", i, ebch[i].pfragment);
      if (ebch[i].pfragment == NULL) {
        free_event_buffer(nfragment);
        cm_msg(MERROR, "source_booking", "Can't allocate space for buffer");
        return BM_NO_MEMORY;
      }
    }
  }

  /* Empty source buffer */
  status = bm_empty_buffers();
  if (status != BM_SUCCESS) {
    cm_msg(MERROR, "source_booking", "bm_empty_buffers failure [%d]", status);
    return status;
  }

  if (debug) {
    printf("bm_empty_buffers stat:%d\n", status);
    for (j = 0; j < ebset.nfragment; j++) {
      printf(" buff:%s", ebch[j].buffer);
      printf(" ser#:%d", ebch[j].serial);
      printf(" hbuf:%2d", ebch[j].hBuf);
      printf(" rqid:%2d", ebch[j].req_id);
      printf(" opst:%d", status1);
      printf(" rqst:%d", status2);
      printf(" evid:%2d", ebch[j].event_id);
      printf(" tmsk:0x%4.4x\n", ebch[j].trigger_mask);
    }
  }

  return SUCCESS;
}

/*--------------------------------------------------------------------*/
INT source_unbooking()
{
  INT i, status;

  /* unbook all source channels */
  for (i = 0; i < nfragment; i++) {

    /* Skip unbooking if already done */
    if (ebch[i].pfragment != NULL) {
      bm_empty_buffers();

      /* Remove event ID registration */
      status = bm_delete_request(ebch[i].req_id);
      if (debug)
        printf("unbook: bm_delete_req[%d] req_id:%d stat:%d\n", i, ebch[i].req_id, status);

      /* Close source buffer */
      status = bm_close_buffer(ebch[i].hBuf);
      if (debug)
        printf("unbook: bm_close_buffer[%d] hndle:%d stat:%d\n", i, ebch[i].hBuf, status);
      if (status != BM_SUCCESS) {
        cm_msg(MERROR, "source_unbooking", "Close buffer[%d] stat:", i, status);
        return status;
      }
    }
  }

  /* release local event buffer memory */
  free_event_buffer(nfragment);

  return EB_SUCCESS;
}

/*--------------------------------------------------------------------*/
INT close_buffers(void)
{
  INT status;
  char error[256];
  EQUIPMENT *eq;

  eq = &equipment[0];

  /* Flush local destination cache */
  bm_flush_cache(equipment[0].buffer_handle, BM_WAIT);
  /* Call user function */
  eb_end_of_run(run_number, error);
  /* Cleanup buffers */
  handFlush();
  /* Detach all source from midas */
  status = source_unbooking();

  /* Compose message */
  stop_time = ss_millitime() - request_stop_time;
  sprintf(error, "Run %d Stop after %1.0lf + %d events sent DT:%d[ms]",
          run_number, eq->stats.events_sent, eq->events_sent, stop_time);
  cm_msg(MINFO, "close_buffers", "%s", error);

  run_state = STATE_STOPPED;
  abort_requested = FALSE;
  return status;
}

/********************************************************************/
/**
   Scan all the fragment source once per call.

   -# This will retrieve the full midas event not swapped (except the
   MIDAS_HEADER) for each fragment if possible. The fragment will
   be stored in the channel event pointer.
   -# if after a full nfrag path some frag are still not cellected, it
   returns with the frag# missing for timeout check.
   -# If ALL fragments are present it will check the midas serial#
   for a full match across all the fragments.
   -# If the serial check fails it returns with "event mismatch"
   and will abort the event builder but not stop the run for now.
   -# If the serial check is passed, it will call the user_build function
   where the destination event is going to be composed.

   @param fmt Fragment format type
   @param eq_info Equipement pointer
   @return   EB_NO_MORE_EVENT, EB_COMPOSE_TIMEOUT
   if different then SUCCESS (bm_compose, rpc_sent error)
*/
INT source_scan(INT fmt, EQUIPMENT_INFO * eq_info)
{
  BOOL complete;
  INT i, status, size;
  INT act_size;
  BOOL found, event_mismatch;
  BANK_HEADER *psbh;

  status = 0;

  /* Scan all channels at least once */
  for (i = 0; i < nfragment; i++) {
    if (!ebset.preqfrag[i] || ebset.received[i]) continue;

    status = get_a_fragment_event(fmt, i, &psbh);
  }

  /* Check if all fragments have been received */
  complete = FALSE;
  for (i = 0; i < nfragment; i++) {
    if (ebset.preqfrag[i] && !ebset.received[i])
      /* no data from some equipment */
      //return BM_ASYNC_RETURN;
      return EB_SKIP;
  }

  complete = TRUE;
  /* Check if serial matches */
  found = event_mismatch = FALSE;
  evb_serial = 0;
  /* Check Serial, mark first serial */
  for (i = 0; i < nfragment; i++) {
    if (ebset.preqfrag[i] && ebset.received[i] && !found) {
      evb_serial = ebch[i].serial;
      found = TRUE;
    } else {
      if (ebset.preqfrag[i] && ebset.received[i] && (evb_serial != ebch[i].serial)) {
        /* Event mismatch */
        event_mismatch = TRUE;
      }
    }
  }

  /* internal action in case of event mismatch */
  if (event_mismatch && debug) {
    char str[256];
    char strsub[128];
    strcpy(str, "event mismatch: ");
    for (i = 0; i < nfragment; i++) {
      sprintf(strsub, "Ser[%d]:%d ", i, ebch[i].serial);
      strcat(str, strsub);
    }
    printf("event serial mismatch %s\n", str);
  }

  /* In any case reset destination buffer */
  memset(dest_event, 0, sizeof(EVENT_HEADER));
  act_size = 0;

  /* Fill reserved header space of destination event with
     final header information */
  bm_compose_event((EVENT_HEADER *) dest_event, eq_info->event_id, eq_info->trigger_mask,
                   act_size, evb_serial);

  /* Pass fragments to user with mismatch flag, for final check before assembly */
  status =
    eb_user(nfragment, event_mismatch, ebch, (EVENT_HEADER *) dest_event,
            (void *) ((EVENT_HEADER *) dest_event + 1), &act_size);

  if (status != EB_SUCCESS) {
    if (status == EB_SKIP) {
      /* Reset mask and timeouts as if event has been successfully send out */
      for (i = 0; i < nfragment; i++) {
        ebch[i].timeout = 0;
        ebset.received[i] = FALSE;
      }
    }
    return status;         // Event mark as EB_SKIP or EB_ABORT by user
  }

  /* Allow bypass of fragment assembly if user did it on its own */
  if (!ebset.user_build) {
    for (i = 0; i < nfragment; i++) {
      if (ebset.preqfrag[i]) {
        status = meb_fragment_add(dest_event, ebch[i].pfragment, &act_size);
        if (status != EB_SUCCESS) {
          cm_msg(MERROR, "source_scan",
                 "compose fragment:%d current size:%d (%d)", i, act_size, status);
          return EB_ERROR;
        }
      }
    }
  }

  /* Overall event to be sent */
  act_size = ((EVENT_HEADER *) dest_event)->data_size + sizeof(EVENT_HEADER);

  /* Send event and wait for completion */
  status = rpc_send_event(equipment[0].buffer_handle, dest_event, act_size, BM_WAIT, 0);
  if (status != BM_SUCCESS) {
    if (debug)
      printf("rpc_send_event returned error %d, event_size %d\n", status, act_size);
    cm_msg(MERROR, "source_scan", "%s: rpc_send_event returned error %d", frontend_name, status);
    return EB_ERROR;
  }

  /* Keep track of the total byte count */
  equipment[0].bytes_sent += act_size;

  /* update destination event count */
  equipment[0].events_sent++;

  /* Reset mask and timeouts as even thave been succesfully send */
  for (i = 0; i < nfragment; i++) {
    ebch[i].timeout = 0;
    ebset.received[i] = FALSE;
  }
  // all fragment recieved for this event

  return status;
}

/***************************************************************/
INT get_a_fragment_event(const INT fmt, const INT idx, BANK_HEADER **ppsbh)
{
  INT size;
  INT status = 0;

  /* Get fragment and store it in ebch[i].pfragment */
  size   = max_event_size;
  status = bm_receive_event(ebch[idx].hBuf, ebch[idx].pfragment, &size, BM_NO_WAIT);
  //printf("call bm_receive_event from %s, serial %d, status %d\n", ebch[i].buffer, serial, status);

  switch (status) {
    /* ----------------------------------------------------------------
       event received
       ---------------------------------------------------------------- */
  case BM_SUCCESS:
    /* Mask event */
    ebset.received[idx] = TRUE;
    /* Keep local serial */
    ebch[idx].serial = ((EVENT_HEADER *) ebch[idx].pfragment)->serial_number;
    /* clear timeout */
    ebch[idx].timeout = 0;
    ebch[idx].time = time(NULL);

    /* Swap event depending on data format */
    switch (fmt) {
    case FORMAT_MIDAS:
      *ppsbh = (BANK_HEADER *) (((EVENT_HEADER *) ebch[idx].pfragment) + 1);
      bk_swap(*ppsbh, FALSE);
      break;
    }

    if (debug1) {
      printf("SUCC: ch:%d ser:%d rec:%d sz:%d\n", idx, ebch[idx].serial, ebset.received[idx], size);
    }
    break;

    /* ----------------------------------------------------------------
       time out
       ---------------------------------------------------------------- */
  case BM_ASYNC_RETURN:
    ebch[idx].timeout++;
    if (debug1) {
      printf("ASYNC: ch:%d ser:%d rec:%d sz:%d, timeout:%d\n", idx, ebch[idx].serial, ebset.received[idx], size, ebch[idx].timeout);
    }
    break;

    /* ----------------------------------------------------------------
       error
       ---------------------------------------------------------------- */
  default:
    cm_msg(MERROR, "get_a_fragment_event", "bm_receive_event error %d", status);
    break;
  }

  return status;
}

/********************************************************************/
/**
   Scan all the fragment source for strecalbt.

   -# This will retrieve the full midas event not swapped (except the
   MIDAS_HEADER) for each fragment if possible. The fragment will
   be stored in the channel event pointer.
   -# if after a full nfrag path some frag are still not cellected, it
   returns with the frag# missing for timeout check.
   -# If ALL fragments are present it will check the midas serial#
   for a full match across all the fragments.
   -# If the serial check fails it returns with "event mismatch"
   and will abort the event builder but not stop the run for now.
   -# If the serial check is passed, it will call the user_build function
   where the destination event is going to be composed.

   @param fmt Fragment format type
   @param eq_info Equipement pointer
   @return   EB_NO_MORE_EVENT, EB_COMPOSE_TIMEOUT
   if different then SUCCESS (bm_compose, rpc_sent error)
*/
INT source_scan_strecalbt(INT fmt, EQUIPMENT_INFO * eq_info)
{
  INT   i, idx, status, size;
  INT   act_size;
  DWORD match_num = -1, match_trgmst_num = -1;
  struct timeval match_time;
  struct timeval res;
  BOOL  found_num, found_trgmst_num, found_time;
  BOOL  anyone_skipped = FALSE;
  BOOL  event_mismatch;
  BOOL clear = FALSE;
  BANK_HEADER *psbh;
  status = 0;

  /* ================================================================
     Load data and store the event information values
     ================================================================ */
  /* Scan all channels at least once */
  for (i = 0; i < nfragment; i++) {
    /* get index in ebch */
    if (!ebset.preqfrag[i] || ebset.received[i]) continue;

    status = get_a_fragment_event(fmt, i, &psbh);

    if(status == BM_SUCCESS){
      /* trigger (master) number */
      if(ebch[i].build_type & (EB_BTYPE_TRGMST | EB_BTYPE_TRGNO) )
        extract_fragment_bank_data(psbh, "EI", 0, sizeof(DWORD), &ebch[i].trgno);

      /* counter number */
      if(ebch[i].build_type & EB_BTYPE_CNTNO)
        extract_fragment_bank_data(psbh, "EI", 0, sizeof(DWORD), &ebch[i].cntno);

      /* time stamp */
      if(ebch[i].build_type & EB_BTYPE_TSTMP)
        extract_fragment_bank_data(psbh, "EI", sizeof(DWORD), sizeof(struct timeval), &ebch[i].tstmp);
    }
  }

  /* Check if all fragments have been received */
  for (i = 0; i < nfragment; i++) {
    if (ebset.preqfrag[i] && !ebset.received[i])
      /* no data from some equipment */
      //return BM_ASYNC_RETURN;
      return EB_SKIP;
  }

  /* ================================================================
     Check event information (trigger / counter number or time stamp)
     ================================================================ */
  found_num = found_trgmst_num = found_time = anyone_skipped = FALSE;

  /* ----------------------------------------------------
     Searching for values that will be compared
     ---------------------------------------------------- */
  for (i = 0; i < nfragment; i++) {
    if(!ebset.preqfrag[i]) continue;

    /* trigger master's number is most prioritized.
       even if it was skipped previously.           */
    if(ebch[i].build_type & EB_BTYPE_TRGMST){
      if(!found_trgmst_num || ebch[i].trgno < match_trgmst_num){
        match_trgmst_num = ebch[i].trgno;
        found_trgmst_num = TRUE;
      }
      continue;
    }

    /* previously skipped events are not taken into account */
    if(ebset.skipped[i]){
      continue;
    }

    if(ebch[i].build_type & EB_BTYPE_TRGNO){
      if(!found_num || ebch[i].trgno < match_num){
        match_num = ebch[i].trgno;
        found_num = TRUE;
      }
    }
    else if(ebch[i].build_type & EB_BTYPE_CNTNO){
      if(!found_num || ebch[i].cntno < match_num){
        match_num = ebch[i].cntno;
        found_num = TRUE;
      }
    }

    if(ebch[i].build_type & EB_BTYPE_TSTMP &&
       (!found_time || timercmp(&ebch[i].tstmp, &match_time, <) )){
      match_time = ebch[i].tstmp;
      found_time = TRUE;
    }
  }
  if(!found_num && !found_trgmst_num && !found_time){
    printf("Error! No number and time to be checked was found.\n");
    return EB_ERROR;
  }

  /* When trgmst_number was found, match_num is forced to be overwritten by it */
  if(found_trgmst_num){
    found_num = TRUE;
    match_num = match_trgmst_num;
  }

  /* -----------------------------------------------------
     Check number / time consisntency
     ----------------------------------------------------- */
  event_mismatch = FALSE;

  for (i = 0; i < nfragment; i++) {
    /* this equipment is not reqired */
    if(!ebset.preqfrag[i]) continue;

    /* ---------------------------------------------------
       check trigger / counter number
       --------------------------------------------------- */
    if(found_num){
      /* trigger number */
      if(ebch[i].build_type & EB_BTYPE_TRGNO  || 
         ebch[i].build_type & EB_BTYPE_TRGMST ){
        if(debug) printf("Trigger number [%d] from %s: %d\n", i, ebch[i].buffer, ebch[i].trgno);

        /* When old event's match_num is less than match_num,
           delete this event, skip building, and read next event in the next call. */
        if(match_num > ebch[i].trgno){
          ebset.received[i] = FALSE;
          clear = TRUE;
        }
        else if(match_num < ebch[i].trgno){
          /* skip this event */
          //event_mismatch = TRUE;
#ifdef REQUIRE_ALL
          anyone_skipped = TRUE;
#else
          ebset.skip[i]  = TRUE;
          if(debug) printf("This trigger number %d from %s is skipped.\n", ebch[i].trgno, ebch[i].buffer);
#endif
        }
      }
      /* counter number */
      else if(ebch[i].build_type & EB_BTYPE_CNTNO){
        if(debug) printf("Counter number [%d]: %d\n", i, ebch[i].cntno);

        /* When old event's num is less than match_num,
           delete this event, skip building, and read next event in the next call. */
        if(match_num > ebch[i].cntno){
          ebset.received[i] = FALSE;
          clear = TRUE;
        }
        else if(match_num < ebch[i].cntno){
          /* skip this event */
          //event_mismatch = TRUE;
#ifdef REQUIRE_ALL
          anyone_skipped = TRUE;
#else
          ebset.skip[i]  = TRUE;
          if(debug) printf("This counter number is skipped.\n");
#endif
        }
      }
    } /* end of check of number */

    /* ---------------------------------------------------
       check time stamp
       --------------------------------------------------- */
    if(found_time && (ebch[i].build_type & EB_BTYPE_TSTMP)){

      /* When old event's num is less than match_num,
         delete this event, skip building, and read next event in the next call. */
      if(timercmp(&match_time, &ebch[i].tstmp, >)){
        ebset.received[i] = FALSE;
        clear = TRUE;
      }
      else{
        /* if match time is faster, check their time distance */
        timersub(&ebch[i].tstmp, &match_time, &res);
        if(debug) printf("time distance [%d]: %d usec\n", i, res.tv_sec*1000000 + res.tv_usec);

        if( res.tv_sec*1000000 + res.tv_usec > ebset.capa_latency ){
          /* skip this event */
          //event_mismatch = TRUE;
          ebset.skip[i]  = TRUE;
          if(debug) printf("This timestamp is skipped.\n");
        }

      }
    } /* end of check of time stamp */
  } /* end of for(nfragment) */

#ifdef REQUIRE_ALL
  if(anyone_skipped){
    for (i = 0; i < nfragment; i++) {
      /* this equipment is not reqired */
      if(!ebset.preqfrag[i]) continue;

      /* trigger master */
      if(ebch[i].build_type & EB_BTYPE_TRGMST){
        if(match_num == ebch[i].trgno){
          ebset.received[i] = FALSE;
          clear = TRUE;
        }
      }

      /* trigger number */
      if(ebch[i].build_type & EB_BTYPE_TRGNO){
        if(match_num == ebch[i].trgno){
          ebset.received[i] = FALSE;
          clear = TRUE;
        }
      }
      /* counter number */
      else if(ebch[i].build_type & EB_BTYPE_CNTNO){
        if(match_num == ebch[i].cntno){
          ebset.received[i] = FALSE;
          clear = TRUE;
        }
      }
    }
  }
#endif

  /* skip this event and read again */
  if(clear) return EB_SKIP;

  /* internal action in case of event mismatch */
  if (event_mismatch && debug) {
    char str[256];
    char strsub[128];
    strcpy(str, "event mismatch: ");
    for (i = 0; i < nfragment; i++) {
      sprintf(strsub, "Ser[%d]:%d ", i, ebch[i].serial);
      strcat(str, strsub);
    }
    printf("%s\n", str);
  }

  /* In any case reset destination buffer */
  memset(dest_event, 0, sizeof(EVENT_HEADER));
  act_size = 0;

  /* Fill reserved header space of destination event with
     final header information.
     Finally, increment static variable 'serial'.
  */
  bm_compose_event((EVENT_HEADER *) dest_event, eq_info->event_id, eq_info->trigger_mask,
                   act_size, evb_serial++);

  /* Pass fragments to user with mismatch flag, for final check before assembly */
  status =
    eb_user(nfragment, event_mismatch, ebch, (EVENT_HEADER *) dest_event,
            (void *) ((EVENT_HEADER *) dest_event + 1), &act_size);

  if (status != EB_SUCCESS) {
    if (status == EB_SKIP) {
      /* Reset mask and timeouts as if event has been successfully send out */
      for (i = 0; i < nfragment; i++) {
        ebch[i].timeout = 0;
        ebset.received[i] = FALSE;
      }
    }
    return status;         // Event mark as EB_SKIP or EB_ABORT by user
  }

  /* Allow bypass of fragment assembly if user did it on its own */
  if (!ebset.user_build) {
    for (i = 0; i < nfragment; i++) {
      /* when reqired and not assigned 'skip' flag */
      if (ebset.preqfrag[i] && !ebset.skip[i]) {
        status = meb_fragment_add(dest_event, ebch[i].pfragment, &act_size);
        if (status != EB_SUCCESS) {
          cm_msg(MERROR, "source_scan_strecalbt",
                 "compose fragment:%d current size:%d (%d)", i, act_size, status);
          return EB_ERROR;
        }
      }
    }
  }

  /* Overall event to be sent */
  act_size = ((EVENT_HEADER *) dest_event)->data_size + sizeof(EVENT_HEADER);

  /* Send event and wait for completion */
  status = rpc_send_event(equipment[0].buffer_handle, dest_event, act_size, BM_WAIT, 0);
  if (status != BM_SUCCESS) {
    if (debug)
      printf("rpc_send_event returned error %d, event_size %d\n", status, act_size);
    cm_msg(MERROR, "source_scan_strecalbt", "%s: rpc_send_event returned error %d", frontend_name, status);
    return EB_ERROR;
  }

  /* Keep track of the total byte count */
  equipment[0].bytes_sent += act_size;

  /* update destination event count */
  equipment[0].events_sent++;

  /* Reset mask and timeouts as even thave been succesfully send */
  for (i = 0; i < nfragment; i++) {
    ebch[i].timeout = 0;

    /* copy the status */
    ebset.skipped[i] = ebset.skip[i];

    /* if this event was skipped, keep its data to the next event building */
    if(ebset.skip[i]) ebset.skip[i] = FALSE;

    /* otherwise, clear data */
    else{
      ebset.received[i] = FALSE;
    }
  }
  // all fragment recieved for this event

  return status;
}

/********************************************************************/
INT extract_fragment_bank_data(char *psrce, char* bank_name,
                               INT offset, INT length, void* data)
{
  INT size, read_size;
  BANK *pbk = NULL;
  void *pdata;
  BOOL found = FALSE;

  size = ((EVENT_HEADER *) psrce)->data_size;
  if(size == 0){
    //cm_msg(MINFO, "extract_fragment_bank_data", "zero event size.");
    return -1;
  }

  /* search for the demand bank */
  while(1){
    size = bk_iterate(psrce, &pbk, &pdata);
    if(pbk == NULL) break;

    /* check if bank_name is contained */
    //printf("bank name = \"%s\"\n", pbk->name);
    if(strncmp(pbk->name, bank_name, strlen(bank_name)) == 0){
      found = TRUE;
      break;
    }
  }
  if(!found){
    /*
      cm_msg(MINFO, "extract_fragment_bank_data",
      "No bank named %s was found.", bank_name);
    */
    return -2;
  }

  /* set pointer */
  pdata = (char*)pdata + offset;

  /* copy */
  memcpy(data, pdata, length);

  return CM_SUCCESS;
}


/*--------------------------------------------------------------------*/
int main(int argc, char **argv)
{
  INT status, size, rstate;
  int i;
  BOOL daemon = FALSE;
  HNDLE hEqkey;
  EBUILDER(ebuilder_str);
  char str[128];
  int auto_restart = 0;
  int restart_count = 0;

  /* init structure */
  memset(&ebch[0], 0, sizeof(ebch));

  /* set default */
  cm_get_environment(host_name, sizeof(host_name), expt_name, sizeof(expt_name));

  /* set default buffer name */
  strcpy(buffer_name,      "SYSTEM");

  /* get parameters */
  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-' && argv[i][1] == 'd')
      debug = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'w')
      wheel = TRUE;
    else if (argv[i][0] == '-') {
      if (i + 1 >= argc || argv[i + 1][0] == '-')
        goto usage;
      if (strncmp(argv[i], "-e", 2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i], "-h", 2) == 0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i], "-b", 2) == 0)
        strcpy(buffer_name, argv[++i]);
    } else {
    usage:
      printf("usage: mevb [-h <Hostname>] [-e <Experiment>] [-b <buffername>] [-d] [-w] [-D]\n");
      printf("  [-h <Hostname>]    Host where midas experiment is running on\n");
      printf("  [-e <Experiment>]  Midas experiment if more than one exists\n");
      printf("  [-b <buffername>]  Specify evnet buffer name, use \"SYSTEM\" by default\n");
      printf("  [-d]               Print debugging output\n");
      printf("  [-w]               Show wheel\n");
      printf("  [-D]               Start as a daemon\n");
      return 0;
    }
  }

  // Print SVN revision
  strcpy(str, mevb_svn_revision+12);
  if (strchr(str, ' '))
    *strchr(str, ' ') = 0;
  printf("Program mevb, revision %s from ", str);
  strcpy(str, mevb_svn_revision+17);
  if (strchr(str, ' '))
    *strchr(str, ' ') = 0;
  printf("%s. Press \"!\" to exit.\n", str);

  if (daemon) {
    printf("Becoming a daemon...\n");
    ss_daemon_init(FALSE);
  }

  /* Connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, frontend_name, NULL);
  if (status != CM_SUCCESS) {
    ss_sleep(5000);
    goto exit;
  }

  if (debug)
    cm_set_watchdog_params(TRUE, 0);

  /* Connect to ODB */
  status = cm_get_experiment_database(&hDB, &hKey);
  if (status != EB_SUCCESS) {
    ss_sleep(5000);
    goto exit;
  }

  /* check if Ebuilder is already running */
  status = cm_exist(frontend_name, FALSE);
  if (status == CM_SUCCESS) {
    cm_msg(MERROR, "main", "%s running already!.", frontend_name);
    cm_disconnect_experiment();
    goto exit;
  }

  /* Check if run in progess if so abort */
  size = sizeof(rstate);
  db_get_value(hDB, 0, "/Runinfo/State", &rstate, &size, TID_INT, FALSE);
  if (rstate != STATE_STOPPED) {
    cm_msg(MERROR, "main", "Run in Progress, EBuilder aborted!.");
    cm_disconnect_experiment();
    goto exit;
  }

  if (ebuilder_init() != SUCCESS) {
    cm_disconnect_experiment();
    /* let user read message before window might close */
    ss_sleep(5000);
    goto exit;
  }

  /* Register single equipment */
  status = register_equipment();
  if (status != EB_SUCCESS) {
    ss_sleep(5000);
    goto exit;
  }

  /* Load Fragment info */
  status = load_fragment();
  if (status != EB_SUCCESS) {
    ss_sleep(5000);
    goto exit;
  }

  /* Register transition for reset counters */
  if (cm_register_transition(TR_START, tr_start, 400) != CM_SUCCESS)
    return status;
  if (cm_register_transition(TR_RESUME, tr_resume, 400) != CM_SUCCESS)
    return status;
  if (cm_register_transition(TR_PAUSE, tr_pause, 600) != CM_SUCCESS)
    goto exit;
  if (cm_register_transition(TR_STOP, tr_stop, 600) != CM_SUCCESS)
    goto exit;

 restart:

  /* Set Initial EB/Settings */
  sprintf(str, "/Equipment/%s/Settings", equipment[0].name);
  if (db_find_key(hDB, 0, str, &hEqkey) != DB_SUCCESS) {
    status = db_create_record(hDB, 0, str, strcomb(ebuilder_str));
  }

  if (auto_restart && restart_count > 0)
    {
      int run_number = 0;
      int size = sizeof(run_number);
      status = db_get_value(hDB, 0, "Runinfo/Run number", &run_number, &size, TID_INT, TRUE);
      assert(status == SUCCESS);

      cm_msg(MINFO, frontend_name, "Restart the run!");

      cm_transition(TR_START, run_number+1, NULL, 0, TR_SYNC, 0);
    }

  /* initialize ss_getchar */
  ss_getchar(0);

  /* Scan fragments... will stay in */
  status = scan_fragment();
  printf("%s-Out of scan_fragment\n", frontend_name);

  /* Detach all source from midas */
  printf("%s-Unbooking\n", frontend_name);
  source_unbooking();

  ebuilder_exit();

  auto_restart = 0;
  db_get_value(hDB, 0, "/Logger/Auto restart", &auto_restart, &size, TID_BOOL, FALSE);

  cm_msg(MINFO, frontend_name, "evb exit status %d, auto_restart %d", status, auto_restart);

  if (status == EB_USER_ERROR)
    {
      restart_count ++;
      goto restart;
    }

  /* reset terminal */
  ss_getchar(TRUE);

 exit:
  /* Free local memory */
  free_event_buffer(ebset.nfragment);

  /* Clean disconnect from midas */
  cm_disconnect_experiment();
  return 0;
}
