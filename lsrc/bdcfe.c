/********************************************************************   \

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "scaler event" which are filled with
                CAMAC or random data. The trigger event is filled
                with two banks (ADC0 and TDC0), the scaler event
                with one bank (SCLR).

  $Id$

\********************************************************************/

#define INCLUDE_SLOW


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"

#ifdef INCLUDE_SLOW
#include "cd_bdccont.h"
#include "dd_nimeasiroc.h"
#endif
#include "sitcp.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  /*-- Globals -------------------------------------------------------*/

  /* Constants */
  #define NCHANNELS 64
  #define SIZE_SC   57
  #define SIZE_PIN  6
  #define TIMEOUT   100

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = "Beam Define Counter";

  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = FALSE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 3000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 10000;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 10000;

  /* triggered timing in poll_event() */
  struct timeval timestamp;

  extern BOOL debug;

  /*-- Function declarations -----------------------------------------*/

  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();

  INT read_trigger_event(char *pevent, INT off);

  INT poll_event(INT source, INT count, BOOL test);
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

  // Initialize EASIROC
  INT init_easiroc();

  // Clear
  INT clear_easiroc();

#ifdef INCLUDE_SLOW
  /* device driver list */
  DEVICE_DRIVER bdccont_driver[] = {
    {"BDCCont%0d", dd_nimeasiroc, NCHANNELS, sitcp_udp, DF_MULTITHREAD, TRUE},
    {""}
  };
#endif

  /*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {

    {"BDC%0d",                /* equipment name */
     {1, 0,                   /* event ID, trigger mask */
      "BBUFF%0d",             /* event buffer */
      EQ_POLLED | EQ_EB,      /* equipment type */
      1,                      /* No event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING |            /* read only when running */
      RO_ODB,                 /* and update ODB */
      5,                      /* poll for 100ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",},
     read_trigger_event,      /* readout routine */
    },

#ifdef INCLUDE_SLOW
   {"BDC%0d Controller",        /* equipment name */
    {4, 0,                      /* event ID, trigger mask */
     "SCB%0d",                  /* event buffer */
     EQ_SLOW ,                  /* equipment type */
     0,                         /* event source */
     "MIDAS",                   /* format */
     TRUE,                      /* enabled */
     RO_RUNNING | RO_TRANSITIONS, /* read when running and on transitions */
     60000,                     /* read every 60 sec */
     30000,                     /* stop run after this event limit */
     0,                         /* number of sub events */
     1,                         /* log history every event */
     "", "", ""} ,
    cd_bdccont_read,            /* readout routine */
    cd_bdccont,                 /* class driver main routine */
    bdccont_driver,             /* device driver list */
    NULL,                       /* init string */
    },
#endif
    {""}
  };

  /*-- Frontend specific variables ------------------------------------*/

  /* ODB */
  HNDLE hDB;

  /* SITCP_INFO */
  void* sitcp_info = NULL;

  /* Bank names */
  char bk_name_data[5] = "BDC";
  char bk_name_info[5] = "EIB";

  /* Software counter */
  DWORD counter;

#ifdef __cplusplus
}
#endif

/********************************************************************   \
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  INT status;
  char str[256];
  HNDLE hKey;
  INT retry = 0;

  if(debug) display_period = 0;

  sprintf(bk_name_data, "BDC%0d", get_frontend_index());
  sprintf(bk_name_info, "EIB%0d", get_frontend_index());

  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  cm_get_experiment_database(&hDB, NULL);

  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &hKey);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_HW;
  }

  /*------------------------------------------------------
    Connect to EASIROC module
    ------------------------------------------------------*/
  while(1){
    if( SUCCESS == sitcp(CMD_INIT, hKey, &sitcp_info) &&
        SUCCESS == sitcp(CMD_OPEN, sitcp_info)){

      break;
    }

    if(++retry >= 5){
      printf("5 times failed to connect to %s.", equipment[0].name);
      ss_sleep(5000);
      return FE_ERR_HW;
    }

    printf("Error! %s's TCP/IP cannot be initialized.\n"
           "Please check /Equipment/%s/SiTCP/Host and Port. "
           "reconection will be tried 10 seconds after.\n", equipment[0].name, equipment[0].name);
    ss_sleep(10000);
  }

  // Show message
  /*
  printf("Connected to %s\n"
         "  Host:      %s\n"
         "  TCP Port:  %d\n"
         "  UDP Port:  %d\n"
         "  Debug:     %s\n",
         equipment[0].name,
         sitcp_info->settings.host,
         sitcp_info->settings.tcp_port,
         sitcp_info->settings.udp_port,
         (sitcp_info->settings.debug != 0)? "Yes":"No");
  */

  // Initialize EASIROC
  if(SUCCESS != init_easiroc()){
    return FE_ERR_HW;
  }

  printf("EASIROC Initialize completed.\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  clear_easiroc();
  end_of_run(0, NULL);
  sitcp(CMD_SITCP_ENABLE_NAGLE, sitcp_info, TRUE);
  sitcp(CMD_EXIT, sitcp_info);

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  DWORD signal = (32<<24) | (100<<16);

  printf("Waiting 5 sec for EROS/ROESTI gets ready.");
  ss_sleep(1000*5);

  clear_easiroc();

  /* clear software counter */
  counter = 0;

  sitcp(CMD_WRITE, sitcp_info, (char*)&signal, sizeof(signal));
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  /* DAQ Stop and counter reset */
  DWORD signal = (16<<24) | (100<<16);
  sitcp(CMD_WRITE, sitcp_info, (char*)&signal, sizeof(signal));
  ss_sleep(1);

  signal = 100 << 16;
  sitcp(CMD_WRITE, sitcp_info, (char*)&signal, sizeof(signal));

  ss_sleep(1);
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  DWORD buf;
  INT retry, status;

  for(retry=0; retry<count; retry++){
    status = sitcp(CMD_POLL, sitcp_info, &buf, sizeof(buf), 2);
    if(test) continue;
    if(status == sizeof(buf) && buf == 0xFFFFEA0C){
      gettimeofday(&timestamp, NULL); // get a timestamp immediately
      return TRUE; // Correct BDC event header
    }
  }
  return FALSE;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
  DWORD *pdata_dword, buff[100];
  WORD  *pdata_word;
  INT    word_num, event_count;
  INT i, recv_bytes;

  /* init bank structure */
  bk_init(pevent);

  // Read header (2 x DWORD)
  sitcp(CMD_READ, sitcp_info, (char*)buff, sizeof(DWORD)*2);

  // Word count and event counter
  word_num    = buff[0] & 0xFFFF;
  event_count = (buff[1]>>16) & 0xFFF;

  /* create structured BEI (BDC Event Info) bank */
  bk_create(pevent, bk_name_info, TID_DWORD, (void **)&pdata_dword);

  /* event counter with offset by 'counter' */
  *pdata_dword++ = event_count + counter;

  /* EASIROC's hardware counter has only 12 bit,
     so internally counting up instead. */
  if(event_count == 0xFFF) counter += 1<<12;

  /* time stamp */
  *(struct timeval*)pdata_dword = timestamp;
  pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);

  bk_close(pevent, pdata_dword);

  /* create structured BDC bank */
  bk_create(pevent, bk_name_data, TID_WORD, (void **)&pdata_word);

  // Read
  recv_bytes = 0;
  do{
    recv_bytes += sitcp(CMD_READ, sitcp_info, (char*)buff + recv_bytes,
                        word_num * sizeof(DWORD) - recv_bytes, 1);
  }while(recv_bytes < word_num*sizeof(DWORD));

  for(i=0; i<word_num; i++){

    *pdata_word++ = (WORD)(0xFFFF & buff[i]);
  }
  bk_close(pevent, pdata_word);

  return bk_size(pevent);
}

//=======================================================
INT clear_easiroc()
//=======================================================
{
  DWORD buff[100];
  while( 0 < sitcp(CMD_POLL, sitcp_info, (char*)buff, sizeof(buff), 1) );
  return SUCCESS;
}

//=======================================================
// write_datum_to_easiroc
//=======================================================
INT write_datum_to_easiroc(DWORD datum)
{
  DWORD buf = -1;

  // Write
  datum += 128 << 24;
  sitcp(CMD_WRITE, sitcp_info, (char*)&datum, sizeof(datum));

  // Receive
  sitcp(CMD_READ, sitcp_info, (char*)&buf, sizeof(buf));

  // Validation
  if(datum != buf){
    printf("### Datum Transmit Error ###\n"
           "Transmited datum is %x\n"
           "Returned datum is   %x\n,", datum, buf);
    return 0;
  }
  return SUCCESS;
}

//=======================================================
// debug_FPGA
//=======================================================
INT debug_FPGA(BDC_SETTINGS* settings)
{
  INT i;

  DWORD data;
  BYTE  pin_data[SIZE_PIN];

  // Zero filling
  for(i = 0; i<SIZE_PIN; i++) pin_data[SIZE_PIN];

  // Set bit to pin_data
#define BIT(name, idx) ( ( settings->fpga. name & 0x1) << idx )
  pin_data[0] = BIT(B0_1,1) | BIT(B0_2,2) | BIT(B0_4,4) | BIT(B0_6,6) | BIT(B0_7,7);
  pin_data[1] = BIT(B1_0,0) | BIT(B1_1,1) | BIT(B1_4,4) | BIT(B1_5,5) | BIT(B1_6,6) | BIT(B1_7,7);
  pin_data[3] = BIT(B3_0,0) | BIT(B3_5,5) | BIT(B3_6,6);
  pin_data[5] = BIT(B5_2,2) | BIT(B5_3,3) | BIT(B5_4,4);
#undef SETBIT

  // Send data to EASIROC
  for(i = 0; i<SIZE_PIN; i++){
    if(i==4) continue; // Skip

    data = i << 16 | (pin_data[i] & 0xFF) << 8;
    if(SUCCESS != write_datum_to_easiroc(data)) {
      printf("Error! data[%d]=0x%02x couldn't be sent to EASIROC\n", i, pin_data[i]);
      return 0;
    }
  }

  return SUCCESS;
}

//=======================================================
// transmit_SC
//=======================================================
INT transmit_SC(BDC_SETTINGS* settings, INT chipnum)
{
  INT i, ch, bit;

  BYTE  sc_data[SIZE_SC];
  INT   bit_count = 0, idx = SIZE_SC-1;

  // Zero filling
  for(i=0; i<SIZE_SC; i++) sc_data[i] = 0;

  // Select chip
  if(chipnum != 1 && chipnum != 2) return FE_ERR_HW;

  //-----------------------------------------------------
  // Filling sc_data
  //-----------------------------------------------------
  for(i=0; i<BDC_SCCONF_NUM; i++){
    struct BDC_SETTINGS_SCCONF* scconf = &(settings->sc_conf[chipnum-1][i]);

    for(ch = 0; ch<scconf->arraynum; ch++){
      for(bit = 0; bit<scconf->bitnum; ++bit){
        if(scconf->bitorder) sc_data[idx] = (sc_data[idx] << 1) | ((scconf->value[ch] >> (scconf->bitnum - bit - 1)) & 1);
        else                 sc_data[idx] = (sc_data[idx] << 1) | ((scconf->value[ch] >> bit) & 1);

        bit_count++;
        if(bit_count == 8){
          idx--;
          bit_count = 0;
        }
      }
    }
  }

  for(i = 0; i<SIZE_SC; ++i){
    for(bit = 0; bit<8; ++bit){
      if(((sc_data[i] >> bit) & 1)) printf("!");
      else                          printf(".");
    }
  }
  printf("\nFilled data.\n");

  //-----------------------------------------------------
  // Send to EASIROC
  //-----------------------------------------------------
  // Set SC mode
  if(SUCCESS != write_datum_to_easiroc((1<<16) | (240<<8))) return -1;

  // SC start
  for(i=0; i<SIZE_SC; i++){
    if(SUCCESS != write_datum_to_easiroc((10<<16) | ((sc_data[i] & 0xFF)<<8))) return -2;
  }

  // Start cycle
  if(SUCCESS != write_datum_to_easiroc((1<<16) | (242<<8))) return -3;
  if(SUCCESS != write_datum_to_easiroc((1<<16) | (240<<8))) return -4;

  // LoadSC
  if(SUCCESS != write_datum_to_easiroc((1<<16) | (241<<8))) return -5;
  if(SUCCESS != write_datum_to_easiroc((1<<16) | (240<<8))) return -6;

  return SUCCESS;
}

//=======================================================
// init_easiroc()
//=======================================================
INT init_easiroc()
{
  BDC_SETTINGS_STR(bdc_settings_str);
  BDC_SETTINGS bdc_settings;

  INT i, status, flag;
  char str[256];
  HNDLE hKey;

  //---------------------------------------------------------
  // Load configuration record
  //---------------------------------------------------------
  sprintf(str, "/Equipment/%s/Settings", equipment[0].name);
  status = db_create_record(hDB, 0, str, bdc_settings_str);
  status = db_find_key(hDB, 0, str, &hKey);
  status = db_open_record(hDB, hKey, &bdc_settings, sizeof(bdc_settings), MODE_READ, NULL, NULL);
  if(status != DB_SUCCESS){
    printf("Cannot open record /Equipment/%s/Settings\n", equipment[0].name);
    ss_sleep(5000);
    return 0;
  }
  /*
  for(i=0; i<BDC_SCCONF_NUM; i++){
    printf("%2d: %s / %d / %d\n", i,
           bdc_settings.sc_conf[0][i].name,
           bdc_settings.sc_conf[0][i].bitnum,
           bdc_settings.sc_conf[0][i].arraynum);
  }
  */

  //---------------------------------------------------------
  // Initialize
  //---------------------------------------------------------
  /*
  // Set timeout for initialize
  tv.tv_sec  = 0; tv.tv_usec = 100000;
  setsockopt(sitcp_info->tcpfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  flag = 1;
  setsockopt(sitcp_info->tcpfd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag));
  */

  // Enable Nagle
  if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, sitcp_info, TRUE)) return 0;

  // Parameters setting ----------------------------------------------------
  if(SUCCESS != debug_FPGA(&bdc_settings)){
    printf("DebugFPGA() failed.\n");
    return 0;
  }
  printf("ASIC Initialize : Done\n");
  ss_sleep(1);

  if(SUCCESS != write_datum_to_easiroc(37888)){
    printf("write_datum_to_easiroc(37888) failed.\n");
    return 0;
  }

  if(SUCCESS != transmit_SC(&bdc_settings, 1)){
    printf("TransmitSC() failed.\n");
    return 0;
  }
  printf("Slow Control chip1 : Done\n");

  ss_sleep(10);

  if(! write_datum_to_easiroc(21504)){
    printf("write_datum_to_easiroc(21504) failed.\n");
    return 0;
  }

  if(SUCCESS != transmit_SC(&bdc_settings, 2)){
    printf("TransmitSC() failed.\n");
    return 0;
  }
  printf("Slow Control chip2 : Done\n");

  if(! write_datum_to_easiroc(5120)){
    printf("write_datum_to_easiroc(5120) failed\n");
    return 0;
  }

  // Set DAQ mode
  if(! write_datum_to_easiroc((31 << 16) | (bdc_settings.daqmode) << 8)){
    printf("DAQ mode setting failed.\n");
    return 0;
  }
  printf("DAQ mode is set to %d.\n", bdc_settings.daqmode);

  // Disable Nagle
  if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, sitcp_info, FALSE)) return FE_ERR_HW;

  /*
  // Set timeout for DAQ
  tv.tv_sec  = 0; tv.tv_usec = 100000;
  setsockopt(sitcp_info->tcpfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));
  */

  return SUCCESS;
}
