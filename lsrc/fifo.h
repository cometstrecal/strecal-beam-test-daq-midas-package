typedef char FIFO_TYPE;

typedef struct FIFO_ELEMENT_t {
  unsigned int length;
  FIFO_TYPE* buff;
  struct FIFO_ELEMENT_t* next;
} FIFO_ELEMENT;

typedef struct {
  int size;
  FIFO_ELEMENT* first;
  FIFO_ELEMENT* last;
} FIFO;

int fifo_init(FIFO** fifo);
int fifo_delete(FIFO** fifo);

int fifo_clear(FIFO* fifo);
int fifo_get_size(FIFO* fifo);
int fifo_get_next_buff_length(FIFO* fifo);
FIFO_TYPE* fifo_get_next_buff_pointer(FIFO* fifo);

BOOL fifo_is_empty(FIFO* fifo);

int fifo_enqueue(FIFO* fifo, unsigned int length, FIFO_TYPE* buff);
int fifo_dequeue(FIFO* fifo, FIFO_TYPE* buff);
int fifo_trash_next(FIFO* fifo);
