/********************************************************************\

  Name:         raspi.h
  Created by:   Kou Oishi

  Contents:     Header file for Raspberry Pi bus driver

  $Id:$

\********************************************************************/
INT raspi(INT cmd, ...);
