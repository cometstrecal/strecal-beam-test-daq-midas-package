/********************************************************************\

  Name:         ad7924.c
  Created by:   Stefan Ritt
  Modified by:  Mark Wong

  Contents:     AD7924 Device Driver. This file can be used as a 
                template to write a read device driver

  $Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include "midas.h"
#include "raspi.h"
#include "wiringPi.h"
#include "ad7924.h"

/**
Raspberry Pi bus driver commands */
#define CMD_RASPI_SET_PINMODE       11000
#define CMD_RASPI_INIT_SPI          11001
#define CMD_RASPI_SPI_WR            11002

/*---- globals -----------------------------------------------------*/
#define MAX_CHANNELS 4
#define DEFAULT_TIMEOUT 10000   /* 10 sec. */

/* Store any parameters the device driver needs in following 
   structure. Edit the AD7924_SETTINGS_STR accordingly. This 
   contains usually the address of the device. For a CAMAC device
   this could be crate and station for example. */

typedef struct {
   INT spi_channel_adc;
   INT spi_channel_dac;
   INT spi_speed;               /* SPI clock speed (Hz)             */
   INT clock_mode;
} AD7924_SETTINGS;

#define AD7924_SETTINGS_STR "\
SPI Channel ADC = INT : 1\n\
SPI Channel DAC = INT : 0\n\
SPI Clock (Hz) = INT : 1000000\n\
Clock Mode = INT : 2\n\
"

/* following structure contains private variables to the device
   driver. It is necessary to store it here in case the device
   driver is used for more than one device in one frontend. If it
   would be stored in a global variable, one device could over-
   write the other device's variables. */

typedef struct {
   AD7924_SETTINGS ad7924_settings;
   INT num_channels;
    INT(*bd) (INT cmd, ...);    /* bus driver entry function */
   void *bd_info;               /* private info of bus driver */
   HNDLE hkey;                  /* ODB key for bus driver info */
} AD7924_INFO;

/*---- device driver routines --------------------------------------*/

/* the init function creates a ODB record which contains the
   settings and initialized it variables as well as the bus driver */

INT ad7924_init(HNDLE hkey, void **pinfo, INT channels, INT(*bd) (INT cmd, ...))
{
   int status, size;
   HNDLE hDB, hkeydd;
   AD7924_INFO *info;

   /* allocate info structure */
   info = calloc(1, sizeof(AD7924_INFO));
   *pinfo = info;

   cm_get_experiment_database(&hDB, NULL);

   /* create AD7924 settings record */
   status = db_create_record(hDB, hkey, "AD7924", AD7924_SETTINGS_STR);
   if (status != DB_SUCCESS)
      return FE_ERR_ODB;

   db_find_key(hDB, hkey, "AD7924", &hkeydd);
   size = sizeof(info->ad7924_settings);
   db_get_record(hDB, hkeydd, &info->ad7924_settings, &size, 0);

   /* initialize driver */
   if (channels > MAX_CHANNELS){
      cm_msg(MERROR, "ad7924", "Number of Channels: %d is too much.", channels);
      return FE_ERR_HW;
   }
   info->num_channels = channels;
   info->bd = bd;
   info->hkey = hkey;

   if (!bd)
      return FE_ERR_ODB;

   /* initialize bus driver */
   status = info->bd(CMD_INIT, info->hkey, &info->bd_info);

   if (status != SUCCESS)
      return status;

   /* initialization of device, something like ... */
   BD_PUTS("init");

   status = raspi(CMD_RASPI_INIT_SPI, info, info->ad7924_settings.spi_channel_adc, info->ad7924_settings.spi_speed, info->ad7924_settings.clock_mode);
   if (status != SUCCESS)
      return status;
   cm_msg(MINFO, "ad7924", "SPI CE1 initialized");

   status = raspi(CMD_RASPI_INIT_SPI, info, info->ad7924_settings.spi_channel_dac, info->ad7924_settings.spi_speed, info->ad7924_settings.clock_mode);
   if (status != SUCCESS)
      return status;
   cm_msg(MINFO, "ad7924", "SPI CE0 initialized");

   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT ad7924_exit(AD7924_INFO * info)
{
   /* call EXIT function of bus driver, usually closes device */
   info->bd(CMD_EXIT, info->bd_info);

   /* free local variables */
   if (info->num_channels)
      free(info->num_channels);

   free(info);

   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT ad7924_set(AD7924_INFO * info, INT channel, float value)
{
//   char str[80];
//
//   /* set channel to a specific value, something like ... */
//   sprintf(str, "SET %d %lf", channel, value);
//   BD_PUTS(str);
//   BD_GETS(str, sizeof(str), ">", DEFAULT_TIMEOUT);
//
//   /* simulate writing by storing value in local array, has to be removed
//      in a real driver */
//   if (channel < info->num_channels)
//      info->array[channel] = value;

   unsigned char buffer[3];
   int dynamic_range = 10;
   int status;
   if(channel == 0) {
     int dac = (int) ((value / dynamic_range) * ((1<<12) -1) );
     buffer[0] = (0x3<<4) | ((1 << (channel %4 )) & 0xF);
     buffer[1] = (dac>>4) & 0xFF;
     buffer[2] = (dac & 0xF)<<4;   
     status = wiringPiSPIDataRW(info->ad7924_settings.spi_channel_dac, buffer, 3);
     sleep(1);
     if(status < 0)
        return FE_ERR_HW;
     cm_msg(MINFO, "ad7924", "Changing settings for AD7924 board channel %d: value %f", channel, value);
   }
   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT ad7924_get(AD7924_INFO * info, INT channel, float *pvalue)
{
   int i, status;
   unsigned char buffer[2];

   /* read value from channel, something like ... */
   buffer[0] = 0xCB;
   buffer[1] = 0x90;
   status = wiringPiSPIDataRW(info->ad7924_settings.spi_channel_adc, buffer, 2);
   if(status < 0)
      return FE_ERR_HW;
   status = wiringPiSPIDataRW(info->ad7924_settings.spi_channel_adc, buffer, 2);
   if(status < 0)
      return FE_ERR_HW;
   sleep(1);
   int ch;
   int adcvalue[info->num_channels];
   for(i=0; i<info->num_channels; i++) {
     buffer[0] = 0x0;
     buffer[1] = 0x0;
     status = wiringPiSPIDataRW(info->ad7924_settings.spi_channel_adc, buffer, 2);
     if(status < 0)
        return FE_ERR_HW;
     ch = (buffer[0]>>4) & 0x3;
     adcvalue[ch] = ((buffer[0] & 0x0F) << 8) + buffer[1];
   }
   if(channel==0)
     *pvalue = adcvalue[0]*2*5/4096;
   if(channel==1)
     *pvalue = adcvalue[1]*2*100/4096;
   if(channel==2) {
     *pvalue = adcvalue[2]*0.215-428.7;
   }

   return FE_SUCCESS;
}

/*---- device driver entry point -----------------------------------*/

INT ad7924(INT cmd, ...)
{
   va_list argptr;
   HNDLE hKey;
   INT channel, status;
   DWORD flags;
   float value, *pvalue;
   void *info, *bd;

   va_start(argptr, cmd);
   status = FE_SUCCESS;

   switch (cmd) {
   case CMD_INIT:
      hKey = va_arg(argptr, HNDLE);
      info = va_arg(argptr, void *);
      channel = va_arg(argptr, INT);
      flags = va_arg(argptr, DWORD);
      bd = va_arg(argptr, void *);
      status = ad7924_init(hKey, info, channel, bd);
      break;

   case CMD_EXIT:
      info = va_arg(argptr, void *);
      status = ad7924_exit(info);
      break;

   case CMD_SET:
      info = va_arg(argptr, void *);
      channel = va_arg(argptr, INT);
      value = (float) va_arg(argptr, double);   // floats are passed as double
      status = ad7924_set(info, channel, value);
      break;

   case CMD_GET:
      info = va_arg(argptr, void *);
      channel = va_arg(argptr, INT);
      pvalue = va_arg(argptr, float *);
      status = ad7924_get(info, channel, pvalue);
      break;

   default:
      break;
   }

   va_end(argptr);

   return status;
}

/*------------------------------------------------------------------*/
