AD7924's interface
Only 3 channels are turned on in the frontend.

AD5684 is built into the AD7924 device driver. If you change the Demand value for a specific channel, it will send instructions to set the channel to the value you desire. Only channel 0 is used at this moment. Extending it is trivial. See ad7924_get

The raspi bus driver has been modified to include the lastest Model 3 build by Sony.
