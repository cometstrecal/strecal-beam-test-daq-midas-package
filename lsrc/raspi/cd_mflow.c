/********************************************************************\

  Name:         generic.c
  Created by:   Stefan Ritt

  Contents:     Generic Class Driver

  $Id$

\********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "ad7924.h"

typedef struct {

   /* ODB keys */
   HNDLE hKeyRoot, hKeyDemand, hKeyMeasured;

   /* globals */
   INT num_channels;
   INT format;
   INT last_channel;

   /* items in /Variables record */
   float *demand;
   float *measured;

   /* items in /Settings */
   char *names;
   float *update_threshold;

   /* mirror arrays */
   float *demand_mirror;
   float *measured_mirror;

   DEVICE_DRIVER **driver;
   INT *channel_offset;

} MFLOW_INFO;

#ifndef abs
#define abs(a) (((a) < 0)   ? -(a) : (a))
#endif

/*------------------------------------------------------------------*/

static void free_mem(MFLOW_INFO * mflow_info)
{
   free(mflow_info->names);
//   free(mflow_info->demand);
   free(mflow_info->measured);

   free(mflow_info->update_threshold);

   free(mflow_info->demand_mirror);
   free(mflow_info->measured_mirror);

   free(mflow_info->channel_offset);
   free(mflow_info->driver);

   free(mflow_info);
}

/*------------------------------------------------------------------*/

INT mflow_read(EQUIPMENT * pequipment, int channel)
{
   int i, status;
   MFLOW_INFO *mflow_info;
   HNDLE hDB;
   mflow_info = (MFLOW_INFO *) pequipment->cd_info;
   cm_get_experiment_database(&hDB, NULL);
   /* if driver is multi-threaded, read all channels at once */
   for (i=0 ; i < mflow_info->num_channels ; i++) {
    
      if (mflow_info->driver[i]->flags & DF_MULTITHREAD) {
         status = device_driver(mflow_info->driver[i], CMD_GET,
                                i - mflow_info->channel_offset[i],
                                &mflow_info->measured[i]);
      }
   }

   /* else read only single channel */
   if (!(mflow_info->driver[channel]->flags & DF_MULTITHREAD)) {
      status = device_driver(mflow_info->driver[channel], CMD_GET,
                             channel - mflow_info->channel_offset[channel],
                             &mflow_info->measured[channel]);
   }
   /* check for update measured */
   for (i = 0; i < mflow_info->num_channels; i++) {
      /* update if change is more than update_threshold */
      if ((ss_isnan(mflow_info->measured[i]) && !ss_isnan(mflow_info->measured_mirror[i])) ||
          (!ss_isnan(mflow_info->measured[i]) && ss_isnan(mflow_info->measured_mirror[i])) ||
          (!ss_isnan(mflow_info->measured[i]) && !ss_isnan(mflow_info->measured_mirror[i]) &&
           abs(mflow_info->measured[i] - mflow_info->measured_mirror[i]) >
           mflow_info->update_threshold[i])) {
         for (i = 0; i < mflow_info->num_channels; i++)
            mflow_info->measured_mirror[i] = mflow_info->measured[i];

         db_set_data(hDB, mflow_info->hKeyMeasured, mflow_info->measured,
                     sizeof(float) * mflow_info->num_channels, mflow_info->num_channels,
                     TID_FLOAT);

         pequipment->odb_out++;

         break;
      }
   }

   /*---- read demand value ----*/

   status = device_driver(mflow_info->driver[channel], CMD_GET_DEMAND,
                          channel - mflow_info->channel_offset[channel],
                          &mflow_info->demand[channel]);

   if ((mflow_info->demand[channel] != mflow_info->demand_mirror[channel] && !ss_isnan(mflow_info->demand[channel])) ||
       (ss_isnan(mflow_info->demand[channel]) && !ss_isnan(mflow_info->demand_mirror[channel])) ||
       (!ss_isnan(mflow_info->demand[channel]) && ss_isnan(mflow_info->demand_mirror[channel]))) {
      mflow_info->demand_mirror[channel] = mflow_info->demand[channel];
      db_set_data(hDB, mflow_info->hKeyDemand, mflow_info->demand,
                  sizeof(float) * mflow_info->num_channels, mflow_info->num_channels,
                  TID_FLOAT);
   }

   return status;
}

/*------------------------------------------------------------------*/

void mflow_demand(INT hDB, INT hKey, void *info)
{
   INT i, status;
   MFLOW_INFO *mflow_info;
   EQUIPMENT *pequipment;

   pequipment = (EQUIPMENT *) info;
   mflow_info = (MFLOW_INFO *) pequipment->cd_info;

   /* set individual channels only if demand value differs */
   for (i = 0; i < mflow_info->num_channels; i++)
      if (mflow_info->demand[i] != mflow_info->demand_mirror[i]) {
         if ((mflow_info->driver[i]->flags & DF_READ_ONLY) == 0) {
            status = device_driver(mflow_info->driver[i], CMD_SET,
                                   i - mflow_info->channel_offset[i], mflow_info->demand[i]);
         }
         mflow_info->demand_mirror[i] = mflow_info->demand[i];
      }

   pequipment->odb_in++;
}

/*------------------------------------------------------------------*/

void mflow_update_label(INT hDB, INT hKey, void *info)
{
   INT i, status;
   MFLOW_INFO *mflow_info;
   EQUIPMENT *pequipment;

   pequipment = (EQUIPMENT *) info;
   mflow_info = (MFLOW_INFO *) pequipment->cd_info;

   /* update channel labels based on the midas channel names */
   for (i = 0; i < mflow_info->num_channels; i++)
      status = device_driver(mflow_info->driver[i], CMD_SET_LABEL,
                             i - mflow_info->channel_offset[i],
                             mflow_info->names + NAME_LENGTH * i);
}

/*------------------------------------------------------------------*/

INT mflow_init(EQUIPMENT * pequipment)
{
   int status, size, i, j, index, offset;
   char str[256];
   HNDLE hDB, hKey, hNames, hThreshold;
   MFLOW_INFO *mflow_info;
   BOOL partially_disabled;

   /* allocate private data */
   pequipment->cd_info = calloc(1, sizeof(MFLOW_INFO));
   mflow_info = (MFLOW_INFO *) pequipment->cd_info;

   /* get class driver root key */
   cm_get_experiment_database(&hDB, NULL);
   sprintf(str, "/Equipment/%s", pequipment->name);
   db_create_key(hDB, 0, str, TID_KEY);
   db_find_key(hDB, 0, str, &mflow_info->hKeyRoot);

   /* save event format */
   size = sizeof(str);
   db_get_value(hDB, mflow_info->hKeyRoot, "Common/Format", str, &size, TID_STRING, TRUE);

   if (equal_ustring(str, "Fixed"))
      mflow_info->format = FORMAT_FIXED;
   else if (equal_ustring(str, "MIDAS"))
      mflow_info->format = FORMAT_MIDAS;
   else if (equal_ustring(str, "YBOS"))
      mflow_info->format = FORMAT_YBOS;

   /* count total number of channels */
   for (i = 0, mflow_info->num_channels = 0; pequipment->driver[i].name[0]; i++) {
      if (pequipment->driver[i].channels == 0) {
         cm_msg(MERROR, "mflow_init", "Driver with zero channels not allowed");
         return FE_ERR_ODB;
      }

      mflow_info->num_channels += pequipment->driver[i].channels;
   }

   if (mflow_info->num_channels == 0) {
      cm_msg(MERROR, "mflow_init", "No channels found in device driver list");
      return FE_ERR_ODB;
   }

   /* Allocate memory for buffers */
   mflow_info->names = (char *) calloc(mflow_info->num_channels, NAME_LENGTH);

   mflow_info->demand = (float *) calloc(mflow_info->num_channels, sizeof(float));
   mflow_info->measured = (float *) calloc(mflow_info->num_channels, sizeof(float));

   mflow_info->update_threshold = (float *) calloc(mflow_info->num_channels, sizeof(float));

   mflow_info->demand_mirror = (float *) calloc(mflow_info->num_channels, sizeof(float));
   mflow_info->measured_mirror = (float *) calloc(mflow_info->num_channels, sizeof(float));

   mflow_info->channel_offset = (INT *) calloc(mflow_info->num_channels, sizeof(INT));
   mflow_info->driver = (void *) calloc(mflow_info->num_channels, sizeof(void *));

   if (!mflow_info->driver) {
      cm_msg(MERROR, "mf_init", "Not enough memory");
      return FE_ERR_ODB;
   }

   /*---- Initialize device drivers ----*/

   /* call init method */
   for (i = 0; pequipment->driver[i].name[0]; i++) {
      sprintf(str, "Settings/Devices/%s", pequipment->driver[i].name);
      status = db_find_key(hDB, mflow_info->hKeyRoot, str, &hKey);
      if (status != DB_SUCCESS) {
         db_create_key(hDB, mflow_info->hKeyRoot, str, TID_KEY);
         status = db_find_key(hDB, mflow_info->hKeyRoot, str, &hKey);
         if (status != DB_SUCCESS) {
            cm_msg(MERROR, "mf_init", "Cannot create %s entry in online database", str);
            free_mem(mflow_info);
            return FE_ERR_ODB;
         }
      }

                                                                                            
      /* check enabled flag */
      size = sizeof(pequipment->driver[i].enabled);
      pequipment->driver[i].enabled = 1;
      sprintf(str, "Settings/Devices/%s/Enabled", pequipment->driver[i].name);
      status = db_get_value(hDB, mflow_info->hKeyRoot, str, &pequipment->driver[i].enabled
                            , &size, TID_BOOL, TRUE);
      if (status != DB_SUCCESS)
         return FE_ERR_ODB;
      
      if (pequipment->driver[i].enabled) {
         status = device_driver(&pequipment->driver[i], CMD_INIT, hKey);
         if (status != FE_SUCCESS) {
            free_mem(mflow_info);
            return status;
         }
      } else
         partially_disabled = TRUE;
   }

   /* compose device driver channel assignment */
   for (i = 0, j = 0, index = 0, offset = 0; i < mflow_info->num_channels; i++, j++) {
      while (j >= pequipment->driver[index].channels && pequipment->driver[index].name[0]) {
         offset += j;
         index++;
         j = 0;
      }

      mflow_info->driver[i] = &pequipment->driver[index];
      mflow_info->channel_offset[i] = offset;
   }

   /*---- create demand variables ----*/

   /* get demand from ODB */
   status =
       db_find_key(hDB, mflow_info->hKeyRoot, "Variables/Demand", &mflow_info->hKeyDemand);
   if (status == DB_SUCCESS) {
      size = sizeof(float) * mflow_info->num_channels;
      db_get_data(hDB, mflow_info->hKeyDemand, mflow_info->demand, &size, TID_FLOAT);
   }
   /* let device driver overwrite demand values, if it supports it */
   for (i = 0; i < mflow_info->num_channels; i++) {
      if (mflow_info->driver[i]->flags & DF_PRIO_DEVICE) {
         device_driver(mflow_info->driver[i], CMD_GET_DEMAND,
                       i - mflow_info->channel_offset[i], &mflow_info->demand[i]);
         mflow_info->demand_mirror[i] = mflow_info->demand[i];
      } else
         mflow_info->demand_mirror[i] = -12345.f; /* use -12345 as invalid value */
   }
   /* write back demand values */
   status =
       db_find_key(hDB, mflow_info->hKeyRoot, "Variables/Demand", &mflow_info->hKeyDemand);
   if (status != DB_SUCCESS) {
      db_create_key(hDB, mflow_info->hKeyRoot, "Variables/Demand", TID_FLOAT);
      db_find_key(hDB, mflow_info->hKeyRoot, "Variables/Demand", &mflow_info->hKeyDemand);
   }
   size = sizeof(float) * mflow_info->num_channels;
   db_set_data(hDB, mflow_info->hKeyDemand, mflow_info->demand, size,
               mflow_info->num_channels, TID_FLOAT);
   db_open_record(hDB, mflow_info->hKeyDemand, mflow_info->demand,
                  mflow_info->num_channels * sizeof(float), MODE_READ, mflow_demand,
                  pequipment);

   /*---- create measured variables ----*/
   db_merge_data(hDB, mflow_info->hKeyRoot, "Variables/Measured",
                 mflow_info->measured, sizeof(float) * mflow_info->num_channels,
                 mflow_info->num_channels, TID_FLOAT);
   db_find_key(hDB, mflow_info->hKeyRoot, "Variables/Measured", &mflow_info->hKeyMeasured);
   for (i=0 ; i<mflow_info->num_channels ; i++)
      mflow_info->measured[i] = (float)ss_nan();

   /*---- get default names from device driver ----*/
   for (i = 0; i < mflow_info->num_channels; i++) {
      sprintf(mflow_info->names + NAME_LENGTH * i, "Default%%CH %d", i);
      device_driver(mflow_info->driver[i], CMD_GET_LABEL,
                    i - mflow_info->channel_offset[i], mflow_info->names + NAME_LENGTH * i);
   }
   db_merge_data(hDB, mflow_info->hKeyRoot, "Settings/Names",
                 mflow_info->names, NAME_LENGTH * mflow_info->num_channels,
                 mflow_info->num_channels, TID_STRING);

   /*---- set labels form midas SC names ----*/
   for (i = 0; i < mflow_info->num_channels; i++) {
      mflow_info = (MFLOW_INFO *) pequipment->cd_info;
      device_driver(mflow_info->driver[i], CMD_SET_LABEL,
                    i - mflow_info->channel_offset[i], mflow_info->names + NAME_LENGTH * i);
   }

   /* open hotlink on channel names */
   if (db_find_key(hDB, mflow_info->hKeyRoot, "Settings/Names", &hNames) == DB_SUCCESS)
      db_open_record(hDB, hNames, mflow_info->names, NAME_LENGTH*mflow_info->num_channels,
                     MODE_READ, mflow_update_label, pequipment);

   /*---- get default update threshold from device driver ----*/
   for (i = 0; i < mflow_info->num_channels; i++) {
      mflow_info->update_threshold[i] = 1.f;      /* default 1 unit */
      device_driver(mflow_info->driver[i], CMD_GET_THRESHOLD,
                    i - mflow_info->channel_offset[i], &mflow_info->update_threshold[i]);
   }
   db_merge_data(hDB, mflow_info->hKeyRoot, "Settings/Update Threshold Measured",
                 mflow_info->update_threshold, sizeof(float)*mflow_info->num_channels,
                 mflow_info->num_channels, TID_FLOAT);

   /* open hotlink on update threshold */
   if (db_find_key(hDB, mflow_info->hKeyRoot, "Settings/Update Threshold Measured", &hThreshold) == DB_SUCCESS)
      db_open_record(hDB, hThreshold, mflow_info->update_threshold, sizeof(float)*mflow_info->num_channels,
                     MODE_READ, NULL, NULL);

   /*---- set initial demand values ----*/
   mflow_demand(hDB, mflow_info->hKeyDemand, pequipment);

   /* initially read all channels */
   for (i = 0; i < mflow_info->num_channels; i++)
      mflow_read(pequipment, i);

   return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT mflow_start(EQUIPMENT * pequipment)
{
   INT i;

   /* call start method of device drivers */
   for (i = 0; pequipment->driver[i].dd != NULL ; i++)
      if (pequipment->driver[i].flags & DF_MULTITHREAD) {
         pequipment->driver[i].pequipment = &pequipment->info;
         device_driver(&pequipment->driver[i], CMD_START);
      }

  return FE_SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT mflow_stop(EQUIPMENT * pequipment)
{
   INT i;

   /* call stop method of device drivers */
   for (i = 0; pequipment->driver[i].dd != NULL && pequipment->driver[i].flags & DF_MULTITHREAD ; i++)
      device_driver(&pequipment->driver[i], CMD_STOP);

   return FE_SUCCESS;
}

/*------------------------------------------------------------------*/

INT mflow_exit(EQUIPMENT * pequipment)
{
   INT i;

   free_mem((MFLOW_INFO *) pequipment->cd_info);

   /* call exit method of device drivers */
   for (i = 0; pequipment->driver[i].dd != NULL; i++)
      device_driver(&pequipment->driver[i], CMD_EXIT);

   return FE_SUCCESS;
}

/*------------------------------------------------------------------*/

INT mflow_idle(EQUIPMENT * pequipment)
{
   INT act, status;
   MFLOW_INFO *mflow_info;

   mflow_info = (MFLOW_INFO *) pequipment->cd_info;

   /* select next measurement channel */
   act = (mflow_info->last_channel + 1) % mflow_info->num_channels;

   /* measure channel */
   status = mflow_read(pequipment, act);
   mflow_info->last_channel = act;

   return status;
}

/*------------------------------------------------------------------*/

INT cd_mflow_read(char *pevent, int offset)
{
   float *pdata;
   MFLOW_INFO *mflow_info;
   EQUIPMENT *pequipment;
#ifdef HAVE_YBOS
   DWORD *pdw;
#endif

   pequipment = *((EQUIPMENT **) pevent);
   mflow_info = (MFLOW_INFO *) pequipment->cd_info;

   if (mflow_info->format == FORMAT_FIXED) {
      memcpy(pevent, mflow_info->demand, sizeof(float) * mflow_info->num_channels);
      pevent += sizeof(float) * mflow_info->num_channels;

      memcpy(pevent, mflow_info->measured, sizeof(float) * mflow_info->num_channels);
      pevent += sizeof(float) * mflow_info->num_channels;

      return 2 * sizeof(float) * mflow_info->num_channels;
   } else if (mflow_info->format == FORMAT_MIDAS) {
      bk_init(pevent);

      /* create DMND bank */
      bk_create(pevent, "DMND", TID_FLOAT, (void **)&pdata);
      memcpy(pdata, mflow_info->demand, sizeof(float) * mflow_info->num_channels);
      pdata += mflow_info->num_channels;
      bk_close(pevent, pdata);

      /* create MSRD bank */
      bk_create(pevent, "MSRD", TID_FLOAT, (void **)&pdata);
      memcpy(pdata, mflow_info->measured, sizeof(float) * mflow_info->num_channels);
      pdata += mflow_info->num_channels;
      bk_close(pevent, pdata);

      return bk_size(pevent);
   } else if (mflow_info->format == FORMAT_YBOS) {
#ifdef HAVE_YBOS
      ybk_init((DWORD *) pevent);

      /* create EVID bank */
      ybk_create((DWORD *) pevent, "EVID", I4_BKTYPE, (DWORD *) (&pdw));
      *(pdw)++ = EVENT_ID(pevent);      /* Event_ID + Mask */
      *(pdw)++ = SERIAL_NUMBER(pevent); /* Serial number */
      *(pdw)++ = TIME_STAMP(pevent);    /* Time Stamp */
      ybk_close((DWORD *) pevent, pdw);

      /* create DMND bank */
      ybk_create((DWORD *) pevent, "DMND", F4_BKTYPE, (DWORD *) & pdata);
      memcpy(pdata, mflow_info->demand, sizeof(float) * mflow_info->num_channels);
      pdata += mflow_info->num_channels;
      ybk_close((DWORD *) pevent, pdata);

      /* create MSRD bank */
      ybk_create((DWORD *) pevent, "MSRD", F4_BKTYPE, (DWORD *) & pdata);
      memcpy(pdata, mflow_info->measured, sizeof(float) * mflow_info->num_channels);
      pdata += mflow_info->num_channels;
      ybk_close((DWORD *) pevent, pdata);

      return ybk_size((DWORD *) pevent);
#else
      assert(!"YBOS support not compiled in");
#endif
   }

   return 0;
}

/*------------------------------------------------------------------*/

INT cd_mflow(INT cmd, EQUIPMENT * pequipment)
{
   INT status;

   switch (cmd) {
   case CMD_INIT:
      status = mflow_init(pequipment);
      break;

   case CMD_START:
     status = mflow_start(pequipment);
     break;
     
   case CMD_STOP:
      status = mflow_stop(pequipment);
      break;

   case CMD_EXIT:
      status = mflow_exit(pequipment);
      break;

   case CMD_IDLE:
      status = mflow_idle(pequipment);
      break;

   default:
      cm_msg(MERROR, "Generic class driver", "Received unknown command %d", cmd);
      status = FE_ERR_DRIVER;
      break;
   }

   return status;
}
