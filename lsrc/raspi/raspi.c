/********************************************************************\

  Name:         raspi.c
  Created by:   Kou Oishi

  Contents:     Raspberry Pi GPIO/SPI bus communication routines

  $Id$

\********************************************************************/

#include "midas.h"
#include "msystem.h"
#include <stdlib.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>

/**
Raspberry Pi bus driver commands */
#define CMD_RASPI_SET_PINMODE       11000
#define CMD_RASPI_INIT_SPI          11001
#define CMD_RASPI_SPI_WR            11002

#define BIT(b) (1<<b)
#define RASPI_PIN_FUNC_DNC       BIT(0)
#define RASPI_PIN_FUNC_GND       BIT(1)
#define RASPI_PIN_FUNC_POW       BIT(2)
#define RASPI_PIN_FUNC_GPIO      BIT(3)
#define RASPI_PIN_FUNC_UART_TxD  BIT(4)
#define RASPI_PIN_FUNC_UART_RxD  BIT(5)
#define RASPI_PIN_FUNC_SPI_SCLK  BIT(6)
#define RASPI_PIN_FUNC_SPI_MOSI  BIT(7)
#define RASPI_PIN_FUNC_SPI_MISO  BIT(8)
#define RASPI_PIN_FUNC_SPI_CE0   BIT(9)
#define RASPI_PIN_FUNC_SPI_CE1   BIT(10)
#define RASPI_PIN_FUNC_I2C_SDA   BIT(11)
#define RASPI_PIN_FUNC_I2C_SCL   BIT(12)

#define RASPI_PIN_FUNC_UART      (RASPI_PIN_FUNC_UART_TxD | RASPI_PIN_FUNC_UART_RxD)
#define RASPI_PIN_FUNC_SPI \
  (RASPI_PIN_FUNC_SPI_SCLK | RASPI_PIN_FUNC_SPI_MOSI |                  \
   RASPI_PIN_FUNC_SPI_MISO | RASPI_PIN_FUNC_SPI_CE0  | RASPI_PIN_FUNC_SPI_CE1 )
#define RASPI_PIN_FUNC_I2C       (RASPI_PIN_FUNC_I2C_SDA | RASPI_PIN_FUNC_I2C_SCL)

#define RASPI_PIN_ASSIGN_BOARD   BIT(1)
#define RASPI_PIN_ASSIGN_BCM     BIT(2)
#define RASPI_PIN_ASSIGN_WPI     BIT(3)

typedef struct {
  int bd;           /* BOARD pin assignment              */
  int bcm;          /* BCM pin assignment                */
  int wpi;          /* wiringPi pin assignment           */
  const char* name; /* pin name                          */
  int func;         /* functionality                     */
} RASPI_PIN_INFO;

/* 
   Pin assignment (all other pin variables are extracted from this) 
   Model A+/B+, 2 ModelB, or 3 ModelB.
*/
#define RASPI_PIN_NUM_Ap_Bp_2B 40
const RASPI_PIN_INFO RASPI_PIN_Ap_Bp_2B[RASPI_PIN_NUM_Ap_Bp_2B] = 
  {
    { 1, -1, -1, "3.3V"         , RASPI_PIN_FUNC_POW                            },
    { 2, -1, -1, "5V"           , RASPI_PIN_FUNC_POW                            },
    { 3,  2,  8, "GPIO2(SDA)"   , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_I2C_SDA  },
    { 4, -1, -1, "5V"           , RASPI_PIN_FUNC_POW                            },
    { 5,  3,  9, "GPIO3(SCL)"   , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_I2C_SCL  },
    { 6, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    { 7,  4,  7, "GPIO4(GCLK)"  , RASPI_PIN_FUNC_GPIO                           },
    { 8, 14, 15, "GPIO14(TxD)"  , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_UART_TxD },
    { 9, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {10, 15, 16, "GPIO15(RxD)"  , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_UART_RxD },
    {11, 17,  0, "GPIO17"       , RASPI_PIN_FUNC_GPIO                           },
    {12, 18,  1, "GPIO18"       , RASPI_PIN_FUNC_GPIO                           },
    {13, 27,  2, "GPIO27"       , RASPI_PIN_FUNC_GPIO                           },
    {14, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {15, 22,  3, "GPIO22"       , RASPI_PIN_FUNC_GPIO                           },
    {16, 23,  4, "GPIO23"       , RASPI_PIN_FUNC_GPIO                           },
    {17, -1, -1, "3.3V"         , RASPI_PIN_FUNC_POW                            },
    {18, 24,  5, "GPIO24"       , RASPI_PIN_FUNC_GPIO                           },
    {19, 10, 12, "GPIO10(MOSI)" , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_SPI_MOSI },
    {20, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {21,  9, 13, "GPIO9(MISO)"  , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_SPI_MISO },
    {22, 25,  6, "GPIO25"       , RASPI_PIN_FUNC_GPIO                           },
    {23, 11, 14, "GPIO11(SCLK)" , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_SPI_SCLK },
    {24,  8, 10, "GPIO8(CE0)"   , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_SPI_CE0  },
    {25, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {26,  7, 11, "GPIO7(CE1)"   , RASPI_PIN_FUNC_GPIO | RASPI_PIN_FUNC_SPI_CE1  },
    {27,  0, 30, "GPIO0"        , RASPI_PIN_FUNC_DNC                            },
    {28,  1, 31, "GPIO1"        , RASPI_PIN_FUNC_DNC                            },
    {29,  5, 21, "GPIO5"        , RASPI_PIN_FUNC_GPIO                           },
    {30, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {31,  6, 22, "GPIO6"        , RASPI_PIN_FUNC_GPIO                           },
    {32, 12, 26, "GPIO12"       , RASPI_PIN_FUNC_GPIO                           },
    {33, 13, 23, "GPIO13"       , RASPI_PIN_FUNC_GPIO                           },
    {34, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {35, 19, 24, "GPIO19"       , RASPI_PIN_FUNC_GPIO                           },
    {36, 16, 27, "GPIO16"       , RASPI_PIN_FUNC_GPIO                           },
    {37, 26, 25, "GPIO26"       , RASPI_PIN_FUNC_GPIO                           },
    {38, 20, 28, "GPIO20"       , RASPI_PIN_FUNC_GPIO                           },
    {39, -1, -1, "GND"          , RASPI_PIN_FUNC_GND                            },
    {40, 21, 29, "GPIO21"       , RASPI_PIN_FUNC_GPIO                           }
  };

static int debug_last = 0, debug_first = TRUE;

typedef struct {
  char pin_assign_type[256]; /* GPIO pin assignment: "BOARD", "BCM", or "wiringPi" */
  int  debug;
} RASPI_SETTINGS;

#define RASPI_SETTINGS_STR "\
PinAssignType = STRING : [256] wiringPi\n\
Debug = INT : 0\n\
"

typedef struct {
  RASPI_SETTINGS settings;

  int num_avail_pins;      /* number of available pins */
  int pin_assign_type;     /* pin assignment type */
  const RASPI_PIN_INFO* pin_def; /* pin definition (depends on revision)*/

  BOOL uart_used, spi_used, i2c_used; /* flag of functionality */

  /* pin_info index for special use pin */
  int uart_txd, uart_rxd;             
  int spi_sclk, spi_mosi, spi_miso, spi_ce0, spi_ce1;
  int i2c_sda, i2c_scl;
  
  /* arrays */
  BOOL* pin_used; /* flag to prevent double-initialize: index is reffered by board num - 1*/
  
} RASPI_INFO;

/*----------------------------------------------------------------------------*/

void raspi_debug(RASPI_INFO * info, char *dbg_str)
{
  FILE *f;
  int delta;

  if (debug_last == 0)
    delta = 0;
  else
    delta = ss_millitime() - debug_last;
  debug_last = ss_millitime();

  f = fopen("raspi.log", "a");

  if (debug_first)
    fprintf(f, "\n==== new session =============\n\n");
  debug_first = FALSE;
  fprintf(f, "{%d} %s\n", delta, dbg_str);

  if (info->settings.debug > 1)
    printf("{%d} %s\n", delta, dbg_str);

  fclose(f);
}

/*------------------------------------------------------------------*/

int raspi_get_revision(char* rname, int size)
{
  FILE* fp;
  char* cmdline = "awk '/Revision/ {print $3}' /proc/cpuinfo | sed 's/1000//'";
  int i;
  
  if ( (fp=popen(cmdline, "r")) == NULL ||
       fgets(rname, size, fp) == NULL ) {
    cm_msg(MERROR, "raspi", "Invalid /proc/cpuinfo");
    return -1;
  }
  pclose(fp);

  for(i=0; i<size; i++){
    if(rname[i] == '\n'){
      rname[i] = 0; /* null char */
      break;
    }
  }
  
  return SUCCESS;
}

/*------------------------------------------------------------------*/

int raspi_set_pin_definition(RASPI_INFO* info)
{
  char rev[100] = "";
  INT  p;
  /* get revision name */
  if(SUCCESS != raspi_get_revision(rev, sizeof(rev))) return -1;

  /* switch for each name */
  if      (0==strcmp(rev, "Beta") || 
           0==strcmp(rev, "0002") || 
           0==strcmp(rev, "0003") ){
    cm_msg(MERROR, "raspi", "This revision is not supported yet.");
    return -1;
  }
  else if (0==strcmp(rev, "0004") || 
           0==strcmp(rev, "0005") || 
           0==strcmp(rev, "0006") || 
           0==strcmp(rev, "0007") || 
           0==strcmp(rev, "0008") || 
           0==strcmp(rev, "0009") ){
    cm_msg(MERROR, "raspi", "This revision is not supported yet.");
    return -1;
  }
  else if (0==strcmp(rev, "000d") || 
           0==strcmp(rev, "000e") || 
           0==strcmp(rev, "000f") ){
    cm_msg(MERROR, "raspi", "This revision is not supported yet.");
    return -1;
  }
  else if (0==strcmp(rev, "0010") || 
           0==strcmp(rev, "0011") || 
           0==strcmp(rev, "0012") ||
           0==strcmp(rev, "a01041") ||
           0==strcmp(rev, "121041") ||
           0==strcmp(rev, "a02082") ||
           0==strcmp(rev, "a32082")
       ){
    cm_msg(MINFO, "raspi", "Using Raspberry Pi Model A+/B+, 2B, or 3B.");
    info->pin_def = RASPI_PIN_Ap_Bp_2B;
    info->num_avail_pins = RASPI_PIN_NUM_Ap_Bp_2B;
    printf("available pins = %d\n", info->num_avail_pins);
    ss_sleep(1000);
  }
  else if (0==strcmp(rev, "900092")){
    cm_msg(MERROR, "raspi", "This revision is not supported yet.");
    return -1;
  }
  else{
    cm_msg(MERROR, "raspi", "Revision %s is invalid.", rev);
    return -1;
  }

  /* set special use pin board number */
  for(p=0; p<info->num_avail_pins; p++){
    switch(info->pin_def[p].func){
    case RASPI_PIN_FUNC_UART_TxD: info->uart_txd = p; break;
    case RASPI_PIN_FUNC_UART_RxD: info->uart_rxd = p; break;
    case RASPI_PIN_FUNC_SPI_SCLK: info->spi_sclk = p; break;
    case RASPI_PIN_FUNC_SPI_MOSI: info->spi_mosi = p; break;
    case RASPI_PIN_FUNC_SPI_MISO: info->spi_miso = p; break;
    case RASPI_PIN_FUNC_SPI_CE0 : info->spi_ce0  = p; break;
    case RASPI_PIN_FUNC_SPI_CE1 : info->spi_ce1  = p; break;
    case RASPI_PIN_FUNC_I2C_SDA : info->i2c_sda  = p; break;
    case RASPI_PIN_FUNC_I2C_SCL : info->i2c_scl  = p; break;
    }
  }

  return SUCCESS;
}

/*------------------------------------------------------------------*/

const RASPI_PIN_INFO* raspi_get_pin_info(RASPI_INFO* info, int pin)
{
  int i;

  switch(info->pin_assign_type){
  case RASPI_PIN_ASSIGN_BOARD:
    for(i=0; i<info->num_avail_pins; i++){
      //printf("pin_def[%d].bd = %d\n", i, info->pin_def[i].bd);
      if(info->pin_def[i].bd == pin) return &(info->pin_def[i]);
    }
    break;
    
  case RASPI_PIN_ASSIGN_BCM:
    for(i=0; i<info->num_avail_pins; i++){
      //printf("pin_def[%d].bcm = %d\n", i, info->pin_def[i].bcm);
      if(info->pin_def[i].bcm == pin) return &(info->pin_def[i]);
    }
    break;

  case RASPI_PIN_ASSIGN_WPI:
    for(i=0; i<info->num_avail_pins; i++){
      //printf("pin_def[%d].wpi = %d\n", i, info->pin_def[i].wpi);
      if(info->pin_def[i].wpi == pin) return &(info->pin_def[i]);
    }
    break;
    
  default:
    cm_msg(MERROR, "raspi", "Invalid pin assign mode is set");
    break;
  }
  printf("Couldn't find pin %d in %d pins.\n", pin, info->num_avail_pins);
  return NULL;
}

/*------------------------------------------------------------------*/

int raspi_pin_is_used(RASPI_INFO* info, const RASPI_PIN_INFO* pin_info, BOOL* is_used)
{
  /* check if the pin is used already */
  *is_used = info->pin_used[pin_info->bd - 1];
  
  return SUCCESS;
}

/*------------------------------------------------------------------*/

int raspi_use_pin(RASPI_INFO * info, const RASPI_PIN_INFO* pin_info)
{
  BOOL is_used;
  if(SUCCESS != raspi_pin_is_used(info, pin_info, &is_used)){
    return -1;
  }
  if(is_used){
    cm_msg(MINFO, "raspi", "Pin %d (BOARD) is already used.", pin_info->bd);
    return -1;
  }
  
  /* set flag */
  info->pin_used[pin_info->bd - 1] = TRUE;
  
  return SUCCESS;
}

/*------------------------------------------------------------------*/

int raspi_exit(RASPI_INFO * info)
{
  if(info){
    free(info->pin_used);
    free(info);
  }  
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_open(RASPI_INFO * info)
{
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_close(RASPI_INFO * info)
{
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_write(RASPI_INFO * info, INT pin, BOOL state)
{
  const RASPI_PIN_INFO* pin_info = raspi_get_pin_info(info, pin);
  if(! pin_info) return -1;
  
  digitalWrite(pin_info->wpi, state? 1:0);
  
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_read(RASPI_INFO * info, INT pin, BOOL* state)
{
  INT in;
  const RASPI_PIN_INFO* pin_info = raspi_get_pin_info(info, pin);
  if(! pin_info) return -1;
  
  in = digitalRead(pin_info->wpi);
  *state = (in? TRUE:FALSE);
  
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_spi_wr(RASPI_INFO * info, INT ch, BYTE* data, INT size)
{
//  if(! info->spi_used){
//    cm_msg(MERROR, "raspi", "SPI is not initialized.");
//    return -1;
//  }
  
  wiringPiSPIDataRW(ch, data, size);
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_set_pinmode(RASPI_INFO * info, INT pin, INT conf)
{
  int status;
  const RASPI_PIN_INFO* pin_info = raspi_get_pin_info(info, pin);
  if(!pin_info){
    cm_msg(MERROR, "raspi", "Set pin %d is invalid.", pin);
    return -1;
  }
  //printf("pininfo: %d\n", pin_info->wpi);
  if(info->spi_used){
    BOOL is_used = FALSE;
    
    if(pin_info->func & RASPI_PIN_FUNC_SPI){
      
      status = raspi_pin_is_used(info, pin_info, &is_used);
      if(status != SUCCESS){
        return -1;
      }
      if(is_used){
        cm_msg(MERROR, "raspi",
               "Cannot set this pin (%d: Board) mode because SPI is already set.",
               pin_info->bd);
        return -1;
      }
    }
  }

  if(conf == INPUT || conf == OUTPUT){
    printf("Set now pin mode to %d on %d\n", conf, pin_info->wpi);
    pinMode(pin_info->wpi, conf);
  }
  else{
    cm_msg(MERROR, "raspi", "Invalid configuration.");
    return -1;
  }
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

int raspi_init(HNDLE hkey, void **pinfo)
{
  HNDLE hDB, hkeybd;
  INT size, status, i;
  RASPI_INFO *info;
  
  /* allocate info structure */
  info = calloc(1, sizeof(RASPI_INFO));
  /* memory allocation error is fatal. */
  if(!info){
    cm_msg(MERROR, "raspi", "Failed to calloc for RASPI_INFO");
    cm_disconnect_experiment();
    exit(1);
  }
  *pinfo = info;
   
  cm_get_experiment_database(&hDB, NULL);
   
  /* create RASPI settings record */
  status = db_create_record(hDB, hkey, "RaspberryPi", RASPI_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
   
  db_find_key(hDB, hkey, "RaspberryPi", &hkeybd);
  size = sizeof(info->settings);
  db_get_record(hDB, hkeybd, &info->settings, &size, 0);
  
  /* set pin assign type */
  if      (0 == strcmp("BOARD", info->settings.pin_assign_type))
    info->pin_assign_type = RASPI_PIN_ASSIGN_BOARD;
  else if (0 == strcmp("BCM", info->settings.pin_assign_type))
    info->pin_assign_type = RASPI_PIN_ASSIGN_BCM;
  else if (0 == strcmp("wiringPi", info->settings.pin_assign_type))
    info->pin_assign_type = RASPI_PIN_ASSIGN_WPI;
  else{
    cm_msg(MERROR, "raspi", 
           "Pin assign type: %s is invalid, set \"BOARD\", \"BCM\", or \"wiringPi\".", 
           info->settings.pin_assign_type);
    return FE_ERR_ODB;
  }

  printf("Set pin definition.\n");
  /* set pin definition depending on the Raspberry Pi revision */
  if(SUCCESS != raspi_set_pin_definition(info)){
    cm_msg(MERROR, "raspi", "Pin definition couldn't be set.");
    return FE_ERR_HW;
  }
  
  /* allocate memory fo pin_used array at first time */
  info->pin_used = (BOOL*)calloc(info->num_avail_pins, sizeof(BOOL));
  if(! info->pin_used){
    cm_msg(MERROR, "raspi", "Failed to calloc for pin_used array.");
    /* memory allocation error is fatal. */
    cm_disconnect_experiment();
    exit(1);
  }
  
  /* initially set true to not-gpio pins */
  for(i=0; i<info->num_avail_pins; i++){
    if(info->pin_def[i].func & RASPI_PIN_FUNC_GPIO) 
      info->pin_used[info->pin_def[i].bd - 1] = FALSE;
    else 
      info->pin_used[info->pin_def[i].bd - 1] = TRUE;
  }
  
  /* setup wiringPi */
  if(geteuid () != 0){
    cm_msg(MERROR, "raspi", "Must be root to run. Program should be suid root.") ;
    return FE_ERR_HW;
  }
  if(wiringPiSetup() == -1){
    cm_msg(MERROR, "raspi", "wiringPi couldn't be setup.");
    return FE_ERR_HW;
  }

  printf("Finished RasPi bus driver initialization.\n");
  
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT raspi_init_spi(RASPI_INFO* info, INT ch, INT speed, INT clk_mode)
{
  INT p;
  //INT clk_mode;
  
  /* check ch */
  if(ch<0 || ch>=2){ //CE0 or CE1
    cm_msg(MERROR, "raspi", "Invalid SPI ch number %d", ch);
    return FE_ERR_HW;
  }
  /* check clk_mode */
  if(clk_mode < 0 || 4 <= clk_mode){
    cm_msg(MERROR, "raspi", "Invalid SPI clocking mode: %d (ch %d)", clk_mode, ch);
    return FE_ERR_HW;
  }
  
  /* check if gpio pins used for SPI are available */
  for(p=0; p<info->num_avail_pins; p++){
    const RASPI_PIN_INFO* pinfo = &(info->pin_def[p]);
    if(pinfo->func & RASPI_PIN_FUNC_SPI){
      BOOL is_used;
      
      if(ch==0 && pinfo->func & RASPI_PIN_FUNC_SPI_CE1) continue;
      if(ch==1 && pinfo->func & RASPI_PIN_FUNC_SPI_CE0) continue;
      
      if(SUCCESS != raspi_pin_is_used(info, pinfo, &is_used)) return FE_ERR_HW;
      if(is_used){
        cm_msg(MERROR, "raspi", "Cannot initialize SPI(ch %d): a spi pin is already used.", ch);
        return FE_ERR_HW;
      }
    }
  }
  
  /* setup */
  if(wiringPiSPISetupMode(ch, speed, clk_mode) < 0){
    cm_msg(MERROR, "raspi", "Couldn't setup SPI ch%d with a speed of %d Hz.", ch, speed);
    return -1;
  }
  
  /* set flag */
  info->spi_used = TRUE;
  for(p=0; p<info->num_avail_pins; p++){
    const RASPI_PIN_INFO* pinfo = &(info->pin_def[p]);
    if(pinfo->func & RASPI_PIN_FUNC_SPI){
      if(ch==0 && pinfo->func & RASPI_PIN_FUNC_SPI_CE1) continue;
      if(ch==1 && pinfo->func & RASPI_PIN_FUNC_SPI_CE0) continue;
      
      if(SUCCESS != raspi_use_pin(info, pinfo)) return FE_ERR_HW;
    }
  }
  
  return SUCCESS;
}

/*----------------------------------------------------------------------------*/

INT raspi(INT cmd, ...)
{
  va_list argptr;
  HNDLE hkey;
  INT status, size, conf;
  INT ch, speed, pin, spi_clk_mode;
  void *info;
  BYTE *buff;
  char *str;
  BOOL state;
  BOOL *pstate;  
  
  va_start(argptr, cmd);
  status = FE_SUCCESS;
  
  switch (cmd) {
  case CMD_INIT:
    hkey = va_arg(argptr, HNDLE);
    info = va_arg(argptr, void *);
    status = raspi_init(hkey, info);
    break;
    
  case CMD_RASPI_SET_PINMODE:
    info = va_arg(argptr, void *);
    pin  = va_arg(argptr, INT);
    conf = va_arg(argptr, INT);
    status = raspi_set_pinmode(info, pin, conf);
    break;

  case CMD_RASPI_INIT_SPI:
    info         = va_arg(argptr, void *);
    ch           = va_arg(argptr, INT);
    speed        = va_arg(argptr, INT);
    spi_clk_mode = va_arg(argptr, INT);
    status = raspi_init_spi(info, ch, speed, spi_clk_mode);
    break;
    
  case CMD_EXIT:
    info = va_arg(argptr, void *);
    status = raspi_exit(info);
    break;

  case CMD_OPEN:
    info = va_arg(argptr, void *);
    status = raspi_open(info);
    break;

  case CMD_CLOSE:
    info = va_arg(argptr, void *);
    status = raspi_close(info);
    break;

  case CMD_NAME:
    info = va_arg(argptr, void *);
    str = va_arg(argptr, char *);
    strcpy(str, "raspi");
    break;

  case CMD_WRITE:
    info  = va_arg(argptr, void *);
    pin   = va_arg(argptr, INT);
    state = va_arg(argptr, BOOL);
    status = raspi_write(info, pin, state);
    break;

  case CMD_READ:
    info   = va_arg(argptr, void *);
    pin    = va_arg(argptr, INT);
    pstate = va_arg(argptr, BOOL *);
    status = raspi_read(info, pin, pstate);
    break;

  case CMD_RASPI_SPI_WR:
    info  = va_arg(argptr, void *);
    ch    = va_arg(argptr, INT);
    buff  = va_arg(argptr, BYTE *);
    size  = va_arg(argptr, INT);
    status = raspi_spi_wr(info, ch, buff, size);
    break;

  case CMD_DEBUG:
    info = va_arg(argptr, void *);
    status = va_arg(argptr, INT);
    ((RASPI_INFO *) info)->settings.debug = status;
    break;
  }
  
  va_end(argptr);
  
  return status;
}
