/********************************************************************\

  Name:         generic.h
  Created by:   Stefan Ritt

  Contents:     Generic Class Driver header file

  $Id:$

\********************************************************************/

/* class driver routines */
INT cd_mflow(INT cmd, PEQUIPMENT pequipment);
INT cd_mflow_read(char *pevent, int);
