#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "fifo.h"

#define DEBUG(str) 
//#define DEBUG(str) printf("DEBUG: %s\n", str)

/* ---------------------------------------------------
   Internal Functions 
   --------------------------------------------------- */
FIFO_ELEMENT* fifo_clear_element(FIFO_ELEMENT** elem)
{
  FIFO_ELEMENT* next;
  
  DEBUG("CLEAR_ELEMENT");

  next = (*elem)->next;
  
  free((*elem)->buff);
  free(*elem);
  *elem = NULL;
  
  return next;
}

/* ---------------------------------------------------
   Opened Functions
   --------------------------------------------------- */
int fifo_init(FIFO** fifo)
{
  DEBUG("INIT");
  if(!*fifo)
    *fifo = (FIFO*)calloc(1, sizeof(FIFO));
  fifo_clear(*fifo);
  
  return SUCCESS;
}


int fifo_delete(FIFO** fifo)
{
  if(*fifo) free(*fifo);
  *fifo = NULL;
  return SUCCESS;
}

int fifo_clear(FIFO* fifo)
{
  FIFO_ELEMENT* elem = fifo->first;

  DEBUG("DELETE");
  while(1){
    if(!elem) break;
    
    elem = fifo_clear_element(&elem);
  }
  fifo->size = 0;
  fifo->first = fifo->last = NULL;
  
  return SUCCESS;
}

int fifo_get_size(FIFO* fifo)
{
  return fifo->size;
}

int fifo_get_next_buff_length(FIFO* fifo)
{
  return fifo->first->length;
}

FIFO_TYPE* fifo_get_next_buff_pointer(FIFO* fifo)
{
  if(fifo_is_empty(fifo)) return NULL;
  return fifo->first->buff;
}

BOOL fifo_is_empty(FIFO* fifo)
{
  if(fifo->size == 0) return TRUE;
  return FALSE;
}

fifo_enqueue(FIFO* fifo, unsigned int length, FIFO_TYPE* buff)
{
  FIFO_ELEMENT* new_elem;
  DEBUG("ENQUEUE");
  
  new_elem = (FIFO_ELEMENT*)calloc(1, sizeof(FIFO_ELEMENT));
  new_elem->length = length;
  new_elem->buff   = (FIFO_TYPE*)calloc(length, sizeof(FIFO_TYPE));
  memcpy(new_elem->buff, buff, length);
  new_elem->next   = NULL;
  
  if(fifo->last) fifo->last->next = new_elem;
  else           fifo->first = new_elem;
  fifo->last = new_elem;
  
  fifo->size++;
  return SUCCESS;
}

fifo_dequeue(FIFO* fifo, FIFO_TYPE* buff)
{
  unsigned int length = 0;
  DEBUG("DEQUEUE");
  
  /* fifo is empty? */
  if(fifo_is_empty(fifo)) return 0;
  
  length = fifo->first->length;

  /* copy buffer */
  if(buff){
    memcpy(buff, fifo->first->buff, length);
  }
  
  /* next element */
  fifo->first = fifo_clear_element(&fifo->first);
  
  fifo->size--;
  
  if(fifo->size == 0) fifo->last = NULL;
  
  return length;
}

int fifo_trash_next(FIFO* fifo)
{
  unsigned int length = 0;
  DEBUG("trash");
  
  /* fifo is empty? */
  if(fifo_is_empty(fifo)) return 0;
  
  length = fifo->first->length;

  /* next element */
  fifo->first = fifo_clear_element(&fifo->first);
  
  fifo->size--;
  
  if(fifo->size == 0) fifo->last = NULL;
  
  return length;
}
