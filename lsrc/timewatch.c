#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "timewatch.h"

void tw_init(TW_DATA* data, const char* name)
{
  data->start.tv_sec = data->start.tv_usec = 0;
  data->stop.tv_sec  = data->stop.tv_usec  = 0;
  
  data->count = 0;
  data->sum = data->sum2 = 0.0;
  data->start_set = 0;
  
  data->name = (char*)calloc(strlen(name)+1, sizeof(char));
  strcpy(data->name, name);
}

void tw_start(TW_DATA* data)
{
  gettimeofday(&data->start, NULL);
  data->start_set = 1;
}

double tw_stop(TW_DATA* data)
{
  double diff;
  
  if(! data->start_set) return -1;
  
  gettimeofday(&data->stop, NULL);

  diff = 1e6 * (data->stop.tv_sec - data->start.tv_sec)
    + data->stop.tv_usec - data->start.tv_usec;
  
  if(data->count == 0){
    data->sum = data->sum2 = 0.0;
  }
  
  data->sum  += diff;
  data->sum2 += diff*diff;
  data->count++;
  
  data->start_set = 0;
  
  return diff;
}

void tw_print(TW_DATA* data)
{
  double ave, stdev;

  if(data->count == 0) return;

  ave   = data->sum/data->count;
  stdev = sqrt(data->sum2/data->count - ave*ave);

  /* convert into msec */
  ave   /= 1e3;
  stdev /= 1e3;
  
  printf("\n"
         " - Timewatch: %20s --- \n"
         "|  Count of data      : %8d      |\n"
         "|  Average Difference : %8.3f msec |\n"
         "|  Standard Deviation : %8.3f msec |\n"
         " ------------------------------------- \n\n",
         data->name, data->count, ave, stdev);
}

