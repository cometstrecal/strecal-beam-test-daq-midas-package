/*
 *
 *
 */
#include <ctime>

class StopWatch {
public:
	StopWatch() {std::cerr << "-- StopWatch created" << std::endl;}
	virtual ~StopWatch() { std::cerr << "-- StopWatch destructed" << std::endl;}
	int Start() {return gettimeofday(&start, NULL);}
	int Stop() {return gettimeofday(&stop, NULL);}
	int Elapse();
	int Reset();
protected:
private:
	struct timeval start;
	struct timeval stop;
	int elapse;
};

int StopWatch::Elapse()
{
	elapse = 1000000 * (stop.tv_sec - start.tv_sec)
		+ (stop.tv_usec - start.tv_usec);
	return elapse;
}

int StopWatch::Reset()
{
	start.tv_sec = 0;
	start.tv_usec = 0;
	stop.tv_sec = 0;
	stop.tv_usec = 0;

	return 0;
}
