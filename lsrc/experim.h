/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Jan 13 23:47:58 2016

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[80];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {    \
    "[.]",                                              \
    "Comment = STRING : [80] Test",                     \
    "",                                                 \
    NULL }

#ifndef EXCL_BDC

#define BDC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} BDC_COMMON;

#define BDC_COMMON_STR(_name) const char *_name[] = {                   \
    "[.]",                                                              \
    "Event ID = WORD : 1",                                              \
    "Trigger mask = WORD : 0",                                          \
    "Buffer = STRING : [32] BUF1",                                      \
    "Type = INT : 2",                                                   \
    "Source = INT : 1",                                                 \
    "Format = STRING : [8] MIDAS",                                      \
    "Enabled = BOOL : y",                                               \
    "Read on = INT : 257",                                              \
    "Period = INT : 5",                                                 \
    "Event limit = DOUBLE : 0",                                         \
    "Num subevents = DWORD : 0",                                        \
    "Log history = INT : 0",                                            \
    "Frontend host = STRING : [32] localhost.localdomain",              \
    "Frontend name = STRING : [32] Beam Define Counter01",              \
    "Frontend file name = STRING : [256] frontend.c",                   \
    "Status = STRING : [256] Beam Define Counter01@localhost.localdomain", \
    "Status color = STRING : [32] greenLight",                          \
    "Hidden = BOOL : n",                                                \
    "",                                                                 \
    NULL }

#define BDC_BD_DEFINED

typedef struct {
  char      host[256];
  INT       port;
  INT       debug;
} BDC_BD;

#define BDC_BD_STR(_name) const char *_name[] = {       \
    "[.]",                                              \
    "Host = STRING : [256] 192.168.10.81",              \
    "Port = INT : 24",                                  \
    "Debug = INT : 0",                                  \
    "",                                                 \
    NULL }

#define BDC_SETTINGS_DEFINED

#define BDC_SCCONF_NUM 47

struct BDC_SETTINGS_SCCONF{
  char name[32]; 
  BYTE bitnum;
  BYTE arraynum; 
  BOOL bitorder; 
  INT  value[32]; 
};

typedef struct {
  BYTE daqmode;
  
  struct BDC_SETTINGS_FPGA {
    BOOL      B0_1;
    BOOL      B0_2;
    BOOL      B0_4;
    BOOL      B0_6;
    BOOL      B0_7;
    BOOL      B1_0;
    BOOL      B1_1;
    BOOL      B1_4;
    BOOL      B1_5;
    BOOL      B1_6;
    BOOL      B1_7;
    BOOL      B3_0;
    BOOL      B3_5;
    BOOL      B3_6;
    BOOL      B5_2;
    BOOL      B5_3;
    BOOL      B5_4;
  } fpga;

  struct BDC_SETTINGS_SCCONF sc_conf[2][BDC_SCCONF_NUM];
  
} BDC_SETTINGS;


#define BDC_SETTINGS_STR(_name) const char *_name = "\
[.] \n\
Mode = BYTE : 3\n\
\
[FPGA]\n\
B0_1_Raz_Ch  = BOOL : n\n\
B0_2_Val_Evt = BOOL : y\n\
B0_4_en_link = BOOL : y\n\
B0_6_test_r  = BOOL : n\n\
B0_7_test_sc = BOOL : n\n\
B1_0_load    = BOOL : n\n\
B1_1_cycle   = BOOL : n\n\
B1_4_rst_pa  = BOOL : y\n\
B1_5_select  = BOOL : y\n\
B1_6_rst_r   = BOOL : y\n\
B1_7_rst_sc  = BOOL : y\n\
B3_0_pwr_on  = BOOL : y\n\
B3_5_Scurve  = BOOL : n\n\
B3_6_rst_Scu = BOOL : n\n\
B5_2_gene    = BOOL : n\n\
B5_3_Scurve0 = BOOL : n\n\
B5_4_Scurve1 = BOOL : n\n\
\
[SC1/EnInputDAC]\n\
Name = STRING : [32] EnInputDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ref8bitDAC]\n\
Name = STRING : [32] ref8bitDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/Input8bitDAC1]\n\
Name = STRING : [32] Input8bitDAC1\n\
BitNum   = BYTE : 9\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 436\n\
[ 1] 460\n\
[ 2] 433\n\
[ 3] 456\n\
[ 4] 439\n\
[ 5] 449\n\
[ 6] 440\n\
[ 7] 468\n\
[ 8] 466\n\
[ 9] 464\n\
[10] 433\n\
[11] 453\n\
[12] 453\n\
[13] 458\n\
[14] 432\n\
[15] 439\n\
[16] 438\n\
[17] 444\n\
[18] 424\n\
[19] 458\n\
[20] 435\n\
[21] 455\n\
[22] 430\n\
[23] 460\n\
[24] 452\n\
[25] 438\n\
[26] 455\n\
[27] 440\n\
[28] 429\n\
[29] 429\n\
[30] 428\n\
[31] 408\n\
\
\
[SC1/biasLGPA]\n\
Name = STRING : [32] biasLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppHGPA]\n\
Name = STRING : [32] ppHGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnHGPA]\n\
Name = STRING : [32] EnHGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppLGPA]\n\
Name = STRING : [32] ppLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnLGPA]\n\
Name = STRING : [32] EnLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/HGPAComp]\n\
Name = STRING : [32] HGPAComp\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 14\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/HGPAFback1]\n\
Name = STRING : [32] HGPAFback1\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 12\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/LGPAFback1]\n\
Name = STRING : [32] LGPAFback1\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 8\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/LGPAComp]\n\
Name = STRING : [32] LGPAComp\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 9\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/PreAmpCtest1]\n\
Name = STRING : [32] PreAmpCtest1\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 0\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 0\n\
[ 1] 0\n\
[ 2] 0\n\
[ 3] 0\n\
[ 4] 0\n\
[ 5] 0\n\
[ 6] 0\n\
[ 7] 0\n\
[ 8] 0\n\
[ 9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
[24] 0\n\
[25] 0\n\
[26] 0\n\
[27] 0\n\
[28] 0\n\
[29] 0\n\
[30] 0\n\
[31] 0\n\
\
BitNum   = BYTE : 2\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 0\n\
[ 1] 0\n\
[ 2] 0\n\
[ 3] 0\n\
[ 4] 0\n\
[ 5] 0\n\
[ 6] 0\n\
[ 7] 0\n\
[ 8] 0\n\
[ 9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
[24] 0\n\
[25] 0\n\
[26] 0\n\
[27] 0\n\
[28] 0\n\
[29] 0\n\
[30] 0\n\
[31] 0\n\
\
\
[SC1/ppLGSSh]\n\
Name = STRING : [32] ppLGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnLGSSh]\n\
Name = STRING : [32] EnLGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/TimeCLGSSh1]\n\
Name = STRING : [32] TimeCLGSSh1\n\
BitNum   = BYTE : 3\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 5\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppHGSSh]\n\
Name = STRING : [32] ppHGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnHGSSh]\n\
Name = STRING : [32] EnHGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/TimeCHGSSh1]\n\
Name = STRING : [32] TimeCHGSSh1\n\
BitNum   = BYTE : 3\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 5\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppFSFollow]\n\
Name = STRING : [32] ppFSFollow\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnFSh]\n\
Name = STRING : [32] EnFSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppFS]\n\
Name = STRING : [32] ppFS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppTandH]\n\
Name = STRING : [32] ppTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnTandH]\n\
Name = STRING : [32] EnTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/biasTandH]\n\
Name = STRING : [32] biasTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnDiscri]\n\
Name = STRING : [32] EnDiscri\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppDiscri]\n\
Name = STRING : [32] ppDiscri\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/LatchDirect]\n\
Name = STRING : [32] LatchDirect\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/DiscriMask1]\n\
Name = STRING : [32] DiscriMask1\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[ 1] 1\n\
[ 2] 1\n\
[ 3] 1\n\
[ 4] 1\n\
[ 5] 1\n\
[ 6] 1\n\
[ 7] 1\n\
[ 8] 1\n\
[ 9] 1\n\
[10] 1\n\
[11] 1\n\
[12] 1\n\
[13] 1\n\
[14] 1\n\
[15] 1\n\
[16] 1\n\
[17] 1\n\
[18] 1\n\
[19] 1\n\
[20] 1\n\
[21] 1\n\
[22] 1\n\
[23] 1\n\
[24] 1\n\
[25] 1\n\
[26] 1\n\
[27] 1\n\
[28] 1\n\
[29] 1\n\
[30] 1\n\
[31] 1\n\
\
\
[SC1/DAC2]\n\
Name = STRING : [32] DAC2\n\
BitNum   = BYTE : 10\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 500\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/DACslope]\n\
Name = STRING : [32] DACslope\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppDAC]\n\
Name = STRING : [32] ppDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnDAC]\n\
Name = STRING : [32] EnDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppBgap]\n\
Name = STRING : [32] ppBgap\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnBgap]\n\
Name = STRING : [32] EnBgap\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppHGOTAq]\n\
Name = STRING : [32] ppHGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppLGOTAq]\n\
Name = STRING : [32] ppLGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppProbeOTAq]\n\
Name = STRING : [32] ppProbeOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/ppLVDS]\n\
Name = STRING : [32] ppLVDS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnHGOTAq]\n\
Name = STRING : [32] EnHGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnLGOTAq]\n\
Name = STRING : [32] EnLGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnProbeOTAq]\n\
Name = STRING : [32] EnProbeOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnLVDS]\n\
Name = STRING : [32] EnLVDS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnDigOut]\n\
Name = STRING : [32] EnDigOut\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/EnOR32]\n\
Name = STRING : [32] EnOR32\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/En32Trig]\n\
Name = STRING : [32] En32Trig\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC1/NC]\n\
Name = STRING : [32] NC\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnInputDAC]\n\
Name = STRING : [32] EnInputDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ref8bitDAC]\n\
Name = STRING : [32] ref8bitDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/Input8bitDAC2]\n\
Name = STRING : [32] Input8bitDAC2\n\
BitNum   = BYTE : 9\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 464\n\
[ 1] 454\n\
[ 2] 458\n\
[ 3] 430\n\
[ 4] 430\n\
[ 5] 428\n\
[ 6] 431\n\
[ 7] 461\n\
[ 8] 431\n\
[ 9] 433\n\
[10] 434\n\
[11] 435\n\
[12] 438\n\
[13] 459\n\
[14] 458\n\
[15] 435\n\
[16] 434\n\
[17] 440\n\
[18] 433\n\
[19] 449\n\
[20] 454\n\
[21] 453\n\
[22] 442\n\
[23] 443\n\
[24] 460\n\
[25] 466\n\
[26] 464\n\
[27] 467\n\
[28] 462\n\
[29] 453\n\
[30] 436\n\
[31] 420\n\
\
\
[SC2/biasLGPA]\n\
Name = STRING : [32] biasLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppHGPA]\n\
Name = STRING : [32] ppHGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnHGPA]\n\
Name = STRING : [32] EnHGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppLGPA]\n\
Name = STRING : [32] ppLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnLGPA]\n\
Name = STRING : [32] EnLGPA\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/HGPAComp]\n\
Name = STRING : [32] HGPAComp\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 14\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/HGPAFback2]\n\
Name = STRING : [32] HGPAFback2\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 12\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/LGPAFback2]\n\
Name = STRING : [32] LGPAFback2\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 8\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/LGPAComp]\n\
Name = STRING : [32] LGPAComp\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 9\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/PreAmpCtest2]\n\
Name = STRING : [32] PreAmpCtest2\n\
BitNum   = BYTE : 2\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 0\n\
[ 1] 0\n\
[ 2] 0\n\
[ 3] 0\n\
[ 4] 0\n\
[ 5] 0\n\
[ 6] 0\n\
[ 7] 0\n\
[ 8] 0\n\
[ 9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
[24] 0\n\
[25] 0\n\
[26] 0\n\
[27] 0\n\
[28] 0\n\
[29] 0\n\
[30] 0\n\
[31] 0\n\
\
\
[SC2/ppLGSSh]\n\
Name = STRING : [32] ppLGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnLGSSh]\n\
Name = STRING : [32] EnLGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/TimeCLGSSh2]\n\
Name = STRING : [32] TimeCLGSSh2\n\
BitNum   = BYTE : 3\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 5\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppHGSSh]\n\
Name = STRING : [32] ppHGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnHGSSh]\n\
Name = STRING : [32] EnHGSSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/TimeCHGSSh2]\n\
Name = STRING : [32] TimeCHGSSh2\n\
BitNum   = BYTE : 3\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : y\n\
Value = INT[32] : \n\
[ 0] 5\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppFSFollow]\n\
Name = STRING : [32] ppFSFollow\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnFSh]\n\
Name = STRING : [32] EnFSh\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppFS]\n\
Name = STRING : [32] ppFS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppTandH]\n\
Name = STRING : [32] ppTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnTandH]\n\
Name = STRING : [32] EnTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/biasTandH]\n\
Name = STRING : [32] biasTandH\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnDiscri]\n\
Name = STRING : [32] EnDiscri\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppDiscri]\n\
Name = STRING : [32] ppDiscri\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/LatchDirect]\n\
Name = STRING : [32] LatchDirect\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/DiscriMask2]\n\
Name = STRING : [32] DiscriMask2\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 32\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[ 1] 1\n\
[ 2] 1\n\
[ 3] 1\n\
[ 4] 1\n\
[ 5] 1\n\
[ 6] 1\n\
[ 7] 1\n\
[ 8] 1\n\
[ 9] 1\n\
[10] 1\n\
[11] 1\n\
[12] 1\n\
[13] 1\n\
[14] 1\n\
[15] 1\n\
[16] 1\n\
[17] 1\n\
[18] 1\n\
[19] 1\n\
[20] 1\n\
[21] 1\n\
[22] 1\n\
[23] 1\n\
[24] 1\n\
[25] 1\n\
[26] 1\n\
[27] 1\n\
[28] 1\n\
[29] 1\n\
[30] 1\n\
[31] 1\n\
\
\
[SC2/DAC2]\n\
Name = STRING : [32] DAC2\n\
BitNum   = BYTE : 10\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 500\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/DACslope]\n\
Name = STRING : [32] DACslope\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppDAC]\n\
Name = STRING : [32] ppDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnDAC]\n\
Name = STRING : [32] EnDAC\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppBgap]\n\
Name = STRING : [32] ppBgap\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnBgap]\n\
Name = STRING : [32] EnBgap\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppHGOTAq]\n\
Name = STRING : [32] ppHGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppLGOTAq]\n\
Name = STRING : [32] ppLGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppProbeOTAq]\n\
Name = STRING : [32] ppProbeOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/ppLVDS]\n\
Name = STRING : [32] ppLVDS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnHGOTAq]\n\
Name = STRING : [32] EnHGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnLGOTAq]\n\
Name = STRING : [32] EnLGOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnProbeOTAq]\n\
Name = STRING : [32] EnProbeOTAq\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnLVDS]\n\
Name = STRING : [32] EnLVDS\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnDigOut]\n\
Name = STRING : [32] EnDigOut\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 1\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/EnOR32]\n\
Name = STRING : [32] EnOR32\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/En32Trig]\n\
Name = STRING : [32] En32Trig\n\
BitNum   = BYTE : 1\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
[SC2/NC]\n\
Name = STRING : [32] NC\n\
BitNum   = BYTE : 4\n\
ArrayNum = BYTE : 1\n\
BitOrder = BOOL : n\n\
Value = INT[32] : \n\
[ 0] 0\n\
[1]-1\n[2]-1\n[3]-1\n[4]-1\n[5]-1\n[6]-1\n[7]-1\n[8]-1\n[9]-1\n[10]-1\n[11]-1\n[12]-1\n[13]-1\n[14]-1\n[15]-1\n[16]-1\n[17]-1\n[18]-1\n[19]-1\n[20]-1\n[21]-1\n[22]-1\n[23]-1\n[24]-1\n[25]-1\n[26]-1\n[27]-1\n[28]-1\n[29]-1\n[30]-1\n[31]-1\n\
\
"

// End of #define BDC_SETTINGS_STR(_name)
#endif

