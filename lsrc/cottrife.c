/********************************************************************   \

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "scaler event" which are filled with
                CAMAC or random data. The trigger event is filled
                with two banks (ADC0 and TDC0), the scaler event
                with one bank (SCLR).

  $Id$

\********************************************************************/

//#define TRG_NUM_DEBUG
#ifdef TRG_NUM_DEBUG
#define TRG_DEBUG_DUMP(str)       fprintf(debug_fp, str "\n")
#define TRG_DEBUG_DUMPV(str, ...) fprintf(debug_fp, str "\n", __VA_ARGS__)
#else
#define TRG_DEBUG_DUMP(str)
#define TRG_DEBUG_DUMPV(str, ...)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "sitcp.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C"
{
#endif

  /*-- Globals -------------------------------------------------------*/
  struct COTTRI_SETTING{
    /* ODB hundle */
    HNDLE hKeyRoot, hKeySet,
      hKeyReadoutMode, hKeyTestMode, hKeyBaseline,
      hKeyThreshold, hKeyClstThreshold,
      hKeyWindowSize, hKeyDelayClkTicks,
      hKeyRndmTrigRate, hKeyLEDDelay, hKeySelfVeto,
      hKeyPolarity, hKeyLedOn;

    /* SITCP_INFO */
    void* sitcp_info;

    /* setting variables */
    BYTE  readout_mode;
    BOOL  test_mode;
    BYTE  threshold;
    WORD  clst_threshold;
    BYTE  baseline;
    WORD  window_size;
    WORD  delay_clk_ticks;
    DWORD rndm_trig_rate;
    INT   led_delay;
    INT   self_veto;
    BOOL  polarity;
    BOOL  led_on;

    /* mirror value */
    BYTE  readout_mode_mirror;
    BOOL  test_mode_mirror;
    BYTE  threshold_mirror;
    WORD  clst_threshold_mirror;
    BYTE  baseline_mirror;
    WORD  window_size_mirror;
    WORD  delay_clk_ticks_mirror;
    DWORD rndm_trig_rate_mirror;
    INT   led_delay_mirror;
    INT   self_veto_mirror;
    BOOL  polarity_mirror;
    BOOL  led_on_mirror;
  };

#define FPGA_CLOCK 40000000

  /* Constants */
#define NCHANNELS 16
#define TIMEOUT   100

  /* Readout mode */
#define COTTRI_RM_NONE  (0)
#define COTTRI_RM_EXTN  (1)
#define COTTRI_RM_SELF  (2)
#define COTTRI_RM_CLST  (3)
#define COTTRI_RM_SELFN (4)
#define COTTRI_RM_LED   (6)
#define COTTRI_RM_RNDM  (7)

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = "COTTRI ";

  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = FALSE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 3000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 40000*5;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 5 * 40000;

  /* triggered timing in poll_event() */
  struct timeval timestamp;

  extern BOOL debug;

  /*-- Function declarations -----------------------------------------*/

  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();

  INT read_trigger_event(char *pevent, INT off);

  INT poll_event(INT source, INT count, BOOL test);
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

  // Initialize an COTTRI
  INT init_cottri();

  // Clear
  INT clear_cottri();

  /*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {

    {
      "COTTRI",            /* equipment name */
      {1, 0,               /* event ID, trigger mask */
       "CBUFF",            /* event buffer */
       EQ_POLLED | EQ_EB,  /* equipment type */
       1,                  /* No event source */
       "MIDAS",            /* format */
       TRUE,               /* enabled */
       RO_RUNNING |        /* read only when running */
       RO_ODB,             /* and update ODB */
       5,                  /* poll for 100ms */
       0,                  /* stop run after this event limit */
       0,                  /* number of sub events */
       0,                  /* don't log history */
       "", "", "",},
      read_trigger_event,  /* readout routine */
    },

    {""}
  };

  /*-- Frontend specific variables ------------------------------------*/

  extern INT run_state; /* STATE_RUNNING, STATE_STOPPED, STATE_PAUSED */

  /* ODB */
  HNDLE hDB;

  /* setting */
  struct COTTRI_SETTING setting;

  /* Bank names */
  char bk_name_info[5] = "EITC";
  char bk_name_main[5] = "DTTC";

#ifdef TRG_NUM_DEBUG
  FILE* debug_fp = NULL;
#endif

#ifdef __cplusplus
}
#endif

/********************************************************************   \
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  INT status, size;
  char str[256];
  HNDLE hKey;
  INT retry = 0;

  if(debug) display_period = 0;

#ifdef TRG_NUM_DEBUG
  debug_fp = fopen("trg_num_debug_cottri.dat", "w");
  if(!debug_fp){
    cm_msg(MERROR, "frontend_init", "Couldn't make trg_num_debug file.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "Made debug file.");
#endif

  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  cm_get_experiment_database(&hDB, NULL);

  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &setting.hKeyRoot);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_ODB;
  }

  //---------------------------------------------------------
  // Load configurations from ODB
  //---------------------------------------------------------
  /* setup setting */
  status = init_setting();
  if(status != SUCCESS){
    cm_msg(MERROR, "frontend_init", "Cannot initialize settings.");
    return FE_ERR_ODB;
  }

  /* -----------------------------------------------
     Connect to SiTCP socket
     ----------------------------------------------- */
  /* connect */
  while(1){
    retry = 0;
    if(SUCCESS == sitcp(CMD_INIT, setting.hKeyRoot, &(setting.sitcp_info)) &&
       SUCCESS == sitcp(CMD_OPEN, setting.sitcp_info)){
      printf("Connected.\n");
      break;
    }

    if(++retry >= 5){
      cm_msg(MERROR, "frontend_init", "5 times failed to connect.");
      return FE_ERR_ODB;
    }

    printf("Error! TCP/IP cannot be initialized.\n"
           "Please check /Equipment/%s/Settings/SiTCP/Host and Port. "
           "reconection will be tried 10 seconds after.\n",
           equipment[0]);
    ss_sleep(10000);
  }

  /* initialize EROS/ROESTI */
  if(SUCCESS != init_cottri()){
    return FE_ERR_HW;
  }

  clear_cottri();

  printf("Initialize completed.\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  end_of_run(0, "");

  sitcp(CMD_SITCP_ENABLE_NAGLE, setting.sitcp_info, TRUE);
  sitcp(CMD_EXIT, setting.sitcp_info);

#ifdef TRG_NUM_DEBUG
  fclose(debug_fp);
#endif

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  char data;

  printf("Waiting 5 sec for EROS/ROESTI gets ready.");
  ss_sleep(1000*5);

  clear_cottri();

  if(SUCCESS != set_configuration(FALSE)){
    cm_msg(MERROR, "begin_of_run", "Cannot initialize with RBCP commands.");
    return FE_ERR_HW;
  }

  ss_sleep(50);

  /* enable trigger input */
  data = 0;
  sitcp(CMD_SITCP_RBCP_WRITE, setting.sitcp_info, 0x04, &data, 1, TRUE);

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  char data;

  /* disable trigger input */
  data = 1;
  sitcp(CMD_SITCP_RBCP_WRITE, setting.sitcp_info, 0x04, &data, 1, TRUE);
  ss_sleep(0.1);

  /* discard data remaining in the socket */
  clear_cottri();

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  DWORD buf;
  INT retry, status;

  for(retry=0; retry<count; retry++){
    status = sitcp(CMD_POLL, setting.sitcp_info, &buf, sizeof(buf), 2);
    if(test) continue;
    if(status == sizeof(buf) && buf == 0x5555FFFF){
      gettimeofday(&timestamp, NULL); // get a timestamp immediately
      return TRUE; // Correct COTTRI event header
    }
  }
  return FALSE;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
  DWORD *pdata_dword;
  BYTE  *pdata_byte;
  INT i, status, recv_bytes;

  DWORD hdbuff[3];
  BYTE  buff[10000];
  DWORD packet_id;
  DWORD datasize;
  DWORD trg_num;

  /* read header (5 x DWORD) */
  status = sitcp(CMD_READ, setting.sitcp_info, (char*)hdbuff, sizeof(DWORD)*3);
  if(sizeof(DWORD)*3 != status){
    printf("Time out!\n");
  }

  /* header info */
  packet_id =  0xFFFFFFFF & ntohl(hdbuff[0]);
  datasize  = (0xFF       & ntohs(hdbuff[1])) * 16;
  trg_num   =  0xFFFFFFFF & ntohl(hdbuff[2]);

  //printf("Datasize = %d Packet ID = 0x%08x Trigger No. %d \n", datasize, packet_id, trg_num);

  if(packet_id != (0x00012345 | (setting.readout_mode_mirror << 24))){
    cm_msg(MERROR, "read_trigger_event", "Illegal packet id : 0x%08x", packet_id);
    return 0;
  }

  /* ----------------------------------------
     read data
     ---------------------------------------- */
  recv_bytes = 0;
  do{
    status = sitcp(CMD_READ_BLOCK, setting.sitcp_info,
                   (char*)(buff) + recv_bytes, datasize - recv_bytes, 50);
    if(status >= 0){
      recv_bytes += status;
    }
    TRG_DEBUG_DUMPV("A loop: %d", status);
  }while(recv_bytes < datasize);

#ifdef TRG_NUM_DEBUG
  for(i=0; i<datasize; i++){
    fprintf(debug_fp, "0x%02x ", (BYTE)buff[i]);
    if((i+1)%16 == 0) fprintf(debug_fp, "\n");
  }
  TRG_DEBUG_DUMPV("Event Trigger Number : %d", trg_num);
#endif

  /* init bank structure */
  bk_init(pevent);

  /* ----------------------------------------
     create Event Info bank
  ---------------------------------------- */
  bk_create(pevent, bk_name_info, TID_DWORD, (void **)&pdata_dword);

  /* event counter */
  *pdata_dword++ = trg_num;

  /* store the time stamp */
  *(struct timeval*)pdata_dword = timestamp;
  pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);

  bk_close(pevent, pdata_dword);

  /* ----------------------------------------
     create Data bank
  ---------------------------------------- */
  bk_create(pevent, bk_name_main, TID_BYTE, (void **)&pdata_byte);

  memcpy(pdata_byte, buff, datasize);
  pdata_byte += datasize;
  bk_close(pevent, pdata_byte);

  return bk_size(pevent);
}

//=======================================================
INT clear_cottri()
//=======================================================
{
  DWORD buff[100];
  //printf("\n\n Start Claer \n " );
  while( 0 < sitcp(CMD_POLL, setting.sitcp_info, (char*)buff, sizeof(buff), 1) ){}
  //printf("Finished Clear \n " );
  return SUCCESS;
}


INT set_configuration(BOOL first)
{
  int status, i, c, size;
  DWORD trig_rate;
  WORD  led_delay;
  DWORD self_veto;

  /* random trigger rate */
  trig_rate = (DWORD)(FPGA_CLOCK/setting.rndm_trig_rate);

  /* delay in LED mode */
  led_delay = 0xFFFF & (WORD)(setting.led_delay/10.0 + 0.5);

  /* length of self veto */
  self_veto = 0x3FFFF & (DWORD)(setting.self_veto/25.0 + 0.5);

  /* setting parameter for a chip */
  SITCP_RBCP_COMMAND ccommand[] = {
    /* ADC chip select (00-03) */
    {0x10,  0x00},

    {0x100, 0x3c, TRUE}, /* ADC chip init */
    {0x108, 0x3,  TRUE}, /* ADC software reset */
    {0x108, 0x0,  TRUE}, /* ADC software reset */

    /* testmode aa 55 if it is 0x4 */
    {0x10D, (char)((setting.test_mode)? 0x4 : 0x0), TRUE},

    /* update chip */
    {0x1ff, 0x1, TRUE}
  };

  /* setting parameter for global */
  SITCP_RBCP_COMMAND command[] = {
    /* RESET serdes module and disable trigger signal (1 when firmware is downloaded) */
    /* When initialize, this is high to disable trigger input. */
    {0x04, 1},

    /* counter reset */
    {0x03, 1},
    {0x03, 0, TRUE},

    /* (External trigger mode:1, Self-trigger mode:0) */
    {0x05, (char)(0xFF & setting.readout_mode)},

    /* baseline */
    {0x0E, (char)(0xFF & setting.baseline) },

    /* threshold */
    {0x0F, (char)(0xFF & setting.threshold) },

    /* threshold for clustering */
    {0x01, (char)(0xFF & setting.clst_threshold>>8)},
    {0x02, (char)(0xFF & setting.clst_threshold   )},

    /* window size */
    {0x06, (char)(0xFF & (setting.window_size>>8)) },
    {0x07, (char)(0xFF & (setting.window_size   )) },

    /* window size */
    {0x08, (char)(0xFF & (setting.delay_clk_ticks>>8)) },
    {0x09, (char)(0xFF & (setting.delay_clk_ticks   )) },

    /* random trigger rate */
    {0x13, (char)(0xFF & (trig_rate>>16)) },
    {0x14, (char)(0xFF & (trig_rate>>8 )) },
    {0x15, (char)(0xFF & (trig_rate    )) },

    /* delay in LED mode */
    {0x16, (char)(0xFF & (led_delay>>8)) },
    {0x17, (char)(0xFF & (led_delay   )) },

    /* length of self veto */
    {0x1A, (char)(0x03 & (self_veto>>16)) },
    {0x1B, (char)(0xFF & (self_veto>>8 )) },
    {0x1C, (char)(0xFF & (self_veto    )) },

    /* signal polarity is positive */
    {0x1D, (char)(setting.polarity? 0:1)},

    /* led on */
    {0x30, (char)(setting.led_on? 0:1)},
    {0x31, (char)0} /* output low when led_on is true */
  };

  /* Chip by chip setting */
  for(c=0; c<4; c++){
    /* set chip id */
    ccommand[0].data = (char)(0xFF & c);
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, setting.sitcp_info,
                        sizeof(ccommand)/sizeof(SITCP_RBCP_COMMAND), ccommand, 100)){
      cm_msg(MERROR, "set_configuration", "Error in ccommand.");
      return FE_ERR_HW;
    }
  }

  /* global setting */
  if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, setting.sitcp_info,
                      sizeof(command)/sizeof(SITCP_RBCP_COMMAND), command, 100)){
    cm_msg(MERROR, "set_configuration", "Error in command.");
    return FE_ERR_HW;
  }

  //printf("Initialized\n");

  return SUCCESS;
}

//=======================================================
// ODB dispatchers
//=======================================================
void set_readout_mode(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.readout_mode == setting.readout_mode_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode",
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }

  switch(setting.readout_mode){
  case COTTRI_RM_SELF:
    printf("Set to self trigger.\n");
    break;

  case COTTRI_RM_SELFN:
    printf("Set to self trigger negative signal.\n");
    break;

  case COTTRI_RM_EXTN:
    printf("Set to external trigger.\n");
    break;

  case COTTRI_RM_CLST:
    printf("Set to simple clustering trigger.\n");
    break;

  case COTTRI_RM_LED:
    printf("Set to random trigger with LED light pulsing.\n");
    break;

  case COTTRI_RM_RNDM:
    printf("Set to random trigger.\n");
    break;

  default:
    /* Illegal command */
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode",
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }

  setting.readout_mode_mirror = setting.readout_mode;

  set_configuration(FALSE);
}

void set_test_mode(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.test_mode == setting.test_mode_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.test_mode = setting.test_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Enable Test Mode",
                 &setting.test_mode, sizeof(setting.test_mode),
                 1, TID_BOOL);
    return;
  }

  if(setting.test_mode) printf("set enable test mode\n");
  else                  printf("set disable test mode\n");
  setting.test_mode_mirror = setting.test_mode;

  set_configuration(FALSE);
}

void set_polarity(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.polarity == setting.polarity_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.polarity = setting.polarity_mirror;
    db_set_value(hDB, setting.hKeySet, "Signal Polarity (y:Pos|n:Neg)",
                 &setting.polarity, sizeof(setting.polarity),
                 1, TID_BOOL);
    return;
  }

  if(setting.polarity) printf("set signal polarity to positive.\n");
  else                 printf("set signal polarity to negative.\n");
  setting.polarity_mirror = setting.polarity;

  set_configuration(FALSE);
}

void set_led_on(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.led_on == setting.led_on_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.led_on = setting.led_on_mirror;
    db_set_value(hDB, setting.hKeySet, "LED ON",
                 &setting.led_on, sizeof(setting.led_on),
                 1, TID_BOOL);
    return;
  }

  if(setting.led_on) printf("Switch on LED.\n");
  else               printf("Swithc off LED\n");
  setting.led_on_mirror = setting.led_on;

  set_configuration(FALSE);
}


void set_baseline(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.baseline == setting.baseline_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED){
    setting.baseline = setting.baseline_mirror;
    db_set_value(hDB, setting.hKeySet, "Baseline",
                 &setting.baseline, sizeof(setting.baseline),
                 1, TID_BYTE);
    return;
  }

  printf("set baseline to 0x%02x\n", setting.baseline);
  setting.baseline_mirror = setting.baseline;

  set_configuration(FALSE);
}

void set_threshold(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.threshold == setting.threshold_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  /*
  if(run_state != STATE_STOPPED){
    setting.threshold = setting.threshold_mirror;
    db_set_value(hDB, setting.hKeySet, "Threshold",
                 &setting.threshold, sizeof(setting.threshold),
                 1, TID_BYTE);
    return;
  }
  */
  printf("set threshold to 0x%02x\n", setting.threshold);
  setting.threshold_mirror = setting.threshold;

  //set_configuration(FALSE);
  SITCP_RBCP_COMMAND command[] = {
    {0x0F, (char)(0xFF & setting.threshold) }
  };
  if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, setting.sitcp_info,
                      sizeof(command)/sizeof(SITCP_RBCP_COMMAND), command, 10)){
    cm_msg(MERROR, "set_configuration", "Error in command.");
  }
}

void set_clst_threshold(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.clst_threshold == setting.clst_threshold_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED ){
    setting.clst_threshold = setting.clst_threshold_mirror;
    db_set_value(hDB, setting.hKeySet, "Threshold for Clustering",
                 &setting.clst_threshold, sizeof(setting.clst_threshold),
                 1, TID_WORD);
    return;
  }

  printf("set threshold for clustering to 0x%04x\n", setting.clst_threshold);
  setting.threshold_mirror = setting.threshold;

  set_configuration(FALSE);
}

void set_window_size(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.window_size == setting.window_size_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED){
    setting.window_size = setting.window_size_mirror;
    db_set_value(hDB, setting.hKeySet, "Window Size",
                 &setting.window_size, sizeof(setting.window_size),
                 1, TID_WORD);
    return;
  }

  printf("set window size to 0x%04x\n", setting.window_size);
  setting.window_size_mirror = setting.window_size;

  set_configuration(FALSE);
}

void set_delay_clk_ticks(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.delay_clk_ticks == setting.delay_clk_ticks_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED){
    setting.delay_clk_ticks = setting.delay_clk_ticks_mirror;
    db_set_value(hDB, setting.hKeySet, "Delay CLK Ticks",
                 &setting.delay_clk_ticks, sizeof(setting.delay_clk_ticks),
                 1, TID_WORD);
    return;
  }

  printf("set delay clk ticks to 0x%04x\n", setting.delay_clk_ticks);
  setting.delay_clk_ticks_mirror = setting.delay_clk_ticks;

  set_configuration(FALSE);
}

void set_rndm_trig_rate(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.rndm_trig_rate == setting.rndm_trig_rate_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED ||
     setting.rndm_trig_rate > FPGA_CLOCK){
    setting.rndm_trig_rate = setting.rndm_trig_rate_mirror;
    db_set_value(hDB, setting.hKeySet, "Random Trigger Rate (Hz)",
                 &setting.rndm_trig_rate, sizeof(setting.rndm_trig_rate),
                 1, TID_DWORD);
    return;
  }

  printf("set random trigger rate to %d Hz\n", setting.rndm_trig_rate);
  setting.rndm_trig_rate_mirror = setting.rndm_trig_rate;

  set_configuration(FALSE);
}

void set_led_delay(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.led_delay == setting.led_delay_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED ||
     setting.led_delay < 0 ||
     setting.led_delay > 1000000){
    setting.led_delay = setting.led_delay_mirror;
    db_set_value(hDB, setting.hKeySet, "Delay in LED Mode (nsec)",
                 &setting.led_delay, sizeof(setting.led_delay),
                 1, TID_INT);
    return;
  }

  printf("set delay in LED mode to %d nsec\n", setting.led_delay);
  setting.led_delay_mirror = setting.led_delay;

  set_configuration(FALSE);
}

void set_self_veto(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.self_veto == setting.self_veto_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED ||
     setting.self_veto < 0 ||
     setting.self_veto > 5000000){
    setting.self_veto = setting.self_veto_mirror;
    db_set_value(hDB, setting.hKeySet, "Self-veto length (nsec)",
                 &setting.self_veto, sizeof(setting.self_veto),
                 1, TID_INT);
    return;
  }

  printf("set length of self-veto to %d nsec\n", setting.self_veto);
  setting.self_veto_mirror = setting.self_veto;

  set_configuration(FALSE);
}


//=======================================================
// init_setting()
//=======================================================
INT init_setting()
{
  INT i, status, size, flag;
  char str[256];
  HNDLE hKeySet, hKey;

  status = db_create_key(hDB, setting.hKeyRoot, "Settings", TID_KEY);
  status = db_find_key(hDB, setting.hKeyRoot, "Settings", &setting.hKeySet);

  //---------------------------------------------------------
  // Load configurations from ODB
  //---------------------------------------------------------
  /* readout mode */
  sprintf(str, "Readout Mode");
  db_create_key(hDB, setting.hKeySet, str, TID_BYTE);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyReadoutMode);
  size = sizeof(setting.readout_mode);
  db_get_value(hDB, setting.hKeySet, str, &(setting.readout_mode), &size, TID_BYTE, FALSE);

  /* check value */
  if(setting.readout_mode != COTTRI_RM_NONE  &&
     setting.readout_mode != COTTRI_RM_SELF  &&
     setting.readout_mode != COTTRI_RM_SELFN &&
     setting.readout_mode != COTTRI_RM_EXTN  &&
     setting.readout_mode != COTTRI_RM_CLST  &&
     setting.readout_mode != COTTRI_RM_RNDM )
    setting.readout_mode_mirror = setting.readout_mode = COTTRI_RM_EXTN;
  else
    setting.readout_mode_mirror = setting.readout_mode;

  db_open_record(hDB, setting.hKeyReadoutMode, &setting.readout_mode,
                 sizeof(setting.readout_mode), MODE_READ, set_readout_mode, NULL);
  db_open_record(hDB, setting.hKeyReadoutMode, &setting.readout_mode_mirror,
                 sizeof(setting.readout_mode_mirror), MODE_WRITE, NULL, NULL);

  /* test mode */
  sprintf(str, "Enable Test Mode");
  db_create_key(hDB, setting.hKeySet, str, TID_BOOL);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyTestMode);
  size = sizeof(setting.test_mode);
  db_get_value(hDB, setting.hKeySet, str, &(setting.test_mode), &size, TID_BOOL, FALSE);

  setting.test_mode_mirror = setting.test_mode;

  db_open_record(hDB, setting.hKeyTestMode, &setting.test_mode,
                 sizeof(setting.test_mode), MODE_READ, set_test_mode, NULL);
  db_open_record(hDB, setting.hKeyTestMode, &setting.test_mode_mirror,
                 sizeof(setting.test_mode_mirror), MODE_WRITE, NULL, NULL);

  /* baseline */
  sprintf(str, "Baseline");
  db_create_key(hDB, setting.hKeySet, str, TID_BYTE);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyBaseline);
  size = sizeof(setting.baseline);
  db_get_value(hDB, setting.hKeySet, str, &(setting.baseline), &size, TID_BYTE, FALSE);

  setting.baseline_mirror = setting.baseline;

  db_open_record(hDB, setting.hKeyBaseline, &setting.baseline,
                 sizeof(setting.baseline), MODE_READ, set_baseline, NULL);
  db_open_record(hDB, setting.hKeyBaseline, &setting.baseline_mirror,
                 sizeof(setting.baseline_mirror), MODE_WRITE, NULL, NULL);

  /* threshold */
  sprintf(str, "Threshold");
  db_create_key(hDB, setting.hKeySet, str, TID_BYTE);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyThreshold);
  size = sizeof(setting.threshold);
  db_get_value(hDB, setting.hKeySet, str, &(setting.threshold), &size, TID_BYTE, FALSE);

  setting.threshold_mirror = setting.threshold;

  db_open_record(hDB, setting.hKeyThreshold, &setting.threshold,
                 sizeof(setting.threshold), MODE_READ, set_threshold, NULL);
  db_open_record(hDB, setting.hKeyThreshold, &setting.threshold_mirror,
                 sizeof(setting.threshold_mirror), MODE_WRITE, NULL, NULL);

  /* threshold for clustering */
  sprintf(str, "Threshold for Clustering");
  db_create_key(hDB, setting.hKeySet, str, TID_WORD);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyClstThreshold);
  size = sizeof(setting.clst_threshold);
  db_get_value(hDB, setting.hKeySet, str, &(setting.clst_threshold), &size, TID_WORD, FALSE);

  setting.clst_threshold_mirror = setting.clst_threshold;

  db_open_record(hDB, setting.hKeyClstThreshold, &setting.clst_threshold,
                 sizeof(setting.clst_threshold), MODE_READ, set_clst_threshold, NULL);
  db_open_record(hDB, setting.hKeyClstThreshold, &setting.clst_threshold_mirror,
                 sizeof(setting.clst_threshold_mirror), MODE_WRITE, NULL, NULL);

  /* window size */
  sprintf(str, "Window Size");
  db_create_key(hDB, setting.hKeySet, str, TID_WORD);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyWindowSize);
  size = sizeof(setting.window_size);
  db_get_value(hDB, setting.hKeySet, str, &(setting.window_size), &size, TID_WORD, FALSE);

  setting.window_size_mirror = setting.window_size;

  db_open_record(hDB, setting.hKeyWindowSize, &setting.window_size,
                 sizeof(setting.window_size), MODE_READ, set_window_size, NULL);
  db_open_record(hDB, setting.hKeyWindowSize, &setting.window_size_mirror,
                 sizeof(setting.window_size_mirror), MODE_WRITE, NULL, NULL);

  /* delay clk ticks */
  sprintf(str, "Delay CLK Ticks");
  db_create_key(hDB, setting.hKeySet, str, TID_WORD);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyDelayClkTicks);
  size = sizeof(setting.delay_clk_ticks);
  db_get_value(hDB, setting.hKeySet, str, &(setting.delay_clk_ticks), &size, TID_WORD, FALSE);

  setting.delay_clk_ticks_mirror = setting.delay_clk_ticks;

  db_open_record(hDB, setting.hKeyDelayClkTicks, &setting.delay_clk_ticks,
                 sizeof(setting.delay_clk_ticks), MODE_READ, set_delay_clk_ticks, NULL);
  db_open_record(hDB, setting.hKeyDelayClkTicks, &setting.delay_clk_ticks_mirror,
                 sizeof(setting.delay_clk_ticks_mirror), MODE_WRITE, NULL, NULL);

  /* random trigger rate */
  sprintf(str, "Random Trigger Rate (Hz)");
  db_create_key(hDB, setting.hKeySet, str, TID_DWORD);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyRndmTrigRate);
  size = sizeof(setting.rndm_trig_rate);
  db_get_value(hDB, setting.hKeySet, str, &(setting.rndm_trig_rate), &size, TID_DWORD, FALSE);

  /* check value */
  if(setting.rndm_trig_rate > FPGA_CLOCK)
    setting.rndm_trig_rate_mirror = setting.rndm_trig_rate = 100;
  else
    setting.rndm_trig_rate_mirror = setting.rndm_trig_rate;

  db_open_record(hDB, setting.hKeyRndmTrigRate, &setting.rndm_trig_rate,
                 sizeof(setting.rndm_trig_rate), MODE_READ, set_rndm_trig_rate, NULL);
  db_open_record(hDB, setting.hKeyRndmTrigRate, &setting.rndm_trig_rate_mirror,
                 sizeof(setting.rndm_trig_rate_mirror), MODE_WRITE, NULL, NULL);

  /* delay in LED mode */
  sprintf(str, "Delay in LED Mode (nsec)");
  db_create_key(hDB, setting.hKeySet, str, TID_INT);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyLEDDelay);
  size = sizeof(setting.led_delay);
  db_get_value(hDB, setting.hKeySet, str, &(setting.led_delay), &size, TID_INT, FALSE);

  /* check value */
  if(setting.led_delay < 0 || setting.led_delay > 1000000)
    setting.led_delay_mirror = setting.led_delay = 100;
  else
    setting.led_delay_mirror = setting.led_delay;

  db_open_record(hDB, setting.hKeyLEDDelay, &setting.led_delay,
                 sizeof(setting.led_delay), MODE_READ, set_led_delay, NULL);
  db_open_record(hDB, setting.hKeyLEDDelay, &setting.led_delay_mirror,
                 sizeof(setting.led_delay_mirror), MODE_WRITE, NULL, NULL);

  /* length of self-veto */
  sprintf(str, "Self-veto length (nsec)");
  db_create_key(hDB, setting.hKeySet, str, TID_INT);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeySelfVeto);
  size = sizeof(setting.self_veto);
  db_get_value(hDB, setting.hKeySet, str, &(setting.self_veto), &size, TID_INT, FALSE);

  /* check value */
  if(setting.self_veto < 0 || setting.self_veto > 5000000)
    setting.self_veto_mirror = setting.self_veto = 100;
  else
    setting.self_veto_mirror = setting.self_veto;

  db_open_record(hDB, setting.hKeySelfVeto, &setting.self_veto,
                 sizeof(setting.self_veto), MODE_READ, set_self_veto, NULL);
  db_open_record(hDB, setting.hKeySelfVeto, &setting.self_veto_mirror,
                 sizeof(setting.self_veto_mirror), MODE_WRITE, NULL, NULL);

  /* polarity */
  sprintf(str, "Signal Polarity (y:Pos|n:Neg)");
  db_create_key(hDB, setting.hKeySet, str, TID_BOOL);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyPolarity);
  size = sizeof(setting.polarity);
  db_get_value(hDB, setting.hKeySet, str, &(setting.polarity), &size, TID_BOOL, FALSE);

  setting.polarity_mirror = setting.polarity;

  db_open_record(hDB, setting.hKeyPolarity, &setting.polarity,
                 sizeof(setting.polarity), MODE_READ, set_polarity, NULL);
  db_open_record(hDB, setting.hKeyPolarity, &setting.polarity_mirror,
                 sizeof(setting.polarity_mirror), MODE_WRITE, NULL, NULL);

  /* led on */
  sprintf(str, "LED ON");
  db_create_key(hDB, setting.hKeySet, str, TID_BOOL);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyLedOn);
  size = sizeof(setting.led_on);
  db_get_value(hDB, setting.hKeySet, str, &(setting.led_on), &size, TID_BOOL, FALSE);

  setting.led_on_mirror = setting.led_on;

  db_open_record(hDB, setting.hKeyLedOn, &setting.led_on,
                 sizeof(setting.led_on), MODE_READ, set_led_on, NULL);
  db_open_record(hDB, setting.hKeyLedOn, &setting.led_on_mirror,
                 sizeof(setting.led_on_mirror), MODE_WRITE, NULL, NULL);


  return SUCCESS;
}

//=======================================================
// init_cottri()
//=======================================================
INT init_cottri()
{
  INT status;

  //---------------------------------------------------------
  // Initialize
  //---------------------------------------------------------
  /*
  // Set timeout for initialize
  tv.tv_sec  = 0; tv.tv_usec = 100000;
  setsockopt(sitcp_info->tcpfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  flag = 1;
  setsockopt(sitcp_info->tcpfd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag));
  */

  // Enable Nagle
  if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, setting.sitcp_info, TRUE)) return 0;

  if(SUCCESS != set_configuration(TRUE)){
    cm_msg(MERROR, "init_cottri", "Cannot initialize with RBCP commands.");
    return FE_ERR_HW;
  }

  // Disable Nagle
  if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, setting.sitcp_info, FALSE)) return FE_ERR_HW;

  /*
  // Set timeout for DAQ
  tv.tv_sec  = 0; tv.tv_usec = 100000;
  setsockopt(sitcp_info->tcpfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));
  */

  return SUCCESS;
}
