#include "Mfc7.hh"
#include <iostream>
#include <iomanip>
#include <cstdlib>

int Mfc7::open(const char* file)
{
  int ret;

  try {
    manager = new uhal::ConnectionManager(file);
    hw = new uhal::HwInterface(manager->getDevice("fc7.udp.0"));
    ret = 0;
  } catch (std::exception e) {
    e.what();
    ret = -1;
  }

  return ret;
}

int Mfc7::close()
{
  delete hw;
  delete manager;

  return 0;
}

int Mfc7::init()
{
  reg_write("operation_mode", 4);
  reg_write("sfp_tx_enable", 0xffff);
  hw->dispatch();
  reg_write("trigger_number_source_internal", 0);
  reg_write("trigger_number_source_external", 8);
  reg_write("fct_trigger_source_port", 12);
  hw->dispatch();

  sram_reset();

  prev_sram_cfa = reg_read("sram_current_fastwrite_address");
  return 0;
}

int Mfc7::wait()
{
  while(0 == check()) {
    usleep(1);
  }
  return 0;
}

int Mfc7::check()
{
  sram_current_addr = reg_read("sram_current_fastwrite_address");
  //if (prev_sram_cfa != sram_current_addr) {
  if (prev_sram_cfa + 256 <= sram_current_addr) {
    prev_sram_cfa = sram_current_addr;
    return 1;
  } else {
    return 0;
  }
}

unsigned int Mfc7::reg_read(const char *reg_name)
{
  uhal::ValWord< uint32_t > mem = hw->getNode(reg_name).read();
  hw->dispatch();
	
  return mem.value();
}

void Mfc7::reg_write(const char *reg_name, unsigned int val)
{
  hw->getNode(reg_name).write(val);
  return;
}

void Mfc7::sram_reset()
{
  reg_write("sram_reset", 0);
  hw->dispatch();
  reg_write("sram_reset", 1);
  hw->dispatch();
  reg_write("sram_reset", 0);
  hw->dispatch();
  return;
}

int Mfc7::check_fct()
{
  return 1;
}
 

int Mfc7::sram_read8(unsigned int addr, unsigned int *buf)
{

  reg_write ("sram_rw_mode", 1);
  reg_write ("sram_address", addr);
  reg_write ("sram_strobe", 0);
  hw->dispatch();
  reg_write ("sram_strobe", 1);
  hw->dispatch();

  #if 0
  buf[0] = reg_read("sram_read_data7");
  buf[1] = reg_read("sram_read_data6");
  buf[2] = reg_read("sram_read_data5");
  buf[3] = reg_read("sram_read_data4");
  buf[4] = reg_read("sram_read_data3");
  buf[5] = reg_read("sram_read_data2");
  buf[6] = reg_read("sram_read_data1");
  buf[7] = reg_read("sram_read_data0");
  #endif

  uhal::ValWord<uint32_t> mem[8];
  mem[0] = hw->getNode("sram_read_data7").read();
  mem[1] = hw->getNode("sram_read_data6").read();
  mem[2] = hw->getNode("sram_read_data5").read();
  mem[3] = hw->getNode("sram_read_data4").read();
  mem[4] = hw->getNode("sram_read_data3").read();
  mem[5] = hw->getNode("sram_read_data2").read();
  mem[6] = hw->getNode("sram_read_data1").read();
  mem[7] = hw->getNode("sram_read_data0").read();
  hw->dispatch();
  for (int i = 0 ; i < 8 ; i++) buf[i] = mem[i].value();
	
  return 0;
}

