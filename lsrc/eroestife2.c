/********************************************************************   
  Name:         eroestife.c
  Created by:   Kou Oishi

  Contents:     A frontend software for daysy-chained ROESTI and EROS.

  $Id$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "sitcp.h"

// Define if needed to change the ADC latency
//#define ENABLE_CHANGE_LATENCY

/* Some definitions related to either ROESTI or EROS 
   Macro variable "EROS" should be defined in Makefile */
#ifdef EROS
#define _EQUIPMENT_NAME_    "EROS"
#define _EVENT_BUFFER_NAME_ "EBUFF"
#define _BANK_NAME_INFO_    "EIE"
#define _BANK_NAME_MAIN_    "ED"
#define _BANK_NAME_SEU_     "ES"
#else
#define _EQUIPMENT_NAME_    "ROESTI"
#define _EVENT_BUFFER_NAME_ "RBUFF"
#define _BANK_NAME_INFO_    "EIR"
#define _BANK_NAME_MAIN_    "RD"
#define _BANK_NAME_SEU_     "RS"
#endif

//#define TRG_NUM_DEBUG
#define RO_DEBUG
#ifdef RO_DEBUG
#define RO_DEBUG_DUMP(buff, size) fwrite((char*)(buff), (sizeof(char)), (size), (debug_ro))
#else
#define RO_DEBUG_DUMP(buff, size) 
#endif

#define DEBUG(str)    printf("DEBUG: " str "\n")
#define DEBUGV(str,v) printf("DEBUG: " str "\n", v)

/* Constants */
#define DEV_NAME_STR "Board%02d"
#define TIMEOUT   100
  
/* Readout mode */
#define EROESTI_RM_NONE (0)
#define EROESTI_RM_BASE (1<<1)
#define EROESTI_RM_SELF (1<<2)
#define EROESTI_RM_EXTN (1<<3)

#define BUFFSIZE_MAIN (20000)
#define BUFFSIZE_SEU  (1000)
  
/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" 
{
#endif
  /*-- Globals -------------------------------------------------------*/
  struct EROESTI_BOARD_SETTING{
    /* board number */
    INT  index;
    
    /* ODB hundle */
    HNDLE hKeyRoot;
    
    /* SITCP_INFO */
    void* sitcp_info;

    BYTE ip[4];
    BYTE mac[6];
  };
  
  struct EROESTI_SETTING{
    INT    num_boards, num_threads;
    HNDLE  hKeySet, hKeyDev;
    HNDLE  hKeyReadoutMode, hKeyThreshold, 
#ifdef ENABLE_CHANGE_LATENCY
      hKeyLatency, 
#endif
      hKeyZeroSuppress, hKeyDaisyChain, hKeyDecisionTriggering;

    /* setting variables */
    BYTE readout_mode;
    BOOL daisy_chain;
    BOOL decision_triggering;
    BOOL zero_suppress;
    WORD threshold;
#ifdef ENABLE_CHANGE_LATENCY
    BYTE latency;
#endif
    
    /* mirror value */
    BYTE readout_mode_mirror;
    BOOL daisy_chain_mirror;
    BOOL decision_triggering_mirror;
    BOOL zero_suppress_mirror;
    WORD threshold_mirror;
#ifdef ENABLE_CHANGE_LATENCY
    BYTE latency_mirror;
#endif

    /* variables for each of boards */
    struct EROESTI_BOARD_SETTING* board;
  };
  
  typedef struct EROESTI_DATA_t{
    WORD   type;
    BYTE   board_ip[2];
    WORD   board_num;
    INT    trg_num;
    
    struct timeval timestamp;
    INT    datasize_main, datasize_seu;
    INT    seu_count, seu_disable_count;
    
    WORD   buff_main[BUFFSIZE_MAIN];
    DWORD  buff_seu[BUFFSIZE_SEU];

    DWORD  crc32;
  } EROESTI_DATA;
  
  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = _EQUIPMENT_NAME_;
  
  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = FALSE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 3000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 40000*10;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 40000*10;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 5 * 40000;
  
  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_trigger_event(char *pevent, INT off);
  
  INT poll_event(INT source, INT count, BOOL test);
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr);
  INT set_configuration(BOOL first);

  // Dispatchers
  void set_readout_mode(INT hDB, INT hKey, void *info);
  void set_daisy_chain(INT hDB, INT hKey, void *info);
  void set_decision_triggering(INT hDB, INT hKey, void *info);
  void set_zero_suppress(INT hDB, INT hKey, void *info);
  void set_threshold(INT hDB, INT hKey, void *info);
#ifdef ENABLE_CHANGE_LATENCY
  void set_latency(INT hDB, INT hKey, void *info);
#endif
  
  // Initialize an EROESTI
  INT init_eroesti(struct EROESTI_BOARD_SETTING*);
  
  // Clear
  INT clear_eroesti();

  // readout routines
  INT read_a_board(EROESTI_DATA* data);

  /*-- Equipment list ------------------------------------------------*/
  EQUIPMENT equipment[] = {
    {
      _EQUIPMENT_NAME_,      /* equipment name */
      {1, 0,                 /* event ID, trigger mask */
       _EVENT_BUFFER_NAME_,  /* event buffer */
       EQ_POLLED | EQ_EB,    /* equipment type */
       1,                    /* No event source */
       "MIDAS",              /*  format */
       TRUE,                 /* enabled */
       RO_RUNNING |          /* read only when running */
       RO_ODB,               /* and update ODB */
       1,                    /* poll for 1 ms */
       0,                    /* stop run after this event limit */
       0,                    /* number of sub events */
       0,                    /* don't log history */
       "", "", "",},
      read_trigger_event,    /* readout routine */
    },
    
    {""}
  };
  
  /*-- Frontend specific variables ------------------------------------*/

  extern INT run_state; /* STATE_RUNNING, STATE_STOPPED, STATE_PAUSED */
  
  /* ODB */
  HNDLE hDB;

  /* setting */
  struct EROESTI_SETTING setting;
  
  /* Bank names */
  char bk_name_main[5] = _BANK_NAME_MAIN_;
  char bk_name_seu[5]  = _BANK_NAME_SEU_;
  char bk_name_info[5] = _BANK_NAME_INFO_;

#ifdef TRG_NUM_DEBUG
  FILE* debug_fp = NULL;
#endif
#ifdef RO_DEBUG
  FILE* debug_ro = NULL;
#endif
  
#ifdef __cplusplus
}
#endif
  

/********************************************************************   \
              Some helper routines
\********************************************************************/
INT connect_disconnect_sitcps(BOOL disconnect)
{
  INT  n_boards = setting.num_boards;
  INT  board, status;
  char data = 0x01; /* command to connect */
  INT  cmd  = CMD_OPEN;
  
  if(disconnect){
    data = 0x00; /* command to disconnect */
    cmd  = CMD_CLOSE;
  }

  /* in daisy chain mode, chained boards are (dis)connected by RBCP command */
  if(setting.daisy_chain_mirror){
    for(board = setting.num_boards-2; board >= 0; board--){  
      status = sitcp(CMD_SITCP_RBCP_WRITE, 
                     setting.board[board].sitcp_info, 0x16A, &data, 1, FALSE);
      if(status != SUCCESS) return status;
    }
    /* only the first board is (dis)connected by PC*/
    n_boards = 1;
  }
  
  for(board = 0; board < n_boards; board++){  
    status = sitcp(cmd, setting.board[board].sitcp_info);
    if(status != SUCCESS){
      cm_msg(MERROR, "connect_disconnect_sitcps",
             "Error! %s:" DEV_NAME_STR "'s TCP/IP cannot be (dis)connected.\n"
             "Please check /Equipment/%s/Settings/Devices/"DEV_NAME_STR"/Host and Port.",
             equipment[0].name, board, equipment[0].name);
        return FE_ERR_HW;
    }
#ifdef TRG_NUM_DEBUG
    fprintf(debug_fp, "Board No. %d (dis)connected..\n", board);
#endif
    
    /* increase the send buffer */
    //if(false && !disconnect){
    if(0){
      /* increase receive buffer */
      const INT recvbuffer_size = 33554432;
      if ( sitcp(CMD_SITCP_SET_SOCKOPT, setting.board[board].sitcp_info, 
                 SOL_SOCKET, SO_RCVBUF, (char *)&recvbuffer_size, sizeof(recvbuffer_size)) < 0){
        cm_msg(MERROR, "frontend_init", "setsockopt failed");
        return FE_ERR_HW;
      }
    }
  }
  return SUCCESS;
}

INT connect_to_sitcps()
{
  return connect_disconnect_sitcps(FALSE);
}
INT disconnect_from_sitcps()
{
  return connect_disconnect_sitcps(TRUE);
}

/* ------------------------------------------------------------------------
   [setup_control_odb_variable]
   used in setup_control_odb_variables()
*/
void setup_control_odb_variable(HNDLE hDB, const char* name, HNDLE* key, 
                                INT tid, INT size, void* data, void* data_mirror,
                                void (*dispatcher) (INT, INT, void *))
{
  /* create newly or get the value rom odb */
  db_create_key(hDB, setting.hKeySet, name, tid);
  db_find_key(hDB, setting.hKeySet, name, key);
  db_get_value(hDB, setting.hKeySet, name, data, &size, tid, FALSE);
  
  /* copy data to mirror data */
  memcpy(data_mirror, data, size);
  
  /* register records with a dispatcher */
  db_open_record(hDB, *key, data, size, MODE_READ, dispatcher, NULL);
  db_open_record(hDB, *key, data_mirror, size, MODE_WRITE, NULL, NULL);
}

/* ------------------------------------------------------------------------
   [setup_control_odb_variables]
   used in frontend_init 
*/
void setup_control_odb_variables()
{
  /* readout mode */
  setup_control_odb_variable(hDB, "Readout Mode", &setting.hKeyReadoutMode, 
                             TID_BYTE, sizeof(setting.readout_mode),
                             &setting.readout_mode, &setting.readout_mode_mirror,
                             set_readout_mode);
  if(setting.readout_mode != EROESTI_RM_NONE && /* check the value */
     setting.readout_mode != EROESTI_RM_BASE &&
     setting.readout_mode != EROESTI_RM_SELF &&
     setting.readout_mode != EROESTI_RM_EXTN ){
    setting.readout_mode_mirror = setting.readout_mode = EROESTI_RM_NONE;
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode", &setting.readout_mode, 
                 sizeof(setting.readout_mode), 1, TID_BYTE);
  }
  /* daisy chain */
  setup_control_odb_variable(hDB, "Daisy Chain", &setting.hKeyDaisyChain, 
                             TID_BOOL, sizeof(setting.daisy_chain),
                             &setting.daisy_chain, &setting.daisy_chain_mirror,
                             set_daisy_chain);
  /* decision triggering  */
  setup_control_odb_variable(hDB, "Decision Triggering", &setting.hKeyDecisionTriggering, 
                             TID_BOOL, sizeof(setting.decision_triggering),
                             &setting.decision_triggering, &setting.decision_triggering_mirror,
                             set_decision_triggering);
  /* zero suppress  */
  setup_control_odb_variable(hDB, "Zero Suppress", &setting.hKeyZeroSuppress, 
                             TID_BOOL, sizeof(setting.zero_suppress),
                             &setting.zero_suppress, &setting.zero_suppress_mirror,
                             set_zero_suppress);
  /* threshold */
#ifndef EROS /* EROS doesn't have ASD */
  setup_control_odb_variable(hDB, "Threshold", &setting.hKeyThreshold,
                             TID_WORD, sizeof(setting.threshold),
                             &setting.threshold, &setting.threshold_mirror,
                             set_threshold);
  if(setting.threshold > 0xFFF){ /* check the value */
    setting.threshold_mirror = setting.threshold = 0x000;
    db_set_value(hDB, setting.hKeySet, "Threshold", &setting.threshold, 
                 sizeof(setting.threshold), 1, TID_WORD);
  }
#endif
  
#ifdef ENABLE_CHANGE_LATENCY
  /* latency  */
  setup_control_odb_variable(hDB, "Latency", &setting.hKeyLatency,
                             TID_BYTE, sizeof(setting.latency),
                             &setting.latency, &setting.latency_mirror,
                             set_latency);
#endif
}

/* ------------------------------------------------------------------------
   [get_board_num_from_ip]
   returns the board number corresponding to the given ip.
   used in readout_thread. 
*/
INT get_board_num_from_ip(BYTE ip0, BYTE ip1, INT* board)
{
  for(*board=0; (*board)<setting.num_boards; (*board)++){
    if(ip1 == setting.board[*board].ip[3] &&
       ip0 == setting.board[*board].ip[2]) return SUCCESS;
  }

  *board = -1;
  cm_msg(MERROR, "get_board_num_from_ip", 
         "Found an unknown ip address, %d.%d", ip0, ip1);
  return FE_ERR_HW;
}

/********************************************************************   \
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/


INT read_until_find(void* sitcp_info, char* key, int key_size, 
                    char* read_buff, int read_buff_size, int timeout,
                    int* readout_bytes)
{
  const int timeout_elem = 1;
  int   status, recv_bytes = 0, retries = 0, i = 0;
  char  test_char;
  
  if(key_size < 1) return -1;

  if(readout_bytes) readout_bytes=0;
  
  while(1){
    /* read a char */
    status = sitcp(CMD_READ, sitcp_info, &test_char, 1, timeout_elem);
    
    /* check error */
    if(status == 0){
      retries++;
      /* 10 times continuous null readout -> time out */
      if(retries == timeout/timeout_elem + 1){
        //printf("read %d bytes but timeout\n", recv_bytes);
        status = -1;
        break;
      }
      continue;
    }
    else if(status < 0){
      cm_msg(MERROR, "read_until_find", "TCP reading process returned error");
      status = -1;
      break;
    }    
    retries = 0; /* reset retries */
    
    /* add to read_buff */
    recv_bytes++;
    if(readout_bytes) readout_bytes++;
    if(read_buff && recv_bytes < read_buff_size){
      read_buff[recv_bytes] = test_char;
    }
    
    /* check matching with the key */
    if(test_char == key[i]) i++; /* i-th char matched to the key */ 
    else{
      /* reset key index */
      i = 0; 
      recv_bytes=0;
      if(readout_bytes) readout_bytes=0;
      continue;
    }
    
    /* all matched */
    if(i == key_size){
      status = SUCCESS;
      break;
    }
  }
  
  if(readout_bytes) *readout_bytes = recv_bytes;
  return status;
}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  INT status, size;
  char str[256];
  HNDLE hKey;
  INT retry = 0;
  INT board;

#ifdef TRG_NUM_DEBUG
  debug_fp = fopen("trg_num_debug.dat", "w");
  if(!debug_fp){
    cm_msg(MERROR, "frontend_init", "Couldn't make trg_num_debug file.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "Made debug file.");
#endif
#ifdef RO_DEBUG
  debug_ro = fopen("ro_debug.dat", "wb");
  if(!debug_ro){
    cm_msg(MERROR, "frontend_init", "Couldn't make ro_debug file.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "Made readout debug file.");
#endif
  
  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  cm_get_experiment_database(&hDB, NULL);
  
  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &hKey);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_ODB;
  }
  
  //---------------------------------------------------------
  // Load configurations from ODB
  //---------------------------------------------------------
  status = db_create_key(hDB, hKey, "Settings", TID_KEY);
  status = db_find_key(hDB, hKey, "Settings", &setting.hKeySet);
  
  /* get num_boards */
  sprintf(str, "Number of Boards");
  db_create_key(hDB, setting.hKeySet, str, TID_INT);
  db_find_key(hDB, setting.hKeySet, str, &setting.num_boards);
  size = sizeof(setting.num_boards);
  db_get_value(hDB, setting.hKeySet, str, &setting.num_boards, &size, TID_INT, FALSE);
  if(setting.num_boards <= 0){
    cm_msg(MERROR, "frontend_init", "No module is assigned.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "%d boards to be initialized.", setting.num_boards);
  
  /* alloc board setting */
  setting.board = (struct EROESTI_BOARD_SETTING*)
    calloc(setting.num_boards, sizeof(struct EROESTI_BOARD_SETTING));
  
  /* device directory */
  status = db_create_key(hDB, setting.hKeySet, "Devices", TID_KEY);
  status = db_find_key(hDB, setting.hKeySet, "Devices", &setting.hKeyDev);
  
  /* Host & MAC address directory */
  status = db_create_key(hDB, setting.hKeySet, "Host List", TID_KEY);

  /* create ODB record of SiTCP for each device */
  for(board = 0; board < setting.num_boards; board++){  
    char  strBoard[256], strLink[256], strDest[256];
    INT status;
    
    /* created root record */
    sprintf(strBoard, DEV_NAME_STR, board);
    status = db_create_key(hDB, setting.hKeyDev, strBoard, TID_KEY);
    status = db_find_key(hDB, setting.hKeyDev, strBoard, &setting.board[board].hKeyRoot);
    
    /* DO NOT establish connection now */
    status = sitcp(CMD_INIT, setting.board[board].hKeyRoot, 
                   &(setting.board[board].sitcp_info));
    if(status != SUCCESS){
      cm_msg(MERROR, "frontend_init", "Cannot initialize SITCP of " DEV_NAME_STR, board);
      return FE_ERR_ODB;
    }
    
    /* link to each Host and MAC address record */
    sprintf(strLink, "/Equipment/%s/Settings/Host List/%s IP", equipment[0].name, strBoard);
    sprintf(strDest, "/Equipment/%s/Settings/Devices/%s/SiTCP/IP", equipment[0].name, strBoard);
    if(DB_SUCCESS != db_find_key(hDB, 0, strLink, &hKey))
      db_create_link(hDB, 0, strLink, strDest);
    sprintf(strLink, "/Equipment/%s/Settings/Host List/%s MAC Address", equipment[0].name, strBoard);
    sprintf(strDest, "/Equipment/%s/Settings/Devices/%s/SiTCP/MAC Address", equipment[0].name, strBoard);
    if(DB_SUCCESS != db_find_key(hDB, 0, strLink, &hKey))
      db_create_link(hDB, 0, strLink, strDest);

    /* get ip/mac address */
    sitcp(CMD_SITCP_GET_IP_ADDRESS, 
          setting.board[board].sitcp_info, setting.board[board].ip);
    sitcp(CMD_SITCP_GET_MAC_ADDRESS, 
          setting.board[board].sitcp_info, setting.board[board].mac);
    cm_msg(MINFO, "frontend_init", "Connected to Board %02d (%d.%d.%d.%d)",
           board, setting.board[board].ip[0],
           setting.board[board].ip[1],
           setting.board[board].ip[2],
           setting.board[board].ip[3]);
  }

  /* control variables in ODB */
  setup_control_odb_variables();

  /* Finish */
  printf(_EQUIPMENT_NAME_ " Initialize completed.\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  INT board;

  //clear_eroesti();
  
  for(board = 0; board < setting.num_boards; board++){
    //sitcp(CMD_SITCP_ENABLE_NAGLE, setting.board[board].sitcp_info, TRUE);
    sitcp(CMD_EXIT, setting.board[board].sitcp_info);
  }
  
  free(setting.board);
  setting.board = NULL;
  
#ifdef TRG_NUM_DEBUG
  fclose(debug_fp);
#endif
#ifdef RO_DEBUG
  fclose(debug_ro);
#endif

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  char data;
  INT  status;
  INT  board;
  
  DEBUG("BEGIN OF RUN");
  clear_eroesti();
  
  /* check the readout mode */
  if(setting.readout_mode_mirror == EROESTI_RM_NONE){
    cm_msg(MERROR, "begin_of_run", "Readout mode is none, set a valid mode");
    return FE_ERR_ODB;;
  }
  
  if(SUCCESS != set_configuration(FALSE)){
    cm_msg(MERROR, "begin_of_run", "Cannot initialize with RBCP commands.");
    return FE_ERR_HW;
  }
  
  /* -----------------------------------------------
     Connect to SiTCP socket 
     ----------------------------------------------- */
  DEBUG("Connecting... SiTCP(s)");
  if(SUCCESS != connect_to_sitcps()){
    DEBUG("Couldn't connect to SiTCP(s)");
    return FE_ERR_HW;    
  }
  DEBUG("Finished.");

  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
  
INT end_of_run(INT run_number, char *error)
{
  INT board;
  INT i;

  // close 
  if(SUCCESS != disconnect_from_sitcps()) return FE_ERR_HW;
  
  clear_eroesti(); /* discard data remaining in the socket */
  
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}
  
/*-- Resuem Run ----------------------------------------------------*/
  
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
  
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/
/* -- read_a_board --------------------------------------------------------------------- */
INT read_a_board(EROESTI_DATA* data)
{
  const INT tcp_data = 1;
  struct timeval tval;
  char  tmpchar, next;
  DWORD hdbuff[6];
  DWORD footer[10];
  INT   recv_bytes, exp_bytes;
  INT   retries;
  int   status;
  DWORD key;
  
  struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[0];
  recv_bytes = sitcp(CMD_READ, pbsetting->sitcp_info, (char*)&hdbuff[1], 
                     5*sizeof(DWORD), 500);
  RO_DEBUG_DUMP(hdbuff, recv_bytes);
  if(recv_bytes < 5*sizeof(DWORD)){
    printf("Too little %d\n", recv_bytes);
    return -1;
  }
  
  /* store to EROESTI_DATA (take care of little endian) */
  data->type              = 0xFFFF & (hdbuff[1]);
  data->board_ip[1]       = 0xFF   & (hdbuff[1]>>16);
  data->board_ip[0]       = 0xFF   & (hdbuff[1]>>24);
  data->datasize_main     = hdbuff[2];
  data->trg_num           = hdbuff[3];
  data->seu_count         = 0xFFFF & (hdbuff[4]);
  data->seu_disable_count = 0xFFFF & (hdbuff[4]>>16);
  data->datasize_seu      = 0xFFFF & (hdbuff[5]);
  data->timestamp         = tval;
  
  /* data is overflowed the buffer */
  exp_bytes = data->datasize_main + data->datasize_seu + sizeof(DWORD)*2;
  if(exp_bytes > sizeof(WORD) * BUFFSIZE_MAIN){
    return FE_ERR_HW;
  }

  // Read everything
  recv_bytes = 0;
  retries = 0;
  do{
    retries++;
    recv_bytes += sitcp(CMD_READ, pbsetting->sitcp_info, 
                        (char*)(data->buff_main) + recv_bytes,
                        exp_bytes - recv_bytes, 200);
  }while(recv_bytes < exp_bytes && retries < 10);
  RO_DEBUG_DUMP(data->buff_main, recv_bytes);
  
  // time out 
  if(retries == 10){
    DEBUG("MAINDATA READ TIMEOUT");
    return -1;
  }

  footer[0] = *(DWORD*)&(data->buff_main[(data->datasize_main + data->datasize_seu)/sizeof(WORD)]);
  footer[1] = *(DWORD*)&(data->buff_main[(data->datasize_main + data->datasize_seu)/sizeof(WORD) + 2]); 
  if(footer[0] != 0xFEDCBA98){
#ifdef DEBUG
    int i;
    printf("ILLEGAL FOOTER WORD 0x%x\n", footer[0]);
    printf("IP %d.%d: size = 0x%x, trg num = 0x%x \n"
           "seu(count %d, disable count %d, size %d)\n",
           data->board_ip[0], data->board_ip[1],
           data->datasize_main, data->trg_num,
           data->seu_count, data->seu_disable_count,
           data->datasize_seu);
    for(i=data->datasize_main/sizeof(WORD)-20; i<data->datasize_main/sizeof(WORD); i++){
      printf("0x%x\n", data->buff_main[i]);
    }
#endif
    return -1;
  }
  data->crc32 = footer[1]; /* CRC32 info */
  
  return SUCCESS;
}


/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. 
   The test flag is used to time the polling */
{
  const INT tcp_data = 1;
  struct timeval tval;
  char  tmpchar, next;
  DWORD hdbuff[6];
  DWORD footer[10];
  INT   recv_bytes, exp_bytes;
  INT   retries;
  int   status;
  DWORD key;
  struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[0];

  //  if(!test) DEBUG("Entered poll_event");

  //header
  next=0xef;
  retries=0;
  while(1){
    recv_bytes = sitcp(CMD_READ, pbsetting->sitcp_info, &tmpchar, 1, 500);
    if(test){
      retries++;
      if(retries >= count) return FALSE;
      continue;
    }

    if(recv_bytes < 1) return FALSE;
    RO_DEBUG_DUMP(&tmpchar, 1);
    
    if(next != tmpchar){
      next=0xef;
      retries++;
      DEBUGV("Skip 0x%02x", (BYTE)tmpchar );
      if(retries >= count) return FALSE;
      return FALSE;
    }
    switch((BYTE)tmpchar){
    case 0xef: next=0xcd; break;
    case 0xcd: next=0xab; break;
    case 0xab: next=0x89; break;
    case 0x89: next=0x00; break;
    }
    if(next==0x00){
      if(retries > 0) printf("Retried %d times\n", retries);
      //  else DEBUG("Got a header!!");
      return TRUE;
    }
  }
  return FALSE;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}
  
/*-- Event readout -------------------------------------------------*/
  
INT read_trigger_event(char *pevent, INT off)
{
  INT i, status, board;
  DWORD trg_num;
  struct timeval timestamp = {0,0};
  EROESTI_DATA data;
  DWORD *pdata_dword;
  WORD  *pdata_word;

  if(SUCCESS != read_a_board(&data)){
    exit(1);
  }
  
  // init bank structure 
  bk_init(pevent);

  /// create Event Info bank 
  bk_create(pevent, bk_name_info, TID_DWORD, (void **)&pdata_dword);
  
  // append header information 
  *pdata_dword++ = trg_num; // event counter 
  *pdata_dword++ = data.type;
  *pdata_dword++ = data.board_ip[0];
  *pdata_dword++ = data.board_ip[1];
  *pdata_dword++ = data.board_num;
  *pdata_dword++ = data.datasize_main;
  *pdata_dword++ = data.seu_count;
  *pdata_dword++ = data.seu_disable_count;
  *pdata_dword++ = data.datasize_seu;
  *pdata_dword++ = data.crc32;
  *(struct timeval*)pdata_dword = data.timestamp;
  pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);
  
  bk_close(pevent, pdata_dword);

  // create main data bank 
  bk_create(pevent, bk_name_main, TID_WORD, (void **)&pdata_word);
  memcpy(pdata_word, data.buff_main, data.datasize_main);
  pdata_word += data.datasize_main/sizeof(WORD);
  bk_close(pevent, pdata_word);
  
  // create SEU info bank 
  if(data.datasize_seu > 0){
      bk_create(pevent, bk_name_seu, TID_DWORD, (void **)&pdata_dword);
      memcpy(pdata_word, data.buff_seu, data.datasize_seu);
      pdata_dword += data.datasize_seu/sizeof(DWORD);
      bk_close(pevent, pdata_dword);
  }
  return bk_size(pevent);
}

//=======================================================
INT clear_eroesti()
//=======================================================
{
  INT board, recv_bytes;
  char dummy[1000];
  SITCP_RBCP_COMMAND command[] = {
    {0x16a, 0x00, TRUE},
    {0x11d, EROESTI_RM_NONE, TRUE},
    {0x110, 0xf0, TRUE},
    {0x112, 0x01, TRUE}
  };
  
  DEBUG("Clear eroesti");
  for(board = 0; board < setting.num_boards; board++){
    struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[board];
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info,
                        1, command, 1000)){
      cm_msg(MERROR, "clear_eroesti", "Error in sending the rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info,
                       3 /*sizeof(command)/sizeof(SITCP_RBCP_COMMAND)*/, &command[1], 50)){
      cm_msg(MERROR, "clear_eroesti", "Error in sending the rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }
    //ss_sleep(00);
  }

  DEBUG("Finished clear eroesti");
  return SUCCESS;
}
  

INT set_configuration(BOOL first)
{
  int status, i, size, board;
  int dindex;

  SITCP_RBCP_COMMAND command[] = {
    {0x110, 0xF0, TRUE}, // reset FPGA
    {0x170, 31,   TRUE}, // sampling speed
    
    /* ADC */
    {0x00030000, 0x08, TRUE},
    {0x00030002, 0x06, TRUE},
    {0x00010000, 0x3C, TRUE},
    {0x00030000, 0x3C, TRUE},
    {0x00010008, 0x03, TRUE},
    {0x00010008, 0x00, TRUE},
    {0x00020008, 0x03, TRUE},
    {0x00020008, 0x00, TRUE},
    {0x00010014, 0x00, TRUE},
    {0x00020014, 0x00, TRUE},
    {0x0001000D, 0x00, TRUE},
    {0x0002000D, 0x00, TRUE},
    
#ifndef EROS
    /* ASD threshold : EROS doesn't have ASD */
    {0x115, (char)(0x0F & (setting.threshold>>8))},
    {0x116, (char)(0xFF & (setting.threshold   ))},
    //    {0x141, 60}, // ASD check start time
    //    {0x142, 70},  // ASD wait width
#endif    

    /* readout mode is none initially */
    //{0x11d, (char)EROESTI_RM_NONE},
    {0x11d, (char)setting.readout_mode_mirror},
    
    {0x171, (char)(setting.decision_triggering_mirror? 1 : 0)}, // decision triggering
    {0x119, 0x01, TRUE}, // DAC Sync
    {0x111, 0xF0, TRUE}, // reset DRS4 
    {0x110, 0xF0, TRUE}, // Internally reset FPGA
    {0x11B, 0x02},       // phase active clock setting (not functional now)

    // ADC Latency (Basically fixed at 2F)
#ifdef ENABLE_CHANGE_LATENCY
    {0x11A, (char)(0xFF & setting.latency_mirror)}, 
#else
    {0x11A, 0x2F},
#endif
    
    {0x112, 0x01, TRUE}, // reset FPGA FIFO
    {0x00010016, 0x04, TRUE},  // ADC
    {0x00020016, 0x04, TRUE},  // ADC

    // daisy chain 
    {0x153, 0x1, TRUE},
    {0x163, 192},
    {0x162, 163},
    {0x161, 10},
    {0x160, 0},
    {0x169, 0x7C},
    {0x168, 0xF0},
    {0x167, 0x98},
    {0x166, 0x01},
    {0x165, 0},
    {0x164, 0},

    {0x130, 0x01, TRUE}, // reset SEU monitor
    {0x124, 0x01, TRUE}, // reset SEU counter
    {0x16E, 0x00, TRUE}  // disable CRC check
    
    /* zero suppress */
    //{0x140, (char)(setting.zero_suppress_mirror? 1:0)}
  };
  
  for(dindex=0; dindex<sizeof(command)/sizeof(SITCP_RBCP_COMMAND); dindex++){
    command[dindex].inhibit_validation = TRUE;
  }
  for(dindex=0; dindex<sizeof(command)/sizeof(SITCP_RBCP_COMMAND); dindex++){
    if(command[dindex].address == 0x153) break;
  }

  // loop for boards
  for(board=0; board<setting.num_boards; board++){
    if(setting.daisy_chain_mirror){
      // get IP/MAC address of the next module 
      if(board+1 == setting.num_boards){ // The farest module
        for(i=0; i<11; i++) command[dindex+i].data = 0;
      }
      else{
        command[dindex].data = 1;
        for(i=0; i<4; i++) command[dindex+1+i].data = setting.board[board+1].ip[i];
        for(i=0; i<6; i++) command[dindex+5+i].data = setting.board[board+1].mac[i];
      }
    }
    else{
      for(i=0; i<11; i++) command[dindex+i].data = 0;
    }

    struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[board];
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info, 
                        sizeof(command)/sizeof(SITCP_RBCP_COMMAND), command, 50)){
      cm_msg(MERROR, "set_configuration", 
             "Error in sending rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }
    
    if(board == 0){
      if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, TRUE))
        return FE_ERR_HW;
    }
    else{
      if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, FALSE))
        return FE_ERR_HW;
    }

    /* daisy_chain configuration */
    /*
    if(setting.daisy_chain_mirror){
      SITCP_RBCP_COMMAND daisy_command[] = {
        {0x153, 0x1},
        {0x163, 192},
        {0x162, 163},
        {0x161, 10},
        {0x160, 0},
        {0x169, 0x7C},
        {0x168, 0xF0},
        {0x167, 0x98},
        {0x166, 0x01},
        {0x165, 0},
        {0x164, 0}
      };  
      
      // get IP/MAC address of the next module 
      if(board+1 == setting.num_boards){ // The farest module
        for(i=0; i<11; i++) daisy_command[i].data = 0;
      }
      else{
        daisy_command[0].data = 1;
        for(i=0; i<4; i++) daisy_command[1+i].data = setting.board[board+1].ip[i];
        for(i=0; i<6; i++) daisy_command[5+i].data = setting.board[board+1].mac[i];
      }
      
#if 0
      printf("DEBUG board %d is daisy-chained to %d.%d.%d.%d ", board,
             (unsigned char)daisy_command[1].data, (unsigned char)daisy_command[2].data,
             (unsigned char)daisy_command[3].data, (unsigned char)daisy_command[4].data);
      printf("(%02x:%02x:%02x:%02x:%02x:%02x)\n",
             (unsigned char)daisy_command[5].data, (unsigned char)daisy_command[6].data,
             (unsigned char)daisy_command[7].data, (unsigned char)daisy_command[8].data,
             (unsigned char)daisy_command[9].data, (unsigned char)daisy_command[10].data);
#endif

      // send commands
      if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info, 
                          sizeof(daisy_command)/sizeof(SITCP_RBCP_COMMAND),
                          daisy_command, 10) ){
        cm_msg(MERROR, "set_configuration", 
               "Error in sending the rbcp cmds for daisy-chain to " DEV_NAME_STR,
               pbsetting->index);
        return FE_ERR_HW;
      }

      // Nagle control (only it of the first board is switched on)
      if(board == 0){
        if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, TRUE))
          return FE_ERR_HW;
      }
      else{
        if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, FALSE))
          return FE_ERR_HW;
      }
    
    } // end of daisy chain configuration
    else {
      // when not using daisy chain, all modules are nagle-off
      if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, FALSE))
        return FE_ERR_HW;
    }
    */
  } // end of loop for boards
  
  return SUCCESS;
}

//=======================================================
// ODB dispatchers
//=======================================================
void set_readout_mode(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.readout_mode == setting.readout_mode_mirror) return;
  
  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode", 
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }

  switch(setting.readout_mode){
  case EROESTI_RM_NONE:
    printf("Stopped\n");
    break;

  case EROESTI_RM_BASE:
    printf("Set readout mode.\n");
    break;

  case EROESTI_RM_SELF:
    printf("Set readout mode.\n");
    break;

  case EROESTI_RM_EXTN:
    printf("Set readout mode.\n");
    break;

  default:
    /* Illegal command */
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode", 
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }
  
  setting.readout_mode_mirror = setting.readout_mode;
  
  set_configuration(FALSE);
}

void set_daisy_chain(INT hDB, INT hKey, void *info)
{
  if(setting.daisy_chain == setting.daisy_chain_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.daisy_chain = setting.daisy_chain_mirror;
    db_set_value(hDB, setting.hKeySet, "Daisy Chain",
                 &setting.daisy_chain, sizeof(setting.daisy_chain),
                 1, TID_BOOL);
  }
  
  if(setting.daisy_chain) printf("enabled daisy chain.\n");
  else                    printf("disabled daisy chain.\n");
  setting.daisy_chain = setting.daisy_chain_mirror;
  
  set_configuration(FALSE);
}

void set_decision_triggering(INT hDB, INT hKey, void *info)
{
  if(setting.decision_triggering == setting.decision_triggering_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.decision_triggering = setting.decision_triggering_mirror;
    db_set_value(hDB, setting.hKeySet, "Decision Triggering",
                 &setting.decision_triggering, sizeof(setting.decision_triggering),
                 1, TID_BOOL);
  }
  
  if(setting.decision_triggering) printf("enabled decision triggering.\n");
  else                      printf("disabled decision triggering.\n");
  setting.decision_triggering = setting.decision_triggering_mirror;
  
  set_configuration(FALSE);
}

void set_zero_suppress(INT hDB, INT hKey, void *info)
{
  if(setting.zero_suppress == setting.zero_suppress_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.zero_suppress = setting.zero_suppress_mirror;
    db_set_value(hDB, setting.hKeySet, "Zero Suppress", 
                 &setting.zero_suppress, sizeof(setting.zero_suppress),
                 1, TID_BOOL);
  }
  
  if(setting.zero_suppress) printf("enabled zero suppress.\n");
  else                      printf("disabled zero suppress.\n");
  setting.zero_suppress = setting.zero_suppress_mirror;
  
  set_configuration(FALSE);
}

void set_threshold(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.threshold == setting.threshold_mirror) return;
  
  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED || setting.threshold > 0xFFF){
    setting.threshold = setting.threshold_mirror;
    db_set_value(hDB, setting.hKeySet, "Threshold", 
                 &setting.threshold, sizeof(setting.threshold),
                 1, TID_WORD);
    return;
  }
  
  printf("set threshold to 0x%03x\n", setting.threshold);
  setting.threshold_mirror = setting.threshold;
  
  set_configuration(FALSE);
}

#ifdef ENABLE_CHANGE_LATENCY
void set_latency(INT hDB, INT hKey, void *info)
{
  if(setting.latency == setting.latency_mirror) return;
  
  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.latency = setting.latency_mirror;
    db_set_value(hDB, setting.hKeySet, "Latency", 
                 &setting.latency, sizeof(setting.latency),
                 1, TID_BYTE);
    return;
  }
  
  printf("set latency to 0x%02x\n", setting.latency);
  setting.latency_mirror = setting.latency;

  set_configuration(FALSE);
}
#endif
