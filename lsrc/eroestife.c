/********************************************************************
  Name:         eroestife.c
  Created by:   Kou Oishi

  Contents:     A frontend software for daysy-chained ROESTI and EROS.

  $Id$

\********************************************************************/

//----------------------------------------------------
// Debugging macros
//----------------------------------------------------
//#define TRG_NUM_DEBUG
#ifdef TRG_NUM_DEBUG
#define TRG_DEBUG_DUMP(str)       fprintf(debug_fp, str "\n")
#define TRG_DEBUG_DUMPV(str, ...) fprintf(debug_fp, str "\n", __VA_ARGS__)
#else
#define TRG_DEBUG_DUMP(str)
#define TRG_DEBUG_DUMPV(str, ...)
#endif

//#define RO_DEBUG
#ifdef RO_DEBUG
#define RO_DEBUG_DUMP(buff, size) fwrite((char*)(buff), (sizeof(char)), (size), (debug_ro))
#else
#define RO_DEBUG_DUMP(buff, size)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "sitcp.h"
#include "fifo.h"

// These debug functions are available in debug mode.
extern BOOL debug;
#define DEBUG(str)       if(debug) printf("DEBUG: " str "\n")
#define DEBUGV(str, ...) if(debug) printf("DEBUG: " str "\n", __VA_ARGS__)


// Define if needed to change the ADC latency
//#define ENABLE_CHANGE_LATENCY

/* Some definitions related to either ROESTI or EROS
   Macro variable "EROS" should be defined in Makefile */
#ifdef EROS
#define _EQUIPMENT_NAME_    "EROS"
#define _EVENT_BUFFER_NAME_ "EBUFF"
#define _BANK_NAME_INFO_    "EIE"
#define _BANK_NAME_MAIN_    "ED"
#define _BANK_NAME_SEU_     "ES"
#elif defined(PMT)
#define _EQUIPMENT_NAME_    "PMT EROS"
#define _EVENT_BUFFER_NAME_ "PBUFF"
#define _BANK_NAME_INFO_    "EIP"
#define _BANK_NAME_MAIN_    "PD"
#define _BANK_NAME_SEU_     "PS"
#else
#define _EQUIPMENT_NAME_    "ROESTI"
#define _EVENT_BUFFER_NAME_ "RBUFF"
#define _BANK_NAME_INFO_    "EIR"
#define _BANK_NAME_MAIN_    "RD"
#define _BANK_NAME_SEU_     "RS"
#endif

/* Pseudo software-based daisy chain or parallel */
#define LOCK_MUTEX(board)   pthread_mutex_lock(&binfo[(board)].mutex)
#define UNLOCK_MUTEX(board) pthread_mutex_unlock(&binfo[(board)].mutex)

/* Constants */
#define DEV_NAME_STR "Board%02d"
#define NCHANNELS 16
#define TIMEOUT   100

/* Readout mode */
#define EROESTI_RM_NONE (0)
#define EROESTI_RM_BASE (1<<1)
#define EROESTI_RM_SELF (1<<2)
#define EROESTI_RM_EXTN (1<<3)

#define BUFFSIZE_MAIN (20000)
#define BUFFSIZE_SEU  (1000)

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C"
{
#endif
  /*-- Globals -------------------------------------------------------*/
  struct EROESTI_BOARD_SETTING{
    /* board number */
    INT  index;

    /* ODB hundle */
    HNDLE hKeyRoot;

    /* SITCP_INFO */
    void* sitcp_info;

    BYTE ip[4];
    BYTE mac[6];
  };

  struct EROESTI_SETTING{
    INT    num_boards, num_threads;
    HNDLE  hKeySet, hKeyDev;
    HNDLE  hKeyReadoutMode, hKeyThreshold, 
#ifdef ENABLE_CHANGE_LATENCY
      hKeyLatency,
#endif
      hKeyZeroSuppress, hKeyDaisyChain, 
      hKeyDecisionTriggering, hKeyFrequency;

    /* setting variables */
    BYTE readout_mode;
    BOOL daisy_chain;
    BOOL decision_triggering;
    BOOL zero_suppress;
    WORD threshold;
#ifdef ENABLE_CHANGE_LATENCY
    BYTE latency;
#endif
    BYTE frequency;

    /* mirror value */
    BYTE readout_mode_mirror;
    BOOL daisy_chain_mirror;
    BOOL decision_triggering_mirror;
    BOOL zero_suppress_mirror;
    WORD threshold_mirror;
#ifdef ENABLE_CHANGE_LATENCY
    BYTE latency_mirror;
#endif
    BYTE frequency_mirror;

    /* variables for each of boards */
    struct EROESTI_BOARD_SETTING* board;
  };

  typedef struct EROESTI_DATA_t{
    WORD   type;
    BYTE   board_ip[2];
    WORD   board_num;
    INT    trg_num;

    struct timeval timestamp;
    INT    datasize_main, datasize_seu;
    INT    seu_count, seu_disable_count;

    WORD   buff_main[BUFFSIZE_MAIN];
    DWORD  buff_seu[BUFFSIZE_SEU];

    DWORD  crc32;
  } EROESTI_DATA;

  struct EROESTI_BOARD_INFO{
    char  bk_name_main[5];
    char  bk_name_seu[5];

    FIFO*         fifo;
    DWORD         next_trg_num;
    BOOL          skip_next;
    EROESTI_DATA  data;

    pthread_mutex_t mutex;
  };

  /* threads */
  pthread_t* thread = NULL;
  BOOL do_exit_thread = FALSE;
  BOOL thread_err_happened = FALSE;

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char *frontend_name = _EQUIPMENT_NAME_;

  /* The frontend file name, don't change it */
  char *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = FALSE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 3000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 40000*10;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 40000*10;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 5 * 40000;

  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();

  INT read_trigger_event(char *pevent, INT off);

  INT poll_event(INT source, INT count, BOOL test);
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr);
  INT set_configuration(BOOL first);

  // Dispatchers
  void set_readout_mode(INT hDB, INT hKey, void *info);
  void set_daisy_chain(INT hDB, INT hKey, void *info);
  void set_decision_triggering(INT hDB, INT hKey, void *info);
  void set_zero_suppress(INT hDB, INT hKey, void *info);
  void set_threshold(INT hDB, INT hKey, void *info);
#ifdef ENABLE_CHANGE_LATENCY
  void set_latency(INT hDB, INT hKey, void *info);
#endif
  void set_frequency(INT hDB, INT hKey, void *info);

  // Initialize an EROESTI
  INT init_eroesti(struct EROESTI_BOARD_SETTING*);

  // Clear
  INT clear_eroesti();

  // readout routines and thread
  INT read_a_board(int board, EROESTI_DATA* data);
  void* readout_thread(void* args);

  /*-- Equipment list ------------------------------------------------*/
  EQUIPMENT equipment[] = {
    {
      _EQUIPMENT_NAME_,      /* equipment name */
      {1, 0,                 /* event ID, trigger mask */
       _EVENT_BUFFER_NAME_,  /* event buffer */
       EQ_POLLED | EQ_EB,    /* equipment type */
       1,                    /* No event source */
       "MIDAS",              /*  format */
       TRUE,                 /* enabled */
       RO_RUNNING |          /* read only when running */
       RO_ODB,               /* and update ODB */
       1,                    /* poll for 1 ms */
       0,                    /* stop run after this event limit */
       0,                    /* number of sub events */
       0,                    /* don't log history */
       "", "", "",},
      read_trigger_event,    /* readout routine */
    },

    {""}
  };

  /*-- Frontend specific variables ------------------------------------*/
  extern INT run_state; /* STATE_RUNNING, STATE_STOPPED, STATE_PAUSED */

  /* ODB */
  HNDLE hDB;

  /* info */
  struct EROESTI_BOARD_INFO* binfo = NULL;

  /* setting */
  struct EROESTI_SETTING setting;

  /* Bank names */
  char bk_name_info[5] = "";

#ifdef TRG_NUM_DEBUG
  FILE* debug_fp = NULL;
#endif
#ifdef RO_DEBUG
  FILE* debug_ro = NULL;
#endif

#ifdef __cplusplus
}
#endif


/********************************************************************   \
              Some helper routines
\********************************************************************/
INT connect_disconnect_sitcps(BOOL disconnect)
{
  INT  n_boards = setting.num_boards;
  INT  board, status;
  char data = 0x01; /* command to connect */
  INT  cmd  = CMD_OPEN;

  if(disconnect){
    data = 0x00; /* command to disconnect */
    cmd  = CMD_CLOSE;
  }

  /* in daisy chain mode, chained boards are (dis)connected by RBCP command */
  if(setting.daisy_chain_mirror){
    for(board = setting.num_boards-2; board >= 0; board--){
      status = sitcp(CMD_SITCP_RBCP_WRITE,
                     setting.board[board].sitcp_info, 0x16A, &data, 1, FALSE);
      if(status != SUCCESS) return status;
    }
    /* only the first board is (dis)connected by PC*/
    n_boards = 1;
  }

  for(board = 0; board < n_boards; board++){
    status = sitcp(cmd, setting.board[board].sitcp_info);
    if(status != SUCCESS){
      cm_msg(MERROR, "connect_disconnect_sitcps",
             "Error! %s:" DEV_NAME_STR "'s TCP/IP cannot be (dis)connected.\n"
             "Please check /Equipment/%s/Settings/Devices/"DEV_NAME_STR"/Host and Port.",
             equipment[0].name, board, equipment[0].name);
        return FE_ERR_HW;
    }
    TRG_DEBUG_DUMPV("Board No. %d (dis)connected..", board);

    /* increase the send buffer */
    if(!disconnect){
      /* increase receive buffer */
      const INT recvbuffer_size = 33554432;
      if ( sitcp(CMD_SITCP_SET_SOCKOPT, setting.board[board].sitcp_info,
                 SOL_SOCKET, SO_RCVBUF, (char *)&recvbuffer_size, sizeof(recvbuffer_size)) < 0){
        cm_msg(MERROR, "frontend_init", "setsockopt failed");
        return FE_ERR_HW;
      }
    }
  }
  return SUCCESS;
}

INT connect_to_sitcps()
{
  return connect_disconnect_sitcps(FALSE);
}
INT disconnect_from_sitcps()
{
  return connect_disconnect_sitcps(TRUE);
}

/* ------------------------------------------------------------------------
   [setup_control_odb_variable]
   used in setup_control_odb_variables()
*/
void setup_control_odb_variable(HNDLE hDB, const char* name, HNDLE* key,
                                INT tid, INT size, void* data, void* data_mirror,
                                void (*dispatcher) (INT, INT, void *))
{
  /* create newly or get the value rom odb */
  db_create_key(hDB, setting.hKeySet, name, tid);
  db_find_key(hDB, setting.hKeySet, name, key);
  db_get_value(hDB, setting.hKeySet, name, data, &size, tid, FALSE);

  /* copy data to mirror data */
  memcpy(data_mirror, data, size);

  /* register records with a dispatcher */
  db_open_record(hDB, *key, data, size, MODE_READ, dispatcher, NULL);
  db_open_record(hDB, *key, data_mirror, size, MODE_WRITE, NULL, NULL);
}

/* ------------------------------------------------------------------------
   [setup_control_odb_variables]
   used in frontend_init
*/
void setup_control_odb_variables()
{
  /* readout mode */
  setup_control_odb_variable(hDB, "Readout Mode", &setting.hKeyReadoutMode,
                             TID_BYTE, sizeof(setting.readout_mode),
                             &setting.readout_mode, &setting.readout_mode_mirror,
                             set_readout_mode);
  if(setting.readout_mode != EROESTI_RM_NONE && /* check the value */
     setting.readout_mode != EROESTI_RM_BASE &&
     setting.readout_mode != EROESTI_RM_SELF &&
     setting.readout_mode != EROESTI_RM_EXTN ){
    setting.readout_mode_mirror = setting.readout_mode = EROESTI_RM_NONE;
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode", &setting.readout_mode,
                 sizeof(setting.readout_mode), 1, TID_BYTE);
  }
  /* daisy chain */
  setup_control_odb_variable(hDB, "Daisy Chain", &setting.hKeyDaisyChain,
                             TID_BOOL, sizeof(setting.daisy_chain),
                             &setting.daisy_chain, &setting.daisy_chain_mirror,
                             set_daisy_chain);
  /* decision triggering  */
  setup_control_odb_variable(hDB, "Decision Triggering", &setting.hKeyDecisionTriggering,
                             TID_BOOL, sizeof(setting.decision_triggering),
                             &setting.decision_triggering, &setting.decision_triggering_mirror,
                             set_decision_triggering);
  /* zero suppress  */
  setup_control_odb_variable(hDB, "Zero Suppress", &setting.hKeyZeroSuppress,
                             TID_BOOL, sizeof(setting.zero_suppress),
                             &setting.zero_suppress, &setting.zero_suppress_mirror,
                             set_zero_suppress);
  /* threshold */
#if !defined(EROS) && !defined(PMT) /* EROS and PMT EROS don't have ASD */
  setup_control_odb_variable(hDB, "Threshold", &setting.hKeyThreshold,
                             TID_WORD, sizeof(setting.threshold),
                             &setting.threshold, &setting.threshold_mirror,
                             set_threshold);
  if(setting.threshold > 0xFFF){ /* check the value */
    setting.threshold_mirror = setting.threshold = 0x000;
    db_set_value(hDB, setting.hKeySet, "Threshold", &setting.threshold,
                 sizeof(setting.threshold), 1, TID_WORD);
  }
#endif

#ifdef ENABLE_CHANGE_LATENCY
  /* latency  */
  setup_control_odb_variable(hDB, "Latency", &setting.hKeyLatency,
                             TID_BYTE, sizeof(setting.latency),
                             &setting.latency, &setting.latency_mirror,
                             set_latency);
#endif

  setup_control_odb_variable(hDB, "Frequency Mode", &setting.hKeyFrequency,
                             TID_BYTE, sizeof(setting.frequency),
                             &setting.frequency, &setting.frequency_mirror,
                             set_frequency);
  if(setting.frequency == 0){ /* check the value */
    setting.frequency_mirror = setting.frequency = 31;
    db_set_value(hDB, setting.hKeySet, "Frequency Mode", &setting.frequency,
                 sizeof(setting.frequency), 1, TID_BYTE);
  }
}

/* ------------------------------------------------------------------------
   [get_board_num_from_ip]
   returns the board number corresponding to the given ip.
   used in readout_thread.
*/
INT get_board_num_from_ip(BYTE ip0, BYTE ip1, INT* board)
{
  for(*board=0; (*board)<setting.num_boards; (*board)++){
    /*
      printf("Compare with %d.%d\n",
           setting.board[*board].ip[2],
           setting.board[*board].ip[3]);
    */
    if(ip1 == setting.board[*board].ip[3] &&
       ip0 == setting.board[*board].ip[2]) return SUCCESS;
  }

  *board = -1;
  cm_msg(MERROR, "get_board_num_from_ip",
         "Found an unknown ip address, %d.%d", ip0, ip1);
  return FE_ERR_HW;
}

/********************************************************************   \
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  INT status, size;
  char str[256];
  HNDLE hKey;
  INT retry = 0;
  INT board;

  if(debug) display_period = 0;

#ifdef TRG_NUM_DEBUG
  debug_fp = fopen("trg_num_debug.dat", "w");
  if(!debug_fp){
    cm_msg(MERROR, "frontend_init", "Couldn't make trg_num_debug file.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "Made debug file.");
#endif
#ifdef RO_DEBUG
  debug_ro = fopen("ro_debug.dat", "wb");
  if(!debug_ro){
    cm_msg(MERROR, "frontend_init", "Couldn't make ro_debug file.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "Made readout debug file.");
#endif

  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  cm_get_experiment_database(&hDB, NULL);

  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &hKey);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_ODB;
  }

  //---------------------------------------------------------
  // Load configurations from ODB
  //---------------------------------------------------------
  status = db_create_key(hDB, hKey, "Settings", TID_KEY);
  status = db_find_key(hDB, hKey, "Settings", &setting.hKeySet);

  /* get num_boards */
  sprintf(str, "Number of Boards");
  db_create_key(hDB, setting.hKeySet, str, TID_INT);
  db_find_key(hDB, setting.hKeySet, str, &setting.num_boards);
  size = sizeof(setting.num_boards);
  db_get_value(hDB, setting.hKeySet, str, &setting.num_boards, &size, TID_INT, FALSE);
  if(setting.num_boards <= 0){
    cm_msg(MERROR, "frontend_init", "No module is assigned.");
    return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontend_init", "%d boards to be initialized.", setting.num_boards);

  /* alloc board setting */
  setting.board = (struct EROESTI_BOARD_SETTING*)
    calloc(setting.num_boards, sizeof(struct EROESTI_BOARD_SETTING));
  /* alloc board info and fifo */
  binfo = (struct EROESTI_BOARD_INFO*)
    calloc(setting.num_boards, sizeof(struct EROESTI_BOARD_INFO));
  for(board = 0; board < setting.num_boards; board++){
    fifo_init(&binfo[board].fifo);
  }

  /* device directory */
  status = db_create_key(hDB, setting.hKeySet, "Devices", TID_KEY);
  status = db_find_key(hDB, setting.hKeySet, "Devices", &setting.hKeyDev);

  /* Host & MAC address directory */
  status = db_create_key(hDB, setting.hKeySet, "Host List", TID_KEY);

  /* create ODB record of SiTCP for each device */
  for(board = 0; board < setting.num_boards; board++){
    char  strBoard[256], strLink[256], strDest[256];
    INT status;

    /* created root record */
    sprintf(strBoard, DEV_NAME_STR, board);
    status = db_create_key(hDB, setting.hKeyDev, strBoard, TID_KEY);
    status = db_find_key(hDB, setting.hKeyDev, strBoard, &setting.board[board].hKeyRoot);

    /* DO NOT establish connection now */
    status = sitcp(CMD_INIT, setting.board[board].hKeyRoot,
                   &(setting.board[board].sitcp_info));
    if(status != SUCCESS){
      cm_msg(MERROR, "frontend_init", "Cannot initialize SITCP of " DEV_NAME_STR, board);
      return FE_ERR_ODB;
    }

    /* link to each Host and MAC address record */
    sprintf(strLink, "/Equipment/%s/Settings/Host List/%s IP", equipment[0].name, strBoard);
    sprintf(strDest, "/Equipment/%s/Settings/Devices/%s/SiTCP/IP", equipment[0].name, strBoard);
    if(DB_SUCCESS != db_find_key(hDB, 0, strLink, &hKey))
      db_create_link(hDB, 0, strLink, strDest);
    sprintf(strLink, "/Equipment/%s/Settings/Host List/%s MAC Address", equipment[0].name, strBoard);
    sprintf(strDest, "/Equipment/%s/Settings/Devices/%s/SiTCP/MAC Address", equipment[0].name, strBoard);
    if(DB_SUCCESS != db_find_key(hDB, 0, strLink, &hKey))
      db_create_link(hDB, 0, strLink, strDest);

    /* get ip/mac address */
    sitcp(CMD_SITCP_GET_IP_ADDRESS,
          setting.board[board].sitcp_info, setting.board[board].ip);
    sitcp(CMD_SITCP_GET_MAC_ADDRESS,
          setting.board[board].sitcp_info, setting.board[board].mac);
    cm_msg(MINFO, "frontend_init", "Connected to Board %02d (%d.%d.%d.%d)",
           board, setting.board[board].ip[0],
           setting.board[board].ip[1],
           setting.board[board].ip[2],
           setting.board[board].ip[3]);
  }

  /* control variables in ODB */
  setup_control_odb_variables();

  /* set bank name */
  for(board = 0; board < setting.num_boards; board++){
    if(get_frontend_index() < 0){
      sprintf(bk_name_info, _BANK_NAME_INFO_);
      sprintf(binfo[board].bk_name_main, _BANK_NAME_MAIN_ "0%0d", board);
      sprintf(binfo[board].bk_name_seu,  _BANK_NAME_SEU_  "0%0d", board);
    }
    else{
      sprintf(bk_name_info, _BANK_NAME_INFO_ "%0d", get_frontend_index());
      sprintf(binfo[board].bk_name_main, _BANK_NAME_MAIN_ "%0d%0d", get_frontend_index(), board);
      sprintf(binfo[board].bk_name_seu,  _BANK_NAME_SEU_  "%0d%0d", get_frontend_index(), board);
    }
  }

  /* alloc thread controller */
  if(setting.daisy_chain_mirror) setting.num_threads = 1;
  else                           setting.num_threads = setting.num_boards;
  thread = (pthread_t*)calloc(setting.num_threads, sizeof(pthread_t));

  /* Finish */
  printf(_EQUIPMENT_NAME_ " Initialize completed.\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  INT board;

  clear_eroesti();

  for(board = 0; board < setting.num_boards; board++){
    sitcp(CMD_SITCP_ENABLE_NAGLE, setting.board[board].sitcp_info, TRUE);
    sitcp(CMD_EXIT, setting.board[board].sitcp_info);
  }

  free(setting.board);
  setting.board = NULL;

  for(board = 0; board < setting.num_boards; board++){
    fifo_delete(&binfo[board].fifo);
  }
  free(binfo);
  binfo = NULL;

#ifdef TRG_NUM_DEBUG
  fclose(debug_fp);
#endif
#ifdef RO_DEBUG
  fclose(debug_ro);
#endif

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  char data;
  INT  status;
  INT  board, thrd;

  DEBUG("BEGIN OF RUN");

  clear_eroesti();

  /* check the readout mode */
  if(setting.readout_mode_mirror == EROESTI_RM_NONE){
    cm_msg(MERROR, "begin_of_run", "Readout mode is none, set a valid mode");
    return FE_ERR_ODB;;
  }
  if(SUCCESS != set_configuration(FALSE)){
    cm_msg(MERROR, "begin_of_run",
           "Cannot initialize " DEV_NAME_STR " with RBCP commands.", board);
    return FE_ERR_HW;
  }

  /* -----------------------------------------------
     Connect to SiTCP socket
     ----------------------------------------------- */
  DEBUG("Connecting... SiTCP(s)");
  if(SUCCESS != connect_to_sitcps()){
    DEBUG("Couldn't connect to SiTCP(s)");
    return FE_ERR_HW;
  }
  DEBUG("Finished.");

  /* change the trigger mode from NONE to ...*/
  /*
  DEBUG("Change the trigger mode to ...");
  data = setting.readout_mode_mirror;
  for(board = setting.num_boards-1; board >= 0; board--){
    sitcp(CMD_SITCP_RBCP_WRITE,
          setting.board[board].sitcp_info, 0x11D, &data, 1, FALSE);
    ss_sleep(50);
  }
  */

  /* init mutex and create readout thread
     in the parallel mode, make a thread per board */
  DEBUG("Create mutexes and threads");
  do_exit_thread = thread_err_happened = FALSE;
  for(board = 0; board < setting.num_boards; board++)
    pthread_mutex_init(&binfo[board].mutex, NULL);
  for(thrd = 0; thrd < setting.num_threads; thrd++){
    pthread_create(&thread[thrd], NULL, readout_thread,
                   /* pass board index to the thread
                      (not 'board' will change soon after this call) */
                   (void*)&(setting.board[thrd].index));
  }
  DEBUG("Can start daq now!");
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  INT board, thrd;

  /* let the thread stop and destroy mutex */
  do_exit_thread = TRUE;
  for(thrd = 0; thrd < setting.num_threads; thrd++)
    pthread_join(thread[thrd], NULL);
  for(board = 0; board < setting.num_boards; board++)
    pthread_mutex_destroy(&binfo[board].mutex);

  clear_eroesti(); /* discard data remaining in the socket */

  // close
  if(SUCCESS != disconnect_from_sitcps()) return FE_ERR_HW;

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Readout thread routine ----------------------------------------*/
void* readout_thread(void* args)
{
  INT status;
  INT board, ac_board;
  EROESTI_DATA data;

  board = *(INT*)args; /* get board number */
#ifdef TRG_NUM_DEBUG
  fprintf(debug_fp, "Thread of No. %d launched.\n", board);
#endif

  /* loop until exit command received */
  while(! do_exit_thread){
    /* readout */
    status = read_a_board(board, &data);
    if(FE_ERR_HW == status){
      cm_msg(MERROR, "readout_thread",
             "Inavoidable error happens this thread quits.");
      thread_err_happened = TRUE;
      break;
    }
    /* readout succeded! store data into FIFO */
    if(SUCCESS == status){
      /* access with board number for daisy chain */
      if(SUCCESS != get_board_num_from_ip(data.board_ip[0],
                                          data.board_ip[1], &ac_board)){
        thread_err_happened = TRUE;
        break;
      }
      data.board_num = ac_board;
      LOCK_MUTEX(ac_board); /* get mutex */
      fifo_enqueue(binfo[ac_board].fifo, sizeof(data), (FIFO_TYPE*)&data); /* enqueue */
      UNLOCK_MUTEX(ac_board); /* return mutex */
    }
  } /* end of while(! do_exit_thread) */
  return NULL;
}

/* -- read_a_board --------------------------------------------------------------------- */
INT read_a_board(int board, EROESTI_DATA* data)
{
  const INT tcp_data = 1;
  struct timeval tval;
  DWORD hdbuff[6];
  DWORD footer[10];
  INT   recv_bytes, exp_bytes;
  INT   retries;
  int   status;
  DWORD key;

  struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[board];

#ifndef OS_DARWIN
  /* quick ack */
  sitcp(CMD_SITCP_SET_SOCKOPT, pbsetting->sitcp_info,
        IPPROTO_TCP, TCP_QUICKACK, &tcp_data, sizeof(tcp_data));
#endif

  /* -----------------------------------------------------
     check header magic word
     ----------------------------------------------------- */
  recv_bytes = sitcp(CMD_POLL, pbsetting->sitcp_info, &hdbuff[0], sizeof(DWORD), 10);
  if(recv_bytes != sizeof(DWORD)) return -1;
  RO_DEBUG_DUMP(&hdbuff[0], sizeof(DWORD));

  if(hdbuff[0] != 0x89abcdef){
    if(debug){
      printf("ILLEGAL HEADER WORD 0x%x\n", hdbuff[0]);
    }
    return -1;
  }

  // readout remaining header info
  recv_bytes = sitcp(CMD_READ, pbsetting->sitcp_info,
                     (char*)&hdbuff[1], 5*sizeof(DWORD), 500);
  RO_DEBUG_DUMP(hdbuff, recv_bytes);
  if(recv_bytes < 5*sizeof(DWORD)){
    DEBUGV("Too little %d", recv_bytes);
    return -1;
  }
  gettimeofday(&tval, NULL); // immediately store the time stamp

  // store to EROESTI_DATA (take care of little endian)
  data->type              = 0xFFFF & (hdbuff[1]);
  data->board_ip[1]       = 0xFF   & (hdbuff[1]>>16);
  data->board_ip[0]       = 0xFF   & (hdbuff[1]>>24);
  data->datasize_main     = hdbuff[2];
  data->trg_num           = hdbuff[3];
  data->seu_count         = 0xFFFF & (hdbuff[4]);
  data->seu_disable_count = 0xFFFF & (hdbuff[4]>>16);
  data->datasize_seu      = 0xFFFF & (hdbuff[5]);
  data->timestamp         = tval;

  // when daisy chain is off, any different board ip must not appear.
  if(!setting.daisy_chain_mirror &&
     (data->board_ip[0] != setting.board[board].ip[2] ||
      data->board_ip[1] != setting.board[board].ip[3])){
    cm_msg(MERROR, "read_a_board",
           "Different board ip %d found from board %d", data->board_ip, board);
    TRG_DEBUG_DUMPV("Different board ip %d from board %d.", data->board_ip, board);
    return FE_ERR_HW;
  }

  /* data is overflowed the buffer */
  exp_bytes = data->datasize_main + data->datasize_seu + sizeof(DWORD)*2;
  if(exp_bytes > sizeof(WORD) * BUFFSIZE_MAIN){
    TRG_DEBUG_DUMPV("Too much buffer size %d", data->datasize_main);
    printf("Too much buffer size %d (0x%x\n", data->datasize_main,data->datasize_main);
    printf("IP %d.%d: size = 0x%x, trg num = 0x%x \n"
           "seu(count %d, disable count %d, size %d)\n",
           data->board_ip[0], data->board_ip[1],
           data->datasize_main, data->trg_num,
           data->seu_count, data->seu_disable_count,
           data->datasize_seu);
    return FE_ERR_HW;
  }

  // readout main data
  recv_bytes = 0;
  retries = 0;
  do{
    if(do_exit_thread) break;
    retries++;
    recv_bytes += sitcp(CMD_READ, pbsetting->sitcp_info,
                        (char*)(data->buff_main) + recv_bytes,
                        data->datasize_main - recv_bytes, 200);
  }while(recv_bytes < data->datasize_main && retries < 10);
  RO_DEBUG_DUMP(data->buff_main, recv_bytes);
  if(do_exit_thread) return -1;
  // time out
  if(retries == 10){
    DEBUG("MAINDATA READ TIMEOUT");
    return -1;
  }

  // readout seu data
  recv_bytes = 0;
  retries = 0;
  do{
    if(do_exit_thread) break;
    retries++;
    recv_bytes += sitcp(CMD_READ, pbsetting->sitcp_info,
                        (char*)(data->buff_seu) + recv_bytes,
                        data->datasize_seu - recv_bytes, 200);
    }while(recv_bytes < data->datasize_seu && retries < 10);
  RO_DEBUG_DUMP(data->buff_seu, recv_bytes);
  if(do_exit_thread) return -1;
  // time out
  if(retries == 10){
    DEBUG("SEUDATA READ TIMEOUT");
    return -1;
  }

  // Check footer
  recv_bytes=0;
  do{
    recv_bytes += sitcp(CMD_READ, pbsetting->sitcp_info,
                        (char*)footer + recv_bytes,
                        2*sizeof(DWORD) - recv_bytes, 100);
  }while(recv_bytes < 2*sizeof(DWORD));
  RO_DEBUG_DUMP(footer, recv_bytes);
  if(footer[0] != 0xFEDCBA98){
#ifdef DEBUG
    int i;
    printf("ILLEGAL FOOTER WORD 0x%x\n", footer[0]);
    printf("IP %d.%d: size = 0x%x, trg num = 0x%x \n"
           "seu(count %d, disable count %d, size %d)\n",
           data->board_ip[0], data->board_ip[1],
           data->datasize_main, data->trg_num,
           data->seu_count, data->seu_disable_count,
           data->datasize_seu);
    for(i=data->datasize_main/sizeof(WORD)-20; i<data->datasize_main/sizeof(WORD); i++){
      printf("0x%x\n", data->buff_main[i]);
    }
#endif
    return -1;
  }
  data->crc32 = footer[1]; // CRC32 info

  TRG_DEBUG_DUMPV("Succeeded readout from " DEV_NAME_STR ".", pbsetting->index);

  return SUCCESS;
}


/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return.
   The test flag is used to time the polling */
{
  int board;
  EROESTI_DATA* data;
  DWORD min_trg_num = 1<<32 - 1;
  BOOL  skip = FALSE;
  INT retries = 0;

  for(retries=0; retries < count; retries++){
    /* check all fifo */
    for(board = 0; board < setting.num_boards; board++){
      /* get mutex and return if fifo is empty */
      LOCK_MUTEX(board);
      if(fifo_is_empty(binfo[board].fifo)){
        UNLOCK_MUTEX(board);
        break;
      }
      /* get next buffer pointer (not dequeue now) */
      data = (EROESTI_DATA*)fifo_get_next_buff_pointer(binfo[board].fifo);
      /* get next trigger id */
      binfo[board].next_trg_num = data->trg_num;
      /* is the trig. id minimum? */
      if(data->trg_num < min_trg_num) min_trg_num = data->trg_num;
      /* return mutex */
      UNLOCK_MUTEX(board);
    }
    /* polling aborts if any fifo is empty */
    if(board != setting.num_boards) continue;

    /* decide skip or not */
    for(board = 0; board < setting.num_boards; board++){
      if(binfo[board].next_trg_num == min_trg_num)
      binfo[board].skip_next = FALSE;
      else{
        binfo[board].skip_next = TRUE;
        skip = TRUE;
      }
    }

    /* discard events in which a part of all modules is not triggered. */
    if(skip){
      for(board = 0; board < setting.num_boards; board++){
        if(binfo[board].next_trg_num == min_trg_num){
          LOCK_MUTEX(board);
          fifo_trash_next(binfo[board].fifo);
          UNLOCK_MUTEX(board);
        }
      }
      continue;
    }
    break;
  } // end of for(retries < count)

  if(test || retries == count ) return FALSE;
  return TRUE;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
  DWORD *pdata_dword;
  WORD  *pdata_word;
  INT i, status, board;
  DWORD trg_num;
  struct timeval timestamp = {0,0};

#ifdef TRG_NUM_DEBUG
  for(board = 0; board < setting.num_boards; board++){
    if(binfo[board].skip_next)
      TRG_DEBUG_DUMPV("   " DEV_NAME_STR " is skipped (%d).",
                      board, binfo[board].next_trg_num);
    else
      TRG_DEBUG_DUMPV("   " DEV_NAME_STR " is included (%d).",
                      board, binfo[board].next_trg_num);
  }
#endif

  /* readout data from fifo */
  for(board = 0; board < setting.num_boards; board++){
    if(binfo[board].skip_next) continue; /* skip this event */
    LOCK_MUTEX(board); /* get mutex */

    fifo_dequeue(binfo[board].fifo, (FIFO_TYPE*)&binfo[board].data);
    trg_num = binfo[board].next_trg_num;

    UNLOCK_MUTEX(board); /* return mutex */
  }

  /* init bank structure */
  bk_init(pevent);

  /* ----------------------------------------
     create Event Info bank
  ---------------------------------------- */
  bk_create(pevent, bk_name_info, TID_DWORD, (void **)&pdata_dword);

  *pdata_dword++ = trg_num;   /* event counter */

  /* find fastest time stamp among all the modules */
  for(board = 0; board < setting.num_boards; board++){
    if(binfo[board].skip_next) continue;
    if(timestamp.tv_sec == 0)
      timestamp = binfo[board].data.timestamp;
    else if(timercmp(&timestamp, &binfo[board].data.timestamp, >)){
      timestamp = binfo[board].data.timestamp;
    }
  }
  /* save the fastest time stamp */
  *(struct timeval*)pdata_dword = timestamp;
  pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);
  *pdata_dword++ = setting.num_boards; /* set number of board */

  /* append each board information */
  for(board = 0; board < setting.num_boards; board++){
    if(binfo[board].skip_next) continue;
    *pdata_dword++ = binfo[board].data.board_num;
    *pdata_dword++ = (binfo[board].data.type<<16 |
                      binfo[board].data.board_ip[0]<<8 |
                      binfo[board].data.board_ip[1]);
    *pdata_dword++ = binfo[board].data.datasize_main;
    *pdata_dword++ = binfo[board].data.seu_count;
    *pdata_dword++ = binfo[board].data.seu_disable_count;
    *pdata_dword++ = binfo[board].data.datasize_seu;
    *pdata_dword++ = binfo[board].data.crc32;
    *(struct timeval*)pdata_dword = binfo[board].data.timestamp;
    pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);
  }
  bk_close(pevent, pdata_dword);

  /* ----------------------------------------
     create main data bank
  ---------------------------------------- */
  for(board = 0; board < setting.num_boards; board++){
    if(binfo[board].skip_next) continue;
    /* create Data bank */
    bk_create(pevent, binfo[board].bk_name_main, TID_WORD, (void **)&pdata_word);
    memcpy(pdata_word, binfo[board].data.buff_main, binfo[board].data.datasize_main);
    pdata_word += binfo[board].data.datasize_main/sizeof(WORD);
    bk_close(pevent, pdata_word);
    /* create SEU info bank */
    if(binfo[board].data.datasize_seu > 0){
      bk_create(pevent, binfo[board].bk_name_seu, TID_DWORD, (void **)&pdata_dword);
      memcpy(pdata_word, binfo[board].data.buff_seu, binfo[board].data.datasize_seu);
      pdata_dword += binfo[board].data.datasize_seu/sizeof(DWORD);
      bk_close(pevent, pdata_dword);
    }
  }
  return bk_size(pevent);
}

//=======================================================
INT clear_eroesti()
//=======================================================
{
  INT board, recv_bytes;
  char dummy[1000];
  SITCP_RBCP_COMMAND command[] = {
    {0x16a, 0x00, TRUE},
    {0x11d, EROESTI_RM_NONE, TRUE},
    {0x110, 0xf0, TRUE},
    {0x112, 0x01, TRUE}
  };

  DEBUG("Clear eroesti");
  for(board = 0; board < setting.num_boards; board++){
    struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[board];
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info,
                        1, command, 1000)){
      cm_msg(MERROR, "clear_eroesti", "Error in sending the rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info,
                        3/*sizeof(command)/sizeof(SITCP_RBCP_COMMAND)*/, &command[1], 50)){
      cm_msg(MERROR, "clear_eroesti", "Error in sending the rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }
  }

  /* clear fifo */
  for(board = 0; board < setting.num_boards; board++){
    fifo_clear(binfo[board].fifo);
  }
  DEBUG("Finished clear eroesti");
  return SUCCESS;
}


INT set_configuration(BOOL first)
{
  int status, i, size, board;
  int dindex;

  SITCP_RBCP_COMMAND command[] = {
    {0x110, 0xF0, TRUE}, // reset FPGA
    {0x170, (char)(setting.frequency),   TRUE}, // sampling speed
    
    /* ADC */
    {0x00030000, 0x08, TRUE},
    {0x00030002, 0x06, TRUE},
    {0x00010000, 0x3C, TRUE},
    {0x00030000, 0x3C, TRUE},
    {0x00010008, 0x03, TRUE},
    {0x00010008, 0x00, TRUE},
    {0x00020008, 0x03, TRUE},
    {0x00020008, 0x00, TRUE},
    {0x00010014, 0x00, TRUE},
    {0x00020014, 0x00, TRUE},
    {0x0001000D, 0x00, TRUE},
    {0x0002000D, 0x00, TRUE},

#if !defined(EROS) && !defined(PMT)
    /* ASD threshold : EROS doesn't have ASD */
    {0x115, (char)(0x0F & (setting.threshold>>8))},
    {0x116, (char)(0xFF & (setting.threshold   ))},
    {0x141, 60}, // ASD check start time
    {0x142, 70}, // ASD wait width
#endif

    /* readout mode is none initially */
    //{0x11d, (char)EROESTI_RM_NONE},
    {0x11d, (char)setting.readout_mode_mirror},

    {0x171, (char)(setting.decision_triggering_mirror? 1 : 0)}, // decision triggering
    {0x119, 0x01, TRUE}, // DAC Sync
    {0x111, 0xF0, TRUE}, // reset DRS4
    {0x110, 0xF0, TRUE}, // Internally reset FPGA
    {0x11B, 0x02},       // phase active clock setting (not functional now)

    // ADC Latency (Basically fixed at 2F)
#ifdef ENABLE_CHANGE_LATENCY
    {0x11A, (char)(0xFF & setting.latency_mirror)},
#else
    {0x11A, 0x2F},
#endif

    {0x112, 0x01, TRUE}, // reset FPGA FIFO
    {0x00010016, 0x04, TRUE},  // ADC
    {0x00020016, 0x04, TRUE},  // ADC

    // daisy chain
    {0x153, 0x1, TRUE},
    {0x163, 192},
    {0x162, 163},
    {0x161, 10},
    {0x160, 0},
    {0x169, 0x7C},
    {0x168, 0xF0},
    {0x167, 0x98},
    {0x166, 0x01},
    {0x165, 0},
    {0x164, 0},

    {0x130, 0x01, TRUE}, // reset SEU monitor
    {0x124, 0x01, TRUE}, // reset SEU counter
    {0x16E, 0x00, TRUE}, // disable CRC check

    /* zero suppress */
    {0x140, (char)(setting.zero_suppress_mirror? 1:0)}
  };

  for(dindex=0; dindex<sizeof(command)/sizeof(SITCP_RBCP_COMMAND); dindex++){
    command[dindex].inhibit_validation = TRUE;
  }
  // find the index where daisy chain commands start
  for(dindex=0; dindex<sizeof(command)/sizeof(SITCP_RBCP_COMMAND); dindex++){
    if(command[dindex].address == 0x153) break;
  }

  // loop for boards
  for(board=0; board<setting.num_boards; board++){
    if(setting.daisy_chain_mirror){
      // get IP/MAC address of the next module
      if(board+1 == setting.num_boards){ // The farest module
        for(i=0; i<11; i++) command[dindex+i].data = 0;
      }
      else{
        command[dindex].data = 1;
        for(i=0; i<4; i++) command[dindex+1+i].data = setting.board[board+1].ip[i];
        for(i=0; i<6; i++) command[dindex+5+i].data = setting.board[board+1].mac[i];
      }
    }
    else{
      for(i=0; i<11; i++) command[dindex+i].data = 0;
    }

    struct EROESTI_BOARD_SETTING* pbsetting = &setting.board[board];
    if(SUCCESS != sitcp(CMD_SITCP_RBCP_WRITE_BLOCK, pbsetting->sitcp_info,
                        sizeof(command)/sizeof(SITCP_RBCP_COMMAND), command, 20)){
      cm_msg(MERROR, "set_configuration",
             "Error in sending rbcp commands to " DEV_NAME_STR,
             pbsetting->index);
      return FE_ERR_HW;
    }

    if(setting.daisy_chain_mirror && board == 0){
      if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, TRUE))
        return FE_ERR_HW;
    }
    else{
      if(SUCCESS != sitcp(CMD_SITCP_ENABLE_NAGLE, pbsetting->sitcp_info, FALSE))
        return FE_ERR_HW;
    }

  } // end of loop for boards

  return SUCCESS;
}

//=======================================================
// ODB dispatchers
//=======================================================
void set_readout_mode(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.readout_mode == setting.readout_mode_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode",
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }

  switch(setting.readout_mode){
  case EROESTI_RM_NONE:
    printf("Stopped\n");
    break;

  case EROESTI_RM_BASE:
    printf("Set readout mode.\n");
    break;

  case EROESTI_RM_SELF:
    printf("Set readout mode.\n");
    break;

  case EROESTI_RM_EXTN:
    printf("Set readout mode.\n");
    break;

  default:
    /* Illegal command */
    setting.readout_mode = setting.readout_mode_mirror;
    db_set_value(hDB, setting.hKeySet, "Readout Mode",
                 &setting.readout_mode, sizeof(setting.readout_mode),
                 1, TID_BYTE);
    return;
  }

  setting.readout_mode_mirror = setting.readout_mode;
}

void set_daisy_chain(INT hDB, INT hKey, void *info)
{
  if(setting.daisy_chain == setting.daisy_chain_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.daisy_chain = setting.daisy_chain_mirror;
    db_set_value(hDB, setting.hKeySet, "Daisy Chain",
                 &setting.daisy_chain, sizeof(setting.daisy_chain),
                 1, TID_BOOL);
  }

  if(setting.daisy_chain) printf("enabled daisy chain.\n");
  else                    printf("disabled daisy chain.\n");
  setting.daisy_chain_mirror = setting.daisy_chain;
}

void set_decision_triggering(INT hDB, INT hKey, void *info)
{
  if(setting.decision_triggering == setting.decision_triggering_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.decision_triggering = setting.decision_triggering_mirror;
    db_set_value(hDB, setting.hKeySet, "Decision Triggering",
                 &setting.decision_triggering, sizeof(setting.decision_triggering),
                 1, TID_BOOL);
  }

  if(setting.decision_triggering) printf("enabled decision triggering.\n");
  else                      printf("disabled decision triggering.\n");
  setting.decision_triggering_mirror = setting.decision_triggering;
}

void set_zero_suppress(INT hDB, INT hKey, void *info)
{
  if(setting.zero_suppress == setting.zero_suppress_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.zero_suppress = setting.zero_suppress_mirror;
    db_set_value(hDB, setting.hKeySet, "Zero Suppress",
                 &setting.zero_suppress, sizeof(setting.zero_suppress),
                 1, TID_BOOL);
  }

  if(setting.zero_suppress) printf("enabled zero suppress.\n");
  else                      printf("disabled zero suppress.\n");
  setting.zero_suppress_mirror = setting.zero_suppress;
}

void set_threshold(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.threshold == setting.threshold_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED || setting.threshold > 0xFFF){
    setting.threshold = setting.threshold_mirror;
    db_set_value(hDB, setting.hKeySet, "Threshold",
                 &setting.threshold, sizeof(setting.threshold),
                 1, TID_WORD);
    return;
  }

  printf("set threshold to 0x%03x\n", setting.threshold);
  setting.threshold_mirror = setting.threshold;
}

#ifdef ENABLE_CHANGE_LATENCY
void set_latency(INT hDB, INT hKey, void *info)
{
  if(setting.latency == setting.latency_mirror) return;

  /* EROS/ROESTI is not stopped */
  if(run_state != STATE_STOPPED){
    setting.latency = setting.latency_mirror;
    db_set_value(hDB, setting.hKeySet, "Latency",
                 &setting.latency, sizeof(setting.latency),
                 1, TID_BYTE);
    return;
  }

  printf("set latency to 0x%02x\n", setting.latency);
  setting.latency_mirror = setting.latency;
}
#endif

void set_frequency(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.frequency == setting.frequency_mirror) return;

  /* EROS/ROESTI is not stopped, or range over */
  if(run_state != STATE_STOPPED){
    setting.frequency = setting.frequency_mirror;
    db_set_value(hDB, setting.hKeySet, "Frequency",
                 &setting.frequency, sizeof(setting.frequency),
                 1, TID_BYTE);
    return;
  }

  printf("set frequency mode to 0x%02x\n", setting.frequency);
  setting.frequency_mirror = setting.frequency;
}
