#include <iostream>

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include "midas.h"

#include "Mfc7.hh"
#include "StopWatch.hh"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/
  /*-- Globals -------------------------------------------------------*/
  struct FC7_SETTING{
    /* ODB hundle */
    HNDLE hKeyRoot, hKeySet,
      hKeyConfXML;
    
    /* setting variables */
    char  confxml_name[256];
    
    /* mirror value */
  };


/* The frontend name (client name) as seen by other MIDAS clients   */
   const char *frontend_name = "FC7";

/* The frontend file name, don't change it */
   const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 3000;

/* maximum event size produced by this frontend */
   INT max_event_size = 100*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 200*1024;

/* triggered timing in poll_event() */
  struct timeval timestamp;

  extern INT run_state;
  extern HNDLE hDB;

  // for debugging
  extern BOOL debug;
#define DEBUG(str)       printf(str "\n");
#define DEBUGV(str, ...) printf(str "\n", __VA_ARGS__);
  
/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();

  INT read_trigger_event(char *pevent, INT off);

  INT poll_event(INT source, INT count, BOOL test);
  INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

  // helper ones
  INT init_odb();
  INT read_data(DWORD* data);

/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {
    {
      "FC7",               /* equipment name */
      {1, 0,               /* event ID, trigger mask */
       "FBUFF",            /* event buffer */
       EQ_POLLED | EQ_EB,  /* equipment type */
       1,                  /* No event source */
       "MIDAS",            /* format */
       TRUE,               /* enabled */
       RO_RUNNING |        /* read only when running */
       RO_ODB,             /* and update ODB */
       5,                  /* poll for 100ms */
       0,                  /* stop run after this event limit */
       0,                  /* number of sub events */
       0,                  /* don't log history */
       "", "", "",},
      read_trigger_event,  /* readout routine */
    },
    
    {""}
  };

  /*-- Frontend specific variables in C ------------------------------------*/

  /* setting */
  struct FC7_SETTING setting;
  
  
#ifdef __cplusplus
}
#endif

/*-- Frontend specific variables in C++ ------------------------------------*/
Mfc7 fc7;
StopWatch sw;

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/


/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  // display in debug mode
  if(debug) display_period = 0;

  if(SUCCESS != init_odb()){
    cm_msg(MERROR, "frontend_init()", "init_odb() returned an error!");
    return FE_ERR_ODB;
  }
  // open FC7
  fc7.open(setting.confxml_name);
  
  uhal::ValWord< uint32_t > mem;
  // user id
  mem = fc7.hw->getNode ("usr_id").read(); fc7.hw->dispatch();
  std::cout << "user_id = " << std::hex << mem.value() << std::endl;
        
  // operation mode
  mem = fc7.hw->getNode("operation_mode").read(); fc7.hw->dispatch();
  std::cout << "operation mode: " <<  mem.value();

  // board id
  mem = fc7.hw->getNode("board_id").read(); fc7.hw->dispatch();
  std::cout << ", board_id: " <<  mem.value() << std::endl;;
  /*
  int idc[4];
  mem = fc7.hw->getNode("board_id.char1").read(); fc7.hw->dispatch();
  idc[0] = mem.value();
  mem = fc7.hw->getNode("board_id.char2").read(); fc7.hw->dispatch();
  idc[1] = mem.value();
  mem = fc7.hw->getNode("board_id.char3").read(); fc7.hw->dispatch();
  idc[2] = mem.value();
  mem = fc7.hw->getNode("board_id.char4").read(); fc7.hw->dispatch();
  idc[3] = mem.value();
  std::cout << " : " << idc[0] << " " << idc[1]
            << " " << idc[2] << " " << idc[3] << std::endl;
  */
  
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  fc7.close();
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{  
  fc7.init();       // init fc7
  //fc7.sram_reset(); // reset sram - moved to fc7.init()
  //return FE_ERR_HW;
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  INT status, retry;
  for(retry=0; retry<count; retry++){
    status = fc7.check(); // check sram
    if(test) continue;
    if(status){
      gettimeofday(&timestamp, NULL); // get a timestamp immediately
      return TRUE;
    }
  }
  return FALSE;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
     break;
   case CMD_INTERRUPT_DISABLE:
     break;
   case CMD_INTERRUPT_ATTACH:
     break;
   case CMD_INTERRUPT_DETACH:
     break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
  DWORD *pdata_dword;
  //DWORD data[258];
  DWORD data[514];
  DWORD trg_num;
  
  read_data(data);
  
  /* init bank structure */
  bk_init(pevent);

  /* ----------------------------------------
     create Event Info bank
  ---------------------------------------- */
  bk_create(pevent, "EIF7", TID_DWORD, (void **)&pdata_dword);

  /* event counter */
  trg_num = data[1];
  *pdata_dword++ = trg_num;

  /* store the time stamp */
  *(struct timeval*)pdata_dword = timestamp;
  pdata_dword = (DWORD*)((struct timeval*)pdata_dword + 1);

  bk_close(pevent, pdata_dword);

  /* ----------------------------------------
     create Event data bank
  ---------------------------------------- */
  bk_create(pevent, "DFC7", TID_DWORD, (void **)&pdata_dword);
  for(int i=0; i<258; i++){
  //for(int i=0; i<514; i++){
    *pdata_dword++ = data[i];
  }
  bk_close(pevent, pdata_dword);
  
  return bk_size(pevent);
}

/*-- Other helper functions ----------------------------------------*/
INT init_odb()
{
  INT status, size;
  char str[256];
  
  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &setting.hKeyRoot);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_ODB;
  }
  // settings
  status = db_create_key(hDB, setting.hKeyRoot, "Settings", TID_KEY);
  status = db_find_key  (hDB, setting.hKeyRoot, "Settings", &setting.hKeySet);
  
  //---------------------------------------------------------
  // Load configurations from ODB
  //---------------------------------------------------------
#define _FC7FE_CREATE_GET_KEY_(name, key, tid, var, pvar)               \
  sprintf(str, name);                                                   \
  db_create_key(hDB, setting.hKeySet, str, tid);                        \
  db_find_key(hDB, setting.hKeySet, str, &setting.hKey##key );          \
  size = sizeof(var);                                                   \
  db_get_value(hDB, setting.hKeySet, str, pvar, &size, tid, FALSE)
  
  _FC7FE_CREATE_GET_KEY_("Configuration XML", ConfXML, TID_STRING, 
                         setting.confxml_name, setting.confxml_name);
  
#undef _FC7FE_CREATE_GET_KEY_
  

  return SUCCESS;
}

INT read_data(DWORD* data)
{
  sw.Start();
  //unsigned int addr = fc7.reg_read("sram_current_fastwrite_address");
  unsigned int addr = fc7.sram_current_addr;
  //DEBUGV("sram data addr: 0x%x", addr);
  
  #if 0
  fc7.sram_read8(addr, data);
  //fc7.sram_read8(addr +  8, data +  8);
  //fc7.sram_read8(addr + 16, data + 16);
  //fc7.sram_read8(addr + 24, data + 32);

  if(debug){
    std::cout << "SRAM:";
    for (int i = 0 ; i < 32 ; i++) {
      if ((i % 8) == 0) std::cout << std::endl;
      std::cout << " " << std::hex << data[i] ;
    }
  }
  #endif

  for (int i = 0 ; i < 32 ; i++) {
  //for (int i = 0 ; i < 64 ; i++) {
   fc7.sram_read8(addr + 8*i - 31*8, &(data[8*i]));
    //fc7.sram_read8(addr + 8*i - 63*8, &(data[8*i]));
    //fc7.sram_read8((addr + 8*i - 31*8) & 0x7fffffff, &(data[8*i]));
  }
  sw.Stop();
  data[256] = sw.Elapse();
  data[257] = 0xbabecafe;
  //data[512] = sw.Elapse();
  //data[513] = 0xbabecafe;
  if (debug) {
    std::cout << "SRAM: " << std::endl;;
    for (int j = 0 ; j < 32 ; j++) {
    //for (int j = 0 ; j < 64 ; j++) {
      printf("%08x : ", addr + 8*j - 31*8);
      for (int i = 0 ; i < 8 ; i++) {
        printf("%08x ", data[i + j*8]);
      }
      printf("\n");
    }
    std::cout << "Elapse : " << sw.Elapse() << std::endl;
  }

  return 0;
}
