#include <sys/time.h>

typedef struct TW_DATA_t
{
  char*  name;

  struct timeval start;
  struct timeval stop;

  int    start_set;
  int    count;
  double sum;
  double sum2;
} TW_DATA;

void   tw_init(TW_DATA* data, const char* name);
void   tw_start(TW_DATA* data);
double tw_stop(TW_DATA* data);

void tw_print(TW_DATA* data);
