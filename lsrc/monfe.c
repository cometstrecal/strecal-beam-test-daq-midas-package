/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Example Slow Control Frontend program. Defines two
                slow control equipments, one for a HV device and one
                for a multimeter (usually a general purpose PC plug-in
                card with A/D inputs/outputs. As a device driver,
                the "null" driver is used which simulates a device
                without accessing any hardware. The used class drivers
                cd_hv and cd_multi act as a link between the ODB and
                the equipment and contain some functionality like
                ramping etc. To form a fully functional frontend,
                the device driver "null" has to be replaces with
                real device drivers.

  $Id$

\********************************************************************/

#include <stdio.h>
#include "midas.h"
#include "cd_raspi_hvtmon.h"
#include "dd_raspi_mcp3208.h"
#include "raspi.h"

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
#ifdef LED_DRIVER_INCLUDED
const char *frontend_name = "HV Temp Monitor with LED";
#else
const char *frontend_name = "HV Temp Monitor";
#endif

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 1000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 10000;

/*-- Equipment list ------------------------------------------------*/

/* device driver list */
DEVICE_DRIVER hvtmon_driver[] = {
  {"Raspi_Mcp3208", dd_raspi_mcp3208, 80, raspi, DF_INPUT | DF_READ_ONLY /*| DF_MULTITHREAD */, TRUE},
  {""}
};


EQUIPMENT equipment[] = {
   {
     /* equipment name */
#ifdef LED_DRIVER_INCLUDED
     "HV Temp Monitor with LED",
#else
     "HV Temp Monitor",
#endif
     {2, 0,                      /* event ID, trigger mask */
      "SYSTEM",                  /* event buffer */
      EQ_SLOW,                   /* equipment type */
      0,                         /* event source */
      "MIDAS",                   /* format */
      TRUE,                      /* enabled */
      RO_RUNNING | RO_TRANSITIONS,        /* read when running and on transitions */
      10000,                     /* read every 10 sec */
      0,                         /* stop run after this event limit */
      0,                         /* number of sub events */
      1,                         /* log history every event */
      "", "", ""} ,
     cd_raspi_hvtmon_read,       /* readout routine */
     cd_raspi_hvtmon,            /* class driver main routine */
     hvtmon_driver,              /* device driver list */
     NULL,                       /* init string */
   },
   {""}
};

// debug flag
extern BOOL debug;

#ifdef LED_DRIVER_INCLUDED

typedef struct
{
  void* raspi_info;

  /* ODB hundle */
  HNDLE hKeyRoot, hKeySet, hKeyPin, hKeyStatus;
  
  INT  pin;
  BOOL led_status, led_status_mirror;
} LED_SETTING;

LED_SETTING setting;

// dispatcher
void set_led(INT hDB, INT hKey, void *info);
#endif

/*-- Dummy routines ------------------------------------------------*/

INT poll_event(INT source[], INT count, BOOL test)
{
   return 1;
};

INT interrupt_configure(INT cmd, INT source[], POINTER_T adr)
{
   return 1;
};

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
#ifdef LED_DRIVER_INCLUDED
  /* ODB */
  HNDLE hDB;

  INT status, size;
  char str[256];
  HNDLE hKey;
  INT retry = 0;
  BOOL led_status;
  
  if(debug) display_period = 0;

  /*------------------------------------------------------
    Open ODB directory
    ------------------------------------------------------*/
  cm_get_experiment_database(&hDB, NULL);
  sprintf(str, "/Equipment/%s", equipment[0].name);
  status = db_find_key(hDB, 0, str, &setting.hKeyRoot);
  if(status != DB_SUCCESS){
    printf("Error! %s is not found in ODB\n", str);
    return FE_ERR_ODB;
  }
  
  //---------------------------------------------------------
  // LED driver control value
  //---------------------------------------------------------
  status = db_create_key(hDB, setting.hKeyRoot, "LED Driver", TID_KEY);
  status = db_find_key(hDB, setting.hKeyRoot, "LED Driver", &setting.hKeySet);

  /* setup setting */
  sprintf(str, "Control Pin");
  db_create_key(hDB, setting.hKeySet, str, TID_INT);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyPin);
  size = sizeof(setting.led_status);
  db_get_value(hDB, setting.hKeySet, str, &setting.pin, &size, TID_INT, FALSE);
  
  sprintf(str, "LED ON");
  db_create_key(hDB, setting.hKeySet, str, TID_BOOL);
  db_find_key(hDB, setting.hKeySet, str, &setting.hKeyStatus);
  size = sizeof(setting.led_status);
  db_get_value(hDB, setting.hKeySet, str, &setting.led_status, &size, TID_BOOL, FALSE);
  setting.led_status_mirror = setting.led_status;
  db_open_record(hDB, setting.hKeyStatus, &setting.led_status,
                 sizeof(setting.led_status), MODE_READ, set_led, NULL);
  
  /* -----------------------------------------------
     Setup raspberry pi
     ----------------------------------------------- */
  /* open device on Raspberry Pi */
  status = raspi(CMD_INIT, setting.hKeySet, &(setting.raspi_info));
  if(status != SUCCESS){
    return FE_ERR_HW;
  }
  // set pin
  printf("Now setting GPIO : %d\n", setting.pin);
  status = raspi(CMD_RASPI_SET_PINMODE, setting.raspi_info, setting.pin, OUTPUT);
  if(status != SUCCESS) return FE_ERR_HW;
  
  // set led;
  set_led(0, 0, 0);
  
  printf("Initialize completed.\n");

#endif
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   return CM_SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return CM_SUCCESS;
}

/*------------------------------------------------------------------*/


#ifdef LED_DRIVER_INCLUDED
//=======================================================
// ODB dispatchers
//=======================================================
void set_led(INT hDB, INT hKey, void *info)
{
  /* No change */
  if(setting.led_status == setting.led_status_mirror) return;

  if(setting.led_status) printf("LED ON\n");
  else                   printf("LED OFF\n");
  setting.led_status_mirror = setting.led_status;

  // set led
  raspi(CMD_WRITE, setting.raspi_info, setting.pin, setting.led_status);
}
#endif
