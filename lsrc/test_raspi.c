#include <stdio.h>

#include "midas.h"
#include "raspi.h"

int main(int argc, char** argv)
{
  const int speed = 100000;
  const int ch = 0;
  int  i;
  char str[256], host_name[256], exp_name[256];
  void* info = NULL;
  INT status;
  HNDLE hDB = NULL, hKey;
  BYTE data[3];
  
  status = cm_get_environment(host_name, sizeof(host_name),
			      exp_name, sizeof(exp_name));
  if(status != CM_SUCCESS){
    printf("Error! cannot get environment.\n");
    goto exit;
  }
  printf("Connect to %s at %s\n", exp_name, host_name);

  status = cm_connect_experiment(host_name, exp_name, "Test_raspi", NULL);

  if(status != CM_SUCCESS){
    printf("Error! cannot connect to the experiment.\n");
    goto exit;
  }
  
  status = cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS){
    printf("Error! data base cannot be obtained.\n");
    goto exit;
  }
  printf("Got database\n");
  
  sprintf(str, "/Equipment/MON");
  status = db_find_key(hDB, 0, str, &hKey);
  if(status != DB_SUCCESS){
    printf("Error! MON is not found in ODB\n");
    goto exit;
  }

  //  float te[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  //db_set_data(hDB, hKey, "test", te, 10*sizeof(float), 10, TID_FLOAT);
  
  printf("Initialize raspi...\n");
  status = raspi(CMD_INIT, hKey, &info);
  if(status != SUCCESS){
    printf("Error! raspi cannot be init.\n");
    goto exit;
  }
  
  /* spi connection  */
  status = raspi(CMD_RASPI_INIT_SPI, info, ch, speed);
  if(status != SUCCESS){
    printf("Error! raspi spi cannot be init.\n");
    goto exit;
  }
  printf("raspi spi setup.\n");
  
  for(i=0; i<100; i++){
    /* data r/w */
    data[0] = 6;
    data[1] = 0<<6;
    data[2] = 0;
    
    status = raspi(CMD_RASPI_SPI_WR, info, ch, data, 3);
    if(status != SUCCESS){
      printf("Error! raspi spi w/r returns error.\n");
      goto exit;
    }
    
    printf("%d  adc = %d\n", i, ((data[1]&0xF)<<8) + data[2] );
    ss_sleep(10);
  }

 exit:
  cm_disconnect_experiment();
  
  return 0;
}
