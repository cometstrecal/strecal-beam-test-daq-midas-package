/********************************************************************\
Name:         mevb.h
  Created by:   Pierre-Andre Amaudruz

  Contents:     Event builder header file

  $Id$

\********************************************************************/

#include <sys/time.h>
#include <time.h>


#define EBUILDER(_name) char const *_name[] = {\
"[.]",\
"Number of Fragment = INT : 0",\
"User build = BOOL : n",\
"User Field = STRING : [64] 100",\
"Capable Timestamp Latency = DWORD : 100",\
"Fragment Required = BOOL[2] :",\
"[0] y",\
"[1] y",\
"",\
NULL }


typedef struct {
   WORD event_id;
   WORD trigger_mask;
   INT  nfragment;
   char buffer[32];
   char format[32];
   BOOL user_build;
   char user_field[64];
   DWORD capa_latency;
   char hostname[64];
   BOOL *preqfrag;
   BOOL *received;
   BOOL *skip;
   BOOL *skipped;
} EBUILDER_SETTINGS;

typedef struct {
   double events_sent;
   double events_per_sec_;
   double kbytes_per_sec_;
} EBUILDER_STATISTICS;

typedef struct {
   INT  idx;
   char buffer[32];
   char format[32];
   WORD event_id;
   WORD trigger_mask;
   INT type;
   INT hBuf;
   INT req_id;
   INT hStat;
   time_t time;
   INT timeout;
   DWORD serial;
   DWORD trgno;
   DWORD cntno;
   struct timeval tstmp;
   char *pfragment;
   INT build_type;
} EBUILDER_CHANNEL;

#define   EB_SUCCESS               1
#define   EB_COMPOSE_TIMEOUT      -1
#define   EB_ERROR              1001
#define   EB_USER_ERROR         1002
#define   EB_ABORTED            1003
#define   EB_SKIP               1004
#define   EB_BANK_NOT_FOUND        0
#define   TIMEOUT              10000
#define   MAX_CHANNELS           128

#define   EB_BTYPE_SERNO      (1<<0)  /* serial number         */
#define   EB_BTYPE_TRGMST     (1<<1)  /* trigger master number */
#define   EB_BTYPE_TRGNO      (1<<2)  /* trigger number        */
#define   EB_BTYPE_CNTNO      (1<<3)  /* counter number        */
#define   EB_BTYPE_TSTMP      (1<<4)  /* time stamp            */
